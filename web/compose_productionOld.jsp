<%@include file="header.jsp"%>

<!-- Main Wrapper -->
<div id="wrapper">
	
	<div class="small-header transition animated fadeIn">
        <div class="hpanel">
            <div class="panel-body">
                <div id="hbreadcrumb" class="pull-right">
                    <ol class="hbreadcrumb breadcrumb">
                        <li><a href="home.jsp">Dashboard</a></li>
                        <li class="active">
                            <span>Production Request</span>
                        </li>
                    </ol>
                </div>
                <h2 class="font-light m-b-xs">
                    Request to Production Environment
                </h2>
                <small>Short description to developer what is this page about</small>
            </div>
        </div>
    </div>
	
	<div class="content animate-panel">
		<div class="hpanel">	
			<ul class="nav nav-tabs">
				<li class="active"><a data-toggle="tab" href="#individual"> Individual</a></li>
				<li class=""><a data-toggle="tab" href="#sole_partner"> Sole Proprietorship / Partnership</a></li>
				<li class=""><a data-toggle="tab" href="#enterprise"> Enterprise</a></li>
			</ul>
			
			<div class="tab-content">
				<div id="individual" class="tab-pane active">
					<div class="row">
						<div class="col-md-12">
							<div class="hpanel email-compose">
								<div class="panel-heading hbuilt">
									<div class="p-xs h4">
										Submit request
									</div>
								</div>
								<div class="panel-heading hbuilt">
									<div class="p-xs">
										<form method="get" class="form-horizontal">
											<div class="form-group"><label class="col-sm-2 control-label text-left">To:</label>
												<div class="col-sm-10"><input type="text" class="form-control input-sm" value="production@tab.com.my" disabled></div><!-- Jack Remark: Email only for sample -->
											</div>
											<!-- Remark: Field with "*" is compulsory -->
											<div class="form-group"><label class="col-sm-2 control-label text-left">I/C No. *:</label>
												<div class="col-sm-10"><input type="text" class="form-control input-sm" value=""></div>
											</div>
											<h3>Billing Details</h3>
											<div class="form-group"><label class="col-sm-2 control-label text-left">Address *</label>
												<div class="col-sm-10"><textarea type="text" class="form-control input-sm" value=""></textarea></div>
											</div>
											<div class="form-group"><label class="col-sm-2 control-label text-left">State *</label>
												<div class="col-sm-10"><input type="text" class="form-control input-sm" value=""></div>
											</div>
											<div class="form-group"><label class="col-sm-2 control-label text-left">Postcode *</label>
												<div class="col-sm-10"><input type="text" class="form-control input-sm" value=""></div>
											</div>
											<div class="form-group"><label class="col-sm-2 control-label text-left">Country *</label>
												<div class="col-sm-10"><input type="text" class="form-control input-sm" value=""></div>
											</div>
											<div class="form-group"><label class="col-sm-2 control-label text-left">Select APIs *:</label>
												<div class="col-sm-10">
													<div class="checkbox">
														<input id="sms" type="checkbox">
														<label for="sms">
															SMS
														</label>
													</div>
													<div class="checkbox">
														<input id="map" type="checkbox" checked="">
														<label for="map">
															Map
														</label>
													</div>
													<div class="checkbox">
														<input id="mfa" type="checkbox">
														<label for="mfa">
															MFA
														</label>
													</div>
													<div class="checkbox">
														<input id="digital_sign" type="checkbox">
														<label for="digital_sign">
															Digital Signing
														</label>
													</div>
												</div>
											</div>								
										</form>
										<div style="background: #fff; height: 200px">
											<div class="col-sm-12">
												<form action="/upload-target" class="dropzone"></form>								
												<small>Upload personal I/C. Support jpeg, gif, png, pdf. File size limit 5mb</small><!-- Jack Remark: Compulsory -->
											</div>
										</div>
									</div>
								</div>

								<div class="panel-footer">
								
									<div class="checkbox">
										<input id="tnc" type="checkbox">
										<label for="tnc">
											Read and agree to <a href="#">Terms of Service</a> to continue subscription <!-- Jack Remark: Compulsory -->
										</label>
									</div>
									<div class="pull-right">
										<div class="btn-group">
										</div>
									</div>
									<button class="btn btn-primary">Subscribe</button>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<div id="sole_partner" class="tab-pane">
					<div class="row">
						<div class="col-md-12">
							<div class="hpanel email-compose">
								<div class="panel-heading hbuilt">
									<div class="p-xs h4">
										Submit request
									</div>
								</div>
								<div class="panel-heading hbuilt">
									<div class="p-xs">
										<form method="get" class="form-horizontal">
											<div class="form-group"><label class="col-sm-2 control-label text-left">To:</label>
												<div class="col-sm-10"><input type="text" class="form-control input-sm" value="production@tab.com.my" disabled></div><!-- Jack Remark: Email only for sample -->
											</div>
											<!-- Remark: Field with "*" is compulsory -->
											<div class="form-group"><label class="col-sm-2 control-label text-left"></label>
												<div class="col-sm-10">
													<div class="radio radio-info radio-inline">
														<input type="radio" id="sole" value="sole" name="radioInline" checked="">
														<label for="sole"> Sole Proprietorship </label> <!-- Jack Remark: Compulsory -->
													</div>
													<div class="radio radio-inline">
														<input type="radio" id="partnership" value="partnership" name="radioInline">
														<label for="partnership"> Partnership </label> <!-- Jack Remark: Compulsory -->
													</div>
												</div>
											</div>
											<div class="form-group"><label class="col-sm-2 control-label text-left">Company Name *</label>
												<div class="col-sm-10"><input type="text" class="form-control input-sm" value=""></div>
											</div>
											<div class="form-group"><label class="col-sm-2 control-label text-left">Company Registration No. *</label>
												<div class="col-sm-10"><input type="text" class="form-control input-sm" value=""></div>
											</div>
											<div class="form-group"><label class="col-sm-2 control-label text-left">GST No.</label>
												<div class="col-sm-10"><input type="text" class="form-control input-sm" value=""></div>
											</div>
											<div class="form-group"><label class="col-sm-2 control-label text-left">Fixed Line Number *</label>
												<div class="col-sm-10"><input type="text" class="form-control input-sm" value=""></div>
											</div>
											<div class="form-group"><label class="col-sm-2 control-label text-left">Address *</label>
												<div class="col-sm-10"><textarea type="text" class="form-control input-sm" value=""></textarea></div>
											</div>
											<div class="form-group"><label class="col-sm-2 control-label text-left">State *</label>
												<div class="col-sm-10"><input type="text" class="form-control input-sm" value=""></div>
											</div>
											<div class="form-group"><label class="col-sm-2 control-label text-left">Postcode *</label>
												<div class="col-sm-10"><input type="text" class="form-control input-sm" value=""></div>
											</div>
											<div class="form-group"><label class="col-sm-2 control-label text-left">Country*</label>
												<div class="col-sm-10"><input type="text" class="form-control input-sm" value=""></div>
											</div>
											<div class="form-group"><label class="col-sm-2 control-label text-left">Industry *:</label>
												<div class="col-sm-10">
													<select class="js-source-states" style="width: 100%">
														<optgroup label="IT">
															<option value="software">Software house</option>
															<option value="web">Web development</option>
														</optgroup>
														<optgroup label="Service">
															<option value="banking">Banking</option>
															<option value="finance">Finance</option>
															<option value="insurance">Insurance</option>
															<option value="medical">Medical</option>
														</optgroup>
													</select><!-- Jack Remark: Temporary, will add-on later -->
												</div>
											</div>
											<h3>Billing Details</h3>
											<small>Fill in if different from office address</small>
											<div class="form-group"><label class="col-sm-2 control-label text-left">Address</label>
												<div class="col-sm-10"><textarea type="text" class="form-control input-sm" value=""></textarea></div>
											</div>
											<div class="form-group"><label class="col-sm-2 control-label text-left">State</label>
												<div class="col-sm-10"><input type="text" class="form-control input-sm" value=""></div>
											</div>
											<div class="form-group"><label class="col-sm-2 control-label text-left">Postcode</label>
												<div class="col-sm-10"><input type="text" class="form-control input-sm" value=""></div>
											</div>
											<div class="form-group"><label class="col-sm-2 control-label text-left">Country</label>
												<div class="col-sm-10"><input type="text" class="form-control input-sm" value=""></div>
											</div>
											<hr/>
											
											<!-- Remark: Following field only display when developer choose payment gateway, all compulsory -->
											<div class="form-group"><label class="col-sm-2 control-label text-left">Product Description</label>
												<div class="col-sm-10"><textarea type="text" class="form-control input-sm" value=""></textarea></div>
											</div>
											<div class="form-group"><label class="col-sm-2 control-label text-left">Website URL</label>
												<div class="col-sm-10"><input type="text" class="form-control input-sm" value=""></div>
											</div>
											<div class="form-group"><label class="col-sm-2 control-label text-left">Select APIs:</label>
												<div class="col-sm-10">
													<div class="checkbox">
														<input id="sms" type="checkbox">
														<label for="sms">
															SMS
														</label>
													</div>
													<div class="checkbox">
														<input id="map" type="checkbox" checked="">
														<label for="map">
															Map
														</label>
													</div>
													<div class="checkbox">
														<input id="mfa" type="checkbox">
														<label for="mfa">
															MFA
														</label>
													</div>
													<div class="checkbox">
														<input id="digital_sign" type="checkbox">
														<label for="digital_sign">
															Digital Signing
														</label>
													</div>
													<div class="checkbox">
														<input id="payment_gateway" type="checkbox">
														<label for="payment_gateway">
															Payment Gateway
														</label>
													</div>
												</div>
											</div>
											<div class="form-group"><label class="col-sm-2 control-label text-left">Bank Name</label>
												<div class="col-sm-10"><input type="text" class="form-control input-sm" value=""></div>
											</div>
											<div class="form-group"><label class="col-sm-2 control-label text-left">Bank Branch</label>
												<div class="col-sm-10"><input type="text" class="form-control input-sm" value=""></div>
											</div>
											<div class="form-group"><label class="col-sm-2 control-label text-left">Bank Account No.</label>
												<div class="col-sm-10"><input type="text" class="form-control input-sm" value=""></div>
											</div>	
											<div class="form-group"><label class="col-sm-2 control-label text-left">SWIFT Code</label>
												<div class="col-sm-10"><input type="text" class="form-control input-sm" value=""></div>
											</div>
											<!-- Remark: Following field only display when developer choose payment gateway (END) -->
										</form>
										<div style="background: #fff; height: 200px">
											<div class="col-sm-12">
												<form action="/upload-target" class="dropzone"></form>								
												<small>Upload Form D/f document. Support jpeg, gif, png, pdf. File size limit 5mb</small> <!-- Jack Remark: Compulsory -->
											</div>
										</div>
									</div>
								</div>

								<div class="panel-footer">
								
									<div class="checkbox">
										<input id="tnc" type="checkbox">
										<label for="tnc">
											Read and agree to <a href="#">Terms of Service</a> to continue subscription <!-- Jack Remark: Compulsory -->
										</label>
									</div>
									<div class="pull-right">
										<div class="btn-group">
										</div>
									</div>
									<button class="btn btn-primary">Subscribe</button>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<div id="enterprise" class="tab-pane">
					<div class="row">
						<div class="col-md-12">
							<div class="hpanel email-compose">
								<div class="panel-heading hbuilt">
									<div class="p-xs h4">
										Submit request
									</div>
								</div>
								<div class="panel-heading hbuilt">
									<div class="p-xs">
										<form method="get" class="form-horizontal">
											<div class="form-group"><label class="col-sm-2 control-label text-left">To:</label>
												<div class="col-sm-10"><input type="text" class="form-control input-sm" value="production@tab.com.my" disabled></div><!-- Jack Remark: Email only for sample -->
											</div>
											<!-- Remark: Field with "*" is compulsory -->
											<div class="form-group"><label class="col-sm-2 control-label text-left"></label>
												<div class="col-sm-10">
													<div class="radio radio-info radio-inline">
														<input type="radio" id="sdn_bhd" value="sdn_bhd" name="radioInline" checked="">
														<label for="sdn_bhd"> Sdn Bhd </label>
													</div>
													<div class="radio radio-inline">
														<input type="radio" id="berhad" value="berhad" name="radioInline">
														<label for="berhad"> Berhad </label>
													</div>
													<div class="radio radio-inline">
														<input type="radio" id="foreign_company" value="foreign_company" name="radioInline">
														<label for="foreign_company"> Foreign Company </label> <!-- Jack Remark: Compulsory -->
													</div>
												</div>
											</div>
											<div class="form-group"><label class="col-sm-2 control-label text-left">Company Name *</label>
												<div class="col-sm-10"><input type="text" class="form-control input-sm" value=""></div>
											</div>
											<div class="form-group"><label class="col-sm-2 control-label text-left">Company Registration No. *</label>
												<div class="col-sm-10"><input type="text" class="form-control input-sm" value=""></div>
											</div>
											<div class="form-group"><label class="col-sm-2 control-label text-left">GST No.</label>
												<div class="col-sm-10"><input type="text" class="form-control input-sm" value=""></div>
											</div>
											<div class="form-group"><label class="col-sm-2 control-label text-left">Fixed Line Number *</label>
												<div class="col-sm-10"><input type="text" class="form-control input-sm" value=""></div>
											</div>
											<div class="form-group"><label class="col-sm-2 control-label text-left">Address *</label>
												<div class="col-sm-10"><textarea type="text" class="form-control input-sm" value=""></textarea></div>
											</div>
											<div class="form-group"><label class="col-sm-2 control-label text-left">State *</label>
												<div class="col-sm-10"><input type="text" class="form-control input-sm" value=""></div>
											</div>
											<div class="form-group"><label class="col-sm-2 control-label text-left">Postcode *</label>
												<div class="col-sm-10"><input type="text" class="form-control input-sm" value=""></div>
											</div>
											<div class="form-group"><label class="col-sm-2 control-label text-left">Country *</label>
												<div class="col-sm-10"><input type="text" class="form-control input-sm" value=""></div>
											</div>
											<div class="form-group"><label class="col-sm-2 control-label text-left">Industry *:</label>
												<div class="col-sm-10">
													<select class="js-source-states" style="width: 100%">
														<optgroup label="IT">
															<option value="software">Software house</option>
															<option value="web">Web development</option>
														</optgroup>
														<optgroup label="Service">
															<option value="banking">Banking</option>
															<option value="finance">Finance</option>
															<option value="insurance">Insurance</option>
															<option value="medical">Medical</option>
														</optgroup>
													</select><!-- Jack Remark: Temporary, will add-on later -->
												</div>
											</div>
											<h3>Billing Details</h3>
											<small>Fill in if different from office address</small>
											<div class="form-group"><label class="col-sm-2 control-label text-left">Address</label>
												<div class="col-sm-10"><textarea type="text" class="form-control input-sm" value=""></textarea></div>
											</div>
											<div class="form-group"><label class="col-sm-2 control-label text-left">State</label>
												<div class="col-sm-10"><input type="text" class="form-control input-sm" value=""></div>
											</div>
											<div class="form-group"><label class="col-sm-2 control-label text-left">Postcode</label>
												<div class="col-sm-10"><input type="text" class="form-control input-sm" value=""></div>
											</div>
											<div class="form-group"><label class="col-sm-2 control-label text-left">Country</label>
												<div class="col-sm-10"><input type="text" class="form-control input-sm" value=""></div>
											</div>
											<hr/>
											<!-- Remark: Following field only display when developer choose payment gateway, all compulsory -->
											<div class="form-group"><label class="col-sm-2 control-label text-left">Product Description</label>
												<div class="col-sm-10"><textarea type="text" class="form-control input-sm" value=""></textarea></div>
											</div>
											<div class="form-group"><label class="col-sm-2 control-label text-left">Website URL</label>
												<div class="col-sm-10"><input type="text" class="form-control input-sm" value=""></div>
											</div>
											<div class="form-group"><label class="col-sm-2 control-label text-left">Select APIs:</label>
												<div class="col-sm-10">
													<div class="checkbox">
														<input id="sms" type="checkbox">
														<label for="sms">
															SMS
														</label>
													</div>
													<div class="checkbox">
														<input id="map" type="checkbox" checked="">
														<label for="map">
															Map
														</label>
													</div>
													<div class="checkbox">
														<input id="mfa" type="checkbox">
														<label for="mfa">
															MFA
														</label>
													</div>
													<div class="checkbox">
														<input id="digital_sign" type="checkbox">
														<label for="digital_sign">
															Digital Signing
														</label>
													</div>
													<div class="checkbox">
														<input id="payment_gateway" type="checkbox">
														<label for="payment_gateway">
															Payment Gateway
														</label>
													</div>
												</div>
											</div>
											<div class="form-group"><label class="col-sm-2 control-label text-left">Bank Name</label>
												<div class="col-sm-10"><input type="text" class="form-control input-sm" value=""></div>
											</div>
											<div class="form-group"><label class="col-sm-2 control-label text-left">Bank Branch</label>
												<div class="col-sm-10"><input type="text" class="form-control input-sm" value=""></div>
											</div>
											<div class="form-group"><label class="col-sm-2 control-label text-left">Bank Account No.</label>
												<div class="col-sm-10"><input type="text" class="form-control input-sm" value=""></div>
											</div>	
											<div class="form-group"><label class="col-sm-2 control-label text-left">SWIFT Code</label>
												<div class="col-sm-10"><input type="text" class="form-control input-sm" value=""></div>
											</div>
											<!-- Remark: Following field only display when developer choose payment gateway (END) -->											
										</form>
										<div style="background: #fff; height: 200px">
											<div class="col-sm-12">
												<form action="/upload-target" class="dropzone"></form>								
												<small>Upload Form 9/49 (Sdn Bhd), Form 8 & 23 (Berhad) / Form 79,90/80A/83/83A (Foreign Company) document. Support jpeg, gif, png, pdf. File size limit 5mb</small> <!-- Jack Remark: Compulsory -->
											</div>
										</div>
									</div>
								</div>

								<div class="panel-footer">
								
									<div class="checkbox">
										<input id="tnc" type="checkbox">
										<label for="tnc">
											Read and agree to <a href="#">Terms of Service</a> to continue subscription <!-- Jack Remark: Compulsory -->
										</label>
									</div>
									<div class="pull-right">
										<div class="btn-group">
										</div>
									</div>
									<button class="btn btn-primary">Subscribe</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
</div>

<script>

$(function(){

	$(".js-source-states").select2();
	
});

</script>
	
    <!-- Footer-->
    <%@include file="footer.jsp"%>