<%@include file="header.jsp"%> 
<script src="scripts/tokenManagement.js" type="text/javascript"></script>
<%    String resourceName = request.getParameter("accessPointName");

%>
<script>
    function acesspointChangeResource(value, resName, version) {
        var s = './getResource.jsp?_apName=' + value;
        $.ajax({
            type: 'GET',
            url: s,
            success: function (data) {
                $('#_ResourceForSlabPricing2').html(data);
                var res = document.getElementById('_ResourceForSlabPricing2').value;
                var ap = '<%=resourceName%>';
                if (res === '-1' && resName === '') {
                } else if (resName === '' && version === '') {
                    showVersion(res, '');
                } else {
                    document.getElementById('_ResourceForSlabPricing2').value = resName;
                    showVersion(resName, version);
                }
            }
        });
    }

    function showVersion(resName, version) {
        var ap = '<%=resourceName%>';
        var s = './getVersion.jsp?_resourceId=' + resName + '&_apId=' + ap;
        $.ajax({
            type: 'GET',
            url: s,
            success: function (data) {
                $('#_VersionForSlabPricing2').html(data);
                var versionData = document.getElementById('_VersionForSlabPricing2').value;
                if (versionData === '-1' && version === '') {
                } else if (version === '') {
                    showAPI(versionData);
                } else {
                    document.getElementById('_VersionForSlabPricing2').value = version;
                    showAPI(version);
                }
            }
        });
    }

    function showAPI(versionData) {
        var ap = '<%=resourceName%>';
        var _resourceId = document.getElementById('_ResourceForSlabPricing2').value;
        var s = './getAPI.jsp?versionData=' + versionData + '&_apId=' + ap + '&_resourceId=' + _resourceId;
        assignToken();
    }

    function assignToken() {
        var accesspointName = '<%=resourceName%>';
        var resourceName = document.getElementById("_ResourceForSlabPricing2").value;
        var version = document.getElementById("_VersionForSlabPricing2").value;
        var s = './assignToken.jsp?_apId=' + accesspointName + '&_resourceId=' + resourceName + '&version=' + version;
        $.ajax({
            type: 'GET',
            url: s,
            success: function (data) {
                $('#_slabPriceWindow').html(data);
                // acesspointChangeAPIPrice(value);
            }
        });
    }

</script>    
<!-- Main Wrapper -->
<script>
    acesspointChangeResource('<%=resourceName%>', '', '');
</script>
<div id="wrapper">

    <div class="small-header transition animated fadeIn">
        <div class="hpanel">
            <div class="panel-body">
                <div id="hbreadcrumb" class="pull-right">
                    <ol class="hbreadcrumb breadcrumb">
                        <li><a href="home.jsp">Dashboard</a></li>
                        <li><a href="token_manager.jsp">Token Management</a></li>
                        <li class="active">
                            <span>SMS</span>
                        </li>
                    </ol>
                </div>
                <h2 class="font-light m-b-xs">
                    API Level Token Management - SMS
                </h2>
                <small>Know your APITokenId from here</small>
            </div>
        </div>
    </div>


    <div class="content animate-panel">

        <div class="row">
            <div class="col-lg-12">
                <div class="hpanel">
                    <div class="panel-heading">
                        <div class="panel-tools">
                            <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                        </div>
                        Access Points 
                    </div>

                    <div class="panel-body">
                        <div class="col-sm-12">
                            <div class="col-sm-3">
                                <select id="_ResourceForSlabPricing2" onload="showVersion(this.value, '')" onchange="showVersion(this.value, '')"  class="form-control m-b" name="api">
                                    <option value="-1" selected>Select Resource</option> 
                                </select>
                            </div>
                            <div class="col-sm-2">
                                <select id="_VersionForSlabPricing2" class="form-control m-b" onchange="showAPI(this.value)" name="ver">
                                    <option value="-1" selected>Select Version</option>
                                </select>
                            </div>
                        </div>
                        <div id="_slabPriceWindow"></div>

                    </div>
                    <div class="modal fade hmodal-success" id="tmTermsAndCondition" tabindex="-1" role="dialog"  aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="color-line"></div>
                                <div class="modal-header" style="padding: 10px 0px !important">
                                    <h4 id="tokenType" style="margin-left: 2%">Your APITokenId is
                                    <button class="close btn btn-default" data-dismiss="modal" aria-hidden="true" style="margin-top: 0%; margin-right: 3%">x</button></h4>                                    
                                </div>
                                <div class="modal-body" style="padding-bottom: 50px !important">
                                    <img src="images/TokenLogo.png" alt="" style="margin-left: 35%; margin-bottom: 7%"/>
                                    <div class="form-group">
                                        <div class="col-lg-7">
                                            <p id="test1"></p>
                                        </div>                                        
                                        <div class="col-lg-5"> 
                                            <div id="inputTag"></div>
                                            <div id="copyDiv"></div>
                                        </div>
                                    </div>
                                </div>                                
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

        <%@include file="footer.jsp"%> 


        <script>
            $(function () {
                // Initialize Example 2
                $('#api').dataTable();
            });
            $(function () {
                // Initialize Example 2
                $('#data_type').dataTable();
            });
            
           
        </script>