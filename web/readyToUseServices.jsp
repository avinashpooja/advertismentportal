
<%@page import="org.bouncycastle.util.encoders.Base64"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.ResourceManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.ResourceDetails"%>
<%@page import="org.json.JSONObject"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.WarFileManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.TransformManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.TransformDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.GlobalStatus"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.Warfiles"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.AccessPointManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.Accesspoint"%>
<%
    Accesspoint[] accessObj = new AccessPointManagement().getAceesspoints();
    Warfiles warfiles = null;
    int dbaccesspointID = 0;
    int dbResId = 0;
    String dbSerDesc = null;
    String dbdisplayName = null;
    int documentUtilityaccesspointID = 0;
    int documentUtilityResId = 0;
    String docUtilityDesc = null;
    String docUtilitylabel = null;
    int gogAuthaccesspointID = 0;
    int gogAuthResId = 0;
    String gogAuthDesc = null;
    String gogAuthlabel = null;
    int imgUtilityAcc = 0;
    int imgUtilityRes = 0;
    String imgUtilityDesc = null;
    String imgUtilitylabel = null;
    int tokanisationAcc = 0;
    int tokanisationRes = 0;
    String tokanisationDesc = null;
    String tokanisationlabel = null;
    int valueAddedAccId = 0;
    int valueAddedResId = 0;
    String valueAddedAccDesc = null;
    String valueAddedReslabel = null;
    int ip2geoAccId = 0;
    int ip2geoResId = 0;
    String ip2geoAccDesc = null;
    String ip2geoAcclabel = null;
    int instantWebChatAccId = 0;
    int instantWebChatResId = 0;
    String instantWebChatDesc = null;
    String instantWebChatlabel = null;
    int reverseProxyAccId = 0;
    int reverseProxytResId = 0;
    String reverseProxyDesc = null;
    String reverseProxylabel = null;
    int inappPushAccId = 0;
    int inappPushResId = 0;
    String inappPushDesc = null;
    String inappPushlabel = null;
    int serMoniAccId = 0;
    int serMoniResId = 0;
    String serMoniDesc = null;
    String serMonilabel = null;
    int digiCertAccId = 0;
    int digiCertResId = 0;
    String digiCertDesc = null;
    String digiCertlabel = null;
    int SSLCertDiscoverytAccId = 0;
    int SSLCertDiscoveryResId = 0;
    String SSLCertDiscoveryDesc = null;
    String SSLCertDiscoverylabel = null;
    int dynamicImgAuthAccId = 0;
    int dynamicImgAuthResId = 0;
    String dynamicImgAuthDesc = null;
    String dynamicImgAuthlabel = null;
    if (accessObj != null) {
        for (int acci = 0; acci < accessObj.length; acci++) {
            if (accessObj[acci].getStatus() != GlobalStatus.DELETED && accessObj[acci].getStatus() != GlobalStatus.SUSPEND) {
                Accesspoint apdetails = accessObj[acci];
                TransformDetails transformDetails = new TransformManagement().getTransformDetails(apdetails.getChannelid(), apdetails.getApId(), Integer.parseInt(apdetails.getResources().split(",")[0]));
                if (transformDetails != null) {
                    warfiles = new WarFileManagement().getWarFile(apdetails.getChannelid(), apdetails.getApId());
                    if (warfiles != null) {
                        String[] resources = apdetails.getResources().split(",");
                        ResourceDetails resObj = null;
                        if (resources != null) {
                            for (int j = 0; j < resources.length; j++) {
                                resObj = new ResourceManagement().getResourceById(Integer.parseInt(resources[j]));
                                if (apdetails.getName().contains("DBAsService")) {
                                    dbaccesspointID = apdetails.getApId();
                                    dbResId = resObj.getResourceId();
                                    if (resObj.getDescription() != null) {
                                        dbSerDesc = resObj.getDescription();
                                    }
                                    if (apdetails.getDisplayName() != null) {
                                        dbdisplayName = apdetails.getDisplayName();
                                    }
                                } else if (apdetails.getName().contains("DocumentUtility")) {
                                    documentUtilityaccesspointID = apdetails.getApId();
                                    documentUtilityResId = resObj.getResourceId();
                                    if (resObj.getDescription() != null) {
                                        docUtilityDesc = resObj.getDescription();
                                    }
                                    if (apdetails.getDisplayName() != null) {
                                        docUtilitylabel = apdetails.getDisplayName();
                                    }
                                } else if (apdetails.getName().contains("GoogleTokenAuth")) {
                                    gogAuthaccesspointID = apdetails.getApId();
                                    gogAuthResId = resObj.getResourceId();
                                    if (resObj.getDescription() != null) {
                                        gogAuthDesc = resObj.getDescription();
                                    }
                                    if (apdetails.getDisplayName() != null) {
                                        gogAuthlabel = apdetails.getDisplayName();
                                    }
                                } else if (apdetails.getName().contains("ImageUtility")) {
                                    imgUtilityAcc = apdetails.getApId();
                                    imgUtilityRes = resObj.getResourceId();
                                    if (resObj.getDescription() != null) {
                                        imgUtilityDesc = resObj.getDescription();
                                    }
                                    if (apdetails.getDisplayName() != null) {
                                        imgUtilitylabel = apdetails.getDisplayName();
                                    }
                                } else if (apdetails.getName().contains("Tokenization")) {
                                    tokanisationAcc = apdetails.getApId();
                                    tokanisationRes = resObj.getResourceId();
                                    if (resObj.getDescription() != null) {
                                        tokanisationDesc = resObj.getDescription();
                                    }
                                    if (apdetails.getDisplayName() != null) {
                                        tokanisationlabel = apdetails.getDisplayName();
                                    }
                                } else if (apdetails.getName().contains("ValueAddedServices")) {
                                    valueAddedAccId = apdetails.getApId();
                                    valueAddedResId = resObj.getResourceId();
                                    if (resObj.getDescription() != null) {
                                        valueAddedAccDesc = resObj.getDescription();
                                    }
                                    if (apdetails.getDisplayName() != null) {
                                        valueAddedReslabel = apdetails.getDisplayName();
                                    }
                                } else if (apdetails.getName().contains("IpToGeo")) {
                                    ip2geoAccId = apdetails.getApId();
                                    ip2geoResId = resObj.getResourceId();
                                    if (resObj.getDescription() != null) {
                                        ip2geoAccDesc = resObj.getDescription();
                                    }
                                    if (apdetails.getDisplayName() != null) {
                                        ip2geoAcclabel = apdetails.getDisplayName();
                                    }
                                } else if (apdetails.getName().contains("InstantWebChat")) {
                                    instantWebChatAccId = apdetails.getApId();
                                    instantWebChatResId = resObj.getResourceId();
                                    if (resObj.getDescription() != null) {
                                        instantWebChatDesc = resObj.getDescription();
                                    }
                                    if (apdetails.getDisplayName() != null) {
                                        instantWebChatlabel = apdetails.getDisplayName();
                                    }
                                } else if (apdetails.getName().contains("ReverseProxy")) {
                                    reverseProxyAccId = apdetails.getApId();
                                    reverseProxytResId = resObj.getResourceId();
                                    if (resObj.getDescription() != null) {
                                        reverseProxyDesc = resObj.getDescription();
                                    }
                                    if (apdetails.getDisplayName() != null) {
                                        reverseProxylabel = apdetails.getDisplayName();
                                    }
                                } else if (apdetails.getName().contains("InAppPush")) {
                                    inappPushAccId = apdetails.getApId();
                                    inappPushResId = resObj.getResourceId();
                                    if (resObj.getDescription() != null) {
                                        inappPushDesc = resObj.getDescription();
                                    }
                                    if (apdetails.getDisplayName() != null) {
                                        inappPushlabel = apdetails.getDisplayName();
                                    }
                                } else if (apdetails.getName().contains("ServerMonitoring")) {
                                    serMoniAccId = apdetails.getApId();
                                    serMoniResId = resObj.getResourceId();
                                    if (resObj.getDescription() != null) {
                                        serMoniDesc = resObj.getDescription();
                                    }
                                    if (apdetails.getDisplayName() != null) {
                                        serMonilabel = apdetails.getDisplayName();
                                    }
                                } else if (apdetails.getName().contains("DigitalCertificateIssuer")) {
                                    digiCertAccId = apdetails.getApId();
                                    digiCertResId = resObj.getResourceId();
                                    if (resObj.getDescription() != null) {
                                        digiCertDesc = resObj.getDescription();
                                    }
                                    if (apdetails.getDisplayName() != null) {
                                        digiCertlabel = apdetails.getDisplayName();
                                    }
                                } else if (apdetails.getName().contains("SSLCertDiscovery")) {
                                    SSLCertDiscoverytAccId = apdetails.getApId();
                                    SSLCertDiscoveryResId = resObj.getResourceId();
                                    if (resObj.getDescription() != null) {
                                        SSLCertDiscoveryDesc = resObj.getDescription();
                                    }
                                    if (apdetails.getDisplayName() != null) {
                                        SSLCertDiscoverylabel = apdetails.getDisplayName();
                                    }
                                } else if (apdetails.getName().contains("DynamicImageAuth")) {
                                    dynamicImgAuthAccId = apdetails.getApId();
                                    dynamicImgAuthResId = resObj.getResourceId();
                                    if (resObj.getDescription() != null) {
                                        dynamicImgAuthDesc = resObj.getDescription();
                                    }
                                    if (apdetails.getDisplayName() != null) {
                                        dynamicImgAuthlabel = apdetails.getDisplayName();
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

%>
<div class="content animate-panel">

    <div class="small-header transition animated fadeIn">
        <div class="row">
            <div class="col-lg-12 col-sm-12 col-md-12">
                <div class="hpanel">
                    <div class="panel-body">
                        <div class="row text-center">
                            <div class="col-md-3">
                                <a onclick="apiServicesV2('<%=dbaccesspointID%>', '<%=dbResId%>')"><h4 class="m-t-lg"><i class="pe-7s-server text-info icon-big fa-3x"></i></h4>
                                        <%if (dbdisplayName == null) {%>
                                    <strong>DB As Service</strong>
                                    <%} else {%>
                                    <strong><%=dbdisplayName%></strong>
                                    <%}
                                        if (dbSerDesc == null) {%>                        
                                    <p>You can expose our database like Oracle, Mysql and Cassandra. You can fire various queries on database as per the requirement and easily access the database.</p>
                                    <%} else {%>
                                    <p><%=new String(Base64.decode(dbSerDesc))%></p>
                                    <%}%>
                                </a>
                            </div>
                            <div class="col-md-3">
                                <a onclick="apiServicesV2('<%=documentUtilityaccesspointID%>', '<%=documentUtilityResId%>')"><h4 class="m-t-lg"><i class="pe-7s-copy-file text-info icon-big fa-3x"></i></h4>
                                        <%if (dbdisplayName == null) {%>
                                    <strong>Document Utility</strong>
                                    <%} else {%>
                                    <strong><%=docUtilitylabel%></strong>
                                    <%}
                                        if (docUtilityDesc == null) {%>         
                                    <p>You can do wonder operations with PDF documents. Operations cover Sign, Convert to image and back, Watermark, Password Protect, Compress, Merge, Rotate on your documents. Our intention is to give more value to your documents and workflow.</p>
                                    <%} else {%>
                                    <p><%=new String(Base64.decode(docUtilityDesc))%></p>
                                    <%}%>
                                </a>
                            </div>
                            <div class="col-md-3">
                                <a onclick="apiServicesV2('<%=gogAuthaccesspointID%>', '<%=gogAuthResId%>')"><h4 class="m-t-lg"><i class="fa fa-google text-info icon-big fa-3x"></i></h4>
                                        <%if (gogAuthlabel == null) {%>
                                    <strong>Google 2FA Token </strong>
                                    <%} else {%>
                                    <strong><%=gogAuthlabel%></strong>
                                    <%}
                                        if (gogAuthDesc == null) {%>       
                                    <p>Every app needs security and 2FA (Second Factor Authentication) is getting more popular. You can use this service for manage your users, their OTP tokens and usage. Enjoy free security with Ready APIs and Google. </p>
                                    <%} else {%>
                                    <p><%=new String(Base64.decode(gogAuthDesc))%></p>
                                    <%}%>
                                </a>
                            </div>
                            <div class="col-md-3">
                                <a onclick="apiServicesV2('<%=imgUtilityAcc%>', '<%=imgUtilityRes%>')"><h4 class="m-t-lg"><i class="fa fa-photo text-info icon-bi fa-3x"></i></h4>
                                        <%if (imgUtilitylabel == null) {%>
                                    <strong>Image Utility</strong>
                                    <%} else {%>
                                    <strong><%=imgUtilitylabel%></strong>
                                    <%}
                                        if (imgUtilityDesc == null) {%>    
                                    <p>You can manipulate images in wonderful way. The APIs can do - Change formats, Hide/Unhide Messages inside Image, Watermark, Fun Stay Calm Creation, Extract meta data from image, Compress Image and Compress Website Images too. </p>
                                    <%} else {%>
                                    <p><%=new String(Base64.decode(imgUtilityDesc))%></p>
                                    <%}%>
                                </a>
                            </div>
                        </div>
                        <div class="row text-center">
                            <div class="col-md-3">
                                <a onclick="apiServicesV2('<%=tokanisationAcc%>', '<%=tokanisationRes%>')"><h4 class="m-t-lg"><i class="fa fa-try text-info icon-big fa-3x"></i></h4>
                                        <%if (tokanisationlabel == null) {%>
                                    <strong>Tokenization</strong>
                                    <%} else {%>
                                    <strong><%=tokanisationlabel%></strong>
                                    <%}
                                        if (tokanisationDesc == null) {%>        
                                    <p>The token is a reference (i.e. identifier) that maps back to the sensitive data through a tokenisation system. With tokens you do not need to share confidential data between various system. You can enjoy complete end to end token lifecycle through tokenization. </p>
                                    <%} else {%>
                                    <p><%=new String(Base64.decode(tokanisationDesc))%></p>
                                    <%}%>
                                </a>
                            </div>
                            <div class="col-md-3">
                                <a onclick="apiServicesV2('<%=valueAddedAccId%>', '<%=valueAddedResId%>')"><h4 class="m-t-lg"><i class="pe-7s-science text-info icon-big fa-3x"></i></h4>
                                        <%if (valueAddedReslabel == null) {%>
                                    <strong>Value Added Service</strong>
                                    <%} else {%>
                                    <strong><%=valueAddedReslabel%></strong>
                                    <%}
                                        if (valueAddedAccDesc == null) {%>       
                                    <p>We have built a lot of Value Added Services that help in quick rapid development of your workflows. Few of them to mention are EmailId Validation, QR Code And Secure QR Code Generation, Shorten URL with Hit count, ICS Calendar Generation, JSON/CSV/XML Format validator, Credit Card Number Validator, Phone Number Validator and Bank Finder.</p>
                                    <%} else {%>
                                    <p><%=new String(Base64.decode(valueAddedAccDesc))%></p>
                                    <%}%>
                                </a>
                            </div>
                            <div class="col-md-3">
                                <a onclick="apiServicesV2('<%=ip2geoAccId%>', '<%=ip2geoResId%>')"><h4 class="m-t-lg"><i class="pe-7s-map-marker text-info icon-big fa-3x"></i></h4>
                                        <%if (ip2geoAcclabel == null) {%>
                                    <strong>IP2GEO</strong>
                                    <%} else {%>
                                    <strong><%=ip2geoAcclabel%></strong>
                                    <%}
                                        if (ip2geoAccDesc == null) {%>         
                                    <p>This service has a goal of providing geolocation service to all users . It provide all details of server location like country, state, latitude, longitude for specific ip address we have pass as input. Every month we update our DB with latets ip2geo data.</p>
                                    <%} else {%>
                                    <p><%=new String(Base64.decode(ip2geoAccDesc))%></p>
                                    <%}%>
                                </a>
                            </div>                    
                            <div class="col-md-3">
                                <a onclick="apiServicesV2('<%=instantWebChatAccId%>', '<%=instantWebChatResId%>')"><h4 class="m-t-lg"><i class="fa fa-users text-info icon-big fa-3x"></i></h4>
                                        <%if (instantWebChatlabel == null) {%>
                                    <strong>Instant WebChat</strong>
                                    <%} else {%>
                                    <strong><%=instantWebChatlabel%></strong>
                                    <%}
                                        if (instantWebChatDesc == null) {%>         
                                    <p>We have build for you to a true value add service for Instant Web Chat (with no client software installation) into your workflow. You can generate conference video call and it works on 97% browsers like chrome, opera, Firefox as well android and iOS browsers. Also you can password protect session for the users joining in</p>
                                    <%} else {%>
                                    <p><%=new String(Base64.decode(instantWebChatDesc))%></p>
                                    <%}%>
                                </a>
                            </div>
                        </div>
                        <div class="row text-center">                    
                            <div class="col-md-3">
                                <a onclick="apiServicesV2('<%=reverseProxyAccId%>', '<%=reverseProxytResId%>')"><h4 class="m-t-lg"><i class="pe-7s-loop text-info icon-big fa-3x"></i></h4>
                                        <%if (reverseProxylabel == null) {%>
                                    <strong>Reverse Proxy</strong>
                                    <%} else {%>
                                    <strong><%=reverseProxylabel%></strong>
                                    <%}
                                        if (reverseProxyDesc == null) {%>             
                                    <p>One of trouble point we have tried to address with this service is, ability for your web application to be behind a proper SSL enabled server. Many websites and mobile app need to have secure connection and putting your own SSL certificate is costly and hardwork. You can use this service to put non SSL server behind reverse proxy provides an additional level of abstraction and control. </p>
                                    <%} else {%>
                                    <p><%=new String(Base64.decode(reverseProxyDesc))%></p>
                                    <%}%>
                                </a>
                            </div>
                            <div class="col-md-3">
                                <a onclick="apiServicesV2('<%=inappPushAccId%>', '<%=inappPushResId%>')"><h4 class="m-t-lg"><i class="pe-7s-comment text-info icon-big fa-3x"></i></h4>
                                        <%if (inappPushlabel == null) {%>
                                    <strong>In App Push</strong>
                                    <%} else {%>
                                    <strong><%=inappPushlabel%></strong>
                                    <%}
                                        if (inappPushDesc == null) {%>         
                                    <p>Push notification has changed the way we give information out to our customers. You can enjoy end to end device and push notification for all media - Android, iOS and Web. Your mobile native app in android and iOS along with your web app can get messages through Ready APIs. Chrome, Firefox, Opera and Safari are popular browsers that are supported.</p>
                                    <%} else {%>
                                    <p><%=new String(Base64.decode(inappPushDesc))%></p>
                                    <%}%>
                                </a>
                            </div>
                            <div class="col-md-3">
                                <a onclick="apiServicesV2('<%=serMoniAccId%>', '<%=serMoniResId%>')"><h4 class="m-t-lg"><i class="pe-7s-network text-info icon-big fa-3x"></i></h4>
                                        <%if (serMonilabel == null) {%>
                                    <strong>Application Server Monitoring</strong>
                                    <%} else {%>
                                    <strong><%=serMonilabel%></strong>
                                    <%}
                                        if (serMoniDesc == null) {%>  
                                    <p>Monitoring your app servers give in-depth visibility into key performance indicators of application performance. From CPU, Memory, Processes etc get checked on periodic basis at application level, you will get alert if consumption is too high. Currently Java JSP is only supported. PHP and .Net ASPx will be supported soon.</p>
                                    <%} else {%>
                                    <p><%=new String(Base64.decode(serMoniDesc))%></p>
                                    <%}%>
                                </a>
                            </div>
                            <div class="col-md-3">
                                <a onclick="apiServicesV2('<%=digiCertAccId%>', '<%=digiCertResId%>')"><h4 class="m-t-lg"><i class="fa fa-file-code-o text-info icon-big fa-3x"></i></h4>
                                        <%if (digiCertlabel == null) {%>
                                    <strong>Digital Cerificate Issuer</strong>
                                    <%} else {%>
                                    <strong><%=digiCertlabel%></strong>
                                    <%}
                                        if (digiCertDesc == null) {%>  
                                    <p>We are promoted of security for all and this service is true reflection of our commitment to security. You can use these service APIs to issue certificates to your user or your system (application).  Using user inputs or directly CSR the service can generate the certficiate for you. Revoke option is also available to remove if needed.</p>
                                    <%} else {%>
                                    <p><%=new String(Base64.decode(digiCertDesc))%></p>
                                    <%}%>
                                </a>
                            </div>                                        
                        </div>
                        <div class="row text-center">
                            <div class="col-md-3">
                                <a onclick="apiServicesV2('<%=SSLCertDiscoverytAccId%>', '<%=SSLCertDiscoveryResId%>')"><h4 class="m-t-lg"><i class="fa  fa-gears text-info icon-big fa-3x"></i></h4>
                                        <%if (SSLCertDiscoverylabel == null) {%>
                                    <strong>SSL Cert Discovery</strong>
                                    <%} else {%>
                                    <strong><%=SSLCertDiscoverylabel%></strong>
                                    <%}
                                        if (SSLCertDiscoveryDesc == null) {%>  
                                    <p>Scan multiple networks and their ports to Scan Vulnerabilities (Beast, Weak Cipher, SSLv2 Enabled, SSLv3Enabled, Heartbleed, Freak Attack, Weak Keys, SAH1 violation, Name Mismatch, Weak Hash Algorithm etc). The system gives your reports that allow you to view the health of your entire network, or drill down into specific trouble areas on bases of certificate information.</p>
                                    <%} else {%>
                                    <p><%=new String(Base64.decode(SSLCertDiscoveryDesc))%></p>
                                    <%}%>
                                </a>
                            </div>                    
                            <div class="col-md-3">
                                <a onclick="apiServicesV2('<%=dynamicImgAuthAccId%>', '<%=dynamicImgAuthResId%>')"><h4 class="m-t-lg"><i class="fa fa-file-picture-o text-info icon-big fa-3x"></i></h4>
                                        <%if (dynamicImgAuthlabel == null) {%>
                                    <strong>Dynamic Image Authentication</strong>
                                    <%} else {%>
                                    <strong><%=dynamicImgAuthlabel%></strong>
                                    <%}
                                        if (dynamicImgAuthDesc == null) {%>  
                                    <p>Once again we wish to bring security with comfort to you. Dynamic image authentication is two way two factor authentication where user desired and selected TEXT + COLOR based image is created along with user known sweet spot on dynamic image. This approach is very effective to give needed mutual authentication with web and mobile apps.</p>
                                    <%} else {%>
                                    <p><%=new String(Base64.decode(dynamicImgAuthDesc))%></p>
                                    <%}%>
                                </a>
                            </div>                    
                        </div>                
                    </div>
                </div>
                <br><br>
            </div>
        </div>
    </div>
</div>