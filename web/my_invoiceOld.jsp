<%@include file="header.jsp"%> 

<!-- Main Wrapper -->
<div id="wrapper">

	<div class="small-header transition animated fadeIn">
        <div class="hpanel">
            <div class="panel-body">
                <div id="hbreadcrumb" class="pull-right">
                    <ol class="hbreadcrumb breadcrumb">
                        <li><a href="home.jsp">Dashboard</a></li>
                        <li class="active">
                            <span>My Invoice</span>
                        </li>
                    </ol>
                </div>
                <h2 class="font-light m-b-xs">
                    Invoice
                </h2>
                <small>Short description to developer what is this page about</small>
            </div>
        </div>
    </div>


<div class="content animate-panel">

    <div class="row">
        <div class="col-lg-12">
            <div class="hpanel">
                <div class="panel-heading">
                    <div class="panel-tools">
                        <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                    </div>
					My Invoices
                </div>
                <div class="panel-body">
                <table id="api" class="table table-striped table-bordered table-hover">
                <thead>
                <tr>
					<th>Date</th>
                    <th>Invoice (PDF)</th>
					<th>Current Month Charge</th>
                    <th>Overdue Charge</th>
					<th>Paid</th>
					<th>Balance</th>
                </tr>
                </thead>
                <tbody>
                <tr>
					<td>02/01/2017</td>
                    <td><a href="#" class="btn btn-primary btn-xs"><i class="fa fa-file-pdf-o"></i> View Invoice</a></td>
					<td>RM 230.00</td>
                    <td> - </td>
					<td>RM 200.00</td>
					<td>RM 30.00</td>
                </tr>
                <tr>
					<td>03/02/2017</td>
                    <td><a href="#" class="btn btn-primary btn-xs"><i class="fa fa-file-pdf-o"></i> View Invoice</a></td>
					<td>RM 300.00</td>
                    <td>RM 30.00 </td>
					<td>RM 150.00</td>
					<td>RM 180.00</td>
                </tr>
                <tr>
					<td>02/03/2017</td>
                    <td><a href="#" class="btn btn-primary btn-xs"><i class="fa fa-file-pdf-o"></i> View Invoice</a></td>
					<td>RM 428.00</td>
                    <td>RM 180.00 </td>
					<td>RM 608.00</td>
					<td>RM 0.00</td>
                </tr>
                </tbody>
                </table>

                </div>
            </div>
        </div>
		
    </div>

    <%@include file="footer.jsp"%> 


<script>

    $(function () {

        // Initialize Example 2
        $('#api').dataTable();

    });
	
	$(function () {

        // Initialize Example 2
        $('#data_type').dataTable();

    });

</script>