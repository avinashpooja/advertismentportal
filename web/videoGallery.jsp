    

    <div class="content animate-panel">
        <div class="row">
            <div class="col-lg-12">
                <div class="hpanel">
                    <div class="panel-body">

                        <div class="lightBoxGallery">

                            <p>
                                <strong>Blueimp Gallery</strong>  is a touch-enabled, responsive and customizable image & video gallery, carousel and
                                lightbox, optimized for both mobile and desktop web browsers.
                                It features swipe, mouse and keyboard navigation, transition effects, slideshow
                                functionality, fullscreen support and on-demand content loading and can be extended to
                                display additional content types.
                            </p>

<!--                            <a href="https://www.youtube.com/watch?v=e-ORhEE9VVg" title="Image from Unsplash" data-gallery="#blueimp-gallery1"><img src="https://img.youtube.com/vi/e-ORhEE9VVg/maxresdefault.jpg"></a>                            
                            <a href="images/gallery/2.jpg" title="Image from Unsplash" data-gallery=""><img src="images/gallery/2s.jpg"></a>
                            <a href="images/gallery/3.jpg" title="Image from Unsplash" data-gallery=""><img src="images/gallery/3s.jpg"></a>
                            <a href="images/gallery/4.jpg" title="Image from Unsplash" data-gallery=""><img src="images/gallery/4s.jpg"></a>
                            <a href="images/gallery/5.jpg" title="Image from Unsplash" data-gallery=""><img src="images/gallery/5s.jpg"></a>
                            <a href="images/gallery/6.jpg" title="Image from Unsplash" data-gallery=""><img src="images/gallery/6s.jpg"></a>
                            <a href="images/gallery/7.jpg" title="Image from Unsplash" data-gallery=""><img src="images/gallery/7s.jpg"></a>
                            <a href="images/gallery/8.jpg" title="Image from Unsplash" data-gallery=""><img src="images/gallery/8s.jpg"></a>
                            <a href="images/gallery/9.jpg" title="Image from Unsplash" data-gallery=""><img src="images/gallery/9s.jpg"></a>
                            <a href="images/gallery/10.jpg" title="Image from Unsplash" data-gallery=""><img src="images/gallery/10s.jpg"></a>

                            <a href="images/gallery/1.jpg" title="Image from Unsplash" data-gallery=""><img src="images/gallery/1s.jpg"></a>
                            <a href="images/gallery/2.jpg" title="Image from Unsplash" data-gallery=""><img src="images/gallery/2s.jpg"></a>
                            <a href="images/gallery/3.jpg" title="Image from Unsplash" data-gallery=""><img src="images/gallery/3s.jpg"></a>
                            <a href="images/gallery/4.jpg" title="Image from Unsplash" data-gallery=""><img src="images/gallery/4s.jpg"></a>
                            <a href="images/gallery/5.jpg" title="Image from Unsplash" data-gallery=""><img src="images/gallery/5s.jpg"></a>
                            <a href="images/gallery/6.jpg" title="Image from Unsplash" data-gallery=""><img src="images/gallery/6s.jpg"></a>
                            <a href="images/gallery/7.jpg" title="Image from Unsplash" data-gallery=""><img src="images/gallery/7s.jpg"></a>
                            <a href="images/gallery/8.jpg" title="Image from Unsplash" data-gallery=""><img src="images/gallery/8s.jpg"></a>
                            <a href="images/gallery/9.jpg" title="Image from Unsplash" data-gallery=""><img src="images/gallery/9s.jpg"></a>
                            <a href="images/gallery/10.jpg" title="Image from Unsplash" data-gallery=""><img src="images/gallery/10s.jpg"></a> -->


                            <div id="blueimp-gallery1" class="blueimp-gallery blueimp-gallery-controls blueimp-gallery-carousel">
                                <div class="slides"></div>
                                <h3 class="title"></h3>
                                <a class="prev">�</a>
                                <a class="next">�</a>
                                <a class="play-pause"></a>
                            </div>
                        </div>                                               


                    </div>
                    <div class="panel-footer">
                        <i class="fa fa-picture-o"> </i> 20 pimages
                    </div>
                </div>
            </div>
        </div>
    </div>

<!-- The Gallery as lightbox dialog, should be a child element of the document body -->
<!--<div id="blueimp-gallery1" class="blueimp-gallery1">
    <div class="slides"></div>
    <h3 class="title"></h3>
    <a class="prev">�</a>
    <a class="next">�</a>
    <a class="close">�</a>
    <a class="play-pause"></a>
    <ol class="indicator"></ol>
</div>-->



<!-- Vendor scripts -->

<script src="vendor/blueimp-gallery/js/jquery.blueimp-gallery.min.js"></script>
<!-- <script src="vendor/blueimp-gallery/js/blueimp-gallery-fullscreen.js"></script> -->
<script src="vendor/blueimp-gallery/js/blueimp-gallery-indicator.js"></script>
<script src="vendor/blueimp-gallery/js/blueimp-gallery-video.js"></script>
<script src="vendor/blueimp-gallery/js/blueimp-gallery-youtube.js"></script>
<script src="vendor/blueimp-gallery/js/blueimp-gallery-vimeo.js"></script>

<!-- Local style for demo purpose -->
<script type="text/javascript">
    blueimp.Gallery([
    {
        title: 'A YouYube video',
        href: 'https://www.youtube.com/watch?v=e-ORhEE9VVg',
        type: 'text/html',
        youtube: 'e-ORhEE9VVg',
        poster: 'https://img.youtube.com/vi/e-ORhEE9VVg/maxresdefault.jpg'
    },
    {
        title: 'A YouYube video',
        href: 'https://www.youtube.com/watch?v=e-ORhEE9VVg',
        type: 'text/html',
        youtube: 'e-ORhEE9VVg',
        poster: 'https://img.youtube.com/vi/e-ORhEE9VVg/maxresdefault.jpg'
    },
    {
        title: 'A YouYube video',
        href: 'https://www.youtube.com/watch?v=e-ORhEE9VVg',
        type: 'text/html',
        youtube: 'e-ORhEE9VVg',
        poster: 'https://img.youtube.com/vi/e-ORhEE9VVg/maxresdefault.jpg'
    },
    {
        title: 'A YouYube video',
        href: 'https://www.youtube.com/watch?v=e-ORhEE9VVg',
        type: 'text/html',
        youtube: 'e-ORhEE9VVg',
        poster: 'https://img.youtube.com/vi/e-ORhEE9VVg/maxresdefault.jpg'
    },
    {
        title: 'A YouYube video',
        href: 'https://www.youtube.com/watch?v=e-ORhEE9VVg',
        type: 'text/html',
        youtube: 'e-ORhEE9VVg',
        poster: 'https://img.youtube.com/vi/e-ORhEE9VVg/maxresdefault.jpg'
    }            
],
{
    container: '#blueimp-gallery1',
    carousel: true
  })
    ;
</script>
<style>

    .lightBoxGallery {
        text-align: center;
    }

    .lightBoxGallery a {
        margin: 5px;
        display: inline-block;
    }

</style>