<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.AccessPointManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.Accesspoint"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.RequestPackageManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgReqbucketdetails"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Iterator"%>
<%@include file="header.jsp"%> 
<script src="scripts/packageDetails.js" type="text/javascript"></script>
<script src="scripts/packageOperation.js" type="text/javascript"></script>
<%
    String packageName = "NA";
    String paymentMode = "NA";
    ArrayList<String> resourceDetails = new ArrayList<String>();
    AccessPointManagement management = new AccessPointManagement();
    if (subscriObject1 != null) {
        packageName = subscriObject1.getBucketName();
        paymentMode = subscriObject1.getPaymentMode();
        String accesspointDetails = subscriObject1.getApRateDetails();
        JSONArray jsOld = new JSONArray(accesspointDetails);
        //String keyS = "";
        for (int j = 0; j < jsOld.length(); j++) {
            JSONObject jsonexists1 = jsOld.getJSONObject(j);
            Iterator<String> keys = jsonexists1.keys();
            while (keys.hasNext()) {
                String keyData = keys.next();
                String[] keyDetails = keyData.split(":");
                Accesspoint accesspoint = management.getAccessPointByNames(keyDetails[0]);
                if (accesspoint != null) {
                    resourceDetails.add(keyDetails[0]);
                }
                accesspoint = null;
            }
        }
        accesspointDetails = subscriObject1.getFlatPrice();
        if (accesspointDetails != null) {
            if (!accesspointDetails.isEmpty()) {
                jsOld = new JSONArray(accesspointDetails);
                //String keyS = "";
                for (int j = 0; j < jsOld.length(); j++) {
                    JSONObject jsonexists1 = jsOld.getJSONObject(j);
                    Iterator<String> keys = jsonexists1.keys();
                    while (keys.hasNext()) {
                        String keyData = keys.next();
                        String[] keyDetails = keyData.split(":");
                        Accesspoint accesspoint = management.getAccessPointByNames(keyDetails[0]);
                        if (accesspoint != null) {
                            if (!resourceDetails.contains(keyDetails[0])) {
                                resourceDetails.add(keyDetails[0]);
                            }
                            accesspoint = null;
                        }
                    }
                }
            }
        }
    }
    boolean prePaidLoanflag = false;
    boolean postpaidPaymentFlag = false;
    if (subscriObject1 != null) {
        if (subscriObject1.getPaymentMode().equalsIgnoreCase("postpaid")) {
            //SgPaymentdetails paymentObj = new PaymentManagement().getPaymentDetailsbyPartnerAndSubscriptionID(subscriObject1.getBucketId(), parObj.getPartnerId());
            if (subscriObject1.getStatus() != GlobalStatus.PAID || subscriObject1.getStatus() != GlobalStatus.DEFAULT) {
                postpaidPaymentFlag = true;
            }
        } else {
            SgLoanDetails loandetailObject = new LoanManagement().getLoanDetails(SessionId, ChannelId, parObj.getPartnerId(), subscriObject1.getBucketId());
            if (loandetailObject != null) {
                if (loandetailObject.getStatus() == GlobalStatus.APPROVED) {
                    prePaidLoanflag = true;
                }
            }
        }
    }
%>
<!-- Main Wrapper -->
<div id="wrapper">
    <div class="small-header transition animated fadeIn">
        <div class="hpanel">
            <div class="panel-body">
                <div id="hbreadcrumb" class="pull-right">
                    <ol class="hbreadcrumb breadcrumb">
                        <li><a href="home.jsp">Dashboard</a></li>
                        <li class="active">
                            <span>My Subscription</span>
                        </li>
                    </ol>
                </div>
                <h2 class="font-light m-b-xs">
                    Subscription Package
                </h2>
                <small>Know your Subscription details from here</small>
            </div>
        </div>
    </div>
    <div class="content animate-panel">
        <div class="row">
            <div class="col-lg-12">
                <div class="text-center m-b-xl">
                    <h3>My Subscription</h3>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="hpanel hred">
                            <div class="panel-body">
                                <h3><%=packageName%></h3>
                                <small><strong>Package Model:</strong> <%=paymentMode%></small>
                               
                                <div class="text-muted font-bold m-b-xs">
                               
                                </div>
                                <%if (subscriObject1 != null) {%>    
                                <a href="my_subscription_package_detail.jsp" class="btn btn-danger btn-sm">View Package</a>
                                <%} else {%>
                                <a href="#" class="btn btn-danger btn-sm">View Package</a>
                                <%}%>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <hr class="m-b-xl"/>
            <div class="col-lg-12" style="display: none">
<!--            <div class="col-lg-12">-->
                <div class="text-center m-b-xl">
                    <h3>Add-on Package</h3>
                </div>
                <div class="row">
                    <%
                        SgReqbucketdetails[] packageObj = null;
                        //int count = 0;                
                        packageObj = new RequestPackageManagement().getPackageRequestsbyStatus(SessionId, GlobalStatus.APPROVED);
                        if (packageObj != null) {
                            for (int i = 0; i < packageObj.length; i++) {
                                ArrayList<String> avaResourceDetails = new ArrayList<String>();
                                if (("," + packageObj[i].getPartnerVisibility()).contains(",all,") || ("," + packageObj[i].getPartnerVisibility()).contains("," + parObj.getPartnerId() + ",")) {
                                    //count++;
                                    String accesspointDetails = packageObj[i].getApRateDetails();
                                    JSONArray jsOld = new JSONArray(accesspointDetails);
                                    //String keyS = "";
                                    if (!avaResourceDetails.isEmpty()) {
                                        avaResourceDetails.clear();
                                    }
                                    for (int j = 0; j < jsOld.length(); j++) {
                                        JSONObject jsonexists1 = jsOld.getJSONObject(j);
                                        Iterator<String> keys = jsonexists1.keys();
                                        while (keys.hasNext()) {
                                            String keyData = keys.next();
                                            String[] keyDetails = keyData.split(":");
                                            Accesspoint accesspoint = management.getAccessPointByNames(keyDetails[0]);
                                            if (accesspoint != null) {
                                                avaResourceDetails.add(keyDetails[0]);
                                            }
                                            accesspoint = null;

                                        }
                                    }

                                    accesspointDetails = packageObj[i].getFlatPrice();
                                    if (accesspointDetails != null) {
                                        if (!accesspointDetails.isEmpty()) {
                                            jsOld = new JSONArray(accesspointDetails);
                                            //String keyS = "";
                                            for (int j = 0; j < jsOld.length(); j++) {
                                                JSONObject jsonexists1 = jsOld.getJSONObject(j);
                                                Iterator<String> keys = jsonexists1.keys();
                                                while (keys.hasNext()) {
                                                    String keyData = keys.next();
                                                    String[] keyDetails = keyData.split(":");
                                                    Accesspoint accesspoint = management.getAccessPointByNames(keyDetails[0]);
                                                    if (accesspoint != null) {
                                                        if (!avaResourceDetails.contains(keyDetails[0])) {
                                                            avaResourceDetails.add(keyDetails[0]);
                                                        }
                                                        accesspoint = null;
                                                    }
                                                }
                                            }
                                        }
                                    }
                    %>

                    <div class="col-lg-4">
                        <div class="hpanel hblue">
                            <div class="panel-body">
                                <h3><%=packageObj[i].getBucketName()%></h3>
                                <small><strong>Package Model:</strong> <%=packageObj[i].getPaymentMode()%></small>
                                <p>The package come with following APIs:</p>
                                <div class="text-muted font-bold m-b-xs">
                                    <ul>
                                        <%
                                            if (avaResourceDetails.size() != 0) {
                                                for (int av = 0; av < avaResourceDetails.size(); av++) {
                                        %>
                                        <li><%=avaResourceDetails.get(av)%></li>
                                            <%
                                                    }
                                                }
                                            %>                                        
                                    </ul>
                                </div>
                                <a href="my_subscription_package_detail_live.jsp?_name=<%=packageObj[i].getBucketName()%>" class="btn btn-info btn-sm">View Package</a>

                                <%if (postpaidPaymentFlag) {%>
                                <a class="btn btn-primary btn-sm ladda-button"  data-style="zoom-in" id="subscriptionErrorButton<%=i%>" onclick="showPackageChange('<%=packageObj[i].getBucketName()%>', '<%=subscriObject1.getPaymentMode()%>',<%=i%>)"> Subscribe</a>                        
                                <%} else if (packageObj[i].getPaymentMode().equalsIgnoreCase("postpaid")) {%>
                                <a  class="btn btn-primary btn-sm ladda-button" data-style="zoom-in" id="subscriptionButton<%=i%>" onclick="subscribePostpaid('<%=packageObj[i].getBucketName()%>', '<%=i%>')"> Subscribe</a>
                                <%}%>
                            </div>
                        </div>
                    </div>                                  
                    <%

                            }
                        }
                    } else {
                    %>
                    <p> No Packages Are Available</p> 
                    <%}%>
                </div>
            </div>
        </div>
    </div>
    <!-- Footer-->
    <%@include file="footer.jsp"%>
