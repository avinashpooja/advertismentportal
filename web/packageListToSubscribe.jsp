<%@page import="java.text.DecimalFormat"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.RequestPackageManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.GlobalStatus"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgReqbucketdetails"%>

<html lang="en">
    <head>
       <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Advertisement Portal</title>
        
        
<script type='text/javascript' src='https://themify.me/wp-includes/js/jquery/jquery.js'></script>
<script type='text/javascript' src='https://themify.me/wp-includes/js/jquery/jquery-migrate.min.js'></script>
<script type='text/javascript' src='https://themify.me/wp-content/themes/themify-v3/js/jquery.sidr.min.js'></script>
<script type='text/javascript' src='https://themify.me/wp-content/themes/themify-v3/js/jquery.inview.min.js'></script>
<script type='text/javascript' src='https://themify.me/wp-content/themes/themify-v3/js/jquery.magnific-popup.min.js'></script>
<script type='text/javascript' src='https://themify.me/wp-includes/js/underscore.min.js'></script>
<script type='text/javascript' src='https://themify.me/wp-content/themes/themify-v3/js/themify.script.js'></script>


        <script src="http://code.jquery.com/jquery-1.11.3.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
        <style type="text/css">
            html, body, div, span, applet, object, iframe,
            h1, h2, h3, h4, h5, h6, p, blockquote, pre,
             abbr, acronym, address, big, cite, code,
            del, dfn, em, img, ins, kbd, q, s, samp,
            small, strike, strong, sub, sup, tt, var,
            b, u, i, center,
            dl, dt, dd, ol, ul, li,
            
            article, aside, canvas, details, embed, 
            figure, figcaption, footer, header, hgroup, 
            menu, nav, output, ruby, section, summary,
            time, mark, audio, video {
                margin: 0;
                padding: 0;
                border: 0;
                font-size: 100%;
               
            }
            /* HTML5 display-role reset for older browsers */
            article, aside, details, figcaption, figure, 
            footer, header, hgroup, menu, nav, section, main {
                display: block;
            }
          
            ol, ul {
                list-style: none;
            }
            blockquote, q {
                quotes: none;
            }
            blockquote:before, blockquote:after,
            q:before, q:after {
                content: '';
                content: none;
            }
           
            *,
            *::after,
            *::before {
                -webkit-box-sizing: border-box;
                -moz-box-sizing: border-box;
                box-sizing: border-box;
            }

            html {
                font-size: 100%;
            }

           
         

           
            .pricing-container {
                width: 100%;
                max-width: 1170px;
                margin: 4em auto;
            }

            .pricing-container {
                margin: 2em auto;
            }
            .pricing-container.full-width {
                width: 100%;
                max-width: none;
            }

            .pricing-switcher {
                text-align: center;
            }

            .pricing-switcher .fieldset {
                display: inline-block;
                position: relative;
                border-radius: 50em;
                border: 2px solid #5dd877;
            }

            .pricing-switcher input[type="radio"] {
                position: absolute;
                opacity: 0;
            }

            .pricing-switcher label {
                position: relative;
                z-index: 1;
                display: inline-block;
                float: left;
                width: 92px;
                height: 40px;
                line-height: 40px;
                cursor: pointer;
                font-size: 0.9rem;
                color: #596887;
            }
            
           .pricing-box {
    border-radius: 2px;
    float: left;
    padding: 2% 15px 2%;
    color: #000;
    font-size: .9em;
}
.pricing-box:first-child {
	margin-left: 0;
}
.pricing-box p {
	margin: 0 0;
}
.pricing-box tr {
  color: #333;
    font-weight: 300;
	}
.pricing-box .pricing-description {
	
}
.pricing-box .pricing-term {
	margin: 0 0 20px;
	font-size: .9em;
	line-height: 1.6em;
}
.pricing-box .pricing-price {
	text-transform: uppercase;
	font: 100 3.3em/1em 'Open Sans', Helvetica, Arial, sans-serif;
	letter-spacing: 1px;
	margin: 20px 20% 10px;
	border-bottom: 1px solid rgba(0,0,0,.1);
	padding-bottom: 15px;
	white-space: nowrap;
}
.pricing-box .pricing-price small {
	font-size: 9px;
	display: block;
	margin: 6px 0 5px;
	line-height: 120%;
}
.pricing-box .pricing-price del {
	font-size: 16px;
	display: block;
	line-height: 100%;
}
.pricing-box .button {
	background: #000;
	padding: .7em 1.3em;
	font-size: 1.2em;
}

/* grid2 pricing-box */

.pricing-box.popular-pricing .pricing-price {
	border-color: rgba(255,255,255,.3);
}
.pricing-box.popular-pricing .popular-text {
	position: absolute;
	top: -75px;
	color:  #41b120;
	font: 1.3em/100% 'Montserrat';
	text-transform: uppercase;
	letter-spacing: 1px;
	margin-left: -5;
	left: 0;
	text-align: center;
	width: 100%;
}
.pricing-box.popular-pricing .popular-text:before {
	font-family: fontello;
	content: "";
	position: absolute;
	bottom: -25px;
	left: 50%;
	width: 30px;
	margin-left: -15px;
}

@-webkit-keyframes animate_popular_text {
	0%, 100% {
		top: -75px;
	}
	50% {
		top: -55px;
	}
}
@keyframes animate_popular_text {
	0%, 100% {
		top: -75px;
	}
	50% {
		top: -55px;
	}
}
.pricing-box.popular-pricing .popular-text {
	-webkit-animation: animate_popular_text 1s ease-out infinite;
	-moz-animation: animate_popular_text 1s ease-out infinite;
	animation: animate_popular_text 1s ease-out infinite;
}

/*
CHANGE LOGS PAGE
================================================ */

/************************************************************************************
THEMIFY ANIMATION
*************************************************************************************/
/* transitions */

/* FLY IN ANIMATION */
.animated li,
.col2-1.animated,
.col3-2.animated,
.col3-1.animated,
.col4-3.animated,
.col4-2.animated,
.col4-1.animated,
.col-full.animated {
	-webkit-backface-visibility: hidden;
	-webkit-animation-duration: 0.5s;
	animation-duration: 0.5s;
	-webkit-animation-fill-mode: both;
	animation-fill-mode: both;
}

/* element first */
.grid4.fly-in.animated li:nth-of-type(4n+1),
.grid3.fly-in.animated li:nth-of-type(3n+1),
.grid2.fly-in.animated li:nth-of-type(2n+1),
.grid2-thumb.fly-in.animated li:nth-of-type(2n+1),
.list-post.fly-in.animated li:nth-of-type(2n+1),
.col2-1.first.fly-in.animated,
.col3-2.first.fly-in.animated,
.col3-1.first.fly-in.animated,
.col4-3.first.fly-in.animated,
.col4-2.first.fly-in.animated,
.col4-1.first.fly-in.animated,
.col-full.animate-first.fly-in.animated {
	-webkit-animation-name: flyInLeft;
	animation-name: flyInLeft;
}

/* element last */

@-webkit-keyframes flyInLeft {
	0% {
		opacity: 0;
		-webkit-transform: translate(-400px, 200px);
		-ms-transform: translate(-400px, 200px);
		transform: translate(-400px, 200px);
	}

	100% {
		opacity: 1;
		-webkit-transform: translate(0px, 0px);
		-ms-transform: translate(0px, 0px);
		transform: translate(0px, 0px);
	}
}

@keyframes flyInLeft {
	0% {
		opacity: 0;
		-webkit-transform: translate(-400px, 200px);
		-ms-transform: translate(-400px, 200px);
		transform: translate(-400px, 200px);
	}

	100% {
		opacity: 1;
		-webkit-transform: translate(0px, 0px);
		-ms-transform: translate(0px, 0px);
		transform: translate(0px, 0px);
	}
}

@-webkit-keyframes flyInRight {
	0% {
		opacity: 0;
		-webkit-transform: translate(400px, 200px);
		-ms-transform: translate(400px, 200px);
		transform: translate(400px, 200px);
	}

	100% {
		opacity: 1;
		-webkit-transform: translate(0px, 0px);
		-ms-transform: translate(0px, 0px);
		transform: translate(0px, 0px);
	}
}

@keyframes flyInRight {
	0% {
		opacity: 0;
		-webkit-transform: translate(400px, 200px);
		-ms-transform: translate(400px, 200px);
		transform: translate(400px, 200px);
	}

	100% {
		opacity: 1;
		-webkit-transform: translate(0px, 0px);
		-ms-transform: translate(0px, 0px);
		transform: translate(0px, 0px);
	}
}

@-webkit-keyframes flyInLeftSecond {
	0% {
		opacity: 0;
		-webkit-transform: translate(-200px, 200px);
		-ms-transform: translate(-200px, 200px);
		transform: translate(-200px, 200px);
	}

	100% {
		opacity: 1;
		-webkit-transform: translate(0px, 0px);
		-ms-transform: translate(0px, 0px);
		transform: translate(0px, 0px);
	}
}

@keyframes flyInLeftSecond {
	0% {
		opacity: 0;
		-webkit-transform: translate(-200px, 200px);
		-ms-transform: translate(-200px, 200px);
		transform: translate(-200px, 200px);
	}

	100% {
		opacity: 1;
		-webkit-transform: translate(0px, 0px);
		-ms-transform: translate(0px, 0px);
		transform: translate(0px, 0px);
	}
}

@-webkit-keyframes flyInRightSecond {
	0% {
		opacity: 0;
		-webkit-transform: translate(200px, 200px);
		-ms-transform: translate(200px, 200px);
		transform: translate(200px, 200px);
	}

	100% {
		opacity: 1;
		-webkit-transform: translate(0px, 0px);
		-ms-transform: translate(0px, 0px);
		transform: translate(0px, 0px);
	}
}

@keyframes flyInRightSecond {
	0% {
		opacity: 0;
		-webkit-transform: translate(200px, 200px);
		-ms-transform: translate(200px, 200px);
		transform: translate(200px, 200px);
	}

	100% {
		opacity: 1;
		-webkit-transform: translate(0px, 0px);
		-ms-transform: translate(0px, 0px);
		transform: translate(0px, 0px);
	}
}

@-webkit-keyframes flyInBottom {
	0% {
		opacity: 0;
		-webkit-transform: translate(0px, 200px);
		-ms-transform: translate(0px, 200px);
		transform: translate(0px, 200px);
	}

	100% {
		opacity: 1;
		-webkit-transform: translate(0px, 0px);
		-ms-transform: translate(0px, 0px);
		transform: translate(0px, 0px);
	}
}

@keyframes flyInBottom {
	0% {
		opacity: 0;
		-webkit-transform: translate(0px, 200px);
		-ms-transform: translate(0px, 200px);
		transform: translate(0px, 200px);
	}

	100% {
		opacity: 1;
		-webkit-transform: translate(0px, 0px);
		-ms-transform: translate(0px, 0px);
		transform: translate(0px, 0px);
	}
}

/* arrow animation */
@keyframes arrowAnimation {
	50% {
		-webkit-transform: translateY(-20px);
		-ms-transform: translateY(-20px);
		transform: translateY(-20px);
	}
}
@-webkit-keyframes arrowAnimation {
	50% {
		-webkit-transform: translateY(-20px);
		-ms-transform: translateY(-20px);
		transform: translateY(-20px);
	}
}


            .pricing-switcher .switch {
                position: absolute;
                top: 2px;
                left: 2px;
                height: 40px;
                width: 90px;
                background-color:  #41b120; 
                border-radius: 50em;
                -webkit-transition: -webkit-transform 0.5s;
                -moz-transition: -moz-transform 0.5s;
                transition: transform 0.5s;
            }

            .pricing-switcher input[type="radio"]:checked + label + .switch,
            .pricing-switcher input[type="radio"]:checked + label:nth-of-type(n) + .switch {
                -webkit-transform: translateX(90px);
                -moz-transform: translateX(90px);
                -ms-transform: translateX(90px);
                -o-transform: translateX(90px);
                transform: translateX(90px);
            }

            .no-js .pricing-switcher {
                display: none;
            }

            .pricing-list {
                margin: 2em 0 0;
            }

            .pricing-list > li {
                position: relative;
                margin-bottom: 1em;
            }

            @media only screen and (min-width: 768px) {
                .pricing-list {
                    margin: 3em 0 0;
                }
                .pricing-list:after {
                    content: "";
                    display: table;
                    clear: both;
                }
                .pricing-list > li {
                    width: 25%;
                    float: left;
                    padding-left: 5px;
                    padding-right: 5px;
                }
                .has-margins .pricing-list > li {
                    width: 25%;
                    float: left;
                    margin-right: 1.5%;
                }
                .has-margins .pricing-list > li:last-of-type {
                    margin-right: 0;
                }
            }

            .pricing-wrapper {
                position: relative;
            }

            .touch .pricing-wrapper {
                -webkit-perspective: 2000px;
                -moz-perspective: 2000px;
                perspective: 2000px;
            }

            .pricing-wrapper.is-switched .is-visible {
                -webkit-transform: rotateY(180deg);
                -moz-transform: rotateY(180deg);
                -ms-transform: rotateY(180deg);
                -o-transform: rotateY(180deg);
                transform: rotateY(180deg);
                -webkit-animation: rotate 0.5s;
                -moz-animation: rotate 0.5s;
                animation: rotate 0.5s;
            }

            .pricing-wrapper.is-switched .is-hidden {
                -webkit-transform: rotateY(0);
                -moz-transform: rotateY(0);
                -ms-transform: rotateY(0);
                -o-transform: rotateY(0);
                transform: rotateY(0);
                -webkit-animation: rotate-inverse 0.5s;
                -moz-animation: rotate-inverse 0.5s;
                animation: rotate-inverse 0.5s;
                opacity: 0;
            }

            .pricing-wrapper.is-switched .is-selected {
                opacity: 1;
            }

            .pricing-wrapper.is-switched.reverse-animation .is-visible {
                -webkit-transform: rotateY(-180deg);
                -moz-transform: rotateY(-180deg);
                -ms-transform: rotateY(-180deg);
                -o-transform: rotateY(-180deg);
                transform: rotateY(-180deg);
                -webkit-animation: rotate-back 0.5s;
                -moz-animation: rotate-back 0.5s;
                animation: rotate-back 0.5s;
            }

            .pricing-wrapper.is-switched.reverse-animation .is-hidden {
                -webkit-transform: rotateY(0);
                -moz-transform: rotateY(0);
                -ms-transform: rotateY(0);
                -o-transform: rotateY(0);
                transform: rotateY(0);
                -webkit-animation: rotate-inverse-back 0.5s;
                -moz-animation: rotate-inverse-back 0.5s;
                animation: rotate-inverse-back 0.5s;
                opacity: 0;
            }

            .pricing-wrapper.is-switched.reverse-animation .is-selected {
                opacity: 1;
            }

            .pricing-wrapper > li {
                background-color: #ffffff;
                -webkit-backface-visibility: hidden;
                backface-visibility: hidden;
                outline: 1px solid transparent;
            }

            .pricing-wrapper > li::after {
                content: '';
                position: absolute;
                top: 0;
                right: 0;
                height: 100%;
                width: 50px;
                pointer-events: none;
                background: -webkit-linear-gradient( right , #ffffff, rgba(255, 255, 255, 0));
                background: linear-gradient(to left, #ffffff, rgba(255, 255, 255, 0));
            }

            .pricing-wrapper > li.is-ended::after {
                display: none;
            }

            .pricing-wrapper .is-visible {
                position: relative;
                z-index: 5;
            }

            .pricing-wrapper .is-hidden {
                position: absolute;
                top: 0;
                left: 0;
                height: 100%;
                width: 100%;
                z-index: 1;
                -webkit-transform: rotateY(180deg);
                -moz-transform: rotateY(180deg);
                -ms-transform: rotateY(180deg);
                -o-transform: rotateY(180deg);
                transform: rotateY(180deg);
            }

            .pricing-wrapper .is-selected {
                z-index: 3 !important;
            }

            @media only screen and (min-width: 768px) {
                .pricing-wrapper > li::before {
                    content: '';
                    position: absolute;
                    z-index: 6;
                    left: -1px;
                    top: 50%;
                    bottom: auto;
                    -webkit-transform: translateY(-50%);
                    -moz-transform: translateY(-50%);
                    -ms-transform: translateY(-50%);
                    -o-transform: translateY(-50%);
                    transform: translateY(-50%);
                    height: 50%;
                    width: 1px;
                    background-color: #b1d6e8;
                }
                .pricing-wrapper > li::after {
                    display: none;
                }
                .exclusive .pricing-wrapper > li {
                    box-shadow: inset 0 0 0 3px #2d3e50;
                }
                .has-margins .pricing-wrapper > li,
                .has-margins .exclusive .pricing-wrapper > li {
                    box-shadow: 0 1px 5px rgba(0, 0, 0, 0.1);
                }
                :nth-of-type(1) > .pricing-wrapper > li::before {
                    display: none;
                }
                .has-margins .pricing-wrapper > li {
                    border-radius: 4px 4px 6px 6px;
                }
                .has-margins .pricing-wrapper > li::before {
                    display: none;
                }
            }

            @media only screen and (min-width: 1500px) {
                .full-width .pricing-wrapper > li {
                    padding: 2.5em 0;
                }
            }

            .no-js .pricing-wrapper .is-hidden {
                position: relative;
                -webkit-transform: rotateY(0);
                -moz-transform: rotateY(0);
                -ms-transform: rotateY(0);
                -o-transform: rotateY(0);
                transform: rotateY(0);
                margin-top: 1em;
            }

            @media only screen and (min-width: 768px) {
                .exclusive .pricing-wrapper > li::before {
                    display: none;
                }
                .exclusive + li .pricing-wrapper > li::before {
                    display: none;
                }
            }

            .green-gr {
/* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#41b120+0,99cb02+100 */
background: #41b120; /* Old browsers */
background: -moz-linear-gradient(left, #41b120 0%, #99cb02 100%); /* FF3.6-15 */
background: -webkit-linear-gradient(left, #41b120 0%,#99cb02 100%); /* Chrome10-25,Safari5.1-6 */
background: linear-gradient(to right, #41b120 0%,#99cb02 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#41b120', endColorstr='#99cb02',GradientType=1 ); /* IE6-9 */	
}

           

            .currency,
            .value {
                font-size: 3rem;
                font-weight: 300;
            }

            .duration {
                font-weight: 700;
                font-size: 1.3rem;
                color: #8dc8e4;
                text-transform: uppercase;
            }

            .exclusive .duration {
                color: #f3b6ab;
            }

            .duration::before {
                content: '/';
                margin-right: 2px;
            }

            .value {
                font-size: 7rem;
                font-weight: 300;
            }

            .currency, 
            .duration {
                color: #1bbc9d;
            }

            .exclusive .currency,
            .exclusive .duration {
                color: #2d3e50;
            }

            .currency {
                display: inline-block;
                margin-top: 10px;
                vertical-align: top;
                font-size: 2rem;
                font-weight: 700;
            }

            .duration {
                font-size: 1.4rem;
            }

            .pricing-body {
                overflow-x: auto;
                -webkit-overflow-scrolling: touch;
            }

            .is-switched .pricing-body {
                overflow: hidden;
            }

            .pricing-body {
                overflow-x: visible;
            }

            .pricing-features {
                width: 600px;
            }

            .pricing-features:after {
                content: "";
                display: table;
                clear: both;
            }

            .pricing-features li {
                width: 100px;
                float: left;
                padding: 1.6em 1em;
                font-size: 1.5rem;
                text-align: center;
                white-space: nowrap;
                overflow: hidden;
                text-overflow: ellipsis;
            }

            .pricing-features em {
                display: block;
                margin-bottom: 5px;
                font-weight: 600;
            }

            .pricing-features {
                width: auto;
            }

            .pricing-features li {
                float: none;
                width: auto;
                padding: 1em;
            }

            .exclusive .pricing-features li {
                margin: 0 3px;
            }

            .pricing-features em {
                display: inline-block;
                margin-bottom: 0;
            }

            .has-margins .exclusive .pricing-features li {
                margin: 0;
            }

            .pricing-footer {
                position: absolute;
                z-index: 1;
                top: 0;
                left: 0;
                height: 80px;
                width: 100%;
            }

            .pricing-footer {
                position: relative;
                height: auto;
                padding: 1.8em 0;
                text-align: center;
            }

            .pricing-footer::after {
                display: none;
            }

            .has-margins .pricing-footer {
                padding-bottom: 0;
            }

            .select {
                position: relative;
                z-index: 1;
                display: block;
                height: 100%;
                overflow: hidden;
                text-indent: 100%;
                white-space: nowrap;
                color: transparent;
            }

            .select {
                position: static;
                display: inline-block;
                height: auto;
                padding: 1.3em 2em;
                color: #1bbc9d;
                border-radius: 8px;
                border: 2px solid #1bbc9d;
                font-size: 1.4rem;
                text-indent: 0;
                text-transform: uppercase;
                letter-spacing: 2px;
                transition: all .6s;
                width: 70%;
            }

            .no-touch .select:hover {
                background-color: #1bbc9d;
                color: #ffffff;
            }

            .exclusive .select {
                background-color: #1bbc9d;
                color: #ffffff;
            }

            .no-touch .exclusive .select:hover {
                background-color: #24e0ba;
            }

            .secondary-theme .exclusive .select {
                background-color: #1bbc9d;
            }

            .no-touch .secondary-theme .exclusive .select:hover {
                background-color: #112e3c;
            }

            .has-margins .select {
                display: block;
                padding: 1.7em 0;
                border-radius: 0 0 4px 4px;
            }

            @-webkit-keyframes rotate {
                0% {
                    -webkit-transform: perspective(2000px) rotateY(0);
                }
                70% {
                    -webkit-transform: perspective(2000px) rotateY(200deg);
                }
                100% {
                    -webkit-transform: perspective(2000px) rotateY(180deg);
                }
            }

            @-moz-keyframes rotate {
                0% {
                    -moz-transform: perspective(2000px) rotateY(0);
                }
                70% {
                    -moz-transform: perspective(2000px) rotateY(200deg);
                }
                100% {
                    -moz-transform: perspective(2000px) rotateY(180deg);
                }
            }

            @keyframes rotate {
                0% {
                    -webkit-transform: perspective(2000px) rotateY(0);
                    -moz-transform: perspective(2000px) rotateY(0);
                    -ms-transform: perspective(2000px) rotateY(0);
                    -o-transform: perspective(2000px) rotateY(0);
                    transform: perspective(2000px) rotateY(0);
                }
                70% {
                    -webkit-transform: perspective(2000px) rotateY(200deg);
                    -moz-transform: perspective(2000px) rotateY(200deg);
                    -ms-transform: perspective(2000px) rotateY(200deg);
                    -o-transform: perspective(2000px) rotateY(200deg);
                    transform: perspective(2000px) rotateY(200deg);
                }
                100% {
                    -webkit-transform: perspective(2000px) rotateY(180deg);
                    -moz-transform: perspective(2000px) rotateY(180deg);
                    -ms-transform: perspective(2000px) rotateY(180deg);
                    -o-transform: perspective(2000px) rotateY(180deg);
                    transform: perspective(2000px) rotateY(180deg);
                }
            }

            @-webkit-keyframes rotate-inverse {
                0% {
                    -webkit-transform: perspective(2000px) rotateY(-180deg);
                }
                70% {
                    -webkit-transform: perspective(2000px) rotateY(20deg);
                }
                100% {
                    -webkit-transform: perspective(2000px) rotateY(0);
                }
            }

            @-moz-keyframes rotate-inverse {
                0% {
                    -moz-transform: perspective(2000px) rotateY(-180deg);
                }
                70% {
                    -moz-transform: perspective(2000px) rotateY(20deg);
                }
                100% {
                    -moz-transform: perspective(2000px) rotateY(0);
                }
            }

            @keyframes rotate-inverse {
                0% {
                    -webkit-transform: perspective(2000px) rotateY(-180deg);
                    -moz-transform: perspective(2000px) rotateY(-180deg);
                    -ms-transform: perspective(2000px) rotateY(-180deg);
                    -o-transform: perspective(2000px) rotateY(-180deg);
                    transform: perspective(2000px) rotateY(-180deg);
                }
                70% {
                    -webkit-transform: perspective(2000px) rotateY(20deg);
                    -moz-transform: perspective(2000px) rotateY(20deg);
                    -ms-transform: perspective(2000px) rotateY(20deg);
                    -o-transform: perspective(2000px) rotateY(20deg);
                    transform: perspective(2000px) rotateY(20deg);
                }
                100% {
                    -webkit-transform: perspective(2000px) rotateY(0);
                    -moz-transform: perspective(2000px) rotateY(0);
                    -ms-transform: perspective(2000px) rotateY(0);
                    -o-transform: perspective(2000px) rotateY(0);
                    transform: perspective(2000px) rotateY(0);
                }
            }

            @-webkit-keyframes rotate-back {
                0% {
                    -webkit-transform: perspective(2000px) rotateY(0);
                }
                70% {
                    -webkit-transform: perspective(2000px) rotateY(-200deg);
                }
                100% {
                    -webkit-transform: perspective(2000px) rotateY(-180deg);
                }
            }

            @-moz-keyframes rotate-back {
                0% {
                    -moz-transform: perspective(2000px) rotateY(0);
                }
                70% {
                    -moz-transform: perspective(2000px) rotateY(-200deg);
                }
                100% {
                    -moz-transform: perspective(2000px) rotateY(-180deg);
                }
            }

            @keyframes rotate-back {
                0% {
                    -webkit-transform: perspective(2000px) rotateY(0);
                    -moz-transform: perspective(2000px) rotateY(0);
                    -ms-transform: perspective(2000px) rotateY(0);
                    -o-transform: perspective(2000px) rotateY(0);
                    transform: perspective(2000px) rotateY(0);
                }
                70% {
                    -webkit-transform: perspective(2000px) rotateY(-200deg);
                    -moz-transform: perspective(2000px) rotateY(-200deg);
                    -ms-transform: perspective(2000px) rotateY(-200deg);
                    -o-transform: perspective(2000px) rotateY(-200deg);
                    transform: perspective(2000px) rotateY(-200deg);
                }
                100% {
                    -webkit-transform: perspective(2000px) rotateY(-180deg);
                    -moz-transform: perspective(2000px) rotateY(-180deg);
                    -ms-transform: perspective(2000px) rotateY(-180deg);
                    -o-transform: perspective(2000px) rotateY(-180deg);
                    transform: perspective(2000px) rotateY(-180deg);
                }
            }

            @-webkit-keyframes rotate-inverse-back {
                0% {
                    -webkit-transform: perspective(2000px) rotateY(180deg);
                }
                70% {
                    -webkit-transform: perspective(2000px) rotateY(-20deg);
                }
                100% {
                    -webkit-transform: perspective(2000px) rotateY(0);
                }
            }

            @-moz-keyframes rotate-inverse-back {
                0% {
                    -moz-transform: perspective(2000px) rotateY(180deg);
                }
                70% {
                    -moz-transform: perspective(2000px) rotateY(-20deg);
                }
                100% {
                    -moz-transform: perspective(2000px) rotateY(0);
                }
            }

            @keyframes rotate-inverse-back {
                0% {
                    -webkit-transform: perspective(2000px) rotateY(180deg);
                    -moz-transform: perspective(2000px) rotateY(180deg);
                    -ms-transform: perspective(2000px) rotateY(180deg);
                    -o-transform: perspective(2000px) rotateY(180deg);
                    transform: perspective(2000px) rotateY(180deg);
                }
                70% {
                    -webkit-transform: perspective(2000px) rotateY(-20deg);
                    -moz-transform: perspective(2000px) rotateY(-20deg);
                    -ms-transform: perspective(2000px) rotateY(-20deg);
                    -o-transform: perspective(2000px) rotateY(-20deg);
                    transform: perspective(2000px) rotateY(-20deg);
                }
                100% {
                    -webkit-transform: perspective(2000px) rotateY(0);
                    -moz-transform: perspective(2000px) rotateY(0);
                    -ms-transform: perspective(2000px) rotateY(0);
                    -o-transform: perspective(2000px) rotateY(0);
                    transform: perspective(2000px) rotateY(0);
                }
            }   
        </style> </head>

    <body>  

<%
    SgReqbucketdetails[] packageObj = null;
    String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
    packageObj = new RequestPackageManagement().getPackageRequestsbyStatus(SessionId, GlobalStatus.APPROVED);
    String basicMonth = null, basicYear = null, studentMonth = null, studentYear = null, standardMonth = null, standardYear = null, enterpriseMonth = null, enterpriseYear = null;
    float basicMAmount = 0;
    float basicYAmount = 0;
    float studentMAMount = 0;
    float studentYAmount = 0;
    float stdMAMount = 0;
    float stdYAmount = 0;
    float entMntAmount = 0;
    float entYAmount = 0;
    String strbasicMAmount = "0.00";
    String strbasicYAmount = "0.00";
    String strstudentMAMount = "0.00";
    String strstudentYAmount = "0.00";
    String strstdMAMount = "0.00";
    String strstdYAmount = "0.00";
    String strentMntAmount = "0.00";
    String strentYAmount = "0.00";
    DecimalFormat df = new DecimalFormat("#0.00");
    if (packageObj != null) {
        for (SgReqbucketdetails reqbucketdetails : packageObj) {
            String bucketName = reqbucketdetails.getBucketName().toLowerCase();
            if (bucketName.contains("basic") && bucketName.contains("month")) {
                basicMonth = reqbucketdetails.getBucketName();
                basicMAmount = reqbucketdetails.getPlanAmount();
                strbasicMAmount = df.format(basicMAmount);
            } else if (bucketName.contains("basic") && bucketName.contains("year")) {
                basicYear = reqbucketdetails.getBucketName();
                basicYAmount = reqbucketdetails.getPlanAmount();
                strbasicYAmount = df.format(basicYAmount);
            } else if (bucketName.contains("student") && bucketName.contains("month")) {
                studentMonth = reqbucketdetails.getBucketName();
                studentMAMount = reqbucketdetails.getPlanAmount();
                strstudentMAMount = df.format(studentMAMount);
            } else if (bucketName.contains("student") && bucketName.contains("year")) {
                studentYear = reqbucketdetails.getBucketName();
                studentYAmount = reqbucketdetails.getPlanAmount();
                strstudentYAmount = df.format(studentYAmount);
            } else if (bucketName.contains("standard") && bucketName.contains("month")) {
                standardMonth = reqbucketdetails.getBucketName();
                stdMAMount = reqbucketdetails.getPlanAmount();
                strstdMAMount = df.format(stdMAMount);
            } else if (bucketName.contains("standard") && bucketName.contains("year")) {
                standardYear = reqbucketdetails.getBucketName();
                stdYAmount = reqbucketdetails.getPlanAmount();
                strstdYAmount = df.format(stdYAmount);
            } else if (bucketName.contains("enterprise") && bucketName.contains("month")) {
                enterpriseMonth = reqbucketdetails.getBucketName();
                strentMntAmount = df.format(reqbucketdetails.getPlanAmount());
            } else if (bucketName.contains("enterprise") && bucketName.contains("year")) {
                enterpriseYear = reqbucketdetails.getBucketName();
                strentYAmount = df.format(reqbucketdetails.getPlanAmount());
            }
        }
    }
%>

        <div class="container mobile-pt-70 pb-30" >
            <div class="row"style="margin-left: -15px;
                 margin-right: -15px;">
                <div class="col-sm-12">
                    <h2 style="font-size: 24px;color: #666;padding-left: 60px;position: relative;padding: 10px 15px;    font-weight: 400;"><i class="mdi mdi-credit-card" style="font-size:40px;color:#41b120;"></i>&nbsp;&nbsp;Subscription Package</h2>

                </div>

                <div class="col-md-12 col-sm-12 col-xs-12" >
                    <div class="hpanel">
                        <div class="pricing-container">
                            <div class="pricing-switcher">
                                <p class="fieldset">
                                    <input type="radio" name="duration-1" value="monthly" id="monthly-1" checked>
                                    <label for="monthly-1">Monthly</label>
                                    <input type="radio" name="duration-1" value="yearly" id="yearly-1">
                                    <label for="yearly-1">Yearly</label>
                                    <span class="switch"></span>
                                </p>
                            </div>
                            <ul class="pricing-list grid3 has-popular fly-in animated ">
                               
                                    <li class="pricing-box yellow">
                                    <ul class="pricing-wrapper">
                                        <li data-type="monthly" class="is-visible">
                                            <div  class="green-gr" style="    padding: 20px 0;
                                                 border-radius: 2px 2px 0 0;
                                                 
                                                 text-align: center;">
                                                <div class="icon-box" style="margin: 0 auto;"><i class="fa fa-users icon" style="font-size: 2em;
                                                                                                 color: #447F71;"></i></div>
                                                <h2 style="color: #fff;
                                                    font-weight: lighter    ;
                                                    margin: 0;
                                                    padding-top: 0.625em;line-height: 1.15; font-size: 25px">Basic</h2>
                                                <br>
                                                <p style="margin: 0;
                                                   color: #447F71;">A$ <%=strbasicMAmount%>/Month</p>
                                               
                                            </div>
                                                   <div class="" style="  background: #fff;
  border: 1px solid #e4e5e7;
  border-radius: 2px;
  padding: 0px;
  position: relative;
">

                                                <table class="table">

                                                    <tbody>
                                                        <tr>
                                                            <td>
                                                                <i class="fa fa-check-square-o"></i> Response Time (5 days)
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <i class="fa fa-check-square-o"></i> Resolution Time (14 days)
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <i class="fa fa-check-square-o"></i> Support Available (Email)
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <i class="fa fa-check-square-o"></i> Product Documentation
                                                            </td>
                                                        </tr>                                        
                                                        <tr>
                                                            <td>
                                                                <i class="fa fa-check-square-o"></i>  Special designed API Console
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <i class="fa fa-check-square-o"></i>   Reports
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <i class="fa fa-check-square-o"></i>   All In One Solution
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <i class="fa fa-square-o"></i>   Webex Remote Resolution
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <i class="fa fa-square-o"></i>   One on One API Discussion
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <i class="fa fa-check-square-o"></i>   Uptime 95%
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <i class="fa fa-check-square-o"></i>   API Calls per Second (3)
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <i class="fa fa-check-square-o"></i>   Support Hours 9x5 Weekdays
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <i class="fa fa-check-square-o"></i>   Non Expiry Monthly Credits (1000 Credits)
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>

                                                <a href="#" style="margin-left:30%" onclick="subscribePrepaid('<%=basicMonth%>', 'prepaid')" class="btn btn-default">Subscribe</a>
                      <br>
                      <br>
                                            </div>
                                        </li>
                                        <li data-type="yearly" class="is-hidden">
                                            <div  class="green-gr" style="    padding: 20px 0;
                                                 border-radius: 2px 2px 0 0;
                                                 
                                                 text-align: center;">
                                              
                                                <div class="icon-box" style="margin: 0 auto;"><i class="fa fa-users icon" style="font-size: 2em;
                                                                                                 color: #447F71;"></i></div>
                                                <h2 style="color: #fff;
                                                    font-weight: lighter;
                                                    margin: 0;
                                                    padding-top: 0.625em;line-height: 1.15;font-size: 25px">Basic</h2>
                                                <br>
                                                <p style="margin: 0;
                                                   color: #447F71;">A$ <%=strbasicYAmount%>/Year</p>
                                               
                                            </div>
                                           <div class="" style="  background: #fff;
  border: 1px solid #e4e5e7;
  border-radius: 2px;
  padding: 0px;
  position: relative;
">       <table class="table">
                               
                                <tbody>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i> Response Time (5 days)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i> Resolution Time (14 days)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i> Support Available (Email)
                                            </td>
                                        </tr>                                        
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i> Product Documentation
                                            </td>
                                        </tr>                                        
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>  Special designed API Console
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   Reports
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   All In One Solution
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-square-o"></i>   Webex Remote Resolution
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-square-o"></i>   One on One API Discussion
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   Uptime 95%
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   API Calls per Second  (3)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   Support Hours 9x5 Weekdays
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   Non Expiry Yearly Credits (50000)
                                            </td>
                                        </tr>
                                    </tbody>
                            </table> 
                                            
                                                 <a style="margin-left:30%" onclick="subscribePrepaid('<%=basicYear%>', 'prepaid')" class="btn btn-default">Subscribe</a>
               <br>
                                         <br>
                                         <br>
                                         <br>
                    </div>    </li>
                                    </ul>
                                </li>
                                    <li class="pricing-box yellow">
                                    <ul class="pricing-wrapper">
                                        <li data-type="monthly" class="is-visible">
                                            <div  class="green-gr" style="    padding: 20px 0;
                                                 border-radius: 2px 2px 0 0;
                                                 
                                                 text-align: center;">
                                              
                                                <div class="icon-box" style="margin: 0 auto;"><i class="fa fa-graduation-cap" style="font-size: 2em;
                                                                                                 color: #447F71;"></i></div>
                                                <h2 style="color: #fff;
                                                    font-weight: lighter;
                                                    margin: 0;
                                                    padding-top: 0.625em;line-height: 1.15;font-size: 25px">Student</h2>
                                                <br>
                                                <p style="margin: 0;
                                                   color: #447F71;"> A$ <%=strstudentMAMount%>/Month</p>
                                               
                                            </div>
                            
                                     <div class="" style="  background: #fff;
  border: 1px solid #e4e5e7;
  border-radius: 2px;
  padding: 0px;
  position: relative;
">         <table class="table">
                               
                                <tbody>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i> Response Time (3 days)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i> Resolution Time (3 days)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i> Support Available (Email)
                                            </td>
                                        </tr>
                                        
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i> Product Documentation
                                            </td>
                                        </tr>                                        
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>  Special designed API Console
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   Reports
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   All In One Solution
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-square-o"></i>   Webex Remote Resolution
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-square-o"></i>   One on One API Discussion
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   Uptime 98%
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   API Calls per Second (5)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   Support Hours 9x5 Weekdays
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   Non Expiry Monthly Credits  (1500)
                                            </td>
                                        </tr>
                                    </tbody>
                            </table>
                           <a href="#" style="margin-left:30%" class="btn btn-default" onclick="subscribePrepaid('<%=studentMonth%>', 'prepaid')">Subscribe</a>
                       <br>
                       <br>
                                            </div>
                                           
                                        </li>
                                        <li data-type="yearly" class="is-hidden">
                                          <div  class="green-gr" style="    padding: 20px 0;
                                                 border-radius: 2px 2px 0 0;
                                                 
                                                 text-align: center;">
                                              
                                                <div class="icon-box" style="margin: 0 auto;"><i class="fa fa-graduation-cap " style="font-size: 2em;
                                                                                                 color: #447F71;"></i></div>
                                                <h2 style="color: #fff;
                                                    font-weight: lighter;
                                                    margin: 0;
                                                    padding-top: 0.625em;line-height: 1.15;font-size: 25px">Student</h2>
                                                <br>
                                                <p style="margin: 0;
                                                   color: #447F71;">A$ <%=strstudentYAmount%>/Year</p>
                                            </div>
                            
     <div class="" style="  background: #fff;
  border: 1px solid #e4e5e7;
  border-radius: 2px;
  padding: 0px;
  position: relative;
">      <table class="table">
                              
                                <tbody>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i> Response Time (3 days)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i> Resolution Time (3 days)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i> Support Available (Email)
                                            </td>
                                        </tr>                                        
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i> Product Documentation
                                            </td>
                                        </tr>                                        
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>  Special designed API Console
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   Reports
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   All In One Solution
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-square-o"></i>   Webex Remote Resolution
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-square-o"></i>   One on One API Discussion
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   Uptime 98%
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   API Calls per Second (5)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   Support Hours 9x5 Weekdays
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   Non Expiry Yearly Credits (150000)
                                            </td>
                                        </tr>
                                    </tbody>
                            </table>             
                                            
                                     <a style="margin-left:30%" onclick="subscribePrepaid('<%=studentYear%>', 'prepaid')" class="btn btn-default">Subscribe</a>
                   <br>
                                         <br>
                                         <br>
                                         <br>
                    </div>      </li>
                                    </ul>
                                </li>
                                  <li class="pricing-box yellow popular-pricing">
                                      <ul class="pricing-wrapper">
                                        <li data-type="monthly" class="is-visible">
                                           <div  class="green-gr" style="    padding: 20px 0;
                                                 border-radius: 2px 2px 0 0;
                                                 
                                                 text-align: center;">
                                              
                                                <div class="icon-box" style="margin: 0 auto;"><i class="fa fa-building-o" style="font-size: 2em;
                                                                                                 color: #447F71;"></i></div>
                                                <h2 style="color: #fff;
                                                    font-weight: lighter;
                                                    margin: 0;
                                                    padding-top: 0.625em;line-height: 1.15;font-size: 25px">Startup</h2>
                                                <br>
                                                <p style="margin: 0;
                                                   color: #447F71;"> A$ <%=strstdMAMount%>/Month</p>
                                             
                                            </div>
                            
  <div class="" style="  background: #fff;
  border: 1px solid #e4e5e7;
  border-radius: 2px;
  padding: 0px;
  position: relative;
">      <table class="table">
                               
                                <tbody>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i> Response Time (1 day)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i> Resolution Time (1 day)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i> Support Available (Email & Call)
                                            </td>
                                        </tr>                                        
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i> Product Documentation
                                            </td>
                                        </tr>                                        
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>  Special designed API Console
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   Reports
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   All In One Solution
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   Webex Remote Resolution (Upto 2/month)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   One on One API Discussion (30mins/month)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   Uptime 99%
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   API Calls per Second (10)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   Support Hours 9x5 Weekdays
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   Non Expiry Monthly Credits (5000)
                                            </td>
                                        </tr>
                                    </tbody>
                            </table>
                                   <a href="#" style="margin-left:30%" class="btn btn-default" onclick="subscribePrepaid('<%=standardMonth%>', 'prepaid')">Subscribe</a>
                     <br>
                     <br>
                                            </div>
                                            
                                        </li>
                                        <li data-type="yearly" class="is-hidden">
                                           <div  class="green-gr" style="    padding: 20px 0;
                                                 border-radius: 2px 2px 0 0;
                                                 
                                                 text-align: center;">
                                              
                                                <div class="icon-box" style="margin: 0 auto;"><i class="fa fa-building-o" style="font-size: 2em;
                                                                                                 color: #447F71;"></i></div>
                                                <h2 style="color: #fff;
                                                    font-weight: lighter;
                                                    margin: 0;
                                                    padding-top: 0.625em;line-height: 1.15;font-size: 25px">Startup</h2>
                                                <br>
                                                <p style="margin: 0;
                                                   color: #447F71;">A$ <%=strstdYAmount%>/Year</p>
                                           </div>
                            
   <div class="" style="  background: #fff;
  border: 1px solid #e4e5e7;
  border-radius: 2px;
  padding: 0px;
  position: relative;
">         <table class="table">
                             
                                <tbody>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i> Response Time (1 day)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i> Resolution Time (1 day)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i> Support Available (Email & Call)
                                            </td>
                                        </tr>                                        
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i> Product Documentation
                                            </td>
                                        </tr>                                        
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>  Special designed API Console
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   Reports
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   All In One Solution
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   Webex Remote Resolution (Upto 2/month)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   One on One API Discussion (30mins/month)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   Uptime 99%
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   API Calls per Second (10)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   Support Hours 9x5 Weekdays
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   Non Expiry Yearly Credits (500000)
                                            </td>
                                        </tr>
                                    </tbody>
                            </table>      
                                            
                                          <a style="margin-left:30%" onclick="subscribePrepaid('<%=standardYear%>', 'prepaid')" class="btn btn-default">Subscribe</a>
         <br>
                                         <br>
                                         <br>
                                         <br>
                    </div>        </li>
                                    </ul>
                                </li>
                                    <li class="pricing-box orange popular-pricing"><span class="popular-text">Popular<br><i class="glyphicon glyphicon-menu-down" style="font-size:40px;color: #41b120;"></i></span>
			        <ul class="pricing-wrapper">
                                        <li data-type="monthly" class="is-visible">
                                           <div  class="green-gr" style="    padding: 20px 0;
                                                 border-radius: 2px 2px 0 0;
                                                 
                                                 text-align: center;">
                                              
                                                <div class="icon-box" style="margin: 0 auto;"><i class="fa fa-bank" style="font-size: 2em;
                                                                                                 color: #447F71;"></i></div>
                                                <h2 style="color: #fff;
                                                    font-weight: lighter;
                                                    margin: 0;
                                                    padding-top: 0.625em;line-height: 1.15;font-size: 25px">Enterprise</h2>
                                                <br>
                                                <p style="margin: 0;
                                                   color: #447F71;">A$ <%=strentMntAmount%>/Month</p>
                                           </div>
                            
  <div class="" style="  background: #fff;
  border: 1px solid #e4e5e7;
  border-radius: 2px;
  padding: 0px;
  position: relative;
">    <table class="table">
                                <tbody>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i> Response Time (2 hours)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i> Resolution Time (2 hours)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i> Support Available (Email & Call) 
                                            </td>
                                        </tr>
                                        
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i> Product Documentation
                                            </td>
                                        </tr>                                       
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>  Special designed API Console
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   Reports
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   All In One Solution
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   Webex Remote Resolution (Upto 5/month)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   One on One API Discussion (90mins/month)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   Uptime 99.90%
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   API Calls per Second (30)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   Support Hours 24x7x365 Weekdays
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   Non Expiry Monthly Credits (36000)
                                            </td>
                                        </tr>
                                    </tbody>
                            </table>
                                    <a href="#" style="margin-left:30%" class="btn btn-default" onclick="subscribePrepaid('<%=enterpriseMonth%>', 'prepaid')">Subscribe</a>
                <br>
                                          <br></div>
                                           
                                        </li>
                                        <li data-type="yearly" class="is-hidden">
                                           <div  class="green-gr" style="    padding: 20px 0;
                                                 border-radius: 2px 2px 0 0;
                                                 
                                                 text-align: center;">
                                              
                                                <div class="icon-box" style="margin: 0 auto;"><i class="fa fa-bank" style="font-size: 2em;
                                                                                                 color: #447F71;"></i></div>
                                                <h2 style="color: #fff;
                                                    font-weight: lighter;
                                                    margin: 0;
                                                    padding-top: 0.625em;line-height: 1.15;font-size: 25px">Enterprise</h2>
                                                <br>
                                                <p style="margin: 0;
                                                   color: #447F71;">A$ <%=strentYAmount%>/Year</p>
                                           </div>
                            
   <div class="" style="  background: #fff;
  border: 1px solid #e4e5e7;
  border-radius: 2px;
  padding: 0px;
  position: relative;
">      <table class="table">
                           
                                <tbody>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i> Response Time (2 hours)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i> Resolution Time (2 hours)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i> Support Available (Email & Call)
                                            </td>
                                        </tr>                                        
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i> Product Documentation
                                            </td>
                                        </tr>                                        
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>  Special designed API Console
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   Reports
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   All In One Solution
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   Webex Remote Resolution (Upto 5/month)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   One on One API Discussion (90mins/month)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   Uptime 99.90%
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   API Calls per Second (30)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   Support Hours 24x7x365
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   Non Expiry Yearly Credits (5400000)
                                            </td>
                                        </tr>
                                    </tbody>
                            </table>       
                                          
                                            <a style="margin-left:30%" onclick="subscribePrepaid('<%=enterpriseYear%>', 'prepaid')" class="btn btn-default">Subscribe</a>
                    <br>
                                         <br>
                                         <br>
                                         <br>
                                         </div>      </li>
                                    </ul>
                                </li>
                                
                            </ul>
                        </div>
                        <script>
                            jQuery(document).ready(function ($) {
                                //hide the subtle gradient layer (.pricing-list > li::after) when pricing table has been scrolled to the end (mobile version only)
                                checkScrolling($('.pricing-body'));
                                $(window).on('resize', function () {
                                    window.requestAnimationFrame(function () {
                                        checkScrolling($('.pricing-body'))
                                    });
                                });
                                $('.pricing-body').on('scroll', function () {
                                    var selected = $(this);
                                    window.requestAnimationFrame(function () {
                                        checkScrolling(selected)
                                    });
                                });

                                function checkScrolling(tables) {
                                    tables.each(function () {
                                        var table = $(this),
                                                totalTableWidth = parseInt(table.children('.pricing-features').width()),
                                                tableViewport = parseInt(table.width());
                                        if (table.scrollLeft() >= totalTableWidth - tableViewport - 1) {
                                            table.parent('li').addClass('is-ended');
                                        } else {
                                            table.parent('li').removeClass('is-ended');
                                        }
                                    });
                                }

                                //switch from monthly to annual pricing tables
                                bouncy_filter($('.pricing-container'));

                                function bouncy_filter(container) {
                                    container.each(function () {
                                        var pricing_table = $(this);
                                        var filter_list_container = pricing_table.children('.pricing-switcher'),
                                                filter_radios = filter_list_container.find('input[type="radio"]'),
                                                pricing_table_wrapper = pricing_table.find('.pricing-wrapper');

                                        //store pricing table items
                                        var table_elements = {};
                                        filter_radios.each(function () {
                                            var filter_type = $(this).val();
                                            table_elements[filter_type] = pricing_table_wrapper.find('li[data-type="' + filter_type + '"]');
                                        });

                                        //detect input change event
                                        filter_radios.on('change', function (event) {
                                            event.preventDefault();
                                            //detect which radio input item was checked
                                            var selected_filter = $(event.target).val();

                                            //give higher z-index to the pricing table items selected by the radio input
                                            show_selected_items(table_elements[selected_filter]);

                                            //rotate each pricing-wrapper 
                                            //at the end of the animation hide the not-selected pricing tables and rotate back the .pricing-wrapper

                                            if (!Modernizr.cssanimations) {
                                                hide_not_selected_items(table_elements, selected_filter);
                                                pricing_table_wrapper.removeClass('is-switched');
                                            } else {
                                                pricing_table_wrapper.addClass('is-switched').eq(0).one('webkitAnimationEnd oanimationend msAnimationEnd animationend', function () {
                                                    hide_not_selected_items(table_elements, selected_filter);
                                                    pricing_table_wrapper.removeClass('is-switched');
                                                    //change rotation direction if .pricing-list has the .bounce-invert class
                                                    if (pricing_table.find('.pricing-list').hasClass('bounce-invert'))
                                                        pricing_table_wrapper.toggleClass('reverse-animation');
                                                });
                                            }
                                        });
                                    });
                                }
                                function show_selected_items(selected_elements) {
                                    selected_elements.addClass('is-selected');
                                }

                                function hide_not_selected_items(table_containers, filter) {
                                    $.each(table_containers, function (key, value) {
                                        if (key != filter) {
                                            $(this).removeClass('is-visible is-selected').addClass('is-hidden');

                                        } else {
                                            $(this).addClass('is-visible').removeClass('is-hidden is-selected');
                                        }
                                    });
                                }
                            });
                        </script>
                    </div>               
                </div>      
            </div>
