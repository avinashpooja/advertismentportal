<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.AdvertiserAdManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgAdvertiserAdDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.AdvertiserCreditManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgAdCreditInfo"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgAdvertiseSubscriptionDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.AdvertiserSubscriptionManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgAdvertiserDetails"%>
<%@page import="java.util.Calendar"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.TransformManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.TransformDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.ResourceManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.ResourceDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.AccessPointManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.Accesspoint"%>
<%@page import="java.net.URL"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.ProductionAccessEnvtManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgDeveloperProductionEnvt"%>
<%@page import="java.math.BigDecimal"%>
<!DOCTYPE html>
<%@page import="org.json.JSONArray"%>
<%@page import="org.json.JSONObject"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.ProductionAccessManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgProductionAccess"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.EmailManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgEmailticket"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.GlobalStatus"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PaymentManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgPaymentdetails"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PackageSubscriptionManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgSubscriptionDetails"%>
<%@page import="com.mollatech.service.nucleus.crypto.LoadSettings"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.LoanManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgLoanDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgCreditInfo"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.CreditManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.PartnerDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.SessionManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgUsers"%>
<%@page import="java.util.List"%>
<html>
    <%response.addHeader("X-FRAME-OPTIONS", "DENY");%>    
<head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <!-- Page title -->
        <title>Advertisement Portal</title>

        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
    <link rel="stylesheet" href="vendor/fontawesome/css/font-awesome.css">
    <link rel="stylesheet" href="vendor/animate.css/animate.css" />
    <link rel="stylesheet" href="vendor/bootstrap/dist/css/bootstrap.css" />
    <link rel="stylesheet" href="vendor/bootstrap-star-rating/css/star-rating.css" />
    <link rel="stylesheet" href="vendor/datatables.net-bs/css/dataTables.bootstrap.min.css" />

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" href="https:////cdn.materialdesignicons.com/2.1.99/css/materialdesignicons.min.css">

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">

    <link rel="stylesheet" href="vendor/datatables.net-bs/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="vendor/clockpicker/dist/bootstrap-clockpicker.min.css">
    <link rel="stylesheet" href="vendor/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css">
    <link rel="stylesheet" href="vendor/summernote/dist/summernote.css">
    <link rel="stylesheet" href="vendor/summernote/dist/summernote-bs3.css">
    <link rel="stylesheet" href="vendor/jquery-ui/themes/base/all.css">
    <link rel="stylesheet" href="vendor/sweetalert/lib/sweet-alert.css">
    <link href="HeaderCSS/dropzone.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="vendor/ladda/dist/ladda-themeless.min.css">
    <link rel="stylesheet" href="vendor/toastr/build/toastr.min.css">
    <link rel="stylesheet" href="vendor/codemirror/style/codemirror.css">
    <link rel="stylesheet" href="vendor/fooTable/css/footable.core.min.css">
    <!--         <link rel="stylesheet" href="vendor/c3/c3.min.css" />-->
    <!-- App styles -->
    <link rel="stylesheet" href="fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css">
    <link rel="stylesheet" href="fonts/pe-icon-7-stroke/css/helper.css">
    <link rel="stylesheet" href="styles/style.css">
    <link href="HeaderCSS/bootstrap.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="HeaderCSS/global.css"/>
    <link rel="stylesheet" type="text/css" href="HeaderCSS/menu.css"/>
    <link rel="stylesheet" type="text/css" href="HeaderCSS/search_component.css"/>
    <link href="css/mCustomScrollbar.css" rel="stylesheet" type="text/css"/>
    <!-- Need call first -->

    <script src="scripts/header.js" type="text/javascript"></script>
    <script src="vendor/jquery/dist/jquery.min.js"></script>        
    <script src="vendor/jquery-ui/jquery-ui.min.js"></script>
    <script src="vendor/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="vendor/summernote/dist/summernote.min.js"></script>
    <script src="vendor/codemirror/script/codemirror.js"></script>
    <script src="vendor/codemirror/javascript.js"></script> 
    <link rel="stylesheet" href="vendor/c3/c3.min.css">
    <script src="vendor/d3/d3.min.js"></script>
    <script src="vendor/c3/c3.min.js"></script>
    <link rel="stylesheet" href="vendor/chartist/custom/chartist.css">
    <script src="vendor/chartist/dist/chartist.min.js"></script>
    <script src="scripts/homepage.js" type="text/javascript"></script>
    <script src="vendor/bootstrap-tour/build/js/bootstrap-tour.min.js"></script>
    <link rel="stylesheet" href="vendor/blueimp-gallery/css/blueimp-gallery.min.css">
    <link rel="stylesheet" href="vendor/blueimp-gallery/css/blueimp-gallery-video.css">
    <!--<script src="vendor/jquery-flot/jquery.flot.pie.js" type="text/javascript"></script>-->

    <!-- Vendor scripts -->
    <script src="scripts/utilityFunction.js" type="text/javascript"></script>
    <script src="vendor/slimScroll/jquery.slimscroll.min.js"></script>
    <script src="vendor/metisMenu/dist/metisMenu.min.js"></script>
    <script src="vendor/iCheck/icheck.min.js"></script>
    <script src="vendor/moment/moment.js"></script>
    <script src="vendor/sparkline/index.js"></script>
    <script src="vendor/chartjs/Chart.min.js"></script>
    <script src="vendor/jquery-flot/jquery.flot.js"></script>
    <script src="vendor/jquery-flot/jquery.flot.resize.js"></script>
    <script src="vendor/jquery-flot/jquery.flot.pie.js"></script>
    <script src="vendor/jquery.flot.spline/index.js"></script>
    <script src="vendor/flot.curvedlines/curvedLines.js"></script>
    <script src="vendor/peity/jquery.peity.min.js"></script>
    <script src="vendor/bootstrap-star-rating/js/star-rating.min.js"></script>
    <script src="vendor/codemirror/script/codemirror.js"></script>
    <script src="vendor/codemirror/javascript.js"></script>
    <script src="vendor/select2-3.5.2/select2.min.js"></script>
    <script src="vendor/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js"></script>
    <script src="vendor/bootstrap-datepicker-master/dist/js/bootstrap-datepicker.min.js"></script>
    <script src="vendor/clockpicker/dist/bootstrap-clockpicker.min.js"></script>
    <script src="vendor/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
    <script src="vendor/dropzone/dropzone.js"></script>
    <script src="scripts/tokenManagement.js" type="text/javascript"></script>
    <script src="vendor/ladda/dist/spin.min.js"></script><style type="text/css"></style>
    <script src="vendor/ladda/dist/ladda.min.js"></script>
    <script src="vendor/ladda/dist/ladda.jquery.min.js"></script>
<!--        <script src="vendor/jquery-ui/jquery-ui.min.js"></script>-->
    <script src="vendor/jquery-validation/jquery.validate.min.js"></script>
    <script src="vendor/sweetalert/lib/sweet-alert.min.js"></script>
    <script src="vendor/toastr/build/toastr.min.js"></script>
    <!-- DataTables -->
    <script src="vendor/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="vendor/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

    <!-- DataTables buttons scripts -->
    <script src="vendor/pdfmake/build/pdfmake.min.js"></script>
    <script src="vendor/pdfmake/build/vfs_fonts.js"></script>
    <script src="vendor/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="vendor/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="vendor/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="vendor/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="vendor/fooTable/dist/footable.all.min.js"></script>
    <script src="scripts/pdfAdReport.js" type="text/javascript"></script>

        <!-- App scripts -->
        
</head>
    <%
        response.setHeader("Cache-Control", "no-cache");
        response.setHeader("Cache-Control", "no-store");
        response.setHeader("Pragma", "no-cache");
        response.setDateHeader("Expires", 0);
        
        String SessionId = (String) request.getSession().getAttribute("_advertisorSessionId");
        String ChannelId = (String) request.getSession().getAttribute("_advertiserChannelId");        
        String profileImageUrl = (String) request.getSession().getAttribute("profileImageUrl");
        SgAdvertiserDetails usrObj = (SgAdvertiserDetails) request.getSession().getAttribute("_advertisorDetails");
        String socialNamed = (String) request.getSession().getAttribute("socialNamed");
        String showTourFirstSignUp = (String) request.getSession().getAttribute("showTourFirstSignUp");
        if (SessionId == null) {
            response.sendRedirect("logout.jsp");
            return;
        }
        if (socialNamed == null) {
            socialNamed = usrObj.getAdvertisername();
        }
        int notificationCount = 0;        

        // find the Service Count
        int serviceCount = 0;        
        String base64ProfilePic = null;                
    %>
<body id="Ashish" class="fixed-navbar">
        <style>
            span.capitalize {
                text-transform: capitalize;
            }
        </style>
        <script language="JavaScript" type="text/javascript">
            function changeclass(id) {
                var NAME = document.getElementById(id);
                NAME.className = "active";
            }
         
        </script>
      <!-- top panel starts here -->

              <%                            
                            double mainCredit = 0;
                            SgAdCreditInfo creditObj1 = null;
                            SgAdvertiseSubscriptionDetails subscriObject1 = new AdvertiserSubscriptionManagement().getAdvertiserSubscriptionbyAdId(usrObj.getAdId());                            
                            creditObj1 = new AdvertiserCreditManagement().getDetails(usrObj.getAdId());
                            session.setAttribute("advertiserSubscriptionObject", subscriObject1);
                        %> 
        <%
                                String expiry = "NA";
                                Calendar calendar = Calendar.getInstance();
                                Calendar expireDate = Calendar.getInstance();
                                BigDecimal bdRemainTotalCredit = new BigDecimal(0);
                                if (creditObj1 != null) {
                                    mainCredit = creditObj1.getMainCredit();                                    
                                    bdRemainTotalCredit = new BigDecimal(mainCredit);
                                    bdRemainTotalCredit = bdRemainTotalCredit.setScale(2, BigDecimal.ROUND_HALF_EVEN);
                                }
                                if(subscriObject1 != null){
                                    SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss a");
                                    expiry = dateFormatter.format(subscriObject1.getExpiryDate());
                                    calendar.setTime(subscriObject1.getExpiryDate());
                                    calendar.add(Calendar.DATE, -1);
                                    expireDate.setTime(subscriObject1.getExpiryDate());
                                }
                                boolean pdfAdDetailsFind = false; boolean emailAdDetailsFind = false;
                                SgAdvertiserAdDetails adDetails = new AdvertiserAdManagement().getAdvertiserDetails(usrObj.getAdId());
                                if(adDetails!= null && adDetails.getPdfAdImage() != null){
                                   pdfAdDetailsFind = true; 
                                }
                                if(adDetails!= null && adDetails.getEmailAdImage() != null){
                                   emailAdDetailsFind = true; 
                                }
                            %>
                            
                            
<div class="row">

                    <div class="top-panel box-shadow">

                        <!--    <div>         
                   
                       <span style="color: #41f450;margin-bottom: 30px;">
                       <font size="6"><b>Advertisement</b></font>
                       </span>
                       <span><font size="6">Portal</font></span>
                        -->

                        
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 green-gr ">

                       
                    
              <div style="margin-top:6%;margin-bottom: 5%;margin-left: 5%;font-weight: 800;
    font-family: Helvetica"><font size='5' color='white'>Advertisement Portal</span></FONT></div>

                            
                            <div class="user-wrap visible-xs"> 

                                <div class="dropdown pull-right">

                                    <button class="user dropdown-toggle"  id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">

                                                   <img src="male_user.jpg" class="" alt=""/>
                                        <!--<strong class="icon-user top-user-name"> Blue B.. </strong> -->

                                        <span class="caret"></span>

                                    </button>

                                    <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">

                                        <li><a href="/users/change_password">Change Password</a></li>


                                        <li><a href="/apidoc" target="_blank">Developers API</a></li>

                                        <li><a href="/users/logout">Logout</a></li>

                                    </ul>

                                </div><!-- /.Dropdown -->

                            </div><!-- /.col-lg-2 -->

                        </div>



                        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 right-top hidden-xs p_none">

                            <!-- <span style="color: #41f450;margin-bottom: 30px;">
                             <font size="6"><b>Advertisement</b></font>
                             </span>
                             <span><font size="6">Portal</font></span> -->


                            <div class="top-wrap">



                                <!-- /.search_bar-->

                                <div class="search_bar clearfix">

                                    <nav class="pull-left">

                                        <div class="nav-fostrap">

                                            <ul class="clearfix">          


                                                <!--                                                <li class="active"><a href="/dashboard"><span class="icon-dashboard"> <img class="manImg" src="img/search_icon.png"></img></span>Push Template</a></li>
                                                
                                                                                                <i class="material-icons prefix">account_circle</i>
                                                
                                                                                                <li ><a  href="#"><span class="icon-company"><img class="manImg" src="img/search_icon.png"></img></span>Push Ad <sup></sup></a></li>
                                                
                                                                                                <li ><a  href="#"><span class="icon-utility"><center><img class="manImg" src="img/mail.png"></img></center></span>Email Ad <sup></sup></a></li>
                                                
                                                                                                <li ><a  href="#"><span class="icon-kyc"><img class="manImg" src="img/search_icon.png"></img></span>PDF Ad <sup></sup></a></li>
                                                
                                                                                                <li ><a  href="#"><span class="icon-company"><img class="manImg" src="img/search_icon.png"></img></span>Invoices <sup></sup></a></li>
                                                
                                                                                                <li ><a  href="#"><span class="icon-identity"><img class="manImg" src="img/search_icon.png"></img></span>Subscription <sup></sup></a></li>
                                                
                                                -->


                                                <li> <a class="active" href="#">&nbsp;&nbsp;&nbsp;<i class="fa fa-home fa-2x" style="font-size:20px;" ></i><br>Home<sup></sup></a> </li>&nbsp;&nbsp;&nbsp;
                                                <li> <a class="active" onclick="pushADDetails()">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i style="font-size:20px;" class="fa fa-commenting-o fa-2x" ></i><br>Push Ad<sup></sup></a> </li>&nbsp;&nbsp;&nbsp;
                                                <li><a onclick="pdfADDetails()">&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-file-pdf-o fa-2x" style="font-size:20px;"></i><br>PDF Ad</a> </li>&nbsp;&nbsp;&nbsp;
                                                <li><a onclick="emailADDetails()">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-envelope fa-2x" style="font-size:20px;"></i><br>Email Ad</a> </li>&nbsp;&nbsp;&nbsp;
                                                <li><a onclick="myInvoice()">&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-file-text-o" style="font-size:20px;" ></i><br>Invoice</a></li>&nbsp;&nbsp;&nbsp;
                                                <li><a onclick="subscription()">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-credit-card" style="font-size:20px;"></i><br>Subscription</a> </li>




                                                <!--<li ><a  href="#"><span class="fa fa-home fa-2x" style="text-align: center ;color:#d3d3d3"></span><br>Push Template <sup></sup></a></li>-->

                                                <!--<li ><a  href="#"><i class="icon-email" style="image-orientation: search_icon.pn"></a></li>-->

                                                <br>                                         

                                            </ul>

                                            <!-- /.Ul --> 

                                        </div>

                                        <!-- /.nav-fostrap --> 

                                    </nav>





                                    <!-- /.sb-search-->




                                    <!-- <div id="sb-search" class="sb-search "> -->



                                    <div id="sb-search" class="sb-search">

                                        <form action="/dashboard" id="SearchForm" method="get" accept-charset="utf-8">                  



                                            <input name="first_name" class="sb-search-input" placeholder="Enter your search term..." id="search" maxlength="100" type="text"/>




                                            <!--<input class="sb-search-submit" type="submit" value="">-->

                                            <span class="sb-icon-search"><img src="img/search_icon.png" alt=""/> </span>

                                        </form>
                                    </div><!-- /.sb-search-->				



                                </div>

                                <!-- /.search_bar-->	


                                <script type="text/javascript">

                                    var UISearch=document.getElementById('sb-search');



                                     $(document).on("click",".sb-icon-search",function(){		

                                     $("#sb-search").toggleClass("sb-search-open");		

                                     });





                                    document.getElementById('search').onkeydown = function (e) {

                                        if (e.keyCode == 13) {

                                            $("#SearchForm").submit();

                                        }

                                    };

                                </script>




                                <div class="hidden-xs user-wrap">

                                    <div class="dropdown pull-right">
                                        <button class="user dropdown-toggle"  id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"> 

                                            <img src="img/male_user.jpg" class="" alt=""/>
                                            <!--<strong class="icon-user top-user-name"> Blue B.. </strong> -->

                                            <span class="caret"></span></button>

                                        <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">

                                            <li><a href="/users/change_password">Change Password</a></li>

                                            <li><a href="/apidoc" target="_blank">Developers API</a></li>

                                            <li><a href="/users/logout">Logout</a></li>

                                        </ul>

                                    </div>

                                    <!-- /.Dropdown --> 


                                </div>



                            </div><!-- /.top-wrap -->

                        </div><!-- /.right-top -->

                    </div><!-- /.top panel -->

                </div><!-- top panel ends here -->
<!-- top panel ends here -->

    

        <!-- Navigation -->
        
        <div id="wrapper3" style="display: none">
            <div id="wrapper" style="min-height: 646px;"><br><br><br><br><br><br><br><br><br><br><br>
                <div id="loading"><div class="text-center"><h3>Loading...<!--3--></h3></div><div class="spinner"> <div class="rect1"></div> <div class="rect2"></div> <div class="rect3"></div> <div class="rect4"></div> <div class="rect5"></div> </div> </div> 
            </div>
        </div>

        <div id="wrapper2" style="display: none;" >
            <div id="wrapper" >
                <div id="otherPage" ></div>
             
</div>
        </div>
         <div id="wrapper1">
            <div id="wrapper">
                <div id="homePage">
                    <div class="content animate-panel">
                        <div class="row">
                            <div class="col-lg-3">
                                <div class="hpanel stats">
                                    <div class="panel-heading">

                                        Today's total credit used
                                    </div>
                                    <div class="panel-body h-200">
                                        <div class="stats-icon text-center ">
                                            <i class="pe-7s-share fa-4x"></i>
                                        </div>
                                        <div class="m-t-xl">
                                            <div class="progress m-t-xs full progress-small">
                                                <div id="percentage" aria-valuemax="100" aria-valuemin="0" aria-valuenow="0" role="progressbar" class=" progress-bar progress-bar-info">
                                                    <span class="sr-only">100%</span>
                                                </div>
                                            </div> 
                                            <div class="row" style="height: 60px">
                                                <div class="col-xs-6">
                                                    <small class="stats-label">Credit used</small>
                                                    <h4  id='creditUsed'></h4>
                                                </div>
                                                <div class="col-xs-6">
                                                    <small class="stats-label">% Credit used</small>
                                                    <h4 id='perCreditUsed'></h4>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="hpanel">
                                    <div class="panel-heading">
                                        Last week total credit used
                                    </div>
                                    <div class="panel-body text-center h-200">
                                        <i class="pe-7s-graph1 fa-4x"></i>

                                        <h1 class="m-b-xs text-info" id="lastWeekAmount"></h1>
                                        <small id="weekMsg"></small>
                                        <h3 class="font-bold no-margins lead text-info row">
                                            Weekly Usage
                                        </h3> <!--<small>Lorem ipsum dolor sit amet, consectetur adipiscing elit</small>-->
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-3">
                                <div class="hpanel">
                                    <div class="panel-heading">

                                        Last month total credit used
                                    </div>
                                    <div class="panel-body text-center h-200">
                                        <i class="pe-7s-global fa-4x"></i>
                                        <h1 class="m-b-xs text-info" id="monthCredit"></h1>

                                        <small id="monthMsg"></small>
                                        <h3 class="font-bold no-margins lead text-info row">
                                            Monthly Usage
                                        </h3>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-lg-3 col-sm-3" id="topmostServices" data-child="hpanel" data-effect="fadeInLeft">
                                <div class="hpanel">
                                    <div class="panel-heading">

                                        <span id="mostlyFive"></span>
                                    </div>
                                    <div class="panel-body" style="height: 213px">
                                        <div class="flot-chart text-center" style="width: 200;height:250">
                                            <div class="flot-chart-content"  id="flot-pie-chart"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3" id = "mostlyservices"  style="display: none">
                                <div class="hpanel">
                                    <div class="panel-heading">
                                        Your mostly used services
                                    </div>
                                    <div class="panel-body" style="text-align: center;">                                        
                                        <img src="images/no_record_found.png" alt="No record found" width="200" height="170"/>
                                        <br>                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12" data-child="hpanel" data-effect="fadeInLeft">
                                <div class="hpanel">
                                    <div class="panel-heading">
                                        Advertisement Information And Statistics
                                    </div>
                                    <div class="panel-body">
                                        <div class="text-left">
                                            <i class="fa fa-bar-chart-o fa-fw"></i><span id="homeReportLable">  Last 7 day expenditure</span>
                                        </div>
                                        <div class="btn-group" style="float: right;">
                                            <button  class="btn btn-default btn-xs ladda-button" data-style="zoom-in" onclick="generateTopServiceV2()">Top Five</button>
                                            <button  class="btn btn-default btn-xs ladda-button" data-style="zoom-in" onclick="creditperSer()">Today's Expense</button>
                                            <button  class="btn btn-default btn-xs ladda-button" data-style="zoom-in" onclick="toptrndingServices()">Today's Trending</button>
                                        </div>
                                        <div style="clear: both"></div>
                                        <div  id="topFiveSer" style="height: 215px" class="c3"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>                
                <%@include file="footer.jsp"%>
            </div>
        </div>
        <div id="tourDiv">
            <!-- Tour API Console -->        
            <div id="apiTour">     
                <div id="wrapper">
                    <div id="wrapperAPIConsoleTour" ></div>
                </div>
            </div>
            <!-- API Console End -->

            <!-- Tour API Console Window -->        
            <div id="apiConsoleTour">     
                <div id="wrapper">
                    <div id="wrapperAPIConsoleWindowTour" ></div>
                </div>
            </div>
            <!-- API Console Window End -->

            <!-- Tour report Window -->        
            <div id="reportTour">     
                <div id="wrapper">
                    <div id="reportWindowTour" ></div>
                </div>
            </div>
            <!-- Report Window End -->

            <!-- Tour invoice Window -->        
            <div id="invoiceTour">     
                <div id="wrapper">
                    <div id="invoiceWindowTour" ></div>
                </div>
            </div>
            <!-- Invoice Window End -->

            <!-- Tour subscribe Window -->        
            <div id="subscribeTour">     
                <div id="wrapper">
                    <div id="subscribeWindowTour" ></div>
                </div>
            </div>
            <!-- Subscribe Window End -->

            <!-- Tour Helpdesk Window -->        
            <div id="helpDeskTour">                 
                <div id="wrapper">
                    <div id="helpDeskWindowTour" ></div>  
                </div>
            </div>


        </div>        
    </div>
          </div>
                       <script type="text/javascript">

            document.onreadystatechange = function () {
                if (document.readyState === 'complete') {

                    $('#perCreditUsed').ready(function () {
                        todayusedCredit("today");
                    });
                }
            };

            $('#wrapper2').hide();
            $('#wrapper3').hide();
        </script>

        <script>            
            $(document).ready(function () {
                var creditUsedData = fifteenDaysCreditExpenditure();
                var obj = [];
                for (var key in creditUsedData) {
                    var value = creditUsedData[key];
                    obj.push(value.txAmount);
                    //        alert(JSON.stringify(key)+" :: "+JSON.stringify(value.apiName));
                }
                $("#sparkline1").sparkline(obj, {
                    type: 'bar',
                    barWidth: 7,
                    height: '30px',
                    barColor: '#2381c4',
                    negBarColor: '#53ac2a'
                });

            });
            //    
            function fifteenDaysCreditExpenditure() {
                var s = './DailyTransactionAmount';
                var jsonData = $.ajax({
                    url: s,
                    dataType: "json",
                    async: false
                }).responseText;
                if (jsonData.length === 0) {
                    jsonData = [{"label": "NA", "value": 0, "value5": 0}];
                    return jsonData;
                } else {
                    var myJsonObj = JSON.parse(jsonData);
                    return myJsonObj;
                }
            }

            function getCreditStatus() {
                var s = './GetCredit';
                $.ajax({
                    type: 'POST',
                    url: s,
                    datatype: 'json',
                    data: $("#getBody").serialize(),
                    success: function (data) {
                        if (data.credits !== 'NA') {
                            $('#mainCreditOfDev').html(data.credits);
                        }
                    }});
            }
            getCreditStatus();
            setTimeout(function () {
                if (typeof (tour) != "undefined") {
                    tour.end();
                }
               
            }, 500);
            setTimeout(function () {
                generateTopServiceV2();
            }, 2000);
            test = document.getElementById("Ashish");
            collapse = document.getElementById("mobile-collapse");

        </script>
        <%if (showTourFirstSignUp != null && showTourFirstSignUp.equalsIgnoreCase("yes")) {
                request.getSession().setAttribute("showTourFirstSignUp", null);
        %>
       
        <%}%>
        </body>
</html>