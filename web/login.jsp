<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.SessionManagement"%>
<%@page import="java.text.SimpleDateFormat"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Page title -->
        <title>Advertisement Portal | Login</title>
        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
        <!-- Vendor styles -->
        <link rel="stylesheet" href="vendor/metisMenu/dist/metisMenu.css" />
        <link rel="stylesheet" href="vendor/toastr/build/toastr.min.css" />
         <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="css/global.css" rel="stylesheet" type="text/css"/>
        <link href="css/icon.css" rel="stylesheet" type="text/css"/>
        <link href="css/slick.css" rel="stylesheet" type="text/css"/>
        
        <script src="js/jquery.min.js" type="text/javascript"></script>
        <script src="js/bootstrap.js" type="text/javascript"></script>
        <script src="js/slick.js" type="text/javascript"></script>
        <script src="js/main.js"></script>
        <script src="js/Materialize.js" type="text/javascript"></script>
        
        <script src="scripts/utilityFunction.js" type="text/javascript"></script>
        <script src="scripts/partnerRequest.js" type="text/javascript"></script>
        <!-- App styles -->
        <link rel="stylesheet" href="fonts/pe-icon-7-stroke/css/helper.css" />
      
    <style type="text/css">
	.back-link a {
		color: #4ca340;
		text-decoration: none; 
		border-bottom: 1px #4ca340 solid;
	}
	.back-link a:hover,
	.back-link a:focus {
		color: #408536; 
		text-decoration: none;
		border-bottom: 1px #408536 solid;
	}
	h1 {
		height: 100%;
		/* The html and body elements cannot have any padding or margin. */
		margin: 0;
		font-size: 14px;
		font-family: 'Open Sans', sans-serif;
		font-size: 32px;
		margin-bottom: 3px;
	}
	.entry-header {
		text-align: left;
		margin: 0 auto 50px auto;
		width: 80%;
        max-width: 978px;
		position: relative;
		z-index: 10001;
	}
	#demo-content {
		padding-top: 100px;
	}
	</style>
</head>

    <body>
        <%
            String showTourFirstSignUp = (String) request.getSession().getAttribute("showTourFirstSignUp");
            if (showTourFirstSignUp == null) {
                response.setHeader("Cache-Control", "no-cache");
                response.setHeader("Cache-Control", "no-store");
                response.setHeader("Pragma", "no-cache");
                response.setDateHeader("Expires", 0);
                session.invalidate();
        %>
        <script>
            function deleteAllCookies() {

                var cookies = document.cookie.split(";");
                for (var i = 0; i < cookies.length; i++) {
                    var cookie = cookies[i];
                    var eqPos = cookie.indexOf("=");
                    var name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
                    document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
                }
            }
            deleteAllCookies();
        </script>
        <%}

        %>
	<!-- container of validate by starts here -->
        <div class="main-body">			
            <!-- Login wrap starts here -->
            <div class="login-container">
                <!-- Login slider strats here -->
                <div class="login-slider pull-left hidden-xs">
                    <div class="green-gr overlay-ab">
                        <div class="table">
                            <div class="table-cell">
                                <div class="login-slider-wrap">
                                    <ul class="login-slider-item">
                                        <li>
                                            <h2>Solutions that enable trust</h2>
                                            <p>We are leading provider of platforms and solutions that enable trust.</p>
                                        </li>

                                        <li>
                                            <h2>Build Trust-On-Demand</h2>
                                            <p>Mitigate Risks on the Fly in a matter of minutes!</p>
                                        </li>

                                        <li>
                                            <h2>Verification Solutions for your Business</h2>
                                            <p>Driven by Technology, Speed and Customer Success</p>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <img src="login-img.jpg" class="img-responsive" alt="login-img.jpg"/>		  
                    </div><!-- /.green-gr  -->

                </div><!-- Login slider ends here -->
                <div class="login-form pull-left">
	<div class="login-form-wrap">
	  <div class="table">
		<div class="table-cell">
	                     <form action="#" id="login_form">
                                 	           <br>
                                <br>
                                <br>
                                <br>
                                
                            <div><span style="color: #27bc34;margin-bottom: 15px;"><font size="6"><b>&nbsp;Advertisement</b></font></span><span><font size="6">&nbsp;Portal</font></span></div>
                                <br>
                                
                                <div class="form-group">
<div class="input-field col s6">
                        <i class="material-icons prefix">account_circle</i>
                    
                                    <input type="hidden" id="loginPage" name="loginPage" value="1">
                                    <input type="text" onkeypress="goFunction(event)" title="Please enter you email" required="" value="" id="_name" name="_name" class="form-control" autofocus>
                      	  <span class="error-mgs">Please enter username.</span>
		      <label for="icon_prefix">Username</label>
                    
                                    <!--                                    <span class="help-block small">Your registered email</span>-->
                                </div>
                           </div>   
                                <div class="form-group"  id="passDiv">
                    <div class="input-field col s6">
                     <i class="material-icons prefix">lock_outline</i>
                  
                                    <input type="password" onkeypress="loginFunction(event)" title="Please enter your password"  required="" value="" id="_passwd" name="_passwd" class="form-control" >
                                    <span class="help-block small"></span>
                     <span class="error-mgs">Please enter password.</span>
                          <label for="icon_prefix">Password</label>
			     
                    </div>
                                
                  </div>  
                                <a  id="goButton" href="#goButton" class="btn btn-success ladda-button btn-block" data-style="zoom-in" style="display: none" onclick="enableLogin()">Go</a>
                                <button class="btn-lg btn-default w100 xs-mg-b40" data-style="zoom-in" onclick="userLogin('html')" id="loginButton" href="#loginButton" style="">Sign In</button>		  

                             </form>

</div>
                    </div>
                </div>
                    </div>
                <!-- Login Form strats here -->


                <!-- Login form ends here -->
<script type="text/javascript">
      if($(window).width() > 768){
        document.write("<script  src=\"js/jquery.mCustomScrollbar.js\"><\/script>");
        document.write("<script  src=\"js/menu-hover.js\"><\/script>");
      }
	$(window).load(function(){
     $('.loader').fadeOut();
});  
	  
	   // Login Slider JS starts here 
      $('.login-slider-item').slick({
        dots: true,
        autoplay: true,
        fade: true,
        arrows: false,
        infinite: true,
        speed: 600,
        slidesToShow: 1,
        adaptiveHeight: true
      });// Login Slider JS ends here
	  
	  
	 // When the document is ready
    $(document).ready(function () {
        $(window).load(function () {
            $('#UserPassword').val('');
            $('#captchaval').val('');
        });
		
		
		$("#LoginFrom").on("submit",function(){			
			var error = 0;
			if($.trim($("#username").val()) == ""){
				error =1;
				$("#username").parents("div.form-group").addClass("has-error");
			}else{
				$("#username").parents("div.form-group").removeClass("has-error");
			}
			
			if($.trim($("#password").val()) == ""){
				error =1;
				$("#password").parents("div.form-group").addClass("has-error");
			}else{
				$("#password").parents("div.form-group").removeClass("has-error");
			}			

			if(error == 1){
				return false;
			}else{
				return true;				
			}
			
		});
		
        /*** Remove Class on Keyup ****/
		$("#username").on("keyup",function(){			
			if($.trim($(this).val())  == ""){
				$("#username").parents("div.form-group").addClass("has-error");
			}else{
				$("#username").parents("div.form-group").removeClass("has-error");
			}
		});
		
		$("#password").on("keyup",function(){			
			if($.trim($(this).val())  == ""){
				$("#password").parents("div.form-group").addClass("has-error");
			}else{
				$("#password").parents("div.form-group").removeClass("has-error");
			}
		});
        
    });
</script>	
	<footer class="container-fluid">
	  <div >
		<div class="container">
		  <div >
		  <div class="col-sm-8 fs-13 dlmc">
			Need Help?
			<ul class="need-help">
			  <li><span class="icon-headPhone"></span>+91-8826988001</li>
			  <li><span class="icon-mail"></span>communication@authbridge.com</li>
			</ul>
		  </div>
		  <div class="col-sm-4 fs-13 c6 drmc mobile-mt-20">
						
		Powered by: 	<a href="http://www.bluebricks.com/"><img src alt=""/>Blue-Bricks</a>
		  </div>
		  </div>
		</div>
	  </div>
	</footer>
<!-- footer ends here -->				
                <!-- footer ends here -->				
            </div><!-- Login wrap ends here -->
        </div><!-- main 

        <!-- Vendor scripts -->
        <script src="vendor/jquery/dist/jquery.min.js"></script>
        <script src="vendor/jquery-ui/jquery-ui.min.js"></script>
        <script src="vendor/slimScroll/jquery.slimscroll.min.js"></script>
        <script src="vendor/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="vendor/metisMenu/dist/metisMenu.min.js"></script>
        <script src="vendor/iCheck/icheck.min.js"></script>
        <script src="vendor/sparkline/index.js"></script>
        <script src="vendor/ladda/dist/spin.min.js"></script>
        <script src="vendor/ladda/dist/ladda.min.js"></script>
        <script src="vendor/ladda/dist/ladda.jquery.min.js"></script>
        <script src="vendor/jquery-validation/jquery.validate.min.js"></script>
        <script src="vendor/toastr/build/toastr.min.js"></script>
        <script src="https://apis.google.com/js/platform.js?onload=onLoadGmail" async defer></script>
        <!--        <script src="//platform.linkedin.com/in.js" type="text/javascript"> lang: en_US</script>-->
        <!-- App scripts -->
        <script src="scripts/homer.js"></script>
        <script src="scripts/socialMedia.js"></script>

        <script>
                                    setLoginFocus();
                                    onLoadfacebook();
                                    $('body').on('keydown', '#_name', function (e) {
                                        if (e.which == 9) {
                                            e.preventDefault();
//                                        document.getElementById("goButton").focus();
//                                        document.getElementById('goButton').href = "#/  ";
                                            document.getElementById("_passwd").focus();
                                        }
                                    });
                                    //server linkedIn key
                                    //var linkedInKey = '8137zz6ug56k1w';

                                    // localhost linkedIn key
                                    var linkedInKey = '81qhnzg756okk4';

                                    initializeToastr();


        </script>

        <script type="text/javascript" src="//platform.linkedin.com/in.js">
                                    api_key:81qhnzg756okk4
                                    onLoad:onLinkedInLoad
                                    authorize:true
        </script>
        <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-107416064-2"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-107416064-2');
</script>
    </body>
</html>


