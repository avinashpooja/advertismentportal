/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



// document ready function ends here
$(document).ready(function(){
  // Mobile menu JS starts here 
  $('.navbar-fostrap').click(function(){
    $('.nav-fostrap').toggleClass('visible');
    $('body').toggleClass('cover-bg');
  });

  $('.menu-overlay').click(function(){
    $('.nav-fostrap').toggleClass('visible');
    $('body').toggleClass('cover-bg');
  });//Mobile menu JS ends here 


// progress JS starts here
  $('.progress .progress-bar').css("width",
        function() {
            return $(this).attr("aria-valuenow") + "%";
        }
  )//progress JS starts here


  //Input foucus JS starts here 
  $('.foucus-input').focus(function() {
    $(this).parent().addClass('foucus');
  });

  $('.foucus-input').blur(function() {
      $(this).parent().removeClass('foucus');
  });//Input foucus JS ends here

  //Date JS starts here
  var monthNames = [ "January", "February", "March", "April", "May", "June",
      "July", "August", "September", "October", "November", "December" ];
  var dayNames= ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"]

  var newDate = new Date();
  newDate.setDate(newDate.getDate());    
  $('#Date').html(dayNames[newDate.getDay()] + " , " + newDate.getDate() + ' ' + monthNames[newDate.getMonth()] + ' ' + newDate.getFullYear());
  //DateJS starts here


   $(function(){
      $('[rel="popover"]').popover({
          html: true,
          container: 'body',
          trigger: 'click',
          content: function () {
              var clone = $($(this).data('popover-content')).clone(true).removeClass('hide');
              return clone;
          }
      }).click(function(e) {
          e.preventDefault();
      });
  });




});// document ready function ends here


