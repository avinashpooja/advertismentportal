<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.AccessPointManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.Accesspoint"%>
<%@page import="com.mollatech.service.nucleus.crypto.LoadSettings"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PartnerManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.operation.SERVICEGUARDOperator"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.OperatorsManagement"%>
<%--<%@include file="header.jsp"%>--%>
<script src="scripts/ticketmanagement.js" type="text/javascript"></script>
<!-- Main Wrapper -->
<%
    String mailId = LoadSettings.g_sSettings.getProperty("support.email.id");
%>
<!--<div id="wrapper">-->

<div class="small-header transition animated fadeIn" id="helpDeskHeader">
    <div class="hpanel">
        <div class="panel-body">
            <h2 class="font-light m-b-xs">
                Compose Ticket
            </h2>
            <!--<small>Short description to developer what is this page about</small>-->
        </div>
    </div>
</div>

<div class="content animate-panel">
    <div class="row tour-HelpDesk">
        <div class="col-md-12">
            <div class="hpanel email-compose">
                <div class="panel-heading hbuilt">
                    <div class="p-xs h4">
                        New message
                    </div>
                </div>
                <div class="panel-heading hbuilt">
                    <div class="p-xs">
                        <form  class="form-horizontal" method="POST" id="ticketComposeRT">
                            <div class="form-group"><label class="col-sm-2 control-label text-left">To:</label>
                                <div class="col-sm-10"><input type="text" class="form-control input-sm" value="<%=mailId%>" disabled></div>
                            </div>
                            <div class="form-group"><label class="col-sm-2 control-label text-left">Contact:</label>
                                <div class="col-sm-10"><input type="text" id="contact_no"  class="form-control number-only input-sm" placeholder="Enter your contact number"></div>
                            </div>
                            <div class="form-group"><label class="col-sm-2 control-label text-left">Subject:</label>
                                <div class="col-sm-10">
                                    <select id="subjectBody" class="js-source-states" style="width: 100%">
                                        <optgroup label="General">
                                            <option value="General Questions">General Questions</option>
                                        </optgroup>
                                        <optgroup label="API">
                                            <%
                                                Accesspoint[] as = new AccessPointManagement().getAceesspoints();
                                                if (as != null) {
                                                    for (Accesspoint accesspoint : as) {%>
                                            <option value="<%=accesspoint.getName()%>"><%=accesspoint.getName()%></option>
                                            <%}
                                                }
                                            %>
                                        </optgroup>
                                        <optgroup label="Fault">
                                            <option value="Access_Token_Issue">Access Token Issue</option>
                                            <option value="password">Can't Change Password</option>
                                            <option value="others">Others</option>
                                        </optgroup>
                                    </select>
                                </div>
                            </div>
                        </form>

                        <div class="panel-body">
                            <div><textarea class="summernote" id="msgBody"></textarea></div>                                    
                        </div>
                        <br><br>
                        <div style="background: #fff; height: 200px">
                            <div class="col-sm-12">
                                <form action="UploadEmailAttachment" method="post" class="dropzone" id="my-dropzone" name="my-dropzone"></form>	
                                <small>Upload attachment. Support Zip File size limit 5mb</small>
                            </div>
                        </div>
                        <br/><br/>
                        <div class="panel-footer">

                            <a id="submitMail" class="btn btn-primary ladda-button" data-style="zoom-in" onclick="sendEmailToRT()">Send email</a>
                            <a class="btn btn-danger" tabindex="2" onclick="helpDesk()"><i class="fa fa-close"></i> Clear</a>
                        </div>                            
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
//    var placeHold = "Hello Jonathan!"+
//    "dummy text of the printing and typesetting industry.dummy text of the printing and typesetting industry."+"\n"+
//    "Lorem Ipsum has been the dustrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more\n\
//All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable.";
    $(function () {
        var elem = $(".summernote").summernote({
//            placeholder: "Hello Jonathan!\n\
//dummy text of the printing and typesetting industry.dummy text of the printing and typesetting industry.Lorem Ipsum has been the dustrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more\n\
//All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable.",
            callbacks: {
                onInit: function () {
                    var editor = elem.next(),
                            placeholder = editor.find(".note-placeholder");

                    function isEditorEmpty() {
                        var code = elem.summernote("code");
                        return code === "<p><br></p>" || code === "";
                    }

                    editor.on("focusin focusout", ".note-editable", function (e) {
                        if (isEditorEmpty()) {
                            placeholder[e.type === "focusout" ? "show" : "hide"]();
                        }
                    });
                }
            }
        });
        // Initialize summernote plugin
//        $('.summernote').summernote({
//             placeholder: "Enter Text Here...",
//            toolbar: [
//                ['headline', ['style']],
//                ['style', ['bold', 'italic', 'underline', 'superscript', 'subscript', 'strikethrough', 'clear']],
//                ['textsize', ['fontsize']],
//                ['alignment', ['ul', 'ol', 'paragraph', 'lineheight']],
//            ]
//        });
//

        $('.number-only').keyup(function (e) {
            if (this.value != '-')
                while (isNaN(this.value))
                    this.value = this.value.split('').reverse().join('').replace(/[\D]/i, '')
                            .split('').reverse().join('');
        })
                .on("cut copy paste", function (e) {
                    e.preventDefault();
                });
    });


    $(document).ready(function () {
        Dropzone.autoDiscover = false;
        var myDropzone = new Dropzone("#my-dropzone", {
            url: "UploadZip",
            uploadMultiple: true,
            maxFilesize: 5,
            maxFiles: 1,
            acceptedFiles: "application/zip",
            dictInvalidFileType: "You can't upload files of this type, only Zip file",
            autoProcessQueue: true,
            parallelUploads: 1,
            addRemoveLinks: true,
//            success:function(file){
//                file_up_names.push(file.name);
//             }
            
            removedfile: function(file) {
//                x = confirm('Do you want to delete?');
//                if(!x)  return false;
                
//
//                    
//
//                    //$.post('delete_file.php',file_name:file_up_names[i]},function(data,status){
//                       
//                    //});
                removeFromSession(file.name);    
                var _ref;
                if (file.previewElement) {
                    if ((_ref = file.previewElement) != null) {
                        _ref.parentNode.removeChild(file.previewElement);
                    }
                }
                return this._updateMaxFilesReachedClass();
//                swal({
//                    title: "Are you sure?",
//                    text: "",
//                    type: "warning",
//                    showCancelButton: true,
//                    confirmButtonColor: "#DD6B55",
//                    confirmButtonText: "Yes",
//                    cancelButtonText: "No",
//                    closeOnConfirm: true,
//                    closeOnCancel: true},
//                    function (isConfirm) {
//                        if (isConfirm) {
//                            
//                            var _ref;
//                            if (file.previewElement) {
//                              if ((_ref = file.previewElement) != null) {
//                                _ref.parentNode.removeChild(file.previewElement);
//                              }
//                            }
//                            return this._updateMaxFilesReachedClass();
//                        } else {
//                               return false;
//                        }
//                    });    
        
            
            }
        });
    });

//function removeFromSession(){
//    alert("Removed");
//}
//function confirmRemoveFile() {
//    swal({
//        title: "Are you sure?",
//        text: "",
//        type: "warning",
//        showCancelButton: true,
//        confirmButtonColor: "#DD6B55",
//        confirmButtonText: "Yes",
//        cancelButtonText: "No",
//        closeOnConfirm: true,
//        closeOnCancel: true},
//            function (isConfirm) {
//                if (isConfirm) {
//                    removeFromSession();
//                } else {
//                   return false;
//                }
//            });
//}
</script>

<!-- Footer-->


