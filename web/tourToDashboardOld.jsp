
<!DOCTYPE html>

<html>
    <%response.addHeader("X-FRAME-OPTIONS", "DENY");%>    
    <head>
        <!--
                <meta charset="utf-8">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
        
                 Page title 
                <title>Ready API</title>
        
                 Place favicon.ico and apple-touch-icon.png in the root directory 
                <link rel="shortcut icon" type="images/front-logo.png" href="images/front-logo.png" />
        
                 Vendor styles 
                <link rel="stylesheet" href="vendor/fontawesome/css/font-awesome.css" />
                <link rel="stylesheet" href="vendor/metisMenu/dist/metisMenu.css" />
                <link rel="stylesheet" href="vendor/animate.css/animate.css" />
                <link rel="stylesheet" href="vendor/bootstrap/dist/css/bootstrap.css" />
                <link rel="stylesheet" href="vendor/bootstrap-star-rating/css/star-rating.css" />
                <link rel="stylesheet" href="vendor/datatables.net-bs/css/dataTables.bootstrap.min.css" />
                <link rel="stylesheet" href="vendor/select2-3.5.2/select2.css" />
                <link rel="stylesheet" href="vendor/select2-bootstrap/select2-bootstrap.css" />
                <link rel="stylesheet" href="vendor/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css" />
                <link rel="stylesheet" href="vendor/bootstrap-datepicker-master/dist/css/bootstrap-datepicker3.min.css" />
                <link rel="stylesheet" href="vendor/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" />
                <link rel="stylesheet" href="vendor/clockpicker/dist/bootstrap-clockpicker.min.css" />
                <link rel="stylesheet" href="vendor/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" />
                <link rel="stylesheet" href="vendor/summernote/dist/summernote.css" />
                <link rel="stylesheet" href="vendor/summernote/dist/summernote-bs3.css" />
                <link rel="stylesheet" href="vendor/jquery-ui/themes/base/all.css" />
                <link rel="stylesheet" href="vendor/dropzone/dropzone.css">
                <link rel="stylesheet" href="vendor/sweetalert/lib/sweet-alert.css" />
                <link rel="stylesheet" href="vendor/ladda/dist/ladda-themeless.min.css" />
                <link rel="stylesheet" href="vendor/toastr/build/toastr.min.css" />
                <link rel="stylesheet" href="vendor/codemirror/style/codemirror.css" />
                <link rel="stylesheet" href="vendor/fooTable/css/footable.core.min.css" />
                         <link rel="stylesheet" href="vendor/c3/c3.min.css" />
                 App styles 
                <link rel="stylesheet" href="fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css" />
                <link rel="stylesheet" href="fonts/pe-icon-7-stroke/css/helper.css" />
                <link rel="stylesheet" href="styles/static_custom.css">
                <link rel="stylesheet" href="styles/style.css">
                <link rel="stylesheet" href="vendor/bootstrap-tour/build/css/bootstrap-tour.min.css" />
        
                 Need call first 
                <script src="scripts/header.js" type="text/javascript"></script>
                <script src="vendor/jquery/dist/jquery.min.js"></script>
                <script src="vendor/jquery-ui/jquery-ui.min.js"></script>
                <script src="vendor/bootstrap/dist/js/bootstrap.min.js"></script>
                <script src="vendor/summernote/dist/summernote.min.js"></script>
                <script src="vendor/codemirror/script/codemirror.js"></script>
                <script src="vendor/codemirror/javascript.js"></script> 
                <link rel="stylesheet" href="vendor/c3/c3.min.css" />
                <script src="vendor/d3/d3.min.js"></script>
                <script src="vendor/c3/c3.min.js"></script>
                <link rel="stylesheet" href="vendor/chartist/custom/chartist.css" />
                <script src="vendor/chartist/dist/chartist.min.js"></script>
                <script src="scripts/homepage.js" type="text/javascript"></script>
                <script src="vendor/bootstrap-tour/build/js/bootstrap-tour.min.js"></script>
                <script src="vendor/jquery-flot/jquery.flot.pie.js" type="text/javascript"></script>
                <script src="scripts/tourToDashboard.js" type="text/javascript"></script>
                
                 Vendor scripts 
                <script src="scripts/utilityFunction.js" type="text/javascript"></script>
                <script src="vendor/slimScroll/jquery.slimscroll.min.js"></script>
                <script src="vendor/metisMenu/dist/metisMenu.min.js"></script>
                <script src="vendor/iCheck/icheck.min.js"></script>
                <script src="vendor/moment/moment.js"></script>
                <script src="vendor/sparkline/index.js"></script>
                <script src="vendor/chartjs/Chart.min.js"></script>
                <script src="vendor/jquery-flot/jquery.flot.js"></script>
                <script src="vendor/jquery-flot/jquery.flot.resize.js"></script>
                <script src="vendor/jquery-flot/jquery.flot.pie.js"></script>
                <script src="vendor/jquery.flot.spline/index.js"></script>
                <script src="vendor/flot.curvedlines/curvedLines.js"></script>
                <script src="vendor/peity/jquery.peity.min.js"></script>
                <script src="vendor/bootstrap-star-rating/js/star-rating.min.js"></script>
                <script src="vendor/codemirror/script/codemirror.js"></script>
                <script src="vendor/codemirror/javascript.js"></script>
                <script src="vendor/xeditable/bootstrap3-editable/js/bootstrap-editable.min.js"></script>
                <script src="vendor/select2-3.5.2/select2.min.js"></script>
                <script src="vendor/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js"></script>
                <script src="vendor/bootstrap-datepicker-master/dist/js/bootstrap-datepicker.min.js"></script>
                <script src="vendor/clockpicker/dist/bootstrap-clockpicker.min.js"></script>
                <script src="vendor/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
                <script src="vendor/dropzone/dropzone.js"></script>
        
                <script src="vendor/ladda/dist/spin.min.js"></script>
                <script src="vendor/ladda/dist/ladda.min.js"></script>
                <script src="vendor/ladda/dist/ladda.jquery.min.js"></script>
                <script src="vendor/jquery-ui/jquery-ui.min.js"></script>
                <script src="vendor/jquery-validation/jquery.validate.min.js"></script>
                <script src="vendor/sweetalert/lib/sweet-alert.min.js"></script>
                <script src="vendor/toastr/build/toastr.min.js"></script>
                 DataTables 
                <script src="vendor/datatables/media/js/jquery.dataTables.min.js"></script>
                <script src="vendor/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
        
                 DataTables buttons scripts 
                <script src="vendor/pdfmake/build/pdfmake.min.js"></script>
                <script src="vendor/pdfmake/build/vfs_fonts.js"></script>
                <script src="vendor/datatables.net-buttons/js/buttons.html5.min.js"></script>
                <script src="vendor/datatables.net-buttons/js/buttons.print.min.js"></script>
                <script src="vendor/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
                <script src="vendor/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
                <script src="vendor/fooTable/dist/footable.all.min.js"></script>
        
                 App scripts 
                <script src="scripts/homer.js"></script>
                <script src="scripts/utilityFunction.js" type="text/javascript"></script>
                <script src="scripts/charts.js"></script>-->

    </head>

    <body class="fixed-navbar">
        <style>
            span.capitalize {
                text-transform: capitalize;
            }
        </style>        
        <!-- Simple splash screen-->

        <!--[if lt IE 7]>
        <p class="alert alert-danger">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->               
        <div id="wrapper1">
            <div id="wrapper" style="min-height:0%!important">
                <div id="homePage">
                    <div class="content animate-panel">
                        <div class="row">
                            <div class="col-lg-3 tour-8">
                                <div class="hpanel stats">
                                    <div class="panel-heading">
                                        
                                        Today's total credit used
                                    </div>
                                    <div class="panel-body h-200">
                                        <div class="stats-icon text-center ">
                                            <i class="pe-7s-share fa-4x"></i>
                                        </div>
                                        <div class="m-t-xl">
                                            <div class="progress m-t-xs full progress-small">
                                                <div id="percentage" aria-valuemax="100" style="width: 10%" aria-valuemin="0" aria-valuenow="0" role="progressbar" class=" progress-bar progress-bar-success">
                                                    <span class="sr-only">100%</span>
                                                </div>
                                            </div> 
                                            <div class="row" style="height: 60px">
                                                <div class="col-xs-6">
                                                    <small class="stats-label">Credit used</small>
                                                    <h4>50</h4>
                                                </div>
                                                <div class="col-xs-6">
                                                    <small class="stats-label">% Credit used</small>
                                                    <h4 class="text-center">10%</h4>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 tour-9">
                                <div class="hpanel">
                                    <div class="panel-heading">
                                        
                                        Last 7 days total credit used
                                    </div>
                                    <div class="panel-body text-center h-200">
                                        <i class="pe-7s-graph1 fa-4x"></i>
                                        <h1 class="m-b-xs text-success" id="lastWeekAmount">300</h1>
                                        <small id="weekMsg">You have used total 300 credits in last week for all services</small>
                                        <h3 class="font-bold no-margins lead text-success row">
                                            Weekly expense
                                        </h3> <!--<small>Lorem ipsum dolor sit amet, consectetur adipiscing elit</small>-->
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-3 tour-10">
                                <div class="hpanel">
                                    <div class="panel-heading">
                                        
                                        Last month total credit used
                                    </div>
                                    <div class="panel-body text-center h-200">
                                        <i class="pe-7s-global fa-4x"></i>
                                        <h1 class="m-b-xs text-success" id="monthCredit">1000</h1>

                                        <small id="monthMsg">You have used total 1000 credits in last week for all services</small>
                                        <h3 class="font-bold no-margins lead text-success row">
                                            Monthly usage
                                        </h3>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 tour-12" id="topmostServices" data-child="hpanel" data-effect="fadeInLeft">
                                <div class="hpanel">
                                    <div class="panel-heading">
                                        
                                        <span id="mostlyFiveTour"></span>
                                    </div>
                                    <div class="panel-body" style="height: 213px">
                                        <div class="flot-chart text-center" style="width: 200;height:250">
                                            <div class="flot-chart-content"  id="flot-pie-chartTour"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3" id = "mostlyservices"  style="display: none">
                                <div class="hpanel">
                                    <div class="panel-heading">
                                        
                                        Your mostly used services
                                    </div>
                                    <div class="panel-body">                                        
                                        <img src="images/no_record_found.jpg" alt="No record found" width="200" height="170"/>
                                        <br>                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row tour-11">
                            <div class="col-lg-12 ">
                                <div class="hpanel ">
                                    <div class="panel-heading">
                                        
                                        Services Information And Statistics
                                    </div>
                                    <div class="panel-body" >
                                        <div class="text-left">
                                            <i class="fa fa-bar-chart-o fa-fw"></i><span id="homeReportLable">  Last 7 day expenditure</span>
                                        </div>
                                        <div class="btn-group" style="padding-left: 60%">
                                            <button  class="btn btn-default btn-xs ladda-button" data-style="zoom-in">Top five service</button>
                                            <button  class="btn btn-default btn-xs ladda-button" data-style="zoom-in" >Today total expenditure</button>
                                            <button  class="btn btn-default btn-xs ladda-button" data-style="zoom-in">Todays trending services</button>
                                        </div>
                                        <div  id="topFiveSerTour"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>                
            </div>
        </div>
        <div id="wrapper" style="min-height:0%!important">
            <div class="content animate-panel">
                <div class="row">
                    <div class="col-lg-6 tour-21">				
                        <div class="hpanel">		
                            <div class="panel-heading">
                                <h3>Your request</h3>
                            </div>				
                            <div class="panel-body">					                                        
                                <form  role="form" class="form-horizontal" id="getBody" name="getBody">
                                    <div class="tour-23">
                                        <h3>Header</h3>
                                        <table id="_headerTable" CELLSPACING="15" >                                                
                                            <tr>                                                    
                                                <td width="35%">
                                                    <input type="text" class="form-control" id="_myNameRM" name="_myNameRM" value="APITokenId">                                                        
                                                </td>
                                                <td width="90%">
                                                    <div style='padding: 1%'>
                                                        <input type="text" class="form-control" id="_myResType" name="_myResType" value="vEBDceyF4TRRkZq1qBo+NaYRgV4=">                                                        
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="35%">
                                                    <input type="text" class="form-control" id="_myNameRM" name="_myNameRM" value="PartnerId">                                                        
                                                </td>
                                                <td width="90%">
                                                    <div style='padding: 1%'>
                                                        <input type="text" class="form-control" id="_myResType" name="_myResType" value="XteIg8dYAYwRiHGz0A300Lwog+w=">                                                        
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="35%">
                                                    <input type="text" class="form-control" id="_myNameRM" name="_myNameRM" value="PartnerTokenId">                                                        
                                                </td>
                                                <td width="90%">
                                                    <div style='padding: 1%'>
                                                        <input type="text" class="form-control" id="_myResType" name="_myResType" value="wkV52UiG64dweuOFX+5UoJXNfG4=">                                                        
                                                    </div>
                                                </td>
                                                <td >                                                        
                                                    <a class='btn btn-info btn-sm'  onclick="addHeader()" id="addUserButton"><span class='fa fa-plus-circle'></span></a>
                                                </td>  
                                                <td style="padding: 1%">
                                                    <a class='btn btn-warning btn-sm' onclick="removeHeader()" id="minusUserButton"><span class='fa fa-minus-circle'></span></a>
                                                </td>                                              
                                            </tr>                                                   
                                        </table>
                                    </div>
                                    <div class="hr-line-dashed"></div>
                                    <div style="display: none">
                                        <h3>Basic Authentication</h3>
                                        <div class="form-group">
                                            <div class="col-sm-6"><input type="text" id="_userNameAuth" name="_userNameAuth"  class="form-control" placeholder="Username"></div>
                                            <div class="col-sm-6"><input type="text" id="_passwordAuth" name="_passwordAuth" class="form-control" placeholder="Password"></div>
                                        </div>
                                        <div class="hr-line-dashed"></div>
                                    </div>
                                    <div class="tour-24">
                                        <h3>Body</h3>
                                        <textarea class="form-control" id="_APIbody" name="_APIbody" style="min-height: 500px;">
    <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:web="http://webservice.wsdl.wadl.sg.mollatech.com/">
        <soapenv:Header/>
        <soapenv:Body>
            <web:createChatRoom>
                <!--Optional:-->
                <developerEmail>robot@mollatech.com</developerEmail>
                <!--Optional:-->
                <numberOfUsers>2</numberOfUsers>
                <!--Optional:-->
                <groupName>Buzz</groupName>
                <!--Zero or more repetitions:-->
                <userEmailIds>jerry@noemail.com</userEmailIds>
                <userEmailIds>tom@noemail.com</userEmailIds>
                <!--Optional:-->
                <urlexpiryTimeInMinutes>15</urlexpiryTimeInMinutes>
            </web:createChatRoom>
        </soapenv:Body>
    </soapenv:Envelope>
                                        </textarea>
                                    </div>
                                    <div class="hr-line-dashed"></div>
                                    <button class="btn btn-primary ladda-button" data-style="zoom-in" id="apiConsoleRequest" type="button"><i class="fa fa-terminal"></i> Submit</button>                                    
                                </form>								
                            </div>					
                        </div>				
                    </div>
                    <div class="col-lg-6 ">
                        <div class="hpanel tour-22">
                            <div class="panel-heading">
                                <h3>Your response</h3>
                            </div>
                            <div class="panel-body">
                                <form>
                                    <textarea style="min-height: 220px;" class="form-control" id="responseID" name="responseID">
<S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/">
    <S:Body>
        <ns2:createChatRoomResponse xmlns:ns2="http://webservice.wsdl.wadl.sg.mollatech.com/" xmlns:ns3="http://com.mollatech.sg.sgholder">
            <return>
                <chatURL>https://www.readyapis.com:9443/WebChat/index.jsp?3a4e8f81-f33d-4c81-8b04-54a162de020c</chatURL>
                <resultCode>0</resultCode>
                <result>3a4e8f81-f33d-4c81-8b04-54a162de020c Chat Room Is Created Successfully.</result>
                <password>55f0a7</password>
            </return>
        </ns2:createChatRoomResponse>
    </S:Body>
</S:Envelope>
                                    </textarea>
                                </form>
                                <br>
                                <div id="startValuesAndTargetExample">
                                    <p class="text-info"><i class="fa fa-check-square"></i> Congratulations, Your API takes  00:06 sec to get response. </p>
                                </div>
                            </div>                            
                        </div>
                    </div>
                    <div id="questionAndAnswer" class="tour-25 col-lg-6 ">
                        <div class="hpanel panel-group " id="accordion" role="tablist" aria-multiselectable="true">
                            <div class="panel-body">
                                <h4 class="m-t-none m-b-none">Need Help </h4>
                                <small class="text-muted">All general questions about our API.</small>&nbsp;&nbsp;&nbsp;
                                <a  class="btn btn-primary btn-xs ladda-button" data-style="zoom-in" type="button" href="#" onclick="apiHelpDesk()" style="margin-left: 1px"><i class="fa fa-check"></i> Raise Ticket</a> <br>
                            </div>
                            <div class="panel-body" id="_apiLevelToken" >
                                <a data-toggle="collapse" data-parent="#accordion" href="#q1" aria-expanded="true">
                                    <i class="fa fa-chevron-down pull-right text-muted"></i>
                                    Where can I get APITokenId?
                                </a>
                                <div id="q1" class="panel-collapse collapse">
                                    <hr>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal.
                                </div>
                            </div>                                                               <!--
                            -->                                <div class="panel-body" id="_headerError">
                                <a data-toggle="collapse" data-parent="#accordion" href="#q2" aria-expanded="true">
                                    <i class="fa fa-chevron-down pull-right text-muted"></i>
                                    Why need Headers and How to add?
                                </a>
                                <div id="q2" class="panel-collapse collapse">
                                    <hr>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal.
                                </div>
                            </div><!--
                            -->                             <div class="panel-body" id="connectionrefuse" style="display: none">
                                <a data-toggle="collapse" data-parent="#accordion" href="#q3" aria-expanded="true">
                                    <i class="fa fa-chevron-down pull-right text-muted"></i>
                                    Connection refused (Connection refused)?
                                </a>
                                <div id="q3" class="panel-collapse collapse">
                                    <hr>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal.
                                </div>
                            </div>                                               
                            <div class="panel-body">
                                <a data-toggle="collapse" data-parent="#accordion" href="#q5" aria-expanded="true">
                                    <i class="fa fa-chevron-down pull-right text-muted"></i>
                                    No Such Webservice is Available.
                                </a>
                                <div id="q5" class="panel-collapse collapse">
                                    <hr>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal.
                                </div>
                            </div>                                                                                                                         
                            <div class="panel-body">
                                <a data-toggle="collapse" data-parent="#accordion" href="#q12" aria-expanded="true">
                                    <i class="fa fa-chevron-down pull-right text-muted"></i>
                                    Service Guard Error: Please Contact Administrator.
                                </a>
                                <div id="q12" class="panel-collapse collapse">
                                    <hr>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal.
                                </div>                                
                            </div>                     
                        </div>
                    </div>
                </div>             
            </div>
        </div>
    </div>

    <div id="wrapper" style="min-height:0%!important">
        <div class="content">
            <div class="row">
                <div class="col-md-12 tour-13">
                    <div class="hpanel">
                        <div class="panel-body">
                            <input type="text" class="form-control input-sm m-b-md" id="filter" placeholder="Search in table">                    
                            <table id="api" class="footable table table-stripped table-responsive " data-page-size="25" data-filter=#filter>    
                                <thead>
                                    <tr>                                    
                                        <th style="text-align: center">API Name</th>
                                        <th style="text-align: center" class="tour-14">API Brief</th>                                    
                                        <th style="text-align: center" class="tour-15">Credits</th>
                                        <th style="text-align: center" class="tour-16">Performace</th>
                                        <th data-hide="phone,tablet" style="text-align: center">Credit Used As</th>                                    
                                        <th style="text-align: center" class="tour-17">Test</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>                                   
                                        <td  style="font-size: 15px">testConnection</td>                               
                                        <td style="text-align: center">                                    
                                            <a class="text-warning ladda-button" data-style="zoom-in" id="reAssignToken"  type="button" href="#" data-toggle="tooltip" data-placement="right" title="Regenerate API Token"><i class="pe pe-7s-refresh-cloud pe-3x"></i></a>                                                    
                                            <a class="text-info ladda-button" data-style="zoom-in" id="viewToken" type="button" href="#" data-toggle="tooltip" data-placement="right" title="View API Token" style="margin-left: 5%"><i class="pe pe-7s-display2 pe-3x"></i></a>
                                            <a  class="text-warning ladda-button" data-style="zoom-in" data-toggle="tooltip" id="viewApiDes" data-placement="right" title=""  style="margin-left: 5%"><i class="pe pe-7s-news-paper pe-3x"></i></a>
                                            <a href="#" class="text-danger" type="button" href="#" data-toggle="tooltip" data-placement="right" title="API Documentation" target="_blank" style="margin-left: 5%"><i class="pe pe-7s-note2 pe-3x"></i></a> 
                                        </td>                                              

                                        <td style="text-align: center">
                                            <a class="text-error ladda-button" data-style="zoom-in" id="todayCredit"  type="button" href="#" data-toggle="tooltip" data-placement="right" title="Today Credit Used for"><i class="pe pe-7s-graph2 pe-3x"></i></a>                                                    
                                            <a class="text-primary-2 ladda-button" data-style="zoom-in" id="monthlyCredit" type="button" href="#" data-toggle="tooltip" data-placement="right" title="Monthly Credit Used for" style="margin-left: 5%"><i class="pe pe-7s-display1 pe-3x"></i></a>
                                        </td>

                                        <td style="text-align: center">
                                            <a class="text-error ladda-button" data-style="zoom-in" id="todayPerformance" type="button" href="#" data-toggle="tooltip" data-placement="right" title="Tody Performance "><i class="pe pe-7s-graph2 pe-3x"></i></a>                                                    
                                            <a class="text-primary-2 ladda-button" data-style="zoom-in" id="monthlyPerformance" type="button" href="#" data-toggle="tooltip" data-placement="right" title="Monthly Performace" style="margin-left: 5%"><i class="pe pe-7s-display1 pe-3x"></i></a>
                                        </td>
                                        <td style="text-align: center">
                                            <p style="font-size: 15px">1 Credit Per API</p>
                                        </td>
                                        <td style="text-align: center" class="tour-iconAPIConsole">
                                            <a onclick = "#" class="text-success " data-toggle="tooltip" data-placement="right" title="Test" id="testFirstAPI"><i class="pe pe-7s-science pe-3x"></i></a>
                                        </td>                                                               
                                    </tr>  

                                    <tr>                                   
                                        <td  style="font-size: 15px">saveConnection</td>                               
                                        <td style="text-align: center">                                    
                                            <a class="text-warning ladda-button" data-style="zoom-in" id="reAssignToken"  type="button" href="#" data-toggle="tooltip" data-placement="right" title="Regenerate API Token"><i class="pe pe-7s-refresh-cloud pe-3x"></i></a>                                                    
                                            <a class="text-info ladda-button" data-style="zoom-in" id="viewToken" type="button" href="#" data-toggle="tooltip" data-placement="right" title="View API Token" style="margin-left: 5%"><i class="pe pe-7s-display2 pe-3x"></i></a>
                                            <a  class="text-warning ladda-button" data-style="zoom-in" data-toggle="tooltip" id="viewApiDes" data-placement="right" title=""  style="margin-left: 5%"><i class="pe pe-7s-news-paper pe-3x"></i></a>
                                            <a href="#" class="text-danger" type="button" href="#" data-toggle="tooltip" data-placement="right" title="API Documentation" target="_blank" style="margin-left: 5%"><i class="pe pe-7s-note2 pe-3x"></i></a> 
                                        </td>                                              

                                        <td style="text-align: center">
                                            <a class="text-error ladda-button" data-style="zoom-in" id="todayCredit"  type="button" href="#" data-toggle="tooltip" data-placement="right" title="Today Credit Used for"><i class="pe pe-7s-graph2 pe-3x"></i></a>                                                    
                                            <a class="text-primary-2 ladda-button" data-style="zoom-in" id="monthlyCredit" type="button" href="#" data-toggle="tooltip" data-placement="right" title="Monthly Credit Used for" style="margin-left: 5%"><i class="pe pe-7s-display1 pe-3x"></i></a>
                                        </td>

                                        <td style="text-align: center">
                                            <a class="text-error ladda-button" data-style="zoom-in" id="todayPerformance" type="button" href="#" data-toggle="tooltip" data-placement="right" title="Tody Performance "><i class="pe pe-7s-graph2 pe-3x"></i></a>                                                    
                                            <a class="text-primary-2 ladda-button" data-style="zoom-in" id="monthlyPerformance" type="button" href="#" data-toggle="tooltip" data-placement="right" title="Monthly Performace" style="margin-left: 5%"><i class="pe pe-7s-display1 pe-3x"></i></a>
                                        </td>
                                        <td style="text-align: center">
                                            <p style="font-size: 15px">1 Credit Per API</p>
                                        </td>
                                        <td style="text-align: center">
                                            <a onclick = "#" class="text-success" data-toggle="tooltip" data-placement="right" title="Test" id="testFirstAPI"><i class="pe pe-7s-science pe-3x"></i></a>
                                        </td>                                                               
                                    </tr>        

                                    <tr>                                   
                                        <td  style="font-size: 15px">getConnectionId</td>                               
                                        <td style="text-align: center">                                    
                                            <a class="text-warning ladda-button" data-style="zoom-in" id="reAssignToken"  type="button" href="#" data-toggle="tooltip" data-placement="right" title="Regenerate API Token"><i class="pe pe-7s-refresh-cloud pe-3x"></i></a>                                                    
                                            <a class="text-info ladda-button" data-style="zoom-in" id="viewToken" type="button" href="#" data-toggle="tooltip" data-placement="right" title="View API Token" style="margin-left: 5%"><i class="pe pe-7s-display2 pe-3x"></i></a>
                                            <a  class="text-warning ladda-button" data-style="zoom-in" data-toggle="tooltip" id="viewApiDes" data-placement="right" title=""  style="margin-left: 5%"><i class="pe pe-7s-news-paper pe-3x"></i></a>
                                            <a href="#" class="text-danger" type="button" href="#" data-toggle="tooltip" data-placement="right" title="API Documentation" target="_blank" style="margin-left: 5%"><i class="pe pe-7s-note2 pe-3x"></i></a> 
                                        </td>                                              

                                        <td style="text-align: center">
                                            <a class="text-error ladda-button" data-style="zoom-in" id="todayCredit"  type="button" href="#" data-toggle="tooltip" data-placement="right" title="Today Credit Used for"><i class="pe pe-7s-graph2 pe-3x"></i></a>                                                    
                                            <a class="text-primary-2 ladda-button" data-style="zoom-in" id="monthlyCredit" type="button" href="#" data-toggle="tooltip" data-placement="right" title="Monthly Credit Used for" style="margin-left: 5%"><i class="pe pe-7s-display1 pe-3x"></i></a>
                                        </td>

                                        <td style="text-align: center">
                                            <a class="text-error ladda-button" data-style="zoom-in" id="todayPerformance" type="button" href="#" data-toggle="tooltip" data-placement="right" title="Tody Performance "><i class="pe pe-7s-graph2 pe-3x"></i></a>                                                    
                                            <a class="text-primary-2 ladda-button" data-style="zoom-in" id="monthlyPerformance" type="button" href="#" data-toggle="tooltip" data-placement="right" title="Monthly Performace" style="margin-left: 5%"><i class="pe pe-7s-display1 pe-3x"></i></a>
                                        </td>
                                        <td style="text-align: center">
                                            <p style="font-size: 15px">1 Credit Per API</p>
                                        </td>
                                        <td style="text-align: center">
                                            <a onclick = "#" class="text-success" data-toggle="tooltip" data-placement="right" title="Test" id="testFirstAPI"><i class="pe pe-7s-science pe-3x"></i></a>
                                        </td>                                                               
                                    </tr>        

                                    <tr>                                   
                                        <td  style="font-size: 15px">getSQLId</td>                               
                                        <td style="text-align: center">                                    
                                            <a class="text-warning ladda-button" data-style="zoom-in" id="reAssignToken"  type="button" href="#" data-toggle="tooltip" data-placement="right" title="Regenerate API Token"><i class="pe pe-7s-refresh-cloud pe-3x"></i></a>                                                    
                                            <a class="text-info ladda-button" data-style="zoom-in" id="viewToken" type="button" href="#" data-toggle="tooltip" data-placement="right" title="View API Token" style="margin-left: 5%"><i class="pe pe-7s-display2 pe-3x"></i></a>
                                            <a  class="text-warning ladda-button" data-style="zoom-in" data-toggle="tooltip" id="viewApiDes" data-placement="right" title=""  style="margin-left: 5%"><i class="pe pe-7s-news-paper pe-3x"></i></a>
                                            <a href="#" class="text-danger" type="button" href="#" data-toggle="tooltip" data-placement="right" title="API Documentation" target="_blank" style="margin-left: 5%"><i class="pe pe-7s-note2 pe-3x"></i></a> 
                                        </td>                                              

                                        <td style="text-align: center">
                                            <a class="text-error ladda-button" data-style="zoom-in" id="todayCredit"  type="button" href="#" data-toggle="tooltip" data-placement="right" title="Today Credit Used for"><i class="pe pe-7s-graph2 pe-3x"></i></a>                                                    
                                            <a class="text-primary-2 ladda-button" data-style="zoom-in" id="monthlyCredit" type="button" href="#" data-toggle="tooltip" data-placement="right" title="Monthly Credit Used for" style="margin-left: 5%"><i class="pe pe-7s-display1 pe-3x"></i></a>
                                        </td>

                                        <td style="text-align: center">
                                            <a class="text-error ladda-button" data-style="zoom-in" id="todayPerformance" type="button" href="#" data-toggle="tooltip" data-placement="right" title="Tody Performance "><i class="pe pe-7s-graph2 pe-3x"></i></a>                                                    
                                            <a class="text-primary-2 ladda-button" data-style="zoom-in" id="monthlyPerformance" type="button" href="#" data-toggle="tooltip" data-placement="right" title="Monthly Performace" style="margin-left: 5%"><i class="pe pe-7s-display1 pe-3x"></i></a>
                                        </td>
                                        <td style="text-align: center">
                                            <p style="font-size: 15px">1 Credit Per API</p>
                                        </td>
                                        <td style="text-align: center">
                                            <a onclick = "#" class="text-success" data-toggle="tooltip" data-placement="right" title="Test" id="testFirstAPI"><i class="pe pe-7s-science pe-3x"></i></a>
                                        </td>                                                               
                                    </tr>        

                                    <tr>                                   
                                        <td  style="font-size: 15px">getSQL</td>                               
                                        <td style="text-align: center">                                    
                                            <a class="text-warning ladda-button" data-style="zoom-in" id="reAssignToken"  type="button" href="#" data-toggle="tooltip" data-placement="right" title="Regenerate API Token"><i class="pe pe-7s-refresh-cloud pe-3x"></i></a>                                                    
                                            <a class="text-info ladda-button" data-style="zoom-in" id="viewToken" type="button" href="#" data-toggle="tooltip" data-placement="right" title="View API Token" style="margin-left: 5%"><i class="pe pe-7s-display2 pe-3x"></i></a>
                                            <a  class="text-warning ladda-button" data-style="zoom-in" data-toggle="tooltip" id="viewApiDes" data-placement="right" title=""  style="margin-left: 5%"><i class="pe pe-7s-news-paper pe-3x"></i></a>
                                            <a href="#" class="text-danger" type="button" href="#" data-toggle="tooltip" data-placement="right" title="API Documentation" target="_blank" style="margin-left: 5%"><i class="pe pe-7s-note2 pe-3x"></i></a> 
                                        </td>                                              

                                        <td style="text-align: center">
                                            <a class="text-error ladda-button" data-style="zoom-in" id="todayCredit"  type="button" href="#" data-toggle="tooltip" data-placement="right" title="Today Credit Used for"><i class="pe pe-7s-graph2 pe-3x"></i></a>                                                    
                                            <a class="text-primary-2 ladda-button" data-style="zoom-in" id="monthlyCredit" type="button" href="#" data-toggle="tooltip" data-placement="right" title="Monthly Credit Used for" style="margin-left: 5%"><i class="pe pe-7s-display1 pe-3x"></i></a>
                                        </td>

                                        <td style="text-align: center">
                                            <a class="text-error ladda-button" data-style="zoom-in" id="todayPerformance" type="button" href="#" data-toggle="tooltip" data-placement="right" title="Tody Performance "><i class="pe pe-7s-graph2 pe-3x"></i></a>                                                    
                                            <a class="text-primary-2 ladda-button" data-style="zoom-in" id="monthlyPerformance" type="button" href="#" data-toggle="tooltip" data-placement="right" title="Monthly Performace" style="margin-left: 5%"><i class="pe pe-7s-display1 pe-3x"></i></a>
                                        </td>
                                        <td style="text-align: center">
                                            <p style="font-size: 15px">1 Credit Per API</p>
                                        </td>
                                        <td style="text-align: center">
                                            <a onclick = "#" class="text-success" data-toggle="tooltip" data-placement="right" title="Test" id="testFirstAPI"><i class="pe pe-7s-science pe-3x"></i></a>
                                        </td>                                                               
                                    </tr>        
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td colspan="6">
                                            <ul class="pagination pull-right"></ul>
                                        </td>
                                    </tr>
                                </tfoot>
                            </table>                   
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>  

    <div id="wrapper" style="min-height:0%!important">
        <div class="small-header transition animated fadeIn">
            <div class="hpanel tour-reportMenu">
                <div class="panel-body">                
                    <div id="hbreadcrumb">                    
                        <p style="font-size:18px !important;" ><span id="resourceName" style="padding-right: 65%;width: 5%"></span> Select Report 
                            <select  style="width: 20%;margin-left: 40px" id="apiConsoleSelectBox" name="apiConsoleSelectBox" onchange="getReportConsole(this.value)">
                                <optgroup label="Select Report">                                
                                    <option value="0" selected>Credit Usage</option>
                                    <option value="1">API Usage</option>
                                    <option value="2">Transaction Report</option> 
                                    <option value="3">Performance Report</option> 
                                </optgroup>
                            </select>
                        </p>    
                    </div>                         
                </div>
            </div>
        </div>                                
        <div class="content animate-panel">        
            <div class="row ">
                <div class="col-lg-12 tour-reportWindow">
                    <div class="hpanel">
                        <div class="panel-heading">
                            
                            <h4>Credit Information And Statistics</h4>
                        </div>
                        <div class="panel-body">
                            <div class="text-left">
                                <i class="fa fa-bar-chart-o fa-fw"></i><span id="homeReportLable">  Last 7 days expenditure</span>
                            </div>
                            <div id="report_data_button">
                                <div class="btn-group" style="padding-left: 70%">
                                    <button  class="btn btn-default btn-xs ladda-button" data-style="zoom-in" > Daily</button>
                                    <button  class="btn btn-default btn-xs ladda-button" data-style="zoom-in" > Last 7 days</button>
                                    <button  class="btn btn-default btn-xs ladda-button" data-style="zoom-in" > Last 30 days</button>
                                </div>
                            </div>
                            <hr class="m-b-xl"/>                         
                            <div id="expenditureReportTour">
                            </div>
                            <div id="noRecordFoundData" style="display: none" style="margin-bottom: 30%">
                                <img src="images/no_record_found.jpg" alt="No record found" width="300px" height="300px" style="margin-left: 35%"/>
                            </div> 
                        </div>
                    </div>
                </div>
            </div>       
        </div>   
    </div>                
    <script>
        $(function () {

            var dateArr2 = ["Data e Ora", '2017-07-30', '2017-07-31', '2017-08-01', '2017-08-02', '2017-08-03', '2017-08-04', '2017-08-05'];
            var txAmount2 = ['CreditUsed', 10, 50, 60, 90, 40, 100, 40];
            var callCountArr2 = ['Call Count', 2, 15, 20, 30, 15, 40, 20];
            console.log("arrXaxis >> " + dateArr2);
            console.log("callCountArr2 >> " + callCountArr2);
            console.log("txAmount2  >> " + txAmount2);

            var chart = c3.generate({
                bindto: '#expenditureReportTour',
                data: {
                    x: 'Data e Ora',
                    xFormat: '%Y-%m-%d',
                    columns: [
                        dateArr2,
                        txAmount2,
                        callCountArr2
                    ],
                    colors: {
                        //                TransactionAmount: '#62cb31',
                        //                callCountArr2: '#FF7F50'
                        dateArr2: '#62cb31',
                        CreditUsed: '#FF7F50'
                    },
                    types: {
                        //CallCount: 'bar',
                        CreditUsed: 'bar'
                    },
                    groups: [
                        ['Transcation Amount', 'Call Count']
                    ]
                },
                subchart: {
                    show: true
                },
                axis: {
                    x: {
                        type: 'timeseries',
                        // if true, treat x value as localtime (Default)
                        // if false, convert to UTC internally                        
                        tick: {
                            format: '%d-%m-%Y'
                                    //fomat:function (x) { return x.getFullYear(); }
                        }
                    }
                }
            });
        });
    </script>        
    <div id="wrapper" style="min-height:0%!important">
        <div class="content animate-panel">
            <div class="row tour-invoiceTour">
                <div class="col-lg-12">
                    <div class="hpanel">
                        <div class="panel-heading">
                            
                            My Invoices
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table id="apiInvoiceTable" class="table table-striped table-bordered table-hover">
                                    <thead id="invoiceHeader">
                                        <tr>
                                            <th>Date</th>
                                            <th>Invoice (PDF)</th>
                                            <th>Invoice No.</th>
                                            <th>Amount</th>
                                            <th>Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>                                
                                        <tr>
                                            <td>22/03/2017 03:58:41</td>                                    
                                            <td><a href="#" class="btn btn-primary btn-xs" onclick="#"><i class="fa fa-file-pdf-o"></i>Payment Invoice</a></td>                                                                                                                                                
                                            <td>17220893124</td>
                                            <td>USD 60.00</td>                                    
                                            <td>Paid</td>                                    
                                        </tr>
                                        <tr>
                                            <td>22/04/2017 03:59:29</td>                                    
                                            <td><a href="#" class="btn btn-primary btn-xs" onclick="#"><i class="fa fa-file-pdf-o"></i>Payment Invoice</a></td>                                                                                                                                                
                                            <td>17220893136</td>
                                            <td>USD 60.00</td>                                    
                                            <td>Paid</td>                                    
                                        </tr>
                                        <tr>
                                            <td>22/05/2017 03:58:41</td>                                    
                                            <td><a href="#" class="btn btn-primary btn-xs" onclick="#"><i class="fa fa-file-pdf-o"></i>Payment Invoice</a></td>                                                                                                                                                
                                            <td>17220893165</td>
                                            <td>USD 60.00</td>                                    
                                            <td>Paid</td>                                    
                                        </tr>
                                    <script>
                                                $(function () {
                                                    $('#apiInvoiceTable').dataTable();
                                                });
                                    </script>                                                               
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>                           
            </div>        
        </div>
    </div>        
    <div id="wrapper" style="min-height:0%!important">
        <div class="content animate-panel" data-effect="rotateInDownRight" data-child="element">
            <div class="row">
                <div class="col-lg-12 ">
                    <div class="row">                                                          
                        <div class="col-sm-3 element tour-subscriptionTour">
                            <div class="hpanel plan-box">
                                <div class="panel-heading hbuilt text-center">
                                    <h4 class="font-bold element">BasicKitMonthly</h4>
                                </div>
                                <div class="panel-body text-center">
                                    <i class="pe pe-7s-cup big-icon text-warning element"></i>
                                    <h4 class="font-bold element">
                                        $ 5.00
                                    </h4>
                                    <p class="text-muted element">                                    
                                        There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.                                    
                                    </p>                                
                                    <a class="btn btn-warning btn-sm ladda-button element" data-style="zoom-in" id="unSubscripbePackage"> Unsubscribe</a>                                                                                                                                           
                                </div>
                            </div>
                        </div>                                                        
                    </div>
                </div>               
            </div>               
        </div>
    </div> 
    <div id="wrapper">
        <div class="content animate-panel">
            <div class="row tour-HelpDesk">
                <div class="col-md-12">
                    <div class="hpanel email-compose">
                        <div class="panel-heading hbuilt">
                            <div class="p-xs h4">
                                New message
                            </div>
                        </div>
                        <div class="panel-heading hbuilt">
                            <div class="p-xs">
                                <form  class="form-horizontal" method="POST" id="ticketComposeRT">
                                    <div class="form-group"><label class="col-sm-2 control-label text-left">To:</label>
                                        <div class="col-sm-10"><input type="text" class="form-control input-sm" value="support@mollatech.com" disabled></div>
                                    </div>
                                    <div class="form-group"><label class="col-sm-2 control-label text-left">Contact:</label>
                                        <div class="col-sm-10"><input type="text" id="contact_no" onkeypress="if (isNaN(String.fromCharCode(event.keyCode)))
                                                    return false;" class="form-control input-sm" placeholder="Enter your contact number"></div>
                                    </div>
                                    <div class="form-group"><label class="col-sm-2 control-label text-left">Subject:</label>
                                        <div class="col-sm-10">
                                            <select id="subjectBody" class="js-source-states" style="width: 100%">
                                                <optgroup label="General">
                                                    <option value="General Questions">General Questions</option>
                                                </optgroup>
                                                <optgroup label="API">
                                                    <option value="SMS">SMS</option>
                                                    <option value="Map">Map</option>
                                                    <option value="MFA">MFA</option>
                                                    <option value="Digital_signing">Digital Signing</option>
                                                    <option value="Payment_gateway">Payment Gateway</option>
                                                </optgroup>
                                                <optgroup label="Fault">
                                                    <option value="Access_Token_Issue">Access Token Issue</option>
                                                    <option value="password">Can't Change Password</option>
                                                    <option value="others">Others</option>
                                                </optgroup>
                                            </select>
                                        </div>
                                    </div>
                                </form>
                                <div style="background: #fff; height: 200px">
                                    <div class="col-sm-12">
                                        <form action="UploadEmailAttachment" method="post" class="dropzone" id="my-dropzone"></form>	
                                        <small>Upload attachment. Support Image File size limit 5mb</small>
                                    </div>
                                </div>
                                <div class="panel-body no-padding">
                                    <div><textarea class="summernote" id="msgBody"></textarea></div>                                    
                                </div>
                                <div class="panel-footer">
                                    <div class="pull-right">
                                        <div class="btn-group">
                                        </div>
                                    </div>
                                    <a id="submitMail" class="btn btn-primary ladda-button" data-style="zoom-in">Send email</a>
                                    <a class="btn btn-danger" tabindex="2" ><i class="fa fa-close"></i> Clear</a>
                                </div>                            
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            $(function () {
                var elem = $(".summernote").summernote({
                    callbacks: {
                        onInit: function () {
                            var editor = elem.next(),
                                    placeholder = editor.find(".note-placeholder");

                            function isEditorEmpty() {
                                var code = elem.summernote("code");
                                return code === "<p><br></p>" || code === "";
                            }

                            editor.on("focusin focusout", ".note-editable", function (e) {
                                if (isEditorEmpty()) {
                                    placeholder[e.type === "focusout" ? "show" : "hide"]();
                                }
                            });
                        }
                    }
                });
            });


            $(document).ready(function () {
                Dropzone.autoDiscover = false;
                var myDropzone = new Dropzone("#my-dropzone", {
                    url: "UploadZip",
                    uploadMultiple: true,
                    maxFilesize: 5,
                    maxFiles: 1,
                    acceptedFiles: "image/*",
                    dictInvalidFileType: "You can't upload files of this type, only Image file",
                    autoProcessQueue: true,
                    parallelUploads: 1,
                    addRemoveLinks: true,
                });
            });


        </script>
        <%@include file="footer.jsp"%>
    </div>
    <!--    <style>
            .tour-step-background{background: none!important;}
        </style>     -->

    <script type="text/javascript">

    </script>
    <script>/*<![CDATA[*/window.zEmbed || function (e, t) {
            var n, o, d, i, s, a = [], r = document.createElement("iframe");
            window.zEmbed = function () {
                a.push(arguments)
            }, window.zE = window.zE || window.zEmbed, r.src = "javascript:false", r.title = "", r.role = "presentation", (r.frameElement || r).style.cssText = "display: none", d = document.getElementsByTagName("script"), d = d[d.length - 1], d.parentNode.insertBefore(r, d), i = r.contentWindow, s = i.document;
            try {
                o = s
            } catch (e) {
                n = document.domain, r.src = 'javascript:var d=document.open();d.domain="' + n + '";void(0);', o = s
            }
            o.open()._l = function () {
                var e = this.createElement("script");
                n && (this.domain = n), e.id = "js-iframe-async", e.src = "https://assets.zendesk.com/embeddable_framework/main.js", this.t = +new Date, this.zendeskHost = "mollatechhelp.zendesk.com", this.zEQueue = a, this.body.appendChild(e)
            }, o.write('<body onload="document._l();">'), o.close()
        }();
        /*]]>*/
    </script>
    <script>
        //    document.onreadystatechange = function(){
        //     if(document.readyState === 'complete'){
        //         $('#homepage').ready(function() {
        //       sethomepage();
        //        });
        //    }};

        $(document).ready(function () {
            //var creditUsedData = fifteenDaysCreditExpenditure();


            var doc = document.getElementById('_APIbody');
            var dataCode = CodeMirror.fromTextArea(doc, {
                lineNumbers: true,
                matchBrackets: true,
                styleActiveLine: true,
                tabMode: "indent",
            });
            dataCode.on('change', function ash() {
                $('#_APIbody').val(dataCode.getValue());
            });

            var doc = document.getElementById('responseID');
            var dataCode = CodeMirror.fromTextArea(doc, {
                lineNumbers: true,
                matchBrackets: true,
                styleActiveLine: true,
                tabMode: "indent",
            });
            dataCode.on('change', function ash() {
                $('#responseID').val(dataCode.getValue());
            });
        });
        generateTopServiceTour();
        yourMostlyServiceTour();
        //    
//        function fifteenDaysCreditExpenditure() {
//            var s = './DailyTransactionAmount';
//            var jsonData = $.ajax({
//                url: s,
//                dataType: "json",
//                async: false
//            }).responseText;
//            if (jsonData.length === 0) {
//                jsonData = [{"label": "NA", "value": 0, "value5": 0}];
//                return jsonData;
//            } else {
//                var myJsonObj = JSON.parse(jsonData);
//                return myJsonObj;
//            }
//        }

//        function getCreditStatus() {
//            var s = './GetCredit';
//            $.ajax({
//                type: 'POST',
//                url: s,
//                datatype: 'json',
//                data: $("#getBody").serialize(),
//                success: function (data) {
//                    if (data.credits !== 'NA') {
//                        $('#mainCreditOfDev').html(data.credits);
//                    }
//                }});
//        }
        //getCreditStatus();
    </script>

