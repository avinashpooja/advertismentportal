<%-- 
    Document   : header
    Created on : 16 Feb, 2018, 4:34:52 PM
    Author     : serviceguard
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>


<!DOCTYPE html>
<html>
    <head>
       
        <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!--
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <-->
    <meta name='viewport' content='width=device-width, initial-scale=1.0, user-scalable=0' >
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
   <title>AuthBridge Research Services:Dashboard</title>

    <!-- Bootstrap -->
	
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" type="text/css" href="css/global.css"/>
	<link rel="stylesheet" type="text/css" href="css/slick.css"/>
	<link rel="stylesheet" type="text/css" href="css/menu.css"/>
	<link rel="stylesheet" type="text/css" href="css/icon.css"/>
	<link rel="stylesheet" type="text/css" href="css/jquery.mCustomScrollbar.css"/>
	<link rel="stylesheet" type="text/css" href="css/custom.css"/>
	<link rel="stylesheet" type="text/css" href="css/inner-page.css"/>
	<link rel="stylesheet" type="text/css" href="css/smk-accordion.css"/>
	<link rel="stylesheet" type="text/css" href="css/validationEngine.jquery.css"/>
	<link rel="stylesheet" type="text/css" href="css/dropzone.css"/>
	<link rel="stylesheet" type="text/css" href="css/search_component.css"/>
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	
	 <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  
    <!-- Include all compiled plugins (below), or include individual files as needed -->
	
	<script type="text/javascript" src="js/jquery_1.12.4.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.js"></script>
	<script type="text/javascript" src="js/modernizr.custom.js"></script>
	<script type="text/javascript" src="js/classie.js"></script>
	<script type="text/javascript" src="js/uisearch.js"></script>
	<script type="text/javascript" src="js/slick.js"></script>
	<script type="text/javascript" src="js/common.js"></script>
	<script type="text/javascript" src="js/smk-accordion.js"></script>
	<script type="text/javascript" src="js/dropzone.js"></script>
	<script type="text/javascript" src="js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="js/jquery.validationEngine-en.js"></script>
	<script type="text/javascript" src="js/jquery.mCustomScrollbar.js"></script>
	<script type="text/javascript" src="js/menu-hover.js"></script>
   
	<script type="text/javascript">
		var SITE_URL = "http://www.truthscreen.com/";
		function blockUI(){
			$("#loadingmessage").show();	
		}
		 
	function  unBlockUI(){
		$("#loadingmessage").hide();	
		
	}
	
	
	
	</script>	
        
    </head>
    <body>    
        
        
    <div id="main"> 
    <div class="container-fluid">	
	    <div id='loadingmessage' style='display:none' class="overlay">
			<div class="show-delay"><img src="/img/google-loading-icon.gif" alt="Loder" style="height:100px;width:100px"/></div>
		</div>
      <!-- top panel starts here -->
      
	   


 



<div class="row">

        <div class="top-panel box-shadow">
            
         <!--    <div>         
    
        <span style="color: #41f450;margin-bottom: 30px;">
        <font size="6"><b>Advertisement</b></font>
        </span>
        <span><font size="6">Portal</font></span>
            -->
            
  


         <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 green-gr text-center"> 

		  <button type="button" class="navbar-fostrap user"> <span></span> <span></span> <span></span> </button>

			<a href="/dashboard" class="logo sprite"></a>         



				<div class="user-wrap visible-xs"> 

				  <div class="dropdown pull-right">

				  <button class="user dropdown-toggle"  id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">

					<!--<img src="images/user.jpg" class="img-responsive"> -->

					<img src="img/male_user.jpg" class="" alt=""/>
					 <!--<strong class="icon-user top-user-name"> Blue B.. </strong> -->

					<span class="caret"></span>

				  </button>

				  <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">

					<li><a href="/users/change_password">Change Password</a></li>


					<li><a href="/apidoc" target="_blank">Developers API</a></li>

					<li><a href="/users/logout">Logout</a></li>

				  </ul>

				</div><!-- /.Dropdown -->

				</div><!-- /.col-lg-2 -->

          </div>

 

          <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 right-top hidden-xs p_none">
              
             <!-- <span style="color: #41f450;margin-bottom: 30px;">
              <font size="6"><b>Advertisement</b></font>
              </span>
              <span><font size="6">Portal</font></span> -->
              

            <div class="top-wrap">

			

			  <!-- /.search_bar-->

            <div class="search_bar clearfix">

              <nav class="pull-left">

                <div class="nav-fostrap">

                    <ul class="clearfix">                     

						<li class="tour-3"><a onclick=""><span class="icon-dashboard"></span>Push Template</a></li>
                                                
                                                <li class="tour-4"><a onclick=''><span class="icon-dashboard"> </span>Push Ad  </a>  </li>
                        
                                                <li class="tour-5"><a  onclick="emailADDetails()"> <span class="icon-dashboard"></span>Email Ad</a></li> 
                        
                                                <li class="tourVideo"><a  onclick="pdfADDetails()"> <span class="icon-dashboard"></span>PDF Ad</a></li>
                                                
                                                
                                                <li class="tourVideo"><a  onclick=""> <span class="icon-dashboard"></span>Invoices</a> </li>                    
                        
                    
                                                <li class="tourVideo"> <a  onclick=""> <span class="icon-dashboard"></span>Subscription</a> </li>                      
                       
                    
                        
                        
                        
                    
                        
                    
									 

						

											
											

										
									<!--		<ul class="dropdown">

												
												<li class="" style="margin-left: 20px;">

													<a href="/Verification/aadhar_verification">Aadhaar</span></a>
												</li>

											
												<li class="" style="margin-left: 20px;">

													<a href="/Verification/pan_verification">PAN</span></a>
												</li>

											
												<li class="" style="margin-left: 20px;">

													<a href="/Verification/passport_verification">Passport</span></a>
												</li>

											
												<li class="" style="margin-left: 20px;">

													<a href="/Verification/driving_detail">Driving License</span></a>
												</li>

											
												<li class="" style="margin-left: 20px;">

													<a href="/Verification/voter_verification">Voter Id</span></a>
												</li>

											
										</ul>

										

									</li> 

									
								
								
									

									

										 <li>	

									 

																				

											
												<a  href="#"><span class="icon-utility"></span>Email AD <sup></sup></a>

											
											

										
											<ul class="dropdown">

												
												<li class="" style="margin-left: 20px;">

													<a href="/Verification/utility">Invoice</span></a>
												</li>

											
												<li class="" style="margin-left: 20px;">

													<a href="/Verification/water_bill_verification">Water Bill</span></a>
												</li>

											
												<li class="" style="margin-left: 20px;">

													<a href="/Verification/gas_bill_verification">Gas Bill</span></a>
												</li>

											
												<li class="" style="margin-left: 20px;">

													<a href="/Verification/telephone_bill_verification">Landline Users</span></a>
												</li>

											
												<li class="" style="margin-left: 20px;">

													<a href="/Verification/property_tax_verification">Property Tax</span></a>
												</li>

											
												<li class="" style="margin-left: 20px;">

													<a href="/Verification/vehicle_reg_verification">Vehicle Registration</span></a>
												</li>

											
												<li class="" style="margin-left: 20px;">

													<a href="/Verification/ration_card_verification">Ration Card</span></a>
												</li>

											
												<li class="" style="margin-left: 20px;">

													<a href="/Verification/getGasConnectionDetail">Gas Connection</span></a>
												</li>

											
										</ul> 

										

									</li>

									
								
								
									

									

										 <li>	

									 

																				

											
												<a  href="#"><span class="icon-kyc"></span>Subscription <sup></sup></a>

											
											

										
										<!--	<ul class="dropdown">

												
												<li class="" style="margin-left: 20px;">

													<a href="/api/generate_otp_using_aadhaar">e-KYC OTP</span></a>
												</li>

											
												<li class="" style="margin-left: 20px;">

													<a href="/api/ekyc_with_biometric">e-KYC Biometric</span></a>
												</li>

											
										</ul> 

										

									</li>

									
								
								
									

									

										 <li>	

									 

																				

											
												<a  href="#"><span class="icon-company"></span>Businesses <sup></sup></a>

											
											

										
											<!--<ul class="dropdown">

												
												<li class="" style="margin-left: 20px;">

													<a href="/search/director_listing">DIN</span></a>
												</li>

											
												<li class="" style="margin-left: 20px;">

													<a href="/search">Company Search</span></a>
												</li>

											
												<li class="" style="margin-left: 20px;">

													<a href="/search/tin_verification">TIN</span></a>
												</li>

											
												<li class="" style="margin-left: 20px;">

													<a href="/search/get_service_tax_detail">Service Tax</span></a>
												</li>

											
												<li class="" style="margin-left: 20px;">

													<a href="/Verification/shop_establishment">Shop & Establishment</span></a>
												</li>

											
												<li class="" style="margin-left: 20px;">

													<a href="/Verification/drug_license_verification">Drug License</span></a>
												</li>

											
												<li class="" style="margin-left: 20px;">

													<a href="/Verification/udyog_aadhaar">Udyog Aadhaar</span></a>
												</li>

											
												<li class="" style="margin-left: 20px;">

													<a href="/Search/pan_director_verification">PAN-Director</span></a>
												</li>

											
												<li class="" style="margin-left: 20px;">

													<a href="/Verification/esic_verification">ESIC</span></a>
												</li>

											
												<li class="" style="margin-left: 20px;">

													<a href="/Verification/iec_verification">IEC</span></a>
												</li>

											
												<li class="" style="margin-left: 20px;">

													<a href="/Verification/gstin_verification">GSTIN</span></a>
												</li>

											
												<li class="" style="margin-left: 20px;">

													<a href="/search/getTanDetails">Know Your TAN</span></a>
												</li>

											
												<li class="" style="margin-left: 20px;">

													<a href="/search/getPanDetails">Know Your PAN</span></a>
												</li>

											
										</ul> 

										

									</li>

									
								
								
									

									

										 <li>	

									 

																				

											
												<a  href="#"><span class="icon-identity"></span>Professionals <sup></sup></a>

											
											

										
										<!--	<ul class="dropdown">

												
												<li class="" style="margin-left: 20px;">

													<a href="/professional/getInsuranceAgentDetails">Insurance Agent Verification</span></a>
												</li>

											
												<li class="" style="margin-left: 20px;">

													<a href="/professional/getCADetails">CA Verification</span></a>
												</li>

											
												<li class="" style="margin-left: 20px;">

													<a href="/professional/getDoctorDetails">Doctor Verification</span></a>
												</li>

											
												<li class="" style="margin-left: 20px;">

													<a href="/professional/getDentistDetail">Dentist Verification</span></a>
												</li>

											
										</ul> 

										

									</li>-->

									
												

				
                     

                    </ul>

                    <!-- /.Ul --> 

                </div>

                <!-- /.nav-fostrap --> 

              </nav>

              

            

				<!-- /.sb-search-->

				
				

				<!-- <div id="sb-search" class="sb-search "> -->

				

                <div id="sb-search" class="sb-search">

               <form action="/dashboard" id="SearchForm" method="get" accept-charset="utf-8">                  

					

					<input name="first_name" class="sb-search-input" placeholder="Enter your search term..." id="search" maxlength="100" type="text"/>
					

					

                    <!--<input class="sb-search-submit" type="submit" value="">-->

                    <span class="sb-icon-search"><img src="img/search_icon.png" alt=""/> </span>

                 </form>
                </div><!-- /.sb-search-->				

				

              </div>

              <!-- /.search_bar-->			  

			

			 <div class="hidden-xs user-wrap">

              <div class="dropdown pull-right">
                <button class="user dropdown-toggle"  id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"> 

				<img src="img/male_user.jpg" class="" alt=""/>
				<!--<strong class="icon-user top-user-name"> Blue B.. </strong> -->

				<span class="caret"></span></button>

					<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">

					  <li><a href="/users/change_password">Change Password</a></li>

					  <li><a href="/apidoc" target="_blank">Developers API</a></li>

					  <li><a href="/users/logout">Logout</a></li>

					</ul>

              </div>

              <!-- /.Dropdown --> 

              
            </div>

            

            </div><!-- /.top-wrap -->

          </div><!-- /.right-top -->

        </div><!-- /.top panel -->

</div><!-- top panel ends here -->
     
    </body>
</html>
