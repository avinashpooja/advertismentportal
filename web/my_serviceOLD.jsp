<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.CaaSManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgCassNumbers"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.NumbersManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgNumbers"%>
<%@include file="header.jsp"%> 
<script src="vendor/toastr/build/toastr.min.js"></script>
<script src="vendor/ladda/dist/spin.min.js"></script>
<script src="vendor/ladda/dist/ladda.min.js"></script>
<script src="vendor/ladda/dist/ladda.jquery.min.js"></script>
<script src="scripts/myservice.js" type="text/javascript"></script>
<%  SgNumbers[] numberList = new NumbersManagement().listNumberByPartnerId(SessionId, ChannelId, parObj.getPartnerId());
//PartnerDetails partnerData = new PartnerManagement().getPartnerDetails(parObj.getPartnerId());
    SgCassNumbers cassNumber = null;
    SimpleDateFormat tmDateFormat = new SimpleDateFormat("dd/MM/yyyy");
    if(parObj.getCaasNumber() != ""){
    String[] casssNo = parObj.getCaasNumber().split(",");
    int numberId = Integer.parseInt(casssNo[1]);
    cassNumber = new CaaSManagement().getNumberById(numberId);
}
String info = "NA";
%>
<!-- Main Wrapper -->
<div id="wrapper">

    <div class="small-header transition animated fadeIn">
        <div class="hpanel">
            <div class="panel-body">
                <div id="hbreadcrumb" class="pull-right">
                    <ol class="hbreadcrumb breadcrumb">
                        <li><a href="home.jsp">Dashboard</a></li>
                        <li class="active">
                            <span>My Service</span>
                        </li>
                    </ol>
                </div>
                <h2 class="font-light m-b-xs">
                    My Service
                </h2>
                <small>Display your virtual number, ID and password for APIs</small>
            </div>
        </div>
    </div>


    <div class="content animate-panel">

        <div class="row">
            <div class="col-lg-12">
                <div class="hpanel">
                    <div class="panel-heading">
                        <div class="panel-tools">
                            <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                        </div>
                        SMS Assigned virtual number
                    </div>
                    <div class="panel-body">
                        <table id="sms" class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th style="text-align: center">No.</th>
                                    <th style="text-align: center">Virtual Number</th>
                                    <th style="text-align: center">IB Id</th>
                                    <th style="text-align: center">IB Password</th>
                                    <th style="text-align: center">MO url</th>
                                    <th style="text-align: center">Environment</th>
                                    <th style="text-align: center">Created On</th>
                                </tr>
                            </thead>
                            <tbody>
                                <%
                                    if (numberList != null) {
                                        int count = 0;
                                        for (int i = 0; i < numberList.length; i++) {
                                            count++;
                                            
                                            String infoId = "NA";
                                            String infoPass = "NA";
                                            if (numberList[i].getInfoblastId() != null) {
                                                infoId = (numberList[i].getInfoblastId());
                                            }
                                            if (numberList[i].getInfoblastPassword() != null) {
                                                infoPass = (numberList[i].getInfoblastPassword());
                                            }
                                %>
                                <tr>
                                    <td style="text-align: center"><%=count%></td>
                                    <td style="text-align: center"><%=numberList[i].getMobileNo()%></td>
                                    <td style="text-align: center"><%=infoId%></td>
                                    <td style="text-align: center"><%=infoPass%></td>
                                    <td style="text-align: center"><a type="button" class="btn btn-success btn-xs ladda-button" onclick="viewUrl('<%=numberList[i].getMo_URL()%>','<%=numberList[i].getNumberId()%>')" data-style="zoom-in" id="viewToken"><i class="fa fa-refresh"></i> <span class="bold">View url</span></a></td>            
                                    <td style="text-align: center"><%=numberList[i].getType()%></td>
                                    <td style="text-align: center"><%=numberList[i].getUpdatedOn()%></td>
                                </tr>
                                <%}
                                } else {%>
                                <tr>
                                    <td style="text-align: center"><%=1%></td>
                                    <td style="text-align: center"><%=info%></td>
                                    <td style="text-align: center"><%=info%></td>
                                    <td style="text-align: center"><%=info%></td>
                                    <td style="text-align: center"><%=info%></td>
                                    <td style="text-align: center"><%=info%></td>
                                </tr>
                                <%}%>                                
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="hpanel">
                    <div class="panel-heading">
                        <div class="panel-tools">
                            <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                        </div>
                        CaaS Assigned virtual number
                    </div>
                    <div class="panel-body">
                        <table id="caas" class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Services</th>
                                    <th>Virtual Number</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>CaaS</td>
                                    <td>01546011001</td>
                                </tr>
                            </tbody>
                        </table>

                    </div>
                </div>

                <div class="hpanel">
                    <div class="panel-heading">
                        <div class="panel-tools">
                            <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                        </div>
                        TMPG Merchant ID & Password
                    </div>
                    <div class="panel-body">
                        <table id="caas" class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Services</th>
                                    <th>Merchant ID</th>
                                    <th>Password</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>TMPG</td>
                                    <td>NA</td>
                                    <td>NA</td>
                                </tr>
                            </tbody>
                        </table>

                    </div>
                    <div class="modal fade hmodal-success" id="moUrl" tabindex="-1" role="dialog"  aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="color-line"></div>
                                <div class="modal-header">
                                    <h4 id="urlType" class="modal-title"></h4>
                                    <small class="font-bold"></small>
                                </div>
                                <div class="modal-body">
                                    <!--<p id="url"></p>-->
                                    <label>Mo url :   </label>
                                    <input type="text" id="url" />
                                    <input type="hidden" id="numberID" name="numberID" />
                                </div>
                                <div class="modal-footer">
                                    <button type="button" id="editMoUrl" class="btn btn-default ladda-button" data-style="zoom-in" onclick="editUrl()">Edit</button>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <%@include file="footer.jsp"%> 


        <script>
            
            $(function () {

                // Initialize Example 2
                $('#sms').dataTable();
                $('#caas').dataTable();
                $('#tmpg').dataTable();

            });

            $(function () {

                // Initialize Example 2
                $('#data_type').dataTable();

            });

        </script>