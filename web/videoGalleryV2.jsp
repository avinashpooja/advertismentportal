<script src="vendor/jquery-ui/jquery-ui.min.js"></script>
<style>
* {
color: #fff;
font-family: "Avenir", sans-serif;
font-size: 18px;

border: 0;
margin: 0;
padding: 0;
}

.container {
display: flex;
align-content: center;

margin-bottom: 20px;
width: 100%;

background-position: center;
background-size: cover;
}

.content {
width: 100%;
}

.container {
background-color: hsl(200, 100%, 95%);

margin: auto;
padding: 0%;
}

.comparecontainer {
margin: 0%;
float: left;

height: auto;
width: 50%;
}

.compare {
padding: 5%;

height: 100%;
width: 100%;
}

.lite {
background-color: hsl(40, 100%, 50%);
}

.full {
background-color: hsl(150, 80%, 50%);
}



.button {
background-color: rgba(255, 255, 255, 0.2);
color: #fff;
font-family: ;
font-size: 18px;
font-weight: 500;
line-height: 200%;
text-decoration: none;
text-transform: uppercase;
display: inline-block;
padding: 1% 3%;
border: 2px solid #fff;
border-radius: 0;
outline: none;
}

.button:hover,
.button:active {
background-color: #fff;
}

.button:hover .lite {
color: hsl(40, 100%, 50%);
}

.button .full:hover {
color: hsl(150, 80%, 50%);
}



h2 {
font-size: 36px;
margin: auto;
}

h3 {
font-size: 21px;
margin: 2.5% 0;
}

h4 {
font-size: 21px;
margin: 10% 0;
}

ul {
list-style: none;
margin: 5% 0;
}

li {
margin: 2.5% 0;
}
</style>
<div class="content animate-panel">
    <div class="row">
        <div class="col-lg-12">
            <div class="hpanel">
                <div class="panel-body">
                    <div class="lightBoxGallery">
                        <p>
                            <strong>Video Gallery</strong>  within our gallery you will find Service Tutorials and Dashboard Tutorials. Learn More about Ready APIs from here.
                        </p>
                        <br>
                        <ul class="nav nav-tabs">                        
                            <li class="active"><a data-toggle="tab" href="#tab-2"><i class="fa fa-desktop fa-2x"></i> Dashboard</a></li>
                            <li class=""><a data-toggle="tab" href="#tab-3"><i class="fa fa-star-o fa-2x"></i> Services</a></li>                        
                        </ul>
                        
                        <div class="tab-content">
                        <div id="tab-2" class="tab-pane active">                            
                            <div class="row">				
                                <div class="col-sm-12">                                 
                                    <div class="col-sm-6">
                                        <ul>
                                            <li id="text"><h4>Sign Up Video </h4><br><p>This is test</p></li>
                                        </ul>
                                    </div>                            
                                    <div class="col-sm-6 col-sm-offset-2">
                                       <div class="lightbox-gallery dark mrb35">
                                          <ul id="videos-without-poster" class="list-unstyled row">
                                            <li class="video" data-src="https://www.youtube.com/watch?v=e-ORhEE9VVg" data-sub-html="Good Girls become Guide Dogs" style="width: 70%!important; height: 70%!important">
                                                <a href="">
                                                      <img class="img-responsive img-thumbnail" src="https://img.youtube.com/vi/e-ORhEE9VVg/maxresdefault.jpg" alt="Good Girls become Guide Dogs">
                                                      <div class="demo-gallery-poster">
                                                          <img src="https://ouresources.usu.edu/_assets/galleries/light-gallery/img/play-button.png">
                                                      </div>
                                                </a> 
                                            </li>
                                              <li class="video" data-src="https://www.youtube.com/watch?v=dQw4w9WgXcQ" data-sub-html="Red Hat Club"><a href=""><img class="img-responsive" src="https://templates.usu.edu/ldp/galleries/.private_ldp/a104300/production/thumb/25df9b02-3c31-4e27-80a2-948cbe043a5d.jpg" alt="Red Hat Club"><div class="demo-gallery-poster"><img src="https://ouresources.usu.edu/_assets/galleries/light-gallery/img/play-button.png"></div></a></li>
                                             <li class="video" data-src="https://www.youtube.com/watch?v=PrQb19l7jOQ" data-sub-html="Curabitur non placerat lorem"><a href=""><img class="img-responsive" src="https://templates.usu.edu/ldp/galleries/.private_ldp/a104300/production/thumb/166b6e0e-156b-4e99-8409-fad627ee398a.jpg" alt="Curabitur non placerat lorem"><div class="demo-gallery-poster"><img src="https://ouresources.usu.edu/_assets/galleries/light-gallery/img/play-button.png"></div></a></li>
                                             <li class="video" data-src="https://www.youtube.com/watch?v=fDq3TK8id6w" data-sub-html="Lorem Ipsum Dolor Sit Amet Consecteteur"><a href=""><img class="img-responsive" src="https://templates.usu.edu/ldp/galleries/.private_ldp/a104300/production/thumb/09194722-27aa-4ea1-b6c4-c84cfeae6641.jpg" alt="Lorem Ipsum Dolor Sit Amet Consecteteur"><div class="demo-gallery-poster"><img src="https://ouresources.usu.edu/_assets/galleries/light-gallery/img/play-button.png"></div></a></li>
                                             <li class="video" data-src="https://www.youtube.com/watch?v=K1_JWrl3_NQ" data-sub-html="Lorem Ipsum Dolor Sit Amet Consecteteur"><a href=""><img class="img-responsive" src="https://templates.usu.edu/ldp/galleries/.private_ldp/a104300/production/thumb/ecaa08f9-b30e-4102-9f50-ae53bf539809.jpg" alt="Lorem Ipsum Dolor Sit Amet Consecteteur"><div class="demo-gallery-poster"><img src="https://ouresources.usu.edu/_assets/galleries/light-gallery/img/play-button.png"></div></a></li>
                                             <li class="video" data-src="https://www.youtube.com/watch?v=K1_JWrl3_NQ" data-sub-html="Lorem Ipsum Dolor Sit Amet Consecteteur"><a href=""><img class="img-responsive" src="https://templates.usu.edu/ldp/galleries/.private_ldp/a104300/production/thumb/9ec3b7e1-9f72-42c1-a8de-ec8a87721979.jpg" alt="Lorem Ipsum Dolor Sit Amet Consecteteur"><div class="demo-gallery-poster"><img src="https://ouresources.usu.edu/_assets/galleries/light-gallery/img/play-button.png"></div></a></li>
                                             <li class="video" data-src="https://www.youtube.com/watch?v=PrQb19l7jOQ" data-sub-html="Lorem Ipsum Dolor Sit Amet Consecteteur"><a href=""><img class="img-responsive" src="https://templates.usu.edu/ldp/galleries/.private_ldp/a104300/production/thumb/e0ab68dc-f0a0-4b14-8bb5-9e82eba3fec1.jpg" alt="Lorem Ipsum Dolor Sit Amet Consecteteur"><div class="demo-gallery-poster"><img src="https://ouresources.usu.edu/_assets/galleries/light-gallery/img/play-button.png"></div></a></li>
                                             <li class="video" data-src="https://www.youtube.com/watch?v=fDq3TK8id6w" data-sub-html="A New Zealand Working Dog"><a href=""><img class="img-responsive" src="https://templates.usu.edu/ldp/galleries/.private_ldp/a104300/production/thumb/00a3c536-1152-4908-9418-490caf5e95e8.jpg" alt="A New Zealand Working Dog"><div class="demo-gallery-poster"><img src="https://ouresources.usu.edu/_assets/galleries/light-gallery/img/play-button.png"></div></a></li>                                        
                                          </ul>
                                       </div>                                    
                                    </div>	
                                </div>
                                                                

                            </div>
                        </div>
                        <div id="tab-3" class="tab-pane">
                            
                            <div class="row">				
                                <div class="col-sm-8 col-sm-offset-2">
                                   <div class="lightbox-gallery dark mrb35">
                                      <ul id="videos-without-poster2" class="list-unstyled row">
                                          
                                        <li class="video" data-src="https://www.youtube.com/watch?v=e-ORhEE9VVg" data-sub-html="Taylor Swift">
                                             
                                             <a href="">
                                                 <img class="img-responsive" src="https://templates.usu.edu/ldp/galleries/.private_ldp/a104300/production/thumb/9a655a8d-622a-4a43-849d-79de45846ce3.jpg" alt="Good Girls become Guide Dogs">
                                                 <div class="demo-gallery-poster">
                                                    <img src="images/video/play-button.png" alt=""/>
                                                 </div>
                                             </a>
                                                 
                                             
                                         </li>
                                         <li class="video" data-src="https://www.youtube.com/watch?v=dQw4w9WgXcQ" data-sub-html="Red Hat Club">
                                            <a href="">
                                                <img class="img-responsive" src="https://templates.usu.edu/ldp/galleries/.private_ldp/a104300/production/thumb/25df9b02-3c31-4e27-80a2-948cbe043a5d.jpg" alt="Red Hat Club">
                                                <div class="demo-gallery-poster">
                                                    <img src="images/video/play-button.png" alt=""/>
                                                </div>
                                            </a>
                                         </li>
                                         <li class="video" data-src="https://www.youtube.com/watch?v=PrQb19l7jOQ" data-sub-html="Curabitur non placerat lorem">
                                            <a href="">
                                                <img class="img-responsive" src="https://templates.usu.edu/ldp/galleries/.private_ldp/a104300/production/thumb/166b6e0e-156b-4e99-8409-fad627ee398a.jpg" alt="Curabitur non placerat lorem">
                                                <div class="demo-gallery-poster">
                                                    <img src="images/video/play-button.png" alt=""/>
                                                </div>
                                            </a>
                                         </li>
                                         <li class="video" data-src="https://www.youtube.com/watch?v=fDq3TK8id6w" data-sub-html="Lorem Ipsum Dolor Sit Amet Consecteteur">
                                             <a href="">
                                                <img class="img-responsive" src="https://templates.usu.edu/ldp/galleries/.private_ldp/a104300/production/thumb/09194722-27aa-4ea1-b6c4-c84cfeae6641.jpg" alt="Lorem Ipsum Dolor Sit Amet Consecteteur">
                                                    <div class="demo-gallery-poster">
                                                        <img src="images/video/play-button.png" alt=""/>
                                                    </div>
                                             </a>
                                         </li>
                                         <li class="video" data-src="https://www.youtube.com/watch?v=K1_JWrl3_NQ" data-sub-html="Lorem Ipsum Dolor Sit Amet Consecteteur">
                                            <a href="">
                                                <img class="img-responsive" src="https://templates.usu.edu/ldp/galleries/.private_ldp/a104300/production/thumb/ecaa08f9-b30e-4102-9f50-ae53bf539809.jpg" alt="Lorem Ipsum Dolor Sit Amet Consecteteur">
                                                <div class="demo-gallery-poster">
                                                    <img src="images/video/play-button.png" alt=""/>
                                                </div>
                                            </a>
                                         </li>
                                         <li class="video" data-src="https://www.youtube.com/watch?v=K1_JWrl3_NQ" data-sub-html="Lorem Ipsum Dolor Sit Amet Consecteteur">
                                            <a href="">
                                                <img class="img-responsive" src="https://templates.usu.edu/ldp/galleries/.private_ldp/a104300/production/thumb/9ec3b7e1-9f72-42c1-a8de-ec8a87721979.jpg" alt="Lorem Ipsum Dolor Sit Amet Consecteteur">
                                                <div class="demo-gallery-poster">
                                                    <img src="images/video/play-button.png" alt=""/>
                                                </div>
                                            </a>
                                         </li>
                                         <li class="video" data-src="https://www.youtube.com/watch?v=PrQb19l7jOQ" data-sub-html="Lorem Ipsum Dolor Sit Amet Consecteteur">
                                             <a href="">
                                                 <img class="img-responsive" src="https://templates.usu.edu/ldp/galleries/.private_ldp/a104300/production/thumb/e0ab68dc-f0a0-4b14-8bb5-9e82eba3fec1.jpg" alt="Lorem Ipsum Dolor Sit Amet Consecteteur">
                                                 <div class="demo-gallery-poster">
                                                    <img src="images/video/play-button.png" alt=""/>
                                                 </div>
                                             </a>
                                         </li>
                                         <li class="video" data-src="https://www.youtube.com/watch?v=fDq3TK8id6w" data-sub-html="A New Zealand Working Dog">
                                            <a href="">
                                                <img class="img-responsive" src="https://templates.usu.edu/ldp/galleries/.private_ldp/a104300/production/thumb/00a3c536-1152-4908-9418-490caf5e95e8.jpg" alt="A New Zealand Working Dog">
                                                <div class="demo-gallery-poster">
                                                    <img src="images/video/play-button.png" alt=""/>
                                                </div>
                                            </a>
                                         </li>
                                      </ul>
                                   </div>
                                </div>			
                            </div>
                        </div>    

                    </div>                                               
                </div>
                <div class="panel-footer">
                    <i class="fa fa-picture-o"> </i> 20 pimages
                </div>
            </div>
        </div>
    </div>
</div>
<!--<script src="scripts/video/jquery.blueimp-gallery.min.js" type="text/javascript"></script>-->
<script src="scripts/video/fileinput.min.js" type="text/javascript"></script>
<script src="scripts/video/bootstrap-select.min.js" type="text/javascript"></script>
<script src="scripts/video/accordionSnippet.js" type="text/javascript"></script>
<script src="scripts/video/tabSnippet.js" type="text/javascript"></script>
<script>
                       $(document).ready(function() {
                       $('.selectpicker').selectpicker();
                       });
</script>
<script src="scripts/video/bootstrap-datetimepicker.js" type="text/javascript"></script>
                <script type="text/javascript">
			$(".form_datetime").datetimepicker({
			format: 'mm-dd-yyyy H:ii P',
			autoclose: 1,
			showMeridian: 1
			});
			$(".form_date").datetimepicker({
			format: 'mm-dd-yyyy',
			autoclose: 1,
			showMeridian: 1,
			minView: 2,
			forceParse: 0
			});
			$(".form_time").datetimepicker({
			format: 'H:ii P',
			autoclose: 1,
			showMeridian: 1,
			startView: 1,
			minView: 0,
			maxView: 1,
			forceParse: 0
			});
		</script>
<script src="scripts/video/jquery.datetimepicker.full.min.js" type="text/javascript"></script>

<script src="scripts/video/picturefill.min.js" type="text/javascript"></script>
<script src="scripts/video/lightgallery.js" type="text/javascript"></script>
<script src="scripts/video/lg-fullscreen.js" type="text/javascript"></script>
<script src="scripts/video/lg-thumbnail.js" type="text/javascript"></script>
<script src="scripts/video/lg-video.js" type="text/javascript"></script>
<script src="scripts/video/lg-autoplay.js" type="text/javascript"></script>
<script src="scripts/video/demos.js" type="text/javascript"></script>
<script src="scripts/video/lg-zoom.js" type="text/javascript"></script>
<script src="scripts/video/lg-hash.js" type="text/javascript"></script>
<script src="scripts/video/lg-pager.js" type="text/javascript"></script>
<script src="scripts/video/jquery.mousewheel.min.js" type="text/javascript"></script>
<link href="scripts/video/lightgallery.css" rel="stylesheet" type="text/css"/>
<link href="scripts/video/videogallery.css" rel="stylesheet" type="text/css"/>