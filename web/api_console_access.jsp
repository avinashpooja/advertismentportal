<%@page import="org.bouncycastle.util.encoders.Base64"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.MethodName"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.Classes"%>
<%@page import="java.util.HashMap"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.Methods"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.Serializer"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.WarFileManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.Warfiles"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.TransformManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.TransformDetails"%>
<%@page import="java.util.Map"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.AccessPointManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.Accesspoint"%>
<%@include file="header.jsp"%> 
<%
    String _apid = request.getParameter("_apid");
    String _resId = request.getParameter("_resId");
//    String envt = request.getParameter("_envt");
//    String ver = request.getParameter("ver");
    int resId = Integer.parseInt(_resId);
    int apid = Integer.parseInt(_apid);
    session.setAttribute("_apId", apid);
    Accesspoint ap = new AccessPointManagement().getAccessPointById(SessionId, ChannelId, apid);
    String apName = ap.getName();
    Map classMap = null;
    Map methodMap = null;
    String value = apName;
    TransformDetails tf = new TransformManagement().getTransformDetails(SessionId, ChannelId, apid, resId);
    Warfiles warFiles = new WarFileManagement().getWarFile(SessionId, ChannelId, ap.getApId());

    byte[] me = tf.getMethods();
    byte[] cl = tf.getClasses();

    if (classMap == null) {
        classMap = new HashMap();
    }
    if (methodMap == null) {
        methodMap = new HashMap();
    }
    int version = 1;
    if (warFiles != null) {
        Map warMap = (Map) (Serializer.deserialize(warFiles.getWfile()));
        version = warMap.size() / 2;
    }
    Object obj = Serializer.deserialize(me);
    Map tmpMethods = (Map) obj;
    Methods methods = (Methods) tmpMethods.get("" + version);
    Object objC = Serializer.deserialize(cl);
    Map tempClass = (Map) objC;
    Classes classes = (Classes) tempClass.get("" + version);
    classMap.put(value, classes);
    methodMap.put(value, methods);
    session.setAttribute("eclasses", classMap);
    session.setAttribute("apiConsolemethods", methodMap);

    int c = classes.pname.classs.size();
%>
<!-- Main Wrapper -->
<div id="wrapper">
    <div class="small-header transition animated fadeIn">
        <div class="hpanel">
            <div class="panel-body">
                <div id="hbreadcrumb" class="pull-right">
                    <ol class="hbreadcrumb breadcrumb">
                        <li><a href="home.jsp">Dashboard</a></li>
                        <li><a href="api_console.jsp">API Console</a></li>
                        <li class="active">
                            <span><%=tf.getResourceName()%></span>
                        </li>
                    </ol>
                </div>
                <h2 class="font-light m-b-xs">
                    API Console - <%=tf.getResourceName()%>
                </h2>
                <small>Test API</small>
            </div>
        </div>
    </div>


    <div class="content animate-panel">
        <a href="<%=apiDocsURL%>apidocs.jsp?g=<%=parObj.getPartnerGroupId()%>" class="btn btn-success m-b-lg run-tour" target="_blank">Documentation</a>

        <a href="compose_email_technical.jsp" class="btn btn-danger m-b-lg run-tour" target="_blank">Helpdesk</a>
        
        <a href="token_manager.jsp" class="btn btn-primary2 m-b-lg run-tour">API Token</a>
        
<!--        <a href="compose_production.jsp" class="btn btn-primary m-b-lg run-tour">Subscribe to Production</a>-->

        <div class="row">
            <div class="col-lg-12">
                <div class="hpanel">
                    <div class="panel-heading">
                        <div class="panel-tools">
                            <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                        </div>
                        Available APIs
                    </div>
                    <div class="panel-body">
                        <table id="api" class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr >
                                    <th class="text-center">API Sequence</th>
                                    <th class="text-center">API Name</th>
                                    <!--					<th>Method</th>-->
                                    <th class="text-center">Return Type</th>
                                    <th class="text-center">Description</th>
                                    <th class="text-center"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <%
                                    String mName = "";
                                    List list = methods.methodClassName.methodNames;
                                    int count = 0;
                                    for (int i = 0; i < list.size(); i++) {
                                        String apiDescription = "NA";
                                        MethodName methodName = (MethodName) list.get(i);
                                        if (methodName.visibility.equalsIgnoreCase("yes")) {
                                            count++;
                                            mName = methodName.methodname.split("::")[0];
                                            if (!methodName.transformedname.equals("")) {
                                                mName = methodName.transformedname.split("::")[0];
                                            }
                                            if (methodName.description != null) {
                                                apiDescription = new String(Base64.decode(methodName.description));
                                            }
                                %>
                                <tr class="text-center">
                                    <td><%=count%></td>
                                    <td><%=mName%></td>
                                    <!--                        <td></td>-->
                                    <td><%=methodName.returntype%></td>
<!--                                    <td width="10px" style="overflow: hidden; width: 20px">
                                        <textarea class="form-control" disabled> <%=apiDescription%></textarea>
                                    </td>-->
                                    <td>
                                        <div class="tooltip-demo text-center">
                                            <button type="button" class="btn btn-sm btn-default" data-toggle="tooltip" data-placement="right" title="<%=apiDescription%>">API Description</button>
                                        </div>
                                    </td>
                                    <td><a class="btn btn-success btn-xs" target="_blank" href="api_console_soap.jsp?_resId=<%=_resId%>&_apid=<%=_apid%>&methodName=<%=mName%>&envt=Test"><i class="fa fa-code"></i> <span class="bold">Open Console</span></a></td>
                                </tr>
                                <%
                                        }
                                    }
                                %>

                            </tbody>
                        </table>

                    </div>
                </div>
            </div>

            <!-- Jack Remark: If data type no function for TM API, please hide it -->
            <div class="col-lg-12" style="display: none">
                <div class="hpanel">
                    <div class="panel-heading">
                        <div class="panel-tools">
                            <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                        </div>
                        Available Data Type
                    </div>
                    <div class="panel-body">
                        <table id="data_type" class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Data Type</th>
                                    <th>Description</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>SgHttpStatus</td>
                                    <td><a class="btn btn-info btn-xs" href="api_datatype_description.jsp"><i class="fa fa-info"></i> <span class="bold">Access</span></a></td>
                                </tr>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>

        </div>

        <%@include file="footer.jsp"%> 


        <script>

            $(function () {

                // Initialize Example 2
                $('#api').dataTable();

            });

            $(function () {

                // Initialize Example 2
                $('#data_type').dataTable();

            });

        </script>