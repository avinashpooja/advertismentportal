<%@page import="com.mollatech.serviceguard.nucleus.db.SgAdvertiseSubscriptionDetails"%>
<%@page import="org.json.JSONObject"%>
<script src="scripts/pdfAd.js" type="text/javascript"></script>
<link href="css/global.css" rel="stylesheet" type="text/css"/>
<link href="css/inner-page.css" rel="stylesheet" type="text/css"/>
     <script src="js/Materialize.js" type="text/javascript"></script>
     
        
            <div class="container mobile-pt-70 pb-30" >
    <div class="row" style="margin-left: -15px;
    margin-right: -15px;">
        <div class="col-sm-12">
                    <h2 style="font-size: 24px;color: #666;padding-left: 60px;position: relative;padding: 10px 15px;    font-weight: 400;"><i class="fa fa-file-pdf-o fa-2x" style="font-size:34px;color:#18ce2a"></i>&nbsp;&nbsp;PDF AD</h2>
</div>

<%
    SgAdvertiseSubscriptionDetails subscriptionDetails = (SgAdvertiseSubscriptionDetails) request.getSession().getAttribute("advertiserSubscriptionObject") ;
    String pdfImageLength = ""; String message = "NA"; int width = 0; int height = 0;  
    if(subscriptionDetails != null){
        String pdfAdDetails = subscriptionDetails.getPdfAdConfiguration();
        JSONObject pdfDetails = new JSONObject(pdfAdDetails);
        if(pdfDetails.has("pdfImageLength")){
            pdfImageLength = pdfDetails.getString("pdfImageLength");
        }
    }
    if(!pdfImageLength.isEmpty()){
        if(pdfImageLength.equalsIgnoreCase("1")){
           message = "Upload Advertisement image. Support Image File size limit 5mb with at least Width = 1024, Height = 768";
           width = 1024;
           height = 768;
        }else if(pdfImageLength.equalsIgnoreCase("1/2")){
           message = "Upload Advertisement image. Support Image File size limit 5mb with at least Width = 640, Height = 480";
           width = 640;
           height = 480;
        }else if(pdfImageLength.equalsIgnoreCase("1/4")){
           message = "Upload Advertisement image. Support Image File size limit 5mb with at least Width = 368, Height = 245";
           width = 368;
           height = 245;
        }
    }
%>
        <div class="col-md-9 col-sm-8 col-xs-12" >
            
            <div class="hpanel" >
                <div class="panel-body white-box radius-4 green-bottom-border xs-mg-b60">                                                                                                                           
                    <h6><span style="font-size:18px;color: #333;font-weight: 600;font-family: 'Source Sans Pro', sans-serif;color: #4CAF50">Logo</span></h6>
                                       <h6 style="margin-bottom: 2%;font-size:12px;margin-top: 2%;color: #999;">Drop advertisement image here to upload</h6>
                    <div style="background: #fff; height: 200px">
                        <div class="col-sm-8 col-lg-8 col-md-8" style="width: 100%;">
                            <form action="UploadEmailAttachme" method="post" class="dropzone" id="my-dropzone">
                                <div class="dz-default dz-message"><span class="sprite doc-iocn mb-10"></span><br><p class="mb-0" style="text-align: center;">You can drop or <strong>browse</strong>  jpg, png file here to upload<br>
                               <span class=""><small class="note needsclick fs-12 c9"><%=message%></small>
</span></p></div></form>         
                               </div>
                    </div> 
                    <br><br>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <div class="col-sm-8">
                            <a style="font-size:15px;" class="btn btn-default" href="./header.jsp">Cancel</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;                     
                            <a class="btn btn-default" id="partUpdate" data-style="zoom-in"  onclick="updatePDFAd()">Save</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
                                <div class="col-md-3 col-sm-4 col-xs-12 mobile-mt-30">
            <div class="white-box radius-4 green-bottom-border xs-mg-b60">
              <h2 class="widget-title">Others</h2>
              <ul class="right-nav">
                                  <li class="tour-4"><a  onclick="pushAD()"><i class="fa fa-commenting-o fa-2x" style="font-size:40px;position: absolute;left: 8px;   top: 6px;    transform: scale(.7); background-position: 0 -96px;color:#18ce2a"></i>&nbsp;&nbsp;Push Ad  </a>  </li>
                        
                                                <li class="tour-5"><a  onclick="emailAD()"><i class="fa fa-envelope-o" style="font-size:40px;position: absolute;left: 8px;   top: 6px;    transform: scale(.7); background-position: 0 -96px;color:#18ce2a"></i>&nbsp;&nbsp;Email Ad</a></li> 
                        
                                                <li class="tourVideo"><a  onclick="pdfAD()"><i class="fa fa-file-pdf-o fa-2x" style="font-size:40px;position: absolute;left: 8px;   top: 6px;    transform: scale(.7); background-position: 0 -96px;color:#18ce2a"></i>&nbsp;&nbsp;PDF Ad</a></li>                                		  

                                                 <li class="tourVideo"><a  onclick="myInvoice()"><i class="fa fa-file-text-o" style="font-size:40px;position: absolute;left: 8px;   top: 6px;    transform: scale(.7); background-position: 0 -96px;color:#18ce2a"></i>&nbsp;&nbsp;Invoices</a></li>                                		  
			  		  
              </ul>
            </div><!-- /.white box -->
</div>
    </div>
</div>
<script>
    $(document).ready(function () {        
        Dropzone.autoDiscover = false;
        var myDropzone = new Dropzone("#my-dropzone", {
            url: "UploadPDFAdImage",
            uploadMultiple: false,            
            maxFilesize: 5,
            maxFiles: 1,
            acceptedFiles: "image/*",
            dictInvalidFileType: "You can't upload files of this type, only Image file",
            autoProcessQueue: true,
            parallelUploads: 1,
            addRemoveLinks: true,
            dictDefaultMessage: "Drop advertisement image here to upload",
            init: function() {
                // Register for the thumbnail callback.
                // When the thumbnail is created the image dimensions are set.
                this.on("thumbnail", function(file) {
                  // Do the dimension checks you want to do
                  console.log("file.width == "+file.width);
                  console.log("file.height == "+file.height);                  
                  if (file.width < <%=width%> || file.height < <%=height%>) {
                    file.rejectDimensions()
                  }
                  else {
                    file.acceptDimensions();
                    //file.rejectDimensions()
                  }
                });
              },
              // Instead of directly accepting / rejecting the file, setup two
            // functions on the file that can be called later to accept / reject
            // the file.
            accept: function(file, done) {
              file.acceptDimensions = done;
              file.rejectDimensions = function() { done("The image must be at least <%=width%> x <%=height%>"); };
              // Of course you could also just put the `done` function in the file
              // and call it either with or without error in the `thumbnail` event
              // callback, but I think that this is cleaner.
            },
            removedfile: function(file) {
                removePDFAdImageFromSession(file.name);    
                var _ref;
                if (file.previewElement) {
                    if ((_ref = file.previewElement) != null) {
                        _ref.parentNode.removeChild(file.previewElement);
                    }
                }
                return this._updateMaxFilesReachedClass();
            }
        });
    });
</script>

