<%@include file="header.jsp"%>

<!-- Main Wrapper -->
<div id="wrapper">

    <div class="small-header transition animated fadeIn">
        <div class="hpanel">
            <div class="panel-body">
                <div id="hbreadcrumb" class="pull-right">
                    <ol class="hbreadcrumb breadcrumb">
                        <li><a href="home.jsp">Dashboard</a></li>
                        <li class="active">
                            <span>Email</span>
                        </li>
                    </ol>
                </div>
                <h2 class="font-light m-b-xs">
                    Compose Email
                </h2>
                <small>Short description to developer what is this page about</small>
            </div>
        </div>
    </div>

    <div class="content animate-panel">
        <div class="row">
            <div class="col-md-12">
                <div class="hpanel email-compose">
                    <div class="panel-heading hbuilt">
                        <div class="p-xs h4">
                            New message
                        </div>
                    </div>
                    <div class="panel-heading hbuilt">
                        <div class="p-xs">
                            <form method="get" class="form-horizontal">
                                <div class="form-group"><label class="col-sm-2 control-label text-left">To:</label>
                                    <div class="col-sm-10"><input type="text" class="form-control input-sm" value="help@tab.com.my" disabled></div>
                                </div>
                                <div class="form-group"><label class="col-sm-2 control-label text-left">Contact:</label>
                                    <div class="col-sm-10"><input type="text" class="form-control input-sm"></div>
                                </div>
                                <div class="form-group"><label class="col-sm-2 control-label text-left">Subject:</label>
                                    <div class="col-sm-10">
                                        <select class="js-source-states" style="width: 100%">
                                            <optgroup label="General">
                                                <option value="sales">Sales Enquiry</option>
                                                <option value="billing">Billing Enquiry</option>
                                            </optgroup>
                                            <optgroup label="Technical">
                                                <option value="technical">Technical Support</option>
                                            </optgroup>
                                            <optgroup label="Profile">
                                                <option value="mobile_phone">Request Change Mobile Phone No.</option>
                                            </optgroup>
                                        </select>
                                    </div>
                                </div>
                            </form>
                            <div style="background: #fff; height: 200px">
                                <div class="col-sm-12">
                                    <form action="/upload-target" class="dropzone"></form>
                                    <small>Upload attachment. Support jpeg, gif, png, pdf. File size limit 5mb</small>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body no-padding">
                        <div class="summernote">
                            <h5>Hello Jonathan! </h5>

                            <p>dummy text of the printing and typesetting industry. <strong>Lorem Ipsum has been the dustrys</strong> standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more
                                <br/><br/>All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable.
                                recently with.</p>

                            <p>Robert Razer
                            </p>
                        </div>
                    </div>


                    <div class="panel-footer">
                        <div class="pull-right">
                            <div class="btn-group">
                            </div>
                        </div>
                        <button class="btn btn-primary">Send email</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(function () {

            // Initialize summernote plugin
            $('.summernote').summernote({
                toolbar: [
                    ['headline', ['style']],
                    ['style', ['bold', 'italic', 'underline', 'superscript', 'subscript', 'strikethrough', 'clear']],
                    ['textsize', ['fontsize']],
                    ['alignment', ['ul', 'ol', 'paragraph', 'lineheight']],
                ]
            });

        });
    </script>

    <!-- Footer-->
    <%@include file="footer.jsp"%>