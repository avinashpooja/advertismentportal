<%@page import="java.util.Calendar"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.GlobalStatus"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONArray"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PackageSubscriptionManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgSubscriptionDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.ResourceManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.ResourceDetails"%>
<%@page import="java.util.List"%>
<%@page import="java.net.URL"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.PartnerDetails"%>
<%@page import="com.sun.xml.internal.messaging.saaj.util.Base64"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.MethodName"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.Classes"%>
<%@page import="java.util.HashMap"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.Methods"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.Serializer"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.WarFileManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.Warfiles"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.TransformManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.TransformDetails"%>
<%@page import="java.util.Map"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.AccessPointManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.Accesspoint"%>
<%
    String _apidReq = request.getParameter("_apid");
    String _resId = request.getParameter("_resId");
    String[] apID = _apidReq.split(":");
    int resId = Integer.parseInt(apID[1]);
    int apid = Integer.parseInt(apID[0]);
    session.setAttribute("_apId", apid);
    String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
    String ChannelId = (String) request.getSession().getAttribute("_ChannelId");
    PartnerDetails parObj = (PartnerDetails) request.getSession().getAttribute("_partnerDetails");

    // Get Credit Deduction of API as per type
    SgSubscriptionDetails subscriObjectByDev = new PackageSubscriptionManagement().getPartnerSubscriptionbyPId(parObj.getPartnerId());

    String appurl = (request.getRequestURL().toString());
    URL myAppUrl = new URL(appurl);
    int port = myAppUrl.getPort();
    if (myAppUrl.getProtocol().equals("https") && port == -1) {
        port = 443;
    } else if (myAppUrl.getProtocol().equals("http") && port == -1) {
        port = 80;
    }
    String apiDocsURL = myAppUrl.getProtocol() + "://" + myAppUrl.getHost() + ":" + port + "/APIDoc" + "/";
    Accesspoint ap = new AccessPointManagement().getAccessPointById(SessionId, ChannelId, apid);
    ResourceDetails rs = new ResourceManagement().getResourceById(resId);
    String resourceName = rs.getName();
    String apName = ap.getName();
    int accesspointId = ap.getApId();
    int resourceId = rs.getResourceId();
    Map classMap = null;
    Map methodMap = null;
    String value = apName;
    TransformDetails tf = new TransformManagement().getTransformDetails(SessionId, ChannelId, apid, resId);
    Warfiles warFiles = new WarFileManagement().getWarFile(SessionId, ChannelId, ap.getApId());

    byte[] me = tf.getMethods();
    byte[] cl = tf.getClasses();

    if (classMap == null) {
        classMap = new HashMap();
    }
    if (methodMap == null) {
        methodMap = new HashMap();
    }
    int version = 1;
    if (warFiles != null) {
        Map warMap = (Map) (Serializer.deserialize(warFiles.getWfile()));
        version = warMap.size() / 2;
    }
    String keyPackage = ap.getName() + ":" + resourceName + ":" + version;
    Object obj = Serializer.deserialize(me);
    Map tmpMethods = (Map) obj;
    Methods methods = (Methods) tmpMethods.get("" + version);
    Object objC = Serializer.deserialize(cl);
    Map tempClass = (Map) objC;
    Classes classes = (Classes) tempClass.get("" + version);
    classMap.put(value, classes);
    methodMap.put(value, methods);
    session.setAttribute("eclasses", classMap);
    session.setAttribute("apiConsolemethods", methodMap);

    int c = classes.pname.classs.size();

    JSONObject valueJson = null;
    if (subscriObjectByDev != null) {
        String apRate = subscriObjectByDev.getApRateDetails();
        JSONArray jsOld = new JSONArray(apRate);
        for (int j = 0; j < jsOld.length(); j++) {
            JSONObject jsonexists1 = jsOld.getJSONObject(j);
            Iterator<String> keys = jsonexists1.keys();
            while (keys.hasNext()) {
                String keyData = keys.next();
                if (keyData.equalsIgnoreCase(keyPackage)) {
                    valueJson = jsonexists1.getJSONObject(keyData);
                    break;
                }
            }
        }
    }

//    System.out.println("monthFormatStr >> "+monthFormatStr);
%>
<style>
    .col-centered{
        float: none;
        margin: 0 auto;
    }
</style>
<!-- Main Wrapper -->   
<div id="wrapper">
    <div class="col-lg-6">
        <div class="modal fade hmodal-success" id="todayExpenditure" tabindex="-1" role="dialog"  aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="color-line"></div>
                    <div class="modal-header" style="padding: 10px 0px !important">
                        <h4 id="todayCreditForAPILable" style="margin-left: 2%">Today Credit Used for
                            <button class="close btn btn-default" data-dismiss="modal" aria-hidden="true" style="margin-top: 0%; margin-right: 3%">x</button></h4>                                    
                    </div>
                    <div class="modal-body" style="padding-bottom: 50px !important;text-align: center;">                                
                        <div id="todayCreditForAPI">                        
                        </div>
                        <div id="noRecordFoundData" style="display: none;text-align: center;" style="margin-bottom: 30%">
                            <img src="images/no_record_found.png" alt="No record found" width="300px" height="200px"/>
                        </div> 
                    </div>                                
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="modal fade hmodal-success" id="todayPerformance" tabindex="-1" role="dialog"  aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="color-line"></div>
                    <div class="modal-header" style="padding: 10px 0px !important">
                        <h4 id="todayPerformanceForAPILable" style="margin-left: 2%">Today Performance of
                            <button class="close btn btn-default" data-dismiss="modal" aria-hidden="true" style="margin-top: 0%; margin-right: 3%">x</button></h4>                                    
                    </div>
                    <div class="modal-body" style="padding-bottom: 50px !important">                                
                        <div id="todayPerformanceForAPI">                        
                        </div>
                        <div id="noRecordFoundDataPerforamce" style="display: none; text-align: center" style="margin-bottom: 30%">
                            <img src="images/no_record_found.png" alt="No record found" width="300px" height="200px"/>
                        </div>                         
                    </div>                                
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="modal fade hmodal-success" id="monthlyPerformance" tabindex="-1" role="dialog"  aria-hidden="true">
            <div class="modal-dialog modal-lg" >
                <div class="modal-content" >
                    <div class="color-line"></div>
                    <div class="modal-header" style="padding: 10px 0px !important">
                        <h4  style="margin-left: 2%"> Monthly Performance of <font id="monthlyPerformanceForAPILable"></font>
                            <button class="close btn btn-default" data-dismiss="modal" aria-hidden="true" style="margin-top: 0%; margin-right: 3%">x</button></h4>                                    
                    </div>
                    <div class="modal-body">
                        <div style="margin-bottom: 20px;">
                            <input type="hidden" id="apiForMothlyPerformance" name="apiForMothlyPerformance">
                            <div class="col-sm-3">
                                <select class="form-control" id="_apiCallMonth" name="_apiCallMonth" onchange="generatePerformanceByMonth()">                                                                        
                                    <%                                        SimpleDateFormat format = new SimpleDateFormat("MMM");
                                        Calendar cal = Calendar.getInstance();
                                        cal.setTime(new Date());
                                        cal.add(Calendar.MONTH, -6);
                                        Calendar updatedCal = Calendar.getInstance();
                                        updatedCal.setTime(new Date());
                                        for (int month = 1; month <= 6; month++) {
                                            int calMonth = cal.get(Calendar.MONTH) + month;
                                            updatedCal.set(Calendar.MONTH, calMonth);
                                            Date upDate = updatedCal.getTime();
                                            String newMonth = format.format(upDate);
                                    %>
                                    <%if (month == 6) {%>
                                    <option value="<%=calMonth + 1%>" selected><%=newMonth%></option> 
                                    <%} else {%>
                                    <option value="<%=calMonth + 1%>"><%=newMonth%></option> 
                                    <%
                                            }
                                        }
                                    %>

                                </select>
                            </div>

                            <!--<div class="col-sm-3"><button id="generatePerformanceByMonth" class="btn btn-success btn-sm ladda-button btn-block" data-style="zoom-in" onclick="generatePerformanceByMonth()"><i class="fa fa-bar-chart"></i> Generate</button></div>-->
                        </div>
                        <br/>
                        <div id="monthlyPerformanceForAPI">                        
                        </div>
                        <div id="noRecordFoundDataPerformance" style="display: none;text-align: center" style="margin-bottom: 27%">
                            <img src="images/no_record_found.png" alt="No record found" width="300px" height="200px"/>
                        </div> 
                    </div>                                
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="modal fade hmodal-success" id="monthlyCredit" tabindex="-1" role="dialog"  aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="color-line"></div>
                    <div class="modal-header" style="padding: 10px 0px !important">
                        <h4 style="margin-left: 2%"> Monthly Credit of <font id="monthlyCreditForAPILable" ></font>
                            <button class="close btn btn-default" data-dismiss="modal" aria-hidden="true" style="margin-top: 0%; margin-right: 3%">x</button></h4>                                    
                    </div>
                    <div class="modal-body" style="padding-bottom: 50px !important">                                
                        <div style="margin-bottom: 20px;" class="col-centered">
                            <input type="hidden" id="apiForMothlyCredit" name="apiForMothlyCredit">
                            <div class="col-sm-3">
                                <select class="form-control" id="_apiCallMonthMonthlyCredit" name="_apiCallMonthMonthlyCredit" onchange="generateCreditByMonth()">                                                                        

                                    <%
                                        cal.setTime(new Date());
                                        cal.add(Calendar.MONTH, -6);
                                        updatedCal.setTime(new Date());
                                        for (int month = 1; month <= 6; month++) {
                                            int calMonth = cal.get(Calendar.MONTH) + month;
                                            updatedCal.set(Calendar.MONTH, calMonth);
                                            Date upDate = updatedCal.getTime();
                                            String newMonth = format.format(upDate);
                                    %>
                                    <%if (month == 6) {%>
                                    <option value="<%=calMonth + 1%>" selected><%=newMonth%></option> 
                                    <%} else {%>
                                    <option value="<%=calMonth + 1%>"><%=newMonth%></option> 
                                    <%
                                            }
                                        }
                                    %>
                                </select>
                            </div>
                        </div>
                        <br><br>
                        <div id="monthlyCreditForAPI">                        
                        </div>
                        <div id="noRecordFoundDataMonthlyCredit" style="display: none ;text-align: center;" style="margin-bottom: 30%">
                            <img src="images/no_record_found.png" alt="No record found" width="300px" height="200px"/>
                        </div> 


                    </div>                                
                </div>
            </div>
        </div>
    </div>
</div>

<div class="content">
    <div class="row">
        <div class="col-md-12 tour-13">
            <div class="hpanel">
                <div class="panel-body">
                    <input type="text" class="form-control input-sm m-b-md" id="filter" placeholder="Search in table">
                    <!--                        <table id="api" class="table table-striped table-bordered table-responsive table-hover">-->
                    <form id="assign_token_form" name="assign_token_form" role="form">
                        <input type="hidden" name="apName" id="apName" value="<%=accesspointId%>">
                        <input type="hidden" name="resourceName" id="resourceName" value="<%=resourceId%>">
                        <input type="hidden" name="version" id="version" value="<%=version%>">
                        <input type="hidden" name="envt" id="envt" value="Live">
                        <input type="hidden" name="apNameAT" id="apNameAT" value="<%=apName%>">
                        <input type="hidden" name="resourceNameAT" id="resourceNameAT" value="<%=resourceName%>">                        
                    </form>
                    <div class="table-responsive">
                        <table id="api" class="footable table table-stripped table-responsive " data-page-size="25" data-filter=#filter>    
                            <thead>
                                <tr>                                    
                                    <th style="text-align: center">API Name</th>
                                    <th style="text-align: center" class="tour-14">API Brief</th>                                    
                                    <th style="text-align: center" class="tour-15">Credits Used</th>
                                    <th style="text-align: center" class="tour-16">Performance</th>
                                    <th data-hide="phone,tablet" style="text-align: center">Credits</th>                                    
                                    <th style="text-align: center" class="tour-17">Test</th>
                                </tr>
                            </thead>
                            <tbody>
                                <%
                                    String mName = "";
                                    List list = methods.methodClassName.methodNames;
                                    int count = 0;
                                    for (int i = 0; i < list.size(); i++) {
                                        String apiDescription = "NA";
                                        MethodName methodName = (MethodName) list.get(i);
                                        if (methodName.visibility.equalsIgnoreCase("yes")) {
                                            count++;
                                            mName = methodName.methodname.split("::")[0];
                                            if (!methodName.transformedname.equals("")) {
                                                mName = methodName.transformedname.split("::")[0];
                                            }
                                            if (methodName.description != null) {
                                                apiDescription = Base64.base64Decode(methodName.description);
                                            }
                                %>
                                <tr>                                   
                                    <td  style="font-size: 15px"><%=mName%></td>
                                    <%if (count == 1) {%>
                                    <td style="text-align: center">
                                        <input type="hidden" name="apiName" id="apiName" value="<%=mName%>">
                                        <a class="text-warning ladda-button" data-style="contract" id="reAssignToken<%=i%>" onclick="confirmAssignToken('<%=mName%>', '<%=i%>')" type="button" href="#" data-toggle="tooltip" data-placement="right" title="Regenerate API Token"><i class="pe pe-7s-refresh-cloud pe-3x"></i></a>                                                    
                                        <a class="text-info ladda-button" data-style="contract" id="viewToken<%=i%>" onclick="viewAPIToken('<%=mName%>', '<%=i%>')" type="button" href="#" data-toggle="tooltip" data-placement="right" title="View API Token" style="margin-left: 5%"><i class="pe pe-7s-display2 pe-3x"></i></a>
    <!--                                        <a  class="text-warning" data-toggle="tooltip" data-placement="right" title="<%=apiDescription%>" style="margin-left: 5%"><i class="pe pe-7s-news-paper pe-3x"></i></a>-->
                                        <a  class="text-warning ladda-button" data-style="zoom-in" data-toggle="tooltip" id="viewApiDes<%=i%>" data-placement="right" title="<%=apiDescription%>" onclick="viewAPIDes('<%=mName%>', '<%=apiDescription%>', '<%=i%>')" style="margin-left: 5%"><i class="pe pe-7s-news-paper pe-3x"></i></a>
                                        <a href="<%=apiDocsURL%>apidocs.jsp?g=<%=parObj.getPartnerGroupId()%>#<%=mName%>" class="text-danger" type="button" href="#" data-toggle="tooltip" data-placement="right" title="API Documentation" target="_blank" style="margin-left: 5%"><i class="pe pe-7s-note2 pe-3x"></i></a> 
                                    </td>              
                                    <%} else {%>
                                    <td style="text-align: center">
                                        <input type="hidden" name="apiName" id="apiName" value="<%=mName%>">
                                        <a class="text-warning ladda-button" data-style="contract" id="reAssignToken<%=i%>" onclick="confirmAssignToken('<%=mName%>', '<%=i%>')" type="button" href="#" data-toggle="tooltip" data-placement="right" title="Regenerate API Token"><i class="pe pe-7s-refresh-cloud pe-3x"></i></a>                                                    
                                        <a class="text-info ladda-button" data-style="contract" id="viewToken<%=i%>" onclick="viewAPIToken('<%=mName%>', '<%=i%>')" type="button" href="#" data-toggle="tooltip" data-placement="right" title="View API Token" style="margin-left: 5%"><i class="pe pe-7s-display2 pe-3x"></i></a>
    <!--                                        <a  class="text-warning" data-toggle="tooltip" data-placement="right" title="<%=apiDescription%>" style="margin-left: 5%"><i class="pe pe-7s-news-paper pe-3x"></i></a>-->
                                        <a  class="text-warning ladda-button" data-style="zoom-in" data-toggle="tooltip" id="viewApiDes<%=i%>" data-placement="right" title="<%=apiDescription%>" onclick="viewAPIDes('<%=mName%>', '<%=apiDescription%>', '<%=i%>')" style="margin-left: 5%"><i class="pe pe-7s-news-paper pe-3x"></i></a>
                                        <a href="<%=apiDocsURL%>apidocs.jsp?g=<%=parObj.getPartnerGroupId()%>#<%=mName%>" class="text-danger" type="button" href="#" data-toggle="tooltip" data-placement="right" title="API Documentation" target="_blank" style="margin-left: 5%"><i class="pe pe-7s-note2 pe-3x"></i></a> 
                                    </td>   
                                    <%}%>
                                    <%if (count == 1) {%>
                                    <td style="text-align: center">
                                        <a class="text-error ladda-button" data-style="zoom-in" id="todayCredit<%=i%>" onclick="todayCredit('<%=mName%>', '<%=i%>')" type="button" href="#" data-toggle="tooltip" data-placement="right" title="Today Credit Used for <%=mName%>"><i class="pe pe-7s-graph2 pe-3x"></i></a>                                                    
                                        <a class="text-primary-2 ladda-button" data-style="zoom-in" id="monthlyCredit<%=i%>" onclick="monthlyCreditChart('<%=mName%>', '<%=i%>')" type="button" href="#" data-toggle="tooltip" data-placement="right" title="Monthly Credit Used for <%=mName%>" style="margin-left: 10%"><i class="pe pe-7s-display1 pe-3x"></i></a>
                                    </td>
                                    <%} else {%>
                                    <td style="text-align: center">
                                        <a class="text-error ladda-button" data-style="zoom-in" id="todayCredit<%=i%>" onclick="todayCredit('<%=mName%>', '<%=i%>')" type="button" href="#" data-toggle="tooltip" data-placement="right" title="Today Credit Used for <%=mName%>"><i class="pe pe-7s-graph2 pe-3x"></i></a>                                                    
                                        <a class="text-primary-2 ladda-button" data-style="zoom-in" id="monthlyCredit<%=i%>" onclick="monthlyCreditChart('<%=mName%>', '<%=i%>')" type="button" href="#" data-toggle="tooltip" data-placement="right" title="Monthly Credit Used for <%=mName%>" style="margin-left: 10%"><i class="pe pe-7s-display1 pe-3x"></i></a>
                                    </td>
                                    <%}%>
                                    <td style="text-align: center">
                                        <a class="text-error ladda-button" data-style="zoom-in" id="todayPerformance<%=i%>" onclick="todayPerformance('<%=mName%>', '<%=i%>')" type="button" href="#" data-toggle="tooltip" data-placement="right" title="Today Performance of <%=mName%>"><i class="pe pe-7s-graph2 pe-3x"></i></a>                                                    
                                        <a class="text-primary-2 ladda-button" data-style="zoom-in" id="monthlyPerformance<%=i%>" onclick="monthlyPerformance('<%=mName%>', '<%=i%>')" type="button" href="#" data-toggle="tooltip" data-placement="right" title="Monthly Performace of <%=mName%>" style="margin-left: 10%"><i class="pe pe-7s-display1 pe-3x"></i></a>
                                    </td>
                                    <td style="text-align: center">
                                        <%
                                            String creditUsedDesc = "NA";
                                            if (valueJson != null && valueJson.has(mName)) {
                                                creditUsedDesc = valueJson.getString(mName);
                                                String[] valueArr = creditUsedDesc.split(":");
                                                switch (Integer.parseInt(valueArr[1])) {
                                                    case GlobalStatus.Per_User_Per_Renewal:
                                                        if (Double.parseDouble(valueArr[0]) == 1) {
                                                            creditUsedDesc = Double.parseDouble(valueArr[0]) + " Credit/Renewal";
                                                        } else {
                                                            creditUsedDesc = Double.parseDouble(valueArr[0]) + " Credits/Renewal";
                                                        }
                                                        break;
                                                    case GlobalStatus.Per_API:
                                                        if (Double.parseDouble(valueArr[0]) == 1) {
                                                            creditUsedDesc = Double.parseDouble(valueArr[0]) + " Credit/API";
                                                        } else {
                                                            creditUsedDesc = Double.parseDouble(valueArr[0]) + " Credits/API";
                                                        }

                                                        break;
                                                    case GlobalStatus.Per_Entry_Per_API:
                                                        if (Double.parseDouble(valueArr[0]) == 1) {
                                                            creditUsedDesc = Double.parseDouble(valueArr[0]) + " Credit/Entry";
                                                        } else {
                                                            creditUsedDesc = Double.parseDouble(valueArr[0]) + " Credits/Entry";
                                                        }

                                                        break;
                                                    default:
                                                        creditUsedDesc = "NA";
                                                        break;
                                                }
                                            }
                                        %>
                                        <p style="font-size: 15px"><%=creditUsedDesc%></p>
                                    </td>
                                    <%if (count == 1) {%>
                                    <td style="text-align: center" class="tour-iconAPIConsole">
    <!--                                        <a class="text-success" href="testAPI.jsp?_resId=<%=apID[1]%>&_apid=<%=apID[0]%>&methodName=<%=mName%>&envt=Production" data-toggle="tooltip" data-placement="right" title="Test <%=mName%>"><i class="pe pe-7s-science pe-3x"></i></a>                                        -->
                                        <a onclick = "testApi('<%=apID[1]%>', '<%=apID[0]%>', '<%=mName%>', 'Production')" class="text-success" data-toggle="tooltip" data-placement="right" title="Test <%=mName%>" id="testFirstAPI"><i class="pe pe-7s-science pe-3x"></i></a>
                                    </td>
                                    <%} else {%>
                                    <td style="text-align: center">
   <!--                                        <a class="text-success" href="testAPI.jsp?_resId=<%=apID[1]%>&_apid=<%=apID[0]%>&methodName=<%=mName%>&envt=Production" data-toggle="tooltip" data-placement="right" title="Test <%=mName%>"><i class="pe pe-7s-science pe-3x"></i></a>                                        -->
                                        <a onclick = "testApi('<%=apID[1]%>', '<%=apID[0]%>', '<%=mName%>', 'Production')" class="text-success" data-toggle="tooltip" data-placement="right" title="Test <%=mName%>"><i class="pe pe-7s-science pe-3x"></i></a>
                                    </td>
                                    <%}%>
                                </tr>
                                <%
                                        }
                                    }
                                %>

                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="6">
                                        <ul class="pagination pull-right"></ul>
                                    </td>
                                </tr>
                            </tfoot>

                        </table>
                    </div>                   
                </div>
            </div>
        </div>
        <div class="col-md-12" style="margin-bottom: 23%"></div>
    </div>
</div>
<script>
    $(function () {
        // Initialize Example 2
        //$('#data_type').dataTable();
        $('#api').footable();
    });
</script>