<%-- 
    Document   : thankYouPage
    Created on : Nov 23, 2017, 1:53:20 PM
    Author     : abhishekingle
--%>

<%@page import="com.mollatech.serviceguard.nucleus.commons.MSConfig"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.SettingsManagement"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Ready APIs</title>
        <link rel="stylesheet" href="vendor/fontawesome/css/font-awesome.css" />
        <link rel="stylesheet" href="vendor/metisMenu/dist/metisMenu.css" />
        <link rel="stylesheet" href="vendor/animate.css/animate.css" />
        <link rel="stylesheet" href="vendor/bootstrap/dist/css/bootstrap.css" />
        <link rel="stylesheet" href="vendor/sweetalert/lib/sweet-alert.css" />
        <link rel="stylesheet" href="vendor/ladda/dist/ladda-themeless.min.css" />
        <link rel="stylesheet" href="vendor/toastr/build/toastr.min.css" />
        <script src="scripts/utilityFunction.js" type="text/javascript"></script>
        <script src="scripts/partnerRequest.js" type="text/javascript"></script>

        <!-- App styles -->
        <link rel="stylesheet" href="fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css" />
        <link rel="stylesheet" href="fonts/pe-icon-7-stroke/css/helper.css" />
        <link rel="stylesheet" href="styles/style.css">
        <style>
/* The Modal (background) */
.modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    padding-top: 100px; /* Location of the box */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
/*    background-color: rgba(0,0,0,0.4);  Black w/ opacity */
}

/* Modal Content */
.modal-content {
    background-color: #fefefe;
    margin: auto;
    padding: 20px;
    border: 1px solid #888;
    width: 50%;
}

/* The Close Button */
.close {
    color: #aaaaaa;
    float: right;
    font-size: 28px;
    font-weight: bold;
}

.close:hover,
.close:focus {
    color: #000;
    text-decoration: none;
    cursor: pointer;
}
</style>
<%
    Object sobject = null;
        sobject = new SettingsManagement().getSetting(SettingsManagement.MATERSLAVE, SettingsManagement.PREFERENCE_ONE);
        MSConfig msConfig = (MSConfig) sobject;
        String ssl = "http";
        if (msConfig.ssl.equalsIgnoreCase("yes")) {
            ssl = "https";
        }

        String apiDocsURL = ssl + "://" + msConfig.aphostIp + ":" + msConfig.aphostPort + "/" + "APIDoc/";
%>
    </head>
    <body>
        <div id="myModal" class="modal">
            <div class="modal-content">
                <span class="close">&times;</span>
                    <div style="text-align: center">
                        <h1 class="text-success" style="font-size: 50px"><b><i class="fa fa-check fa-2xx text-success"></i> Thanks! </b></h1>
                        <p style="font-size: 20px; font-family: Times New Roman, Georgia, Serif;"> You have been successfully sign in</p>
                        <p style="font-size: 20px; font-family: Times New Roman, Georgia, Serif;">Check your email for your newly issued password.</p>
                        <p style="font-size: 20px; font-family: Times New Roman, Georgia, Serif;">Insight dashboard you can explore your work with APIs.<br></p>                                                                                                
                    </div>
<!--                <p style="font-size: 18px;font-family: Times New Roman">Can't wait for more experts insights? We recommend that you sign up for this Wednesday Live Webiner. It's happening at 3 p.m. Eastern and will help you rapidaly expand your mail list within next 12 months.  </p>                        -->
                <button class='btn btn-info btn-block'>GoTo Dashboard</button>
                <br>                
                <p style=""><b>Note:</b> We are recommend to flow our <a href="<%=apiDocsURL%>" target="_blank">API documentation</a> before you start to use APIs</p>
            </div>
        </div>
        <button id="myBtn">Open Modal</button>
    </body>
</html>
<script>
// Get the modal
var modal = document.getElementById('myModal');

// Get the button that opens the modal
var btn = document.getElementById("myBtn");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks the button, open the modal 

btn.onclick = function() {
    modal.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
    modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}
</script>
