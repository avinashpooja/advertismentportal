<%
    String type = request.getParameter("_type");
    int types = -1;
    if (type != null) {
        types = Integer.parseInt(type);
    }
    System.out.println(types);
    String _partner = request.getParameter("_partner");
    if (!_partner.equals("-1")) {
        int prid = Integer.parseInt(_partner);
    }
%>
<div class="tab-pane active" id="usercharts">
    <div class="row-fluid">
        <div class="span12">
            <%
                if (types == 1) {%>
            <div class="span12" style="alignment-adjust: central">    
                <h5 align="center" class="text-primary" style="font-weight: bold">TRANSACTION STATUS REPORT</h5>
                <font style="font-family: monospace">
                Transactions count
                </font>
                <div>
                    <canvas  id="bargraph"></canvas>
                </div>    
                <div  align="center" style="font-family: monospace" >
                    Transactions Status
                </div>
            </div>
            <br>
            <div  class="span12" style="alignment-adjust: central">
                <h5 align="center" class="text-primary" style="font-weight: bold">TRANSACTION PER DAY REPORT</h5>
                <font style="font-family: monospace">
                Transactions count
                </font>
                <div>
                    <canvas id="linegraph"></canvas>
                </div>         
                <div  align="center" style="font-family: monospace" >
                    Day
                </div>
            </div> 
            <%} else if (types == 2) {%>
            <h5 align="center" class="text-primary" style="font-weight: bold">RESPONSE REPORT</h5>
            <font style="font-family: monospace">
            Response time in Milliseconds
            </font>
            <div  class="span12" style="alignment-adjust: central">
                <div>
                    <canvas id="linertgraph"></canvas>
                </div>           
                <div  align="center" style="font-family: monospace" >
                    Time
                </div>
            </div>
            <%}%>
        </div>
    </div>
    <div class="tab-pane" id="userreport">            
    </div>
</div>    
<br>
<br>                    
