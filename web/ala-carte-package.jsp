<%@page import="com.mollatech.serviceguard.nucleus.db.SgLoanDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.PartnerDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PackageSubscriptionManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgSubscriptionDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.LoanManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PaymentManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgPaymentdetails"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONArray"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.RequestPackageManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.GlobalStatus"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgReqbucketdetails"%>

<script src="scripts/packageDetails.js" type="text/javascript"></script>
<script src="scripts/packageOperation.js" type="text/javascript"></script>
<!--<div id="wrapper">-->
    <div class="small-header transition animated fadeIn">
        <div class="hpanel">
            <div class="panel-body">
<!--                <div id="hbreadcrumb" class="pull-right">
                    <ol class="hbreadcrumb breadcrumb">
                        <li><a href="home.jsp">Dashboard</a></li>
                        <li class="active">
                            <span>Ala carte package</span>
                        </li>
                    </ol>
                </div>-->
                <h2 class="font-light m-b-xs">
                    Subscription Package
                </h2>
                <small>Subscribe package</small>
            </div>
        </div>
    </div>
    <div class="content animate-panel" data-effect="rotateInDownRight" data-child="element">
        <div class="row">
            <div class="col-lg-12">
                <div class="text-center m-b-xl">
                    <h3>Best pricing for your app</h3>
                </div>
                <div class="row">
                    <%  
                        boolean prePaidLoanflag = false;
                        boolean postpaidPaymentFlag = false;
                        boolean packageFound = true;
                        boolean manualPaymentFlag = false;
                        String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
                        String ChannelId = (String) request.getSession().getAttribute("_ChannelId");
                        PartnerDetails parObj = (PartnerDetails) request.getSession().getAttribute("_partnerDetails");
                        SgSubscriptionDetails subscriObject1 = new PackageSubscriptionManagement().getPartnerSubscriptionbyPId(parObj.getPartnerId());                         
                        if (subscriObject1 != null) {
                            if (subscriObject1.getPaymentMode().equalsIgnoreCase("prepaid")) {
                                SgPaymentdetails paymentObj = new PaymentManagement().getPaymentDetailsbyPartnerAndSubscriptionID(subscriObject1.getBucketId(), parObj.getPartnerId());

                                if (subscriObject1.getStatus() != GlobalStatus.PAID && subscriObject1.getStatus() != GlobalStatus.DEFAULT) {
                                    postpaidPaymentFlag = true;
                                }

                                if (paymentObj != null) {
                                    if (subscriObject1.getStatus() != GlobalStatus.PAID && paymentObj.getInvoiceSendedOn() != null) {
                                        manualPaymentFlag = true;
                                    }
                                }
                            } else {
                                SgLoanDetails loandetailObject = new LoanManagement().getLoanDetails(SessionId, ChannelId, parObj.getPartnerId(), subscriObject1.getBucketId());
                                if (loandetailObject != null) {
                                    if (loandetailObject.getStatus() == GlobalStatus.APPROVED) {
                                        prePaidLoanflag = true;
                                    }
                                }
                            }
                        }
                        SgReqbucketdetails[] packageObj = null;
                        int count = 0;
                        packageObj = new RequestPackageManagement().getPackageRequestsbyStatus(SessionId, GlobalStatus.APPROVED);
                        if (packageObj != null) {
                            for (int i = 0; i < packageObj.length; i++) {
                                if(packageObj[i].getMarkAsDefault() != null && packageObj[i].getMarkAsDefault() != GlobalStatus.DEFAULT || packageObj[i].getPaymentMode().equalsIgnoreCase("postpaid")){
                                    continue;
                                }                            
                                if (("," + packageObj[i].getPartnerVisibility()).contains(",all,") || ("," + packageObj[i].getPartnerVisibility()).contains("," + parObj.getPartnerId() + ",")) {
                                    if (count == 4) {
                                        count = 1;
                                    } else {
                                        count++;
                                    }
                                    ArrayList<String> avaResourceDetails = new ArrayList<String>();
                                    String accesspointDetails = packageObj[i].getApRateDetails();
                                    JSONArray jsOld = new JSONArray(accesspointDetails);
                                    if (!avaResourceDetails.isEmpty()) {
                                        avaResourceDetails.clear();
                                    }
                                    for (int j = 0; j < jsOld.length(); j++) {
                                        JSONObject jsonexists1 = jsOld.getJSONObject(j);
                                        Iterator<String> keys = jsonexists1.keys();
                                        while (keys.hasNext()) {
                                            String keyData = keys.next();
                                            String[] keyDetails = keyData.split(":");
                                            avaResourceDetails.add(keyDetails[0]);
                                        }
                                    }

                                    accesspointDetails = packageObj[i].getFlatPrice();
                                    if (avaResourceDetails.size() != 0) {
                                        packageFound = false;
                                        if (packageObj[i].getMarkAsDefault() == null) {
                                            packageObj[i].setMarkAsDefault(0);
                                        }
                                        if (count == 1) {
                    %>                                           
                    <div class="col-sm-3 element">
                        <div class="hpanel plan-box">
                            <div class="panel-heading hbuilt text-center">
                                <h4 class="font-bold element"><%=packageObj[i].getBucketName()%></h4>
                            </div>
                            <div class="panel-body text-center">
                                <i class="pe pe-7s-cup big-icon text-warning element"></i>
                                <h4 class="font-bold element">
                                    $ <%=packageObj[i].getPlanAmount()%>
                                </h4>
                                <p class="text-muted element">
                                     <%if(packageObj[i].getPackageDescription() == null){%>
                                    There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.
                                    <%}else{%>
                                        <%=packageObj[i].getPackageDescription()%>
                                    <%}%>
                                </p>
                                <a href="#" onclick="packagedetails('<%=packageObj[i].getBucketName()%>')" class="btn btn-default btn-sm element">View Package</a>
                                <%if (postpaidPaymentFlag) {%>
                                <a class="btn btn-warning btn-sm ladda-button element" data-style="zoom-in" id="subscriptionErrorButton<%=i%>" onclick="showPackageChange('<%=packageObj[i].getBucketName()%>', '<%=subscriObject1.getPaymentMode()%>', '<%=i%>')"> Subscribe</a>                        
                                <%} else if (manualPaymentFlag) {%>
                                <a class="btn btn-warning btn-sm ladda-button element" data-style="zoom-in" id="manualPaymentErrorButton<%=i%>" onclick="manualPaymentEnable('<%=i%>')"> Subscribe</a>                        
                                <%} else if (packageObj[i].getPaymentMode().equalsIgnoreCase("prepaid") && packageObj[i].getMarkAsDefault() != null && packageObj[i].getMarkAsDefault() != GlobalStatus.DEFAULT) {%>
                                <a  class="btn btn-warning btn-sm ladda-button element" data-style="zoom-in" id="subscriptionButton<%=i%>" onclick="subscribePrepaid('<%=packageObj[i].getBucketName()%>', 'prepaid')"> Subscribe</a>                               
                                <%}%>
                            </div>
                        </div>
                    </div>
                    <%} else if (count == 2) {%>
                    <div class="col-sm-3 element">
                        <div class="hpanel plan-box">
                            <div class="panel-heading hbuilt text-center">
                                <h4 class="font-bold element"><%=packageObj[i].getBucketName()%></h4>
                            </div>
                            <div class="panel-body text-center">
                                <i class="pe pe-7s-science element big-icon text-success"></i>
                                <h4 class="font-bold element">
                                    $ <%=packageObj[i].getPlanAmount()%>
                                </h4>

                                <p class="text-muted element">
                                    <%if(packageObj[i].getPackageDescription() == null){%>
                                    There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.
                                    <%}else{%>
                                        <%=packageObj[i].getPackageDescription()%>
                                    <%}%>
                                </p>
                                <a href="my_subscription_package_detail_live.jsp?_name=<%=packageObj[i].getBucketName()%>" class="btn btn-default btn-sm element">View Package</a>
                                <%if (postpaidPaymentFlag) {%>
                                <a class="btn btn-success btn-sm ladda-button element" data-style="zoom-in" id="subscriptionErrorButton<%=i%>" onclick="showPackageChange('<%=packageObj[i].getBucketName()%>', '<%=subscriObject1.getPaymentMode()%>', '<%=i%>')"> Subscribe</a>                        
                                <%} else if (manualPaymentFlag) {%>
                                <a class="btn btn-success btn-sm ladda-button element" data-style="zoom-in" id="manualPaymentErrorButton<%=i%>" onclick="manualPaymentEnable('<%=i%>')"> Subscribe</a>                        
                                <%} else if (packageObj[i].getPaymentMode().equalsIgnoreCase("prepaid") && packageObj[i].getMarkAsDefault() != null && packageObj[i].getMarkAsDefault() != GlobalStatus.DEFAULT) {%>
                                <a  class="btn btn-success btn-sm ladda-button element" data-style="zoom-in" id="subscriptionButton<%=i%>" onclick="subscribePrepaid('<%=packageObj[i].getBucketName()%>', 'prepaid')"> Subscribe</a>                                
                                <%}%>
                            </div>
                        </div>
                    </div>
                    <%} else if (count % 3 == 0) {%>
                    <div class="col-sm-3 element">
                        <div class="hpanel plan-box">
                            <div class="panel-heading hbuilt text-center">
                                <h4 class="font-bold element"><%=packageObj[i].getBucketName()%></h4>
                            </div>
                            <div class="panel-body text-center">
                                <i class="pe pe-7s-refresh-cloud big-icon text-info element"></i>
                                <h4 class="font-bold element">
                                    $ <%=packageObj[i].getPlanAmount()%>
                                </h4>

                                <p class="text-muted element">
                                     <%if(packageObj[i].getPackageDescription() == null){%>
                                    There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.
                                    <%}else{%>
                                        <%=packageObj[i].getPackageDescription()%>
                                    <%}%>
                                </p>
                                <a href="my_subscription_package_detail_live.jsp?_name=<%=packageObj[i].getBucketName()%>" class="btn btn-default btn-sm element">View Package</a>
                                <%if (postpaidPaymentFlag) {%>
                                <a class="btn btn-info btn-sm ladda-button element" data-style="zoom-in" id="subscriptionErrorButton<%=i%>" onclick="showPackageChange('<%=packageObj[i].getBucketName()%>', '<%=subscriObject1.getPaymentMode()%>', '<%=i%>')"> Subscribe</a>                        
                                <%} else if (manualPaymentFlag) {%>
                                <a class="btn btn-info btn-sm ladda-button element" data-style="zoom-in" id="manualPaymentErrorButton<%=i%>" onclick="manualPaymentEnable('<%=i%>')"> Subscribe</a>                        
                                <%} else if (packageObj[i].getPaymentMode().equalsIgnoreCase("prepaid") && packageObj[i].getMarkAsDefault() != null && packageObj[i].getMarkAsDefault() != GlobalStatus.DEFAULT) {%>
                                <a  class="btn btn-info btn-sm ladda-button element" data-style="zoom-in" id="subscriptionButton<%=i%>" onclick="subscribePrepaid('<%=packageObj[i].getBucketName()%>', 'prepaid')"> Subscribe</a>
                                <%}%>
                            </div>
                        </div>
                    </div>
                    <%} else if (count % 4 == 0) {%>
                    <div class="col-sm-3 element">
                    <div class="hpanel plan-box">
                        <div class="panel-heading hbuilt text-center">
                            <h4 class="font-bold element"><%=packageObj[i].getBucketName()%></h4>
                        </div>
                        <div class="panel-body text-center">
                            <i class="pe pe-7s-server element big-icon text-danger"></i>
                            <h4 class="font-bold element">
                                $ <%=packageObj[i].getPlanAmount()%>
                            </h4>

                            <p class="text-muted element">
                                 <%if(packageObj[i].getPackageDescription() == null){%>
                                    There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.
                                    <%}else{%>
                                        <%=packageObj[i].getPackageDescription()%>
                                    <%}%>
                            </p>
                            <a href="my_subscription_package_detail_live.jsp?_name=<%=packageObj[i].getBucketName()%>" class="btn element btn-default btn-sm">View Package</a>
                            <%if (postpaidPaymentFlag) {%>
                            <a class="btn btn-danger btn-sm ladda-button element" data-style="zoom-in" id="subscriptionErrorButton<%=i%>" onclick="showPackageChange('<%=packageObj[i].getBucketName()%>', '<%=subscriObject1.getPaymentMode()%>', '<%=i%>')"> Subscribe</a>                        
                            <%} else if (manualPaymentFlag) {%>
                            <a class="btn btn-danger btn-sm ladda-button element" data-style="zoom-in" id="manualPaymentErrorButton<%=i%>" onclick="manualPaymentEnable('<%=i%>')"> Subscribe</a>                        
                            <%} else if (packageObj[i].getPaymentMode().equalsIgnoreCase("prepaid") && packageObj[i].getMarkAsDefault() != null && packageObj[i].getMarkAsDefault() != GlobalStatus.DEFAULT) {%>
                            <a  class="btn btn-danger btn-sm ladda-button element" data-style="zoom-in" id="subscriptionButton<%=i%>" onclick="subscribePrepaid('<%=packageObj[i].getBucketName()%>', 'prepaid')"> Subscribe</a>
                            <%}%>
                        </div>
                    </div>
                </div>
            </div>

                    <%
                        }

                        }
                            }
                                }
                        if (packageFound) {
                    %>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="hpanel">
                                <div class="panel-body">
                                    <i class="pe-7s-way text-success big-icon"></i>
                                    <h1>Not found</h1>
                                    <strong>No package found</strong>
                                    <p>
                                        Sorry, but the package list as per your production request has not been found.

                                    </p>
                                    <a href="#" class="btn btn-xs btn-success ladda-button" data-style="zoom-in" id="previousButton" onclick="goBack()">Go back to previous page</a>
                                </div>
                            </div>
                        </div>
                    </div>     
                    <%}
                    } else {%>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="hpanel">
                                <div class="panel-body">
                                    <i class="pe-7s-way text-success big-icon"></i>
                                    <h1>Not found</h1>
                                    <strong>No package found</strong>
                                    <p>
                                        Sorry, but the package list as per your production request has not been found.

                                    </p>
                                    <a href="#" class="btn btn-xs btn-success ladda-button" data-style="zoom-in" id="previousButton" onclick="goBack()">Go back to previous page</a>
                                </div>
                            </div>
                        </div>
                    </div> 
                    <%}%>
                </div>
            </div>
        </div>
        <!-- Footer-->
        <%@include file="footer.jsp"%>        
<!--    </div>    -->
<!--</div>-->

<script>
jQuery(function($){
  $('.stripe-button').on('token', function(e, token){
    var id = $('<span/>');
    id.text(token.id);
    $('.stripe-button-container').replaceWith('<p>Your Stripe token is: <strong>' + id.html() + '</strong>.</p>');
  });
});


</script>