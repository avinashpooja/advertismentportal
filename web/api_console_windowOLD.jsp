<%@page import="java.util.Iterator"%>
<%@page import="org.json.JSONArray"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.MethodParams"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.Params"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.Classes"%>
<%@page import="com.mollatech.ParamsData"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.MethodName"%>
<%@page import="java.util.List"%>
<%@page import="org.bouncycastle.util.encoders.Base64"%>
<%@page import="java.util.HashMap"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.HttpMethodParams"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.HttpMethodName"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.HttpResorcemanagment"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.HttpresourceDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.PartnerDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.ResourceManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.ResourceDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.AccessPointManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.TransformManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.WarFileManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.Warfiles"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.TransformDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.Accesspoint"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.MSConfig"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.SettingsManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.Methods"%>
<%@page import="java.util.Map"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.AccessPolicy"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.Serializer"%>
<%@page import="org.json.JSONObject"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.ClassName" %>
<script src="scripts/apiTest.js" type="text/javascript"></script>
<script src="scripts/tokenManagement.js" type="text/javascript"></script>
<script src="scripts/easytimer.min.js" type="text/javascript"></script>
<%

    //new change
    String reqData = request.getParameter("apiResource");
    String[] reqArr = reqData.split(":");
    int resId = Integer.parseInt(reqArr[1]);
    //new change end
    //String _resId = request.getParameter("_resId");
    //int resId = Integer.parseInt(_resId);
    //String _apid = request.getParameter("_apid");
    String envmt = request.getParameter("envt");
    //int apId = Integer.parseInt(_apid);
    int apId = Integer.parseInt(reqArr[0]);
    String mName = request.getParameter("methodName");

    String requestType = request.getParameter("type");
    String restType = request.getParameter("restType");

    String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
    String ChannelId = (String) request.getSession().getAttribute("_ChannelId");
    PartnerDetails parObj = (PartnerDetails) request.getSession().getAttribute("_partnerDetails");
    if (SessionId == null) {
        response.sendRedirect("logout.jsp");
        return;
    }

    int _preference1 = SettingsManagement.PREFERENCE_ONE;
    int _type1 = SettingsManagement.MATERSLAVE;
    Accesspoint ap = new AccessPointManagement().getAccessPointById(SessionId, ChannelId, apId);
    String apName = ap.getName();
    TransformDetails tf = new TransformManagement().getTransformDetails(SessionId, ChannelId, apId, resId);
    Warfiles warFiles = new WarFileManagement().getWarFile(SessionId, ChannelId, ap.getApId());
    Map map = (Map) Serializer.deserialize(warFiles.getWfile());
    ResourceDetails details = new ResourceManagement().getResourceById(resId);
    String ver = "1";
    //String env = "";
    String implName = "";
    //String strEnv   = "Production";
    String soapURL = "";
    //new c

    int versionDesc = map.size() / 2;
    byte[] meBy = tf.getMethods();
    Object obj = Serializer.deserialize(meBy);
    Map tmpMethods = (Map) obj;
    Methods methodsDesc = (Methods) tmpMethods.get("" + versionDesc);

    byte[] clBy = tf.getClasses();
    Object objC = Serializer.deserialize(clBy);
    Map classMap = (Map) objC;

    for (Object key : map.keySet()) {
        if (key.toString().split(":")[1].equalsIgnoreCase("Running")) {
            ver = key.toString().split(":")[0].replace("SB", "");
            //env = "";
            org.json.JSONObject object = new JSONObject(tf.getImplClassName());
            implName = object.getString(ver);
//            if (key.toString().split(":")[0].endsWith("SB")) {
//                env = "SB";
//                strEnv = "Test";
//            }
        }
    }
    Classes classes = null;
    classes = (Classes) classMap.get("" + versionDesc);

    Map methodMap = (Map) request.getSession().getAttribute("apiConsolemethods");
    Methods methods = (Methods) methodMap.get(apName);
    Object sobject = null;
    String restResponse = "json";
    sobject = new SettingsManagement().getSetting(SessionId, ChannelId, _type1, _preference1);
    MSConfig msConfig = (MSConfig) sobject;
    String ssl = "http";
    if (msConfig != null) {
        if (msConfig.ssl.equalsIgnoreCase("yes")) {
            ssl = "https";
        }
    }
    if (methods != null) {
        restResponse = methods.methodClassName.restResponse;
    }
    int version = Integer.parseInt(ver);
    String url = "";
    String tmpurl = "";
    if (envmt != null) {
        if (envmt.trim().equalsIgnoreCase("Test")) {
            url = ssl + "://" + msConfig.hostIp + ":" + msConfig.hostPort + "/" + apName.replaceAll(" ", "") + "SBV" + ver + "/" + implName;
        } else {
            url = ssl + "://" + msConfig.hostIp + ":" + msConfig.hostPort + "/" + apName.replaceAll(" ", "") + "V" + ver + "/" + implName;
        }
    }

    String partnerToken = parObj.getToken();
    AccessPolicy policy = (AccessPolicy) Serializer.deserialize(ap.getAccessPolicyEntry());
    if (!envmt.trim().equalsIgnoreCase("Test")) {
        partnerToken = parObj.getTokenForLive();
        policy = (AccessPolicy) Serializer.deserialize(ap.getLaccessPolicyEntry());
    }
    String key = ap.getName() + ":" + details.getName() + ":" + version + ":" + mName;
    String apiToken = null;
    JSONObject apiKeys = null;
    if (parObj.getApiLevelToken() != null) {
        apiKeys = new JSONObject(parObj.getApiLevelToken());
        if (!apiKeys.isNull(key)) {
            apiToken = apiKeys.getString(key);
        }
    }

    String restRequest = request.getParameter("rest");
%>
<style>
    .copied::after {
        position: absolute;
        top: 12%;
        right: 110%;
        display: block;
        content: "copied";
        font-size: 0.75em;
        padding: 2px 3px;
        color: #FFF;
        background-color: #2381c4;
        border-radius: 3px;
        opacity: 0;
        will-change: opacity, transform;
        animation: showcopied 1.5s ease;
    }

    @keyframes showcopied {
        0% {
            opacity: 0;
            transform: translateX(100%);
        }
        70% {
            opacity: 1;
            transform: translateX(0);
        }
        100% {
            opacity: 0;
        }
    }
</style>
<ul class="nav nav-tabs" id="consoleType" style="display: none">
    <li id="" class="active"><a data-toggle="tab" href="#soap"> SOAP Console</a></li>
    <li id="restConsole" class=""><a data-toggle="tab" href="#restful" onclick="LoadRestBody()"> RESTFUL Console</a></li>
</ul>

<div class="tab-content">
    <div id="soap" class="tab-pane active">
        <!--                <div class="panel-body">-->
        <div class="hpanel" style="margin-bottom: 0px!important">
            <div class="panel-body">                
                <%
                    soapURL = url + "?wsdl";
                %>      
                <div>
                    <table>
                        <tr>
                            <td><h5><a href="<%=soapURL%>" target="_blank"><%=soapURL%></a></h5></td>
                            <td>
                                <font class="col-lg-2" id="SOAPURLCopyDiv"></font>
                            </td>
                        </tr>
                    </table>

                    <input type='text' id='copyTarget' style='display: none'> 
                    <input type='text' id='soapURLData' name="soapURLData" style='display: none' value="<%=soapURL%>">

                </div>
            </div>
        </div>

        <div class="content animate-panel" style="padding: 1px 15px 15px 15px !important">
            <div class="row">
                <div class="col-lg-6 tour-21">				
                    <div class="hpanel">		
                        <div class="panel-heading">
                            <h3>Your request</h3>
                        </div>				
                        <div class="panel-body">					                                        
                            <form  role="form" class="form-horizontal" id="getBody" name="getBody">
                                <input type="hidden" name="paramcountRM" id="paramcountRM">
                                <input type="hidden" name="soapUserDefinedHeader" id="soapUserDefinedHeader">
                                <input type="hidden" name="queryParamcount" id="queryParamcount">
                                <input type="hidden" name="_residRM" id="_residRM" value="1">
                                <input type="hidden" name="resId" id="resId" value="<%=resId%>">
                                <input type="hidden" name="apId" id="apId" value="<%=apId%>">
                                <input type="hidden" name="type" id="type" value="SOAP">
                                <input type="hidden" name="methodName" id="methodName" value="<%=mName%>">
                                <input type="hidden" name="restType" id="restType" value="<%=restType%>">
                                <input type="hidden" name="url" id="url" value="<%=soapURL%>">
                                <input type="hidden" name="version" id="version" value="<%=version%>">
                                <input type="hidden" name="headerC" id="headerC">
                                <input type='text' id='headerContent' style='display: none'>

                                <!--                                <h3>WSDL </h3>
                                                                <div class="form-group">
                                                                    <div class="col-sm-10 col-lg-10 col-md-10">
                                                                        <a href="<%=soapURL%>" style="word-wrap: break-word;" target="_blank"><%=soapURL%></a>
                                                                        
                                                                        <a href='#/' style='font-size:10px; margin-left:15%;text-decoration:none' onclick="copy('<%=soapURL%>')" id="copyDiv"><b>Copy</b></a>
                                                                    </div>
                                                                    <div class="col-sm-2 col-lg-2 col-md-2">
                                                                        <div id="SOAPURLCopyDiv"></div>
                                                                    </div>
                                                                </div>-->
                                <div class="form-group">
                                    <div class="col-sm-2"><h3>Header</h3></div>
                                    <div class="col-sm-2" style="margin-top: 3%">
                                        <div id="headerCopyDiv"></div>
                                    </div>
                                </div>
                                <table id="_headerTable" CELLSPACING="15" class="tour-23">                                                
                                    <tr>                                                    
                                        <td width="35%">
                                            <input type="text" class="form-control" id="_myNameRM" name="_myNameRM" value="APITokenId">                                                        
                                        </td>
                                        <td width="90%">
                                            <div style='padding: 1%'>
                                                <input type="text" class="form-control" id="_myResType" name="_myResType" value="vEBDceyF4TRRkZq1qBo+NaYRgV4=">                                                        
                                            </div>
                                        </td>
                                        <td >                                                        
                                            <a class='btn btn-info btn-sm'  onclick="addHeader()" id="addUserButton"><span class='fa fa-plus-circle'></span></a>
                                        </td>  
                                        <td style="padding: 1%">
                                            <a class='btn btn-warning btn-sm' onclick="removeHeader()" id="minusUserButton"><span class='fa fa-minus-circle'></span></a>
                                        </td>                                              
                                    </tr>                                                   
                                </table>
                                <div class="hr-line-dashed"></div>
                                <div style="display: none">
                                    <h3>Basic Authentication</h3>
                                    <div class="form-group">
                                        <div class="col-sm-6"><input type="text" id="_userNameAuth" name="_userNameAuth"  class="form-control" placeholder="Username"></div>
                                        <div class="col-sm-6"><input type="text" id="_passwordAuth" name="_passwordAuth" class="form-control" placeholder="Password"></div>
                                    </div>
                                    <div class="hr-line-dashed"></div>
                                </div>
                                <div class="tour-24">
                                    <div class="form-group">
                                        <div class="col-sm-2"><h3>Body</h3></div>
                                        <div class="col-sm-2" style="margin-top: 3%">
                                            <div id="bodyCopyDiv"></div>
                                        </div>
                                    </div>

                                    <textarea class="form-control" id="_APIbody" name="_APIbody" style="min-height: 500px;">																	
                                    </textarea>
                                </div>
                                <div class="hr-line-dashed"></div>
                                <button class="btn btn-primary ladda-button" data-style="zoom-in" onclick="SendRequest()" id="apiConsoleRequest" type="button"><i class="fa fa-check"></i> Submit</button>                                    
                            </form>								
                        </div>					
                    </div>				
                </div>
                <div class="col-lg-6 tour-22">
                    <div class="hpanel">
                        <div class="panel-heading">
                            <h3>Your response</h3>
                        </div>
                        <div class="panel-body">
                            <form>
                                <textarea style="min-height: 220px;" class="form-control" id="responseID" name="responseID"></textarea>
                            </form>
							<input type="hidden" class="form-control" id="responseIDCopy" name="responseIDCopy" />
                            <br>
							<br>
                            <table>
                                <tr>
                                    <td>
                                        <font class="col-lg-1" id="soapResponseCopyDiv"></font>
                                    </td>
                                </tr>
                            </table>
                            <div id="startValuesAndTargetExample">
                                <!--                                    <div class="values"></div>-->
                                <!--                                    <div class="progress_bar">.</div>-->
                            </div>
                        </div>                            
                    </div>
                    <div id="questionAndAnswer" style="display: none">
                        <div class="hpanel panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                            <div class="panel-body">
                                <h4 class="m-t-none m-b-none">Need Help </h4>
                                <small class="text-muted">All general questions about our API.</small>&nbsp;&nbsp;&nbsp;
                                <a href="./raiseTicket.jsp" target="_blank" class="btn btn-primary btn-xs ladda-button" data-style="zoom-in" type="button" href="#"  style="margin-left: 1px"><i class="fa fa-check"></i> Raise Ticket</a> <br>
                            </div>
                            <div class="panel-body" id="_apiLevelToken" style="display: none">
                                <a data-toggle="collapse" data-parent="#accordion" href="#q1" aria-expanded="true">
                                    <i class="fa fa-chevron-down pull-right text-muted"></i>
                                    Where can I get APITokenId?
                                </a>
                                <div id="q1" class="panel-collapse collapse">
                                    <hr>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal.
                                </div>
                            </div>                                                               <!--
                            -->                                <div class="panel-body" id="_headerError" style="display: none">
                                <a data-toggle="collapse" data-parent="#accordion" href="#q2" aria-expanded="true">
                                    <i class="fa fa-chevron-down pull-right text-muted"></i>
                                    Why need Headers and How to add?
                                </a>
                                <div id="q2" class="panel-collapse collapse">
                                    <hr>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal.
                                </div>
                            </div><!--
                            -->                             <div class="panel-body" id="connectionrefuse" style="display: none">
                                <a data-toggle="collapse" data-parent="#accordion" href="#q3" aria-expanded="true">
                                    <i class="fa fa-chevron-down pull-right text-muted"></i>
                                    Connection refused (Connection refused)?
                                </a>
                                <div id="q3" class="panel-collapse collapse">
                                    <hr>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal.
                                </div>
                            </div><!--
                            
                            -->                                <div class="panel-body" id="_noPartnerAvailable" style="display: none">
                                <a data-toggle="collapse" data-parent="#accordion" href="#q4" aria-expanded="true">
                                    <i class="fa fa-chevron-down pull-right text-muted"></i>
                                    No Such Partner is Available.
                                </a>
                                <div id="q4" class="panel-collapse collapse">
                                    <hr>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal.
                                </div>
                            </div><!--
                            -->                                <div class="panel-body">
                                <a data-toggle="collapse" data-parent="#accordion" href="#q5" aria-expanded="true">
                                    <i class="fa fa-chevron-down pull-right text-muted"></i>
                                    No Such Webservice is Available.
                                </a>
                                <div id="q5" class="panel-collapse collapse">
                                    <hr>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal.
                                </div>
                            </div><!--
                            -->                                <div class="panel-body" id="_groupDisable" style="display: none">
                                <a data-toggle="collapse" data-parent="#accordion" href="#q6" aria-expanded="true">
                                    <i class="fa fa-chevron-down pull-right text-muted"></i>
                                    Your Associated Group Is Not Active. Contact Administrator.
                                </a>
                                <div id="q6" class="panel-collapse collapse">
                                    <hr>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal.
                                </div>
                            </div><!--
                            --> <div class="panel-body" id="_resourseUnAvai" style="display: none">
                                <a data-toggle="collapse" data-parent="#accordion" href="#q7" aria-expanded="true">
                                    <i class="fa fa-chevron-down pull-right text-muted"></i>
                                    Internal Resource IS Not Active. Contact Administrator.
                                </a>
                                <div id="q7" class="panel-collapse collapse">
                                    <hr>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal.
                                </div>
                            </div><!--
                            
                            --><div class="panel-body" id="_accDiasable" style="display: none">
                                <a data-toggle="collapse" data-parent="#accordion" href="#q8" aria-expanded="true">
                                    <i class="fa fa-chevron-down pull-right text-muted"></i>
                                    Your Account Is Not Active. Contact Administrator.
                                </a>
                                <div id="q8" class="panel-collapse collapse">
                                    <hr>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal.
                                </div>
                            </div><!--
                            -->                                <div class="panel-body" id="_partnerDisable" style="display: none">
                                <a data-toggle="collapse" data-parent="#accordion" href="#q9" aria-expanded="true">
                                    <i class="fa fa-chevron-down pull-right text-muted"></i>
                                    Partner Is Not Active.
                                </a>
                                <div id="q9" class="panel-collapse collapse">
                                    <hr>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal.
                                </div>
                            </div><!--
                            -->                                <div class="panel-body" id="initializeApiToken" style="display: none">
                                <a data-toggle="collapse" data-parent="#accordion" href="#q10" aria-expanded="true">
                                    <i class="fa fa-chevron-down pull-right text-muted"></i>
                                    Service Guard Error: Please Initialise API Level Token.
                                </a>
                                <div id="q10" class="panel-collapse collapse">
                                    <hr>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal.
                                </div>
                            </div><!--
                            -->                                <div class="panel-body" id="_parameterMismatch" style="display: none">
                                <a data-toggle="collapse" data-parent="#accordion" href="#q11" aria-expanded="true">
                                    <i class="fa fa-chevron-down pull-right text-muted"></i>
                                    Service Guard Error: Parameter Mismatch
                                </a>
                                <div id="q11" class="panel-collapse collapse">
                                    <hr>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal.
                                </div>
                            </div>                                
                            <div class="panel-body">
                                <a data-toggle="collapse" data-parent="#accordion" href="#q12" aria-expanded="true">
                                    <i class="fa fa-chevron-down pull-right text-muted"></i>
                                    Service Guard Error: Please Contact Administrator.
                                </a>
                                <div id="q12" class="panel-collapse collapse">
                                    <hr>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal.
                                </div>                                
                            </div><!--
                            -->                                <div class="panel-body" id="_unAuthorizedPart" style="display: none">
                                <a data-toggle="collapse" data-parent="#accordion" href="#q13" aria-expanded="true">
                                    <i class="fa fa-chevron-down pull-right text-muted"></i>
                                    You Are Not Autherized Partner.
                                </a>
                                <div id="q13" class="panel-collapse collapse">
                                    <hr>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal.
                                </div>
                            </div><!--
                            -->                                <div class="panel-body" id="_reachedLimit" style="display: none">
                                <a data-toggle="collapse" data-parent="#accordion" href="#q14" aria-expanded="true">
                                    <i class="fa fa-chevron-down pull-right text-muted"></i>
                                    Service Guard Error: You have reached your Daily limit.
                                </a>
                                <div id="q14" class="panel-collapse collapse">
                                    <hr>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal.
                                </div>
                            </div><!--
                            
                            -->                                <div class="panel-body" id="_transactionsec" style="display: none">
                                <a data-toggle="collapse" data-parent="#accordion" href="#q15" aria-expanded="true">
                                    <i class="fa fa-chevron-down pull-right text-muted"></i>
                                    Service Guard Error: Transaction Per Second Error.
                                </a>
                                <div id="q15" class="panel-collapse collapse">
                                    <hr>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal.
                                </div>
                            </div><!--
                            --><div class="panel-body" id="_accessinvalidIp" style="display: none">
                                <a data-toggle="collapse" data-parent="#accordion" href="#q16" aria-expanded="true">
                                    <i class="fa fa-chevron-down pull-right text-muted"></i>
                                    You are accessing from invalid IP.
                                </a>
                                <div id="q16" class="panel-collapse collapse">
                                    <hr>please check if you are behind proxy or using dynamic IP.
                                </div>
                            </div><!--
                            -->                                <div class="panel-body" id="_serNotaccesible" style="display: none">
                                <a data-toggle="collapse" data-parent="#accordion" href="#q17" aria-expanded="true">
                                    <i class="fa fa-chevron-down pull-right text-muted"></i>
                                    You cannot access service now as it is not permitted for this day in the week.
                                </a>
                                <div id="q17" class="panel-collapse collapse">
                                    <hr>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal.
                                </div>
                            </div><!--
                            -->                                <div class="panel-body" id="serNOtPermited" style="display: none">
                                <a data-toggle="collapse" data-parent="#accordion" href="#q18" aria-expanded="true">
                                    <i class="fa fa-chevron-down pull-right text-muted"></i>
                                    You cannot access service now as it is not permitted for this time of the day.
                                </a>
                                <div id="q18" class="panel-collapse collapse">
                                    <hr>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal.
                                </div>
                            </div><!--
                            -->                                <div class="panel-body" id="invalidCert" style="display: none">
                                <a data-toggle="collapse" data-parent="#accordion" href="#q20" aria-expanded="true">
                                    <i class="fa fa-chevron-down pull-right text-muted"></i>
                                    You are offering invalid/empty certificate.
                                </a>
                                <div id="q20" class="panel-collapse collapse">
                                    <hr>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal.
                                </div>
                            </div><!--
                            -->                                <div class="panel-body" id="_invalidToken" style="display: none">
                                <a data-toggle="collapse" data-parent="#accordion" href="#q21" aria-expanded="true">
                                    <i class="fa fa-chevron-down pull-right text-muted"></i>
                                    You are offering invalid/empty Token.
                                </a>
                                <div id="q21" class="panel-collapse collapse">
                                    <hr>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal.
                                </div>
                            </div><!--
                            -->                                <div class="panel-body" id="_ipblock" style="display: none">
                                <a data-toggle="collapse" data-parent="#accordion" href="#q22" aria-expanded="true">
                                    <i class="fa fa-chevron-down pull-right text-muted"></i>
                                    IP BLOCK.
                                </a>
                                <div id="q22" class="panel-collapse collapse">
                                    <hr>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal.
                                </div>
                            </div><!--
                            -->                                <div class="panel-body" id="_invalidToken" style="display: none">
                                <a data-toggle="collapse" data-parent="#accordion" href="#q23" aria-expanded="true">
                                    <i class="fa fa-chevron-down pull-right text-muted"></i>
                                    You are offering invalid/empty Token For API.
                                </a>
                                <div id="q23" class="panel-collapse collapse">
                                    <hr>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal.
                                </div>
                            </div><!--
                            -->                             <div class="panel-body" style="display: none">
                                <a data-toggle="collapse" data-parent="#accordion" href="#q24" aria-expanded="true">
                                    <i class="fa fa-chevron-down pull-right text-muted"></i>
                                    Signature Verification Failed.
                                </a>
                                <div id="q24" class="panel-collapse collapse">
                                    <hr>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal.
                                </div>
                            </div> 
                            <div class="panel-body" id="_operationTimeOut" style="display: none">
                                <a data-toggle="collapse" data-parent="#accordion" href="#q25" aria-expanded="true">
                                    <i class="fa fa-chevron-down pull-right text-muted"></i>
                                    Operation timed out (Connection timed out).
                                </a>
                                <div id="q25" class="panel-collapse collapse">
                                    <hr>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal.
                                </div>
                            </div>     
                            <!--                            </div>                            -->
                        </div>
                    </div>
                </div>             
            </div>
        </div>
    </div>
    <!--    </div>-->
    <div id="restful" class="tab-pane">
        <%
            if (envmt != null) {
                if (envmt.trim().equalsIgnoreCase("Test")) {
                    tmpurl = ssl + "://" + msConfig.hostIp + ":" + msConfig.hostPort + "/" + apName.replaceAll(" ", "") + "SBV" + ver + "/" + implName;
                    tmpurl = tmpurl + "/" + implName + "RS/" + mName;
                } else {
                    tmpurl = ssl + "://" + msConfig.hostIp + ":" + msConfig.hostPort + "/" + apName.replaceAll(" ", "") + "V" + ver + "/" + implName;
                    tmpurl = tmpurl + "/" + implName + "RS/" + mName;
                }
            }
            url += "/application.wadl";
        %>

        <div class="hpanel" style="margin-bottom: 0px!important">
            <div class="panel-body">                                                     
                <div>
                    <table>
                        <tr>
                            <td><h5><a href="<%=url%>" target="_blank"><%=url%></a></h5></td>
                            <td>
                                <font class="col-lg-2" id="RestURLCopyDiv"></font>
                            </td>
                        </tr>
                    </table>
                    <input type='text' id='copyTargetRest' style='display: none'> 
                    <input type='text' id='restURLData' name="restURLData" style='display: none' value="<%=url%>">
                </div>
            </div>
        </div>

        <div class="content animate-panel" style="padding: 1px 15px 15px 15px !important">
            <div class="row">
                <div class="col-lg-6">						
                    <div class="hpanel">							
                        <div class="panel-heading"> 
                            <h3>Your request</h3>
                        </div>
                        <div class="panel-body">
                            <form  role="form" class="form-horizontal" id="getRestBody" name="getRestBody">
                                <input type="hidden" name="paramcountRMRest" id="paramcountRMRest">
                                <input type="hidden" name="restUserDefinedHeader" id="restUserDefinedHeader">
                                <input type="hidden" name="paramcountFormData" id="paramcountFormData">
                                <input type="hidden" name="paramcountUrlEncodedFormData" id="paramcountUrlEncodedFormData">
                                <input type="hidden" name="queryParamcount" id="queryParamcount">
                                <input type="hidden" name="_residRM" id="_residRM" value="1">
                                <input type="hidden" name="resId" id="resId" value="<%=resId%>">
                                <input type="hidden" name="apId" id="apId" value="<%=apId%>">
                                <input type="hidden" name="type" id="type" value="rest">
                                <input type="hidden" name="methodName" id="methodName" value="<%=mName%>">
                                <input type="hidden" name="restType" id="restType" value="<%=restResponse%>">
                                <input type="hidden" name="url" id="url" value="<%=tmpurl%>">
                                <input type="hidden" name="version" id="version" value="<%=version%>">
                                <input type="hidden" name="restHeaderData" id="restHeaderData">
                                <input type="hidden" name="restURLRequestType" id="restURLRequestType" value="<%=tmpurl%>">
                                <!--                                <h3>WADL </h3>
                                                                <div class="form-group">
                                                                    <div class="col-sm-10 col-lg-10 col-md-10">
                                                                        <a href="<%=url%>" target="_blank" style="word-wrap: break-word;" ><%=url%></a>
                                                                    </div>
                                                                    <div class="col-sm-2 col-lg-2 col-md-2">
                                                                        <div id="RestURLCopyDiv"></div>
                                                                    </div>
                                                                </div>-->
                                <div class="form-group">
                                    <div class="col-sm-4 col-lg-4 col-md-4">
                                        <h3>Request Type</h3>
                                    </div>
                                    <div class="col-sm-2 col-lg-2 col-md-2" style="margin-top: 3%">
                                        <div id="RESTRequestURLCopyDiv" ></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-3">
                                        <select class="js-source-states form-control" style="width: 100%"  id="requestHTTPMethod" name="requestHTTPMethod">                                                                                            
                                            <%!
                                                static Map WADLMethods = null;
                                                static Map WADLRawMethods = new HashMap();

                                                public static void getRawMethods() {
                                                    for (Object key : WADLMethods.keySet()) {
                                                        HttpMethodName mn = new HttpMethodName();
                                                        mn.httpPostType = key.toString().split(":")[1];
                                                        mn.produces = key.toString().split(":")[2];
                                                        mn.consumes = key.toString().split(":")[3];
                                                        mn.methodname = key.toString().split(":")[0];
                                                        mn.httpPath = key.toString().split(":")[5];
                                                        mn.parameters = new ArrayList<HttpMethodParams>();
                                                        WADLRawMethods.put(mn.methodname, mn);
                                                    }
                                                }
                                            %>

                                            <%
                                                String annotation = "POST";
                                                if (details.getType().equalsIgnoreCase("WADL")) {
                                                    HttpresourceDetails httpresourceDetails = null;
                                                    httpresourceDetails = new HttpResorcemanagment().getHttpReourceId(SessionId, ChannelId, resId);
                                                    if (httpresourceDetails != null) {
                                                        WADLMethods = (Map) Serializer.deserialize(httpresourceDetails.getImplclass());
                                                        getRawMethods();
                                                        HttpMethodName methodName = (HttpMethodName) WADLRawMethods.get(mName);
                                                        annotation = methodName.httpPostType;
                                                    }
                                                }
                                            %>
                                            <option value="<%=annotation%>" selected><%=annotation%></option>                                                                                                                                        
                                        </select>
                                    </div>
                                    <div class="col-sm-9"><input type="text" class="form-control" disabled name="restURL" id="restURL" value="<%=tmpurl%>"></div>
                                </div>
                                <div class="hr-line-dashed"></div>
                                <div class="form-group">
                                    <div class="col-sm-2"><h3>Header</h3></div>
                                    <div class="col-sm-2" style="margin-top: 3%">
                                        <div id="headerRestCopyDiv"></div>
                                    </div>
                                </div>
                                <table id="_headerRestTable" CELLSPACING="15">                                                
                                    <tr>                                                    
                                        <td width="35%">
                                            <input type="text" class="form-control" id="_myNameRMRest" name="_myNameRMRest" value="APITokenId">                                                        
                                        </td>
                                        <td width="90%">
                                            <div style='padding: 1%'>
                                                <input type="text" class="form-control" id="_myResTypeRest" name="_myResTypeRest" value="vEBDceyF4TRRkZq1qBo+NaYRgV4=">                                                        
                                            </div>
                                        </td>
                                        <td>                                                        
                                            <a class='btn btn-info btn-sm'  onclick="addRestHeader()" id="addUserButtonRest"><span class='fa fa-plus-circle'></span></a>
                                        </td>  
                                        <td style="padding: 1%">
                                            <a class='btn btn-warning btn-sm' onclick="removeRestHeader()" id="minusUserButtonRest"><span class='fa fa-minus-circle'></span></a>
                                        </td>                                              
                                    </tr>                                                   
                                </table>
                                <div style="display: none">
                                <div class="hr-line-dashed"></div>

                                <h3>Query Param</h3>
                                <table id="_queryParamTable" CELLSPACING="15">                                                
                                    <tr>                                                    
                                        <td width="35%">
                                            <input type="text" class="form-control" id="_queryParamFirst" name="_queryParamFirst" placeholder="Key">                                                        
                                        </td>
                                        <td width="90%">
                                            <div style='padding: 1%'>
                                                <input type="text" class="form-control" id="_queryParamFirstValue" name="_queryParamFirstValue" placeholder="Value">                                                        
                                            </div>
                                        </td>
                                        <td>                                                        
                                            <a class='btn btn-info btn-sm'  onclick="addQueryParamData()" id="addQueryParamButton"><span class='fa fa-plus-circle'></span></a>
                                        </td>  
                                        <td style="padding: 1%">
                                            <a class='btn btn-warning btn-sm' onclick="removeQueryParamData()" id="minusQueryParamButton"><span class='fa fa-minus-circle'></span></a>
                                        </td>                                              
                                    </tr>                                                   
                                </table>
                                </div>
                                <div class="hr-line-dashed"></div>

                                <div class="form-group">
                                    <div class="col-sm-2"><h3>Body</h3></div>
                                    <div class="col-sm-2" style="margin-top: 3%">
                                        <div id="bodyRestCopyDiv"></div>
                                    </div>
                                </div>

                                <!--                                <div class="form-group"><label class="col-sm-1 control-label text-left"></label>
                                                                    <div class="col-sm-11">
                                                                        <div class="radio radio-inline">
                                                                            <input type="radio" id="bodyRadio" value="0" name="bodyRadio" checked>
                                                                            <label for="x-www-form-urlencoded"> Default </label>
                                                                        </div>
                                                                        <div class="radio radio-info radio-inline" style="display: none">
                                                                            <input type="radio" id="bodyRadio" value="1" name="bodyRadio" >
                                                                            <label for="form-data"> form-data </label>
                                                                        </div>
                                                                        <div class="radio radio-inline" style="display: none">
                                                                            <input type="radio" id="bodyRadio" value="2" name="bodyRadio" >
                                                                            <label for="x-www-form-urlencoded"> x-www-form-urlencoded </label>
                                                                        </div>                                            
                                                                    </div>
                                                                </div>-->
                                <div id="body0" class="bodydesc">
                                    <textarea class="form-control" id="_RESTAPIbody" name="_RESTAPIbody" style="min-height: 500px;">																	
                                    </textarea>
                                </div>
                                <div id="body1" class="bodydesc" style="display: none">
                                    <table id="_formDataTable" CELLSPACING="15">                                                
                                        <tr>                                                    
                                            <td width="35%">
                                                <input type="text" class="form-control" id="_formDataFirst" name="_formDataFirst" placeholder="Key">                                                        
                                            </td>
                                            <td width="90%">
                                                <div style='padding: 1%'>
                                                    <input type="text" class="form-control" id="_formDataFirstValue" name="_formDataFirstValue" placeholder="Value">                                                        
                                                </div>
                                            </td>
                                            <td>                                                        
                                                <a class='btn btn-info btn-sm'  onclick="addFormData()" id="addFormDataButton"><span class='fa fa-plus-circle'></span></a>
                                            </td>  
                                            <td style="padding: 1%">
                                                <a class='btn btn-warning btn-sm' onclick="removeFormData()" id="minusFormDataButton"><span class='fa fa-minus-circle'></span></a>
                                            </td>                                              
                                        </tr>                                                   
                                    </table>
                                </div>
                                <div id="body2" class="bodydesc" style="display: none">
                                    <table id="_urlencodedformDataTable" CELLSPACING="15">                                                
                                        <tr>                                                    
                                            <td width="35%">
                                                <input type="text" class="form-control" id="_urlencodedformDataFirst" name="_urlencodedformDataFirst" placeholder="Key">                                                        
                                            </td>
                                            <td width="90%">
                                                <div style='padding: 1%'>
                                                    <input type="text" class="form-control" id="_urlencodedformDataFirstValue" name="_urlencodedformDataFirstValue" placeholder="Value">                                                        
                                                </div>
                                            </td>
                                            <td>                                                        
                                                <a class='btn btn-info btn-sm'  onclick="addURLEncodedFormData()" id="addUrlencodedFormDataButton"><span class='fa fa-plus-circle'></span></a>
                                            </td>  
                                            <td style="padding: 1%">
                                                <a class='btn btn-warning btn-sm' onclick="removeURLEncodedFormData()" id="minusUrlencodedFormDataButton"><span class='fa fa-minus-circle'></span></a>
                                            </td>                                              
                                        </tr>                                                   
                                    </table>
                                </div>
                                <div class="hr-line-dashed"></div>
                                <button  class="btn btn-primary ladda-button" data-style="zoom-in" type="button" onclick="submitRestRequest()" id="apiConsoleRestRequest"><i class="fa fa-check"></i> Submit</button>
                            </form>
                        </div>						
                    </div>						
                </div>
                <div class="col-lg-6">
                    <div class="hpanel">
                        <div class="panel-heading">
                            <h3>Your response</h3>
                        </div>
                        <div class="panel-body">
                            <form>
                                <textarea class="form-control" name="restResponseID" id="restResponseID" style="min-height: 270px;"></textarea>
                            </form>
							<input type="hidden" class="form-control" id="restresponseIDCopy" name="restresponseIDCopy" />
                            <br>
							<table>
                                <tr>
                                    <td>
                                        <font class="col-lg-1" id="restResponseCopyDiv"></font>
                                    </td>
                                </tr>
                            </table>
                            <div id="startValuesAndTargetExampleREST">
                                <div class="values"></div>
                                <!--                                    <div class="progress_bar">.</div>-->
                            </div>
                        </div>
                    </div>
                    <br>
                    <div id="questionAndAnswerREST" style="display: none">
                        <div class="hpanel panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                            <div class="panel-body">
                                <h4 class="m-t-none m-b-none">Need Help </h4>
                                <small class="text-muted">All general questions about our API.</small>&nbsp;&nbsp;&nbsp;
                                <a  href="./raiseTicket.jsp" target="_blank" class="btn btn-primary btn-xs ladda-button" data-style="zoom-in" type="button"  style="margin-left: 1px" target="_blank"><i class="fa fa-check"></i> Raise Ticket</a> <br>
                            </div>
                            <div class="panel-body" id="_apiLevelTokenRest" style="display: none">
                                <a data-toggle="collapse" data-parent="#accordion" href="#q1" aria-expanded="true">
                                    <i class="fa fa-chevron-down pull-right text-muted"></i>
                                    Where can I get APITokenId?
                                </a>
                                <div id="q1" class="panel-collapse collapse">
                                    <hr>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal.
                                </div>
                            </div>                                                               <!--
                            -->                             <div class="panel-body" id="_headerErrorRest" style="display: none">
                                <a data-toggle="collapse" data-parent="#accordion" href="#q2" aria-expanded="true">
                                    <i class="fa fa-chevron-down pull-right text-muted"></i>
                                    Why need Headers and How to add?
                                </a>
                                <div id="q2" class="panel-collapse collapse">
                                    <hr>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal.
                                </div>
                            </div>
                            <div class="panel-body" id="connectionrefuseRest" style="display: none">
                                <a data-toggle="collapse" data-parent="#accordion" href="#q3" aria-expanded="true">
                                    <i class="fa fa-chevron-down pull-right text-muted"></i>
                                    Connection refused (Connection refused)?
                                </a>
                                <div id="q3" class="panel-collapse collapse">
                                    <hr>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal.
                                </div>
                            </div><!--
                            
                            -->                                <div class="panel-body" id="_noPartnerAvailableRest" style="display: none">
                                <a data-toggle="collapse" data-parent="#accordion" href="#q4" aria-expanded="true">
                                    <i class="fa fa-chevron-down pull-right text-muted"></i>
                                    No Such Partner is Available.
                                </a>
                                <div id="q4" class="panel-collapse collapse">
                                    <hr>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal.
                                </div>
                            </div><!--
                            -->                             <div class="panel-body">
                                <a data-toggle="collapse" data-parent="#accordion" href="#q5" aria-expanded="true">
                                    <i class="fa fa-chevron-down pull-right text-muted"></i>
                                    No Such Webservice is Available.
                                </a>
                                <div id="q5" class="panel-collapse collapse">
                                    <hr>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal.
                                </div>
                            </div><!--
                            -->                                <div class="panel-body" id="_groupDisableRest" style="display: none">
                                <a data-toggle="collapse" data-parent="#accordion" href="#q6" aria-expanded="true">
                                    <i class="fa fa-chevron-down pull-right text-muted"></i>
                                    Your Associated Group Is Not Active. Contact Administrator.
                                </a>
                                <div id="q6" class="panel-collapse collapse">
                                    <hr>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal.
                                </div>
                            </div><!--
                            --> <div class="panel-body" id="_resourseUnAvaiRest" style="display: none">
                                <a data-toggle="collapse" data-parent="#accordion" href="#q7" aria-expanded="true">
                                    <i class="fa fa-chevron-down pull-right text-muted"></i>
                                    Internal Resource IS Not Active. Contact Administrator.
                                </a>
                                <div id="q7" class="panel-collapse collapse">
                                    <hr>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal.
                                </div>
                            </div><!--
                            
                            --><div class="panel-body" id="_accDiasableRest" style="display: none">
                                <a data-toggle="collapse" data-parent="#accordion" href="#q8" aria-expanded="true">
                                    <i class="fa fa-chevron-down pull-right text-muted"></i>
                                    Your Account Is Not Active. Contact Administrator.
                                </a>
                                <div id="q8" class="panel-collapse collapse">
                                    <hr>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal.
                                </div>
                            </div><!--
                            -->                                <div class="panel-body" id="_partnerDisableRest" style="display: none">
                                <a data-toggle="collapse" data-parent="#accordion" href="#q9" aria-expanded="true">
                                    <i class="fa fa-chevron-down pull-right text-muted"></i>
                                    Partner Is Not Active.
                                </a>
                                <div id="q9" class="panel-collapse collapse">
                                    <hr>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal.
                                </div>
                            </div><!--
                            -->                                <div class="panel-body" id="initializeApiTokenRest" style="display: none">
                                <a data-toggle="collapse" data-parent="#accordion" href="#q10" aria-expanded="true">
                                    <i class="fa fa-chevron-down pull-right text-muted"></i>
                                    Service Guard Error: Please Initialise API Level Token.
                                </a>
                                <div id="q10" class="panel-collapse collapse">
                                    <hr>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal.
                                </div>
                            </div><!--
                            -->                                <div class="panel-body" id="_parameterMismatchRest" style="display: none">
                                <a data-toggle="collapse" data-parent="#accordion" href="#q11" aria-expanded="true">
                                    <i class="fa fa-chevron-down pull-right text-muted"></i>
                                    Service Guard Error: Parameter Mismatch
                                </a>
                                <div id="q11" class="panel-collapse collapse">
                                    <hr>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal.
                                </div>
                            </div>

                            <div class="panel-body">
                                <a data-toggle="collapse" data-parent="#accordion" href="#q12" aria-expanded="true">
                                    <i class="fa fa-chevron-down pull-right text-muted"></i>
                                    Service Guard Error: Please Contact Administrator.
                                </a>
                                <div id="q12" class="panel-collapse collapse">
                                    <hr>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal.
                                </div><!--
                                -->                             </div><!--
                            -->                                <div class="panel-body" id="_unAuthorizedPartRest" style="display: none">
                                <a data-toggle="collapse" data-parent="#accordion" href="#q13" aria-expanded="true">
                                    <i class="fa fa-chevron-down pull-right text-muted"></i>
                                    You Are Not Autherized Partner.
                                </a>
                                <div id="q13" class="panel-collapse collapse">
                                    <hr>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal.
                                </div>
                            </div><!--
                            -->                                <div class="panel-body" id="_reachedLimitRest" style="display: none">
                                <a data-toggle="collapse" data-parent="#accordion" href="#q14" aria-expanded="true">
                                    <i class="fa fa-chevron-down pull-right text-muted"></i>
                                    Service Guard Error: You have reached your Daily limit.
                                </a>
                                <div id="q14" class="panel-collapse collapse">
                                    <hr>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal.
                                </div>
                            </div><!--
                            
                            -->                                <div class="panel-body" id="_transactionsecRest" style="display: none">
                                <a data-toggle="collapse" data-parent="#accordion" href="#q15" aria-expanded="true">
                                    <i class="fa fa-chevron-down pull-right text-muted"></i>
                                    Service Guard Error: Transaction Per Second Error.
                                </a>
                                <div id="q15" class="panel-collapse collapse">
                                    <hr>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal.
                                </div>
                            </div><!--
                            --><div class="panel-body" id="_accessinvalidIpRest" style="display: none">
                                <a data-toggle="collapse" data-parent="#accordion" href="#q16" aria-expanded="true">
                                    <i class="fa fa-chevron-down pull-right text-muted"></i>
                                    You are accessing from invalid IP.
                                </a>
                                <div id="q16" class="panel-collapse collapse">
                                    <hr>please check if you are behind proxy or using dynamic IP.
                                </div>
                            </div><!--
                            -->                                <div class="panel-body" id="_serNotaccesibleRest" style="display: none">
                                <a data-toggle="collapse" data-parent="#accordion" href="#q17" aria-expanded="true">
                                    <i class="fa fa-chevron-down pull-right text-muted"></i>
                                    You cannot access service now as it is not permitted for this day in the week.
                                </a>
                                <div id="q17" class="panel-collapse collapse">
                                    <hr>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal.
                                </div>
                            </div><!--
                            -->                                <div class="panel-body" id="serNOtPermitedRest" style="display: none">
                                <a data-toggle="collapse" data-parent="#accordion" href="#q18" aria-expanded="true">
                                    <i class="fa fa-chevron-down pull-right text-muted"></i>
                                    You cannot access service now as it is not permitted for this time of the day.
                                </a>
                                <div id="q18" class="panel-collapse collapse">
                                    <hr>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal.
                                </div>
                            </div><!--
                            -->                                <div class="panel-body" id="invalidCertRest" style="display: none">
                                <a data-toggle="collapse" data-parent="#accordion" href="#q20" aria-expanded="true">
                                    <i class="fa fa-chevron-down pull-right text-muted"></i>
                                    You are offering invalid/empty certificate.
                                </a>
                                <div id="q20" class="panel-collapse collapse">
                                    <hr>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal.
                                </div>
                            </div><!--
                            -->                                <div class="panel-body" id="_invalidTokenRest" style="display: none">
                                <a data-toggle="collapse" data-parent="#accordion" href="#q21" aria-expanded="true">
                                    <i class="fa fa-chevron-down pull-right text-muted"></i>
                                    You are offering invalid/empty Token.
                                </a>
                                <div id="q21" class="panel-collapse collapse">
                                    <hr>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal.
                                </div>
                            </div>
                            <div class="panel-body" id="_ipblockRest" style="display: none">
                                <a data-toggle="collapse" data-parent="#accordion" href="#q22" aria-expanded="true">
                                    <i class="fa fa-chevron-down pull-right text-muted"></i>
                                    IP BLOCK.
                                </a>
                                <div id="q22" class="panel-collapse collapse">
                                    <hr>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal.
                                </div>
                            </div>
                            <div class="panel-body" id="_invalidTokenRest" style="display: none">
                                <a data-toggle="collapse" data-parent="#accordion" href="#q23" aria-expanded="true">
                                    <i class="fa fa-chevron-down pull-right text-muted"></i>
                                    You are offering invalid/empty Token For API.
                                </a>
                                <div id="q23" class="panel-collapse collapse">
                                    <hr>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal.
                                </div>
                            </div><!--
                            -->                             <div class="panel-body" id="_signFailedRest" style="display: none">
                                <a data-toggle="collapse" data-parent="#accordion" href="#q24" aria-expanded="true">
                                    <i class="fa fa-chevron-down pull-right text-muted"></i>
                                    Signature Verification Failed.
                                </a>
                                <div id="q24" class="panel-collapse collapse">
                                    <hr>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal.
                                </div>
                            </div>
                            <div class="panel-body" id="_operationTimeOutRest" style="display: none">
                                <a data-toggle="collapse" data-parent="#accordion" href="#q25" aria-expanded="true">
                                    <i class="fa fa-chevron-down pull-right text-muted"></i>
                                    Operation timed out (Connection timed out).
                                </a>
                                <div id="q25" class="panel-collapse collapse">
                                    <hr>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal.
                                </div>
                            </div>    
                            <!--                            </div>                            -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--        </div>-->
</div>


<div class="content animate-panel" id="APIDescOnPage" style="padding:1px 30px 40px 40px !important">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="hpanel">
                <div class="panel-heading">
                    <h3><%=mName%></h3>
                </div>
                <div class="panel-body">
                    <%

                        List list = methodsDesc.methodClassName.methodNames;
                        for (int k = 0; k < list.size(); k++) {
                            MethodName methodName = (MethodName) list.get(k);
                            if (methodName != null) {

                                String name = methodName.methodname;
                                if (methodName.transformedname != null && !methodName.transformedname.isEmpty()) {
                                    name = methodName.transformedname;
                                }                               
                                if (name.equalsIgnoreCase(mName)) {
                                    if (methodName.visibility.equalsIgnoreCase("yes")) {
                                        ///count++;
                                        mName = methodName.methodname.split("::")[0];
                                        if (!methodName.transformedname.equals("")) {
                                            mName = methodName.transformedname.split("::")[0];
                                        }
                    %>
                    <h4 id="query-parameters"><b>API Description</b></h4>
                    <%if (methodName.description != null) {%>
                    <%=new String(Base64.decode(methodName.description))%>
                    <%} else {%>
                    OOPs! No Description available.
                    <%}%>
                    <h4 id="query-parameters"><b>Parameters Description</b></h4>

                    <table id="apiDesc" class="footable table table-hover table-responsive">
                        <thead>
                            <tr>
                                <th>Parameter</th>
                                <th>Parameter Type</th>
                                <th data-hide="phone">Required</th>
                                <th data-hide="phone">Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <%
                                int count = 0;
                                if (methodName.paramses != null && !methodName.paramses.isEmpty()) {
                                    ParamsData paramsData = new ParamsData(classes);
                                    for (int l = 0; l < methodName.paramses.size(); l++) {
                                        List<List<Params>> userDefClasses = new ArrayList();
                                        MethodParams params = methodName.paramses.get(l);
                                        String ParamName = params.name.split(";")[0];
                                        String ParamType = params.type.replace("<", "&lt;").replace(">", "&gt;");
                                        String tempType = params.type;
                                        tempType = tempType.replace("List<", "").replace(">", "");
                                        if (params.newname != null && !params.newname.isEmpty()) {
                                            ParamName = params.newname.split(";")[0];
                                        }
                                        boolean isClass = false;
                                        if (params.visibility.equalsIgnoreCase("yes") && (params.defaultvalue != null && params.defaultvalue.isEmpty())) {
                                            count++;
                                            ParamsData.Data data = paramsData.getData(tempType);
                                            if (data.paramses != null) {
                                                userDefClasses.add(data.paramses);
                                                isClass = true;
                                            }
                                            if (data.transformedclassname != null && !data.transformedclassname.isEmpty()) {
                                                ParamType = ParamType.replace(tempType, data.transformedclassname);
                                            }
                            %> 
                            <tr>
                                <td><%=ParamName%></td>
                                <td><%if (isClass) {%><a href="#query-parameters-<%=tempType%>"><%=ParamType%></a><%} else {%><%=ParamType%><%}%></td>
                                    <%if (methodName.paramses.get(l).isMandatory == true) {%>
                                <td><span style="color:green "> Mandatory</span></td>
                                <%} else {%>
                                <td> Optional</td>
                                <%}%>
                                <td><%if (methodName.paramses.get(l).pDescription != null) {%>
                                    <%=new String(Base64.decode(methodName.paramses.get(l).pDescription))%>
                                    <%} else {%>
                                    No information available
                                    <%}%>
                                </td>
                            </tr>
                            <%}
                                    }
                                }
                                if (count > 0) {
                                } else {%>        
                            <tr>
                                <td>Not required</td>
                                <td>Not required</td>
                                <td>Not required</td>
                                <td>Not required</td>
                            </tr>
                            <%}%>
                        </tbody>
                    </table>
                        <script>
                        $(function () {        
                            $('#apiDesc').footable();
                        });
                        </script>    
                    <h4 id="query-parameters"><b>Response Description</b></h4>    
                    <%
                        String resDesc = methodName.rDescription;
                        if (resDesc != null && !resDesc.isEmpty()) {
                            resDesc = new String(Base64.decode(resDesc));
                        } else {
                            resDesc = "OOPs! No Description available.";
                        }
                    %>    
                    <p><%=resDesc%></p> 
                    <%
                        ParamsData paramsData = new ParamsData(classes);
                        boolean isClass = false;
                        String ParamType = methodName.returntype.replace("<", "&lt;").replace(">", "&gt;");
                        String tempName = methodName.returntype.replace("List<", "").replace(">", "");
                        ParamsData.Data datas = paramsData.getData(tempName);
                        if (datas.paramses != null) {
                            isClass = true;
                        }
                        if (datas.transformedclassname != null && !datas.transformedclassname.isEmpty()) {
                            ParamType = ParamType.replace(tempName, datas.transformedclassname);
                        }%>
                    <h4 id="query-parameters"><b>Response Type: <%if (isClass) {%><a href="#query-parameters-<%=tempName%>"><%=ParamType%></a><%} else {%><%=ParamType%><%}%></b></h4>
                            <%
                                if (details.getResponseInformationDetails() != null && !details.getResponseInformationDetails().isEmpty()) {
                                    String jsonString = details.getResponseInformationDetails();
                                    int titleCount = 0;
                                    JSONArray jsOld = new JSONArray(jsonString);
                                    for (int temp = 0; temp < jsOld.length(); temp++) {
                                        JSONObject jsonexists1 = jsOld.getJSONObject(temp);
                                        if (jsonexists1.has(mName)) {
                                            String responseKey = jsonexists1.getString(mName);
                                            JSONObject responseDetails = new JSONObject(responseKey);
                                            Iterator itr = responseDetails.keys();
                                            while (itr.hasNext()) {
                                                Object jsonkey = itr.next();
                                                String keyData = (String) jsonkey;
                                                String value = responseDetails.getString((String) keyData);
                                                JSONObject responseAPIDetails = new JSONObject(value);
                                                titleCount++;
                                                if (titleCount == 1) {
                            %>
                    <h4 id="query-parameters"><b>Response</b></h4>
                    <%}%>     
                    <%if (titleCount == 1) {%>
                    <table class="footable table table-hover table-responsive">
                        <thead>
                            <tr>
                                <th>Field Name</th>
                                <th>Data Type</th>                                
                                <th>Description</th>
                            </tr>
                        </thead>

                        <tbody>
                            <%}%> 
                            <%
                                Iterator itrAPIDetails = responseAPIDetails.keys();
                                while (itrAPIDetails.hasNext()) {
                                    Object jsonAPIkey = itrAPIDetails.next();
                                    String keyAPIData = (String) jsonAPIkey;
                                    String apivalue = responseAPIDetails.getString((String) keyAPIData);
                                    String[] keyAPIDataArr = keyAPIData.split(";");
                            %>
                            <tr>
                                <td><code class="prettyprint"><%=keyData%>.<%=keyAPIDataArr[1]%></code></td>
                                <td><%=keyAPIDataArr[2]%></td>                                
                                <td><%=apivalue%></td>
                            </tr>
                            <%}%>

                            <%  }
                                        }
                                    }
                                }
                            %>
                        </tbody>
                    </table>
                    <br/>
                    <h4 id="query-parameters"> <b>User Defined Classes</b></h4>
                    <%
                        if (classes != null) {
                            List<ClassName> classs = classes.pname.classs;
                            for (ClassName clsname : classs) {
                                String className = clsname.classname;
                                ParamsData data = new ParamsData(classes);
                                ParamsData.Data d = data.getData(className);
                                if (clsname.transformedclassname != null && !clsname.transformedclassname.isEmpty()) {
                                    className = clsname.transformedclassname;
                                }

                    %>
                    <span id="query-parameters-<%=clsname.classname%>" style="color: #093554">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b><%=className%></b><br></span>
                    <br>
                    <table id="query-parameters-table<%=clsname.classname%>" class="footable table table-hover table-responsive">
                        <thead>
                            <tr>
                                <th>Name</th>
                                
                                <th>Type</th>
                                <th data-hide="phone">Required</th>
                                <th data-hide="phone">Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <%
                                List<Params> paramses = d.paramses;
                                if (paramses != null) {
                                    for (Params params : paramses) {
                                        if (params.visibility.equalsIgnoreCase("yes")) {
                                            String paramName = params.name;
                                            String paramType = params.type, tempName1;
                                            if (params.newname != null && !params.newname.isEmpty()) {
                                                paramName = params.newname;
                                            }
                                            tempName1 = paramType = paramType.replace("List<", "").replace(">", "");
                                            ParamsData.Data d2 = data.getData(paramType);
                                            boolean isClass1 = false;
                                            if (d2.paramses != null) {
                                                isClass1 = true;
                                                if (d2.transformedclassname != null && !d2.transformedclassname.isEmpty()) {
                                                    paramType = params.type.replace(paramType, d2.transformedclassname);
                                                } else {
                                                    paramType = params.type;
                                                }
                                            }
                                            String Descripton = "No information available";
                                            if (params.pDescription != null) {
                                                Descripton = new String(Base64.decode(params.pDescription));
                                            }
                            %>

                            <tr>
                                <td><%=paramName%></td>
                                <td><%if (isClass1) {%>
                                    <a href="#query-parameters-<%=tempName1%>"><%=paramType.replace("<", "&lt;").replace(">", "&gt;")%></a>
                                    <%} else {%>
                                    <%=params.type.replace("<", "&lt;").replace(">", "&gt;")%>
                                    <%}%>
                                </td>
                                <%if (params.isMandatory == true) {%>
                                <td><span style="color:green "> Mandatory</span></td>
                                <%} else {%>
                                <td> Optional</td>
                                <%}%>
                                <td><%=Descripton%></td>
                            </tr>

                            <%}
                                    }
                                }
                            %>
                        </tbody>
                    </table>
                    <script>
                        $(function () {        
                            $('#query-parameters-table<%=clsname.classname%>').footable();
                        });
                    </script>    
                    <%}
                    %>
                    
                    <%}%>
                    <br/>
                    <h4 id="query-parameters"> <b>Error Codes</b></h4>
                    <%
                        String errorCodeDetails = details.getErrorCodeDetails();
                        JSONObject errJSON = null;
                        if (errorCodeDetails != null) {
                            errJSON = new JSONObject(errorCodeDetails);
                        }
                        if (errJSON != null && errJSON.length() != 0) {
                    %>
                    <table class="footable table table-hover table-responsive">
                        <thead>
                            <tr>
                                <th>Error Code</th>
                                <th>Meaning</th>
                            </tr>
                        </thead>
                        <tbody>
                            <%
                                Iterator<String> keys = errJSON.keys();
                                while (keys.hasNext()) {
                                    String keyData = keys.next();
                                    String value = errJSON.getString(keyData);
                            %>
                        </tbody>
                        <tr>
                            <td><%=keyData%></td>
                            <td><%=value%></td>
                        </tr>
                        <%}%>
                    </table>
                    <%} else {%>
                    <p>OOPs! No Description available.</p>        

                    <%}
                                    }
                                    break;
                                }
                            }
                        }

                    %>                   
                </div>
            </div>             
        </div>
    </div>
    <textarea id="mySoapHeaders" style="display: none"></textarea>
    <textarea id="myNewSoapHeaders" style="display: none"></textarea>
    <textarea id="myRestHeaders" style="display: none"></textarea>
</div>
<!--</div>-->

<script language="javascript" type="text/javascript">
    LoadBody();
    document.getElementById("addUserButton").click();
    document.getElementById("addUserButtonRest").click();
    document.getElementById("RMText0").value = "PartnerId";
    document.getElementById("RMType0").value = "<%=parObj.getPartnerUniqueId()%>";
    document.getElementById("RMTextRest0").value = "PartnerId";
    document.getElementById("RMTypeRest0").value = "<%=parObj.getPartnerUniqueId()%>";


    <%
        if (policy.authentication.equals("Enable")) {
            if (policy.tokenCheck.equalsIgnoreCase("yes")) {%>
    document.getElementById("addUserButton").click();
    document.getElementById("addUserButtonRest").click();
    document.getElementById("RMText1").value = "PartnerTokenId";
    document.getElementById("RMType1").value = "<%=partnerToken%>";
    document.getElementById("RMTextRest1").value = "PartnerTokenId";
    document.getElementById("RMTypeRest1").value = "<%=partnerToken%>";
    <%}
        }
    %>
    <%
        if (apiToken == null) {%>
    autoAssignAPIToken('<%=mName%>');
    <%} else {%>

    document.getElementById("_myNameRM").value = "APITokenId";
    document.getElementById("_myResType").value = "<%=apiToken%>";

    document.getElementById("_myNameRMRest").value = "APITokenId";
    document.getElementById("_myResTypeRest").value = "<%=apiToken%>";
    <%}%>

    $(document).ready(function () {

        window.setTimeout(function () {

            var apiTokenId = document.getElementById("_myResType").value;
            var labelApiTokenId = document.getElementById("_myNameRM").value;

            var partnerId = document.getElementById("RMType0").value;
            var labelPartnerId = document.getElementById("RMText0").value;

            var partnerTokenIdd = document.getElementById("RMType1").value;
            var labelpartnerTokenId = document.getElementById("RMText1").value;
            var soapHeaderContent = labelApiTokenId + ":" + apiTokenId + "," + labelPartnerId + ":" + partnerId + "," + labelpartnerTokenId + ":" + partnerTokenIdd;
            //document.getElementById("headerContent").value = labelApiTokenId + ":" +apiTokenId+","+labelPartnerId+":"+partnerId+","+labelpartnerTokenId+":"+partnerTokenIdd;
            var array = soapHeaderContent.split(',');
            var headers = "";
            for (a in array) {
                headers += array[a] + "\n"; // Explicitly include base as per �lvaro's comment
            }
//            document.getElementById("copyTarget").style.display = 'block';
            $("#mySoapHeaders").val(headers);
//            document.getElementById("copyTarget").style.display = 'none';
            console.log("Header Content >> " + soapHeaderContent);
            document.getElementById("headerC").value = soapHeaderContent;
            var copyTag = "<a href='#/' style='font-size:10px; margin-left:15%;text-decoration:none' class='btn btn-info btn-xs' onclick=copy('" + soapHeaderContent + "','copyHeader')><b>Copy</b></a>";
            console.log("copyTag >>> " + copyTag);
            document.getElementById('headerCopyDiv').innerHTML = copyTag;

            var apiTokenRestId = document.getElementById("_myResTypeRest").value;
            var labelApiTokenIdRest = document.getElementById("_myNameRMRest").value;

            var partnerIdRest = document.getElementById("RMTypeRest0").value;
            var labelPartnerIdRest = document.getElementById("RMTextRest0").value;

            var partnerTokenIddRest = document.getElementById("RMTypeRest1").value;
            var labelpartnerTokenIdRest = document.getElementById("RMTextRest1").value;
            var restHeaderContent = labelApiTokenIdRest + ":" + apiTokenRestId + "," + labelPartnerIdRest + ":" + partnerIdRest + "," + labelpartnerTokenIdRest + ":" + partnerTokenIddRest;
            console.log("REST Header Content >> " + restHeaderContent);
            document.getElementById("restHeaderData").value = restHeaderContent;
            var copyTagRest = "<a href='#/' style='font-size:10px; margin-left:15%;text-decoration:none' class='btn btn-info btn-xs'  onclick=copy('" + restHeaderContent + "','copyRestHeader')><b>Copy</b></a>";
            console.log("copyRestTag >>> " + copyTagRest);
            document.getElementById('headerRestCopyDiv').innerHTML = copyTagRest;

            var restURL = document.getElementById("restURLData").value;
            var copyTagRestURL = "<a href='#/' style='font-size:10px; margin-left:15%;text-decoration:none' class='btn btn-info btn-xs' onclick=copy('" + restURL + "','copyRestURL')><b>Copy</b></a>";
            console.log("copyRestTag >>> " + copyTagRestURL);
            document.getElementById('RestURLCopyDiv').innerHTML = copyTagRestURL;

            var soapURL = document.getElementById("soapURLData").value;
            var copyTagSoapURL = "<a href='#/' style='font-size:10px; text-decoration:none' class='btn btn-info btn-xs' onclick=copy('" + soapURL + "','copySOAPURL')><b>Copy</b></a>";
            console.log("copySoapTag >>> " + copyTagSoapURL);
            document.getElementById('SOAPURLCopyDiv').innerHTML = copyTagSoapURL;

            var array = restHeaderContent.split(',');
            var headers = "";
            for (a in array) {
                headers += array[a] + "\n"; // Explicitly include base as per �lvaro's comment
            }
            $("#myRestHeaders").val(headers);
            var restURLRequestType = document.getElementById("restURLRequestType").value;
            var copyRestRequestURL = "<a href='#/' style='font-size:10px; text-decoration:none' class='btn btn-info btn-xs' onclick=copy('" + restURLRequestType + "','copyRestRequest')><b>Copy</b></a>";
            console.log("copyRestRequestURL >>> " + copyRestRequestURL);
            document.getElementById('RESTRequestURLCopyDiv').innerHTML = copyRestRequestURL;

            //var soapBodyData = document.getElementById("_APIbody").value;
            var soapBodyData = "SOAP";
            var copySOAPBody = "<a href='#/' style='font-size:10px; text-decoration:none' class='btn btn-info btn-xs' onclick=copy('" + soapBodyData + "','copySOAPBody')><b>Copy</b></a>";
            console.log("copySOAPBody >>> " + copySOAPBody);
            document.getElementById('bodyCopyDiv').innerHTML = copySOAPBody;

            var restBodyData = "Rest";
            var copyRestBody = "<a href='#/' style='font-size:10px; text-decoration:none' class='btn btn-info btn-xs' onclick=copy('" + restBodyData + "','copyRESTBody')><b>Copy</b></a>";
            console.log("copyRestBody >>> " + copyRestBody);
            document.getElementById('bodyRestCopyDiv').innerHTML = copyRestBody;
        }, 2000);
    });
    function copy(text, id) {
        var t;
        if (id === 'copyHeader') {
            t = document.getElementById("headerCopyDiv");
        } else if (id === 'copySOAPBody') {
            t = document.getElementById("bodyCopyDiv");
        } else if (id === 'copyRestHeader') {
            t = document.getElementById("headerRestCopyDiv");
        } else if (id === 'copyRestURL') {
            t = document.getElementById("RestURLCopyDiv");
        } else if (id === 'copySOAPURL') {
            t = document.getElementById("SOAPURLCopyDiv");
        } else if (id === 'copyRestRequest') {
            t = document.getElementById("RESTRequestURLCopyDiv");
        } else if (id === 'copyRESTBody') {
            t = document.getElementById("bodyRestCopyDiv");
        } else if (id === 'copySOAPResponse') {
            t = document.getElementById("soapResponseCopyDiv");
        }else if (id === 'copyRESTResponse') {
            t = document.getElementById("restResponseCopyDiv");
        }else {
            t = document.getElementById("copyDiv");
        }
        t.classList.add('copied');
        setTimeout(function () {
            t.classList.remove('copied');
        }, 1500);
        console.log('text >> ' + text);
        console.log("id >> " + id);

        if (id === 'copyHeader') {
            $("#myNewSoapHeaders").val("");
            var soapHeaderCount = document.getElementById("soapUserDefinedHeader").value;
            if (soapHeaderCount !== 2) {
                var headerSoap = document.getElementById("mySoapHeaders").value;
                for (var soapH = 2; soapH < soapHeaderCount; soapH++) {
                    var soapHK = document.getElementById('RMText' + soapH).value;
                    var soapHV = document.getElementById('RMType' + soapH).value;
                    headerSoap += soapHK + ":" + soapHV + "\n";
                }
                console.log("Soap Extra Header " + headerSoap);
                document.getElementById("myNewSoapHeaders").style.display = "block";
                $("#myNewSoapHeaders").val(headerSoap);
                $("#myNewSoapHeaders").select();
                document.execCommand('copy');
                document.getElementById("myNewSoapHeaders").style.display = "none";
            } else {
                document.getElementById("myNewSoapHeaders").style.display = "block";
                $("#myNewSoapHeaders").val(document.getElementById("soapUserDefinedHeader").value);
                $("#myNewSoapHeaders").select();
                document.execCommand('copy');
                document.getElementById("myNewSoapHeaders").style.display = "none";
            }
        } else if (id === 'copySOAPBody') {
            // copyToClipboardMsg(document.getElementById("copyTarget"), "copyBody");
            var soapBody = document.getElementById("_APIbody").value;
            $("#myNewSoapHeaders").val("");
            document.getElementById("myNewSoapHeaders").style.display = "block";
            $("#myNewSoapHeaders").val(soapBody);
            $("#myNewSoapHeaders").select();
            document.execCommand('copy');
            document.getElementById("myNewSoapHeaders").style.display = "none";

        } else if (id === 'copyRESTBody') {
            // copyToClipboardMsg(document.getElementById("copyTarget"), "copyBody");
            var restBody = document.getElementById("_RESTAPIbody").value;
            $("#myNewSoapHeaders").val("");
            document.getElementById("myNewSoapHeaders").style.display = "block";
            $("#myNewSoapHeaders").val(restBody);
            $("#myNewSoapHeaders").select();
            document.execCommand('copy');
            document.getElementById("myNewSoapHeaders").style.display = "none";

        }else if (id === 'copyRESTResponse') {
            // copyToClipboardMsg(document.getElementById("copyTarget"), "copyBody");
            var restResponse = document.getElementById("restresponseIDCopy").value;
            console.log("restResponse >> "+restResponse);
            //var soapResponse = text;
            $("#myNewSoapHeaders").val("");
            document.getElementById("myNewSoapHeaders").style.display = "block";
            $("#myNewSoapHeaders").val(restResponse);
            $("#myNewSoapHeaders").select();
            document.execCommand('copy');
            document.getElementById("myNewSoapHeaders").style.display = "none";

        }else if (id === 'copySOAPResponse') {
            // copyToClipboardMsg(document.getElementById("copyTarget"), "copyBody");
            var soapResponse = document.getElementById("responseIDCopy").value;
            console.log("soapResponse >> "+soapResponse);
            //var soapResponse = text;
            $("#myNewSoapHeaders").val("");
            document.getElementById("myNewSoapHeaders").style.display = "block";
            $("#myNewSoapHeaders").val(soapResponse);
            $("#myNewSoapHeaders").select();
            document.execCommand('copy');
            document.getElementById("myNewSoapHeaders").style.display = "none";

        } else if (id === 'copyRestHeader') {
            $("#myNewSoapHeaders").val("");
            var restHeaderCount = document.getElementById("restUserDefinedHeader").value;
            if (restHeaderCount !== 2) {
                var headerrest = document.getElementById("myRestHeaders").value;
                for (var restH = 2; restH < restHeaderCount; restH++) {
                    var restHK = document.getElementById('RMTextRest' + restH).value;
                    var restHV = document.getElementById('RMTypeRest' + restH).value;
                    headerrest += restHK + ":" + restHV + "\n";
                }
                console.log("Rest Extra Header " + headerrest);
                document.getElementById("myNewSoapHeaders").style.display = "block";
                $("#myNewSoapHeaders").val(headerrest);
                $("#myNewSoapHeaders").select();
                document.execCommand('copy');
                document.getElementById("myNewSoapHeaders").style.display = "none";
            } else {
                document.getElementById("myNewSoapHeaders").style.display = "block";
                $("#myNewSoapHeaders").val(document.getElementById("restUserDefinedHeader").value);
                $("#myNewSoapHeaders").select();
                document.execCommand('copy');
                document.getElementById("myNewSoapHeaders").style.display = "none";
            }

        } else if (id === 'copyRestURL') {
            var restURL = document.getElementById("restURLData").value;
            $("#myNewSoapHeaders").val("");
            document.getElementById("myNewSoapHeaders").style.display = "block";
            $("#myNewSoapHeaders").val(restURL);
            $("#myNewSoapHeaders").select();
            document.execCommand('copy');
            document.getElementById("myNewSoapHeaders").style.display = "none";
        } else if (id === 'copySOAPURL') {
            $("#myNewSoapHeaders").val("");
            var soapURL = document.getElementById("soapURLData").value;
            document.getElementById("myNewSoapHeaders").style.display = "block";
            $("#myNewSoapHeaders").val(soapURL);
            $("#myNewSoapHeaders").select();
            document.execCommand('copy');
            document.getElementById("myNewSoapHeaders").style.display = "none";
        } else if (id === 'copyRestRequest') {
            $("#myNewSoapHeaders").val("");
            document.getElementById("myNewSoapHeaders").style.display = "block";
            var restRequest = document.getElementById("restURL").value;
            $("#myNewSoapHeaders").val(restRequest);
            $("#myNewSoapHeaders").select();
            document.execCommand('copy');
            document.getElementById("myNewSoapHeaders").style.display = "none";
        }
    }

    function copyToClipboardMsg(elem, msgElem) {
        var succeed = copyToClipboard(elem);
        var msg;
        if (!succeed) {
            msg = "Copy not supported or blocked.  Press Ctrl+c to copy."
        } else {
            msg = "Text copied to the clipboard."
//        var copyTag = "<a href='#/' style='font-size:15px; margin-left:15%;text-decoration:none' onclick=copy('" + elem + "')><b>Copied</b></a>";
//        document.getElementById("copyDiv").innerHTML = copyTag;

        }
        console.log(msg);
//                if (typeof msgElem === "string") {
//                    msgElem = document.getElementById(msgElem);
//                }
//                msgElem.innerHTML = msg;
//                setTimeout(function () {
//                    msgElem.innerHTML = "";
//                }, 2000);
    }

    function copyToClipboard(elem) {
        // create hidden text element, if it doesn't already exist
        var targetId = "_hiddenCopyText_";
        var isInput = elem.tagName === "INPUT" || elem.tagName === "TEXTAREA";
        var origSelectionStart, origSelectionEnd;
        if (isInput) {
            // can just use the original source element for the selection and copy
            target = elem;
            origSelectionStart = elem.selectionStart;
            origSelectionEnd = elem.selectionEnd;
        } else {
            // must use a temporary form element for the selection and copy
            target = document.getElementById(targetId);
            if (!target) {
                var target = document.createElement("textarea");
                target.style.position = "absolute";
                target.style.left = "-9999px";
                target.style.top = "0";
                target.id = targetId;
                document.body.appendChild(target);
            }
            target.textContent = elem.textContent;
        }
        // select the content
        var currentFocus = document.activeElement;
        target.focus();
        target.setSelectionRange(0, target.value.length);

        // copy the selection
        var succeed;
        try {
            succeed = document.execCommand("copy");
        } catch (e) {
            succeed = false;
        }
        // restore original focus
        if (currentFocus && typeof currentFocus.focus === "function") {
            currentFocus.focus();
        }

        if (isInput) {
            // restore prior selection
            elem.setSelectionRange(origSelectionStart, origSelectionEnd);
        } else {
            // clear temporary content
            target.textContent = "";
        }
        return succeed;
    }
</script>
