
<%@page import="com.mollatech.service.nucleus.crypto.LoadSettings"%>
<!DOCTYPE html>
<html>
    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <!-- Page title -->
        <title>Ready API</title>

        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
        <!--<link rel="shortcut icon" type="image/ico" href="favicon.ico" />-->

        <!-- Vendor styles -->
        <link rel="stylesheet" href="vendor/fontawesome/css/font-awesome.css" />
        <link rel="stylesheet" href="vendor/metisMenu/dist/metisMenu.css" />
        <link rel="stylesheet" href="vendor/animate.css/animate.css" />
        <link rel="stylesheet" href="vendor/bootstrap/dist/css/bootstrap.css" />

        <!-- App styles -->
        <link rel="stylesheet" href="fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css" />
        <link rel="stylesheet" href="fonts/pe-icon-7-stroke/css/helper.css" />
        <link rel="stylesheet" href="styles/static_custom.css">
        <link rel="stylesheet" href="styles/style.css">
        <script src="scripts/mobileSDK.js" type="text/javascript"></script>
    </head>
    <body class="landing-page">
        <%
            String sdkFilePath = LoadSettings.g_strPath + "mobileTrustSDK.zip";

        %>
        <!-- Simple splash screen-->
        <div class="splash"> <div class="color-line"></div><div class="splash-title"><p>Loading ... </p><div class="spinner"> <div class="rect1"></div> <div class="rect2"></div> <div class="rect3"></div> <div class="rect4"></div> <div class="rect5"></div> </div> </div> </div>
        <!--[if lt IE 7]>
        <p class="alert alert-danger">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a href="mobileTrustSDK.jsp" class="navbar-brand">Mobile Trust SDK</a>            
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li class="active"><a class="page-scroll" href="#page-top">Home</a></li>
                        <!--                <li><a class="page-scroll" page-scroll href="#features2">Services</a></li>-->
                        <!--                <li><a class="page-scroll" page-scroll href="#components">Features</a></li>-->
                        <li><a class="page-scroll" page-scroll href="#billingFAQ">How To Use</a></li>
                        <li><a class="page-scroll" page-scroll href="#contact">Contact</a></li>
                    </ul>
                </div>
            </div>
        </nav>

        <header id="page-top">
            <div class="container">
                <div class="heading">
                    <h1>
                        Welcome to Moblie Trust SDK
                    </h1>
                    <!--            <span>Contrary to popular belief, Lorem Ipsum is not<br/> simply random text for print.</span>-->
                    <!--            <span><a href="<%=sdkFilePath%>" target="_blank">Download SDK files</a></span>-->
                    <p class="small" style="max-width: 490px!important;font-size: 115%!important">
                        Business application will have Axiom protect for make secure transaction with Axiom Protect's Mobile Trust. The intention is to provide off the shelf security options that can be added to any mobile native, web and mobile web applications. Axiom Protect offers backend Web service APIs along with mobile /web side SDK for end to end authentication and identity + transaction protection.
                    </p>            
                </div>
                <div class="heading-image animate-panel" data-child="img-animate" data-effect="fadeInRight">
<!--                    <p class="small">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>-->
                    <!--            <img class="img-animate" src="images/report2.png" width="155px" height="155px"/>
                                <img class="img-animate" src="images/mostUsedServices.png" width="155px" height="155px"/>
                                <img class="img-animate" src="images/developerSignedIn.png" width="155px" height="155px"/>
                                <img class="img-animate" src="images/todayVsWeekCreditUsage.png" width="155px" height="155px"/>-->

                    <br/>                       
                    <br/>
                    <br/><br/>
                    <img class="img-animate" src="images/landing/Collage.jpg" width="155px" height="155px"/>
                    <img class="img-animate" src="images/landing/mobile-security.jpg" width="155px" height="155px"/>
                    <img class="img-animate" src="images/landing/mobileTrust.jpg" width="155px" height="155px"/>
<!--                    <img class="img-animate" src="images/report3.png" width="155px" height="155px">-->
                </div>
            </div>
        </header>
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <h4>Our Expertise</h4>
                        <p style="font-size: 120%">Our Mobile SDK provides comprehensive identity options starting with One Time Passwords, Signature One Time Passwords, Digital certificate based digital signatures (for non-repudiation) and in addition to it gives geo fencing, device profiling, time stamping, self-destroy, multi user on single device and multiple device for single user as its features to enjoy from.</p>
                        <!--            <p><a class="navy-link btn btn-sm" href="#" role="button">Learn more</a></p>-->
                    </div>
                    <div class="col-md-4">
                        <h4>Overall Structure</h4>
                        <p style="font-size: 120%">There are two components needed for enabling the end-to- end authentication. First component is server side platform for user, token, certificate management and other component is mobile side (SDK) that will get integrated inside the app.</p>
                        <!--            <p><a class="navy-link btn btn-sm" href="#" role="button">Learn more</a></p>-->
                    </div>
                    <div class="col-md-4">
                        <h4>Deliverables</h4>
                        <p style="font-size: 120%">The Web services are built using SOAP standard so any .NET/Java application will be able to import the WSDL and enjoy the services.</p>                        
                    </div>
                </div>
            </div>
        </section>
        <!--<section id="components" class="bg-light">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <h2><span class="text-success"><b>Many features </span>to discover with Mobile Trust SDK</b></h2>
                        <p>Lorem Ipsum available, but the majority have suffered alteration euismod. </p>
                    </div>
                </div>
                <div class="row m-t-md">
                    <div class="col-md-8">
                        <h3>Special designed API Console</h3>
                        <p style="font-size: 15px;text-align: justify">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum.</p>
                    </div>
                    <div class="col-md-4">              
                        <img src="images/SS-Technology.png" class="img-responsive" width="70%" height="70%"/>
        <img src="images/crossplatform.svg" class="img-responsive" width="50%" height="50%" style="margin-left: 45%"></a>
                    </div>
                </div>
                <div class="row m-t-xl">
                    <div class="col-md-4">
                        <img src="images/seamless.png" class="img-responsive" width="90%" height="70%"/>
                        <img src="images/decentralization.svg" class="img-responsive" width="50%" height="50%"></a>
                    </div>
                    <div class="col-md-8">
                        <h3>Integration and Compatibility </h3>
                        <p style="font-size: 15px;text-align: justify">We have language bindings in 7 languages from Ruby,PHP, Java, JS, Python and many more! You can view code sample by our experts to suit your business and industry specific needs.</p>
                    </div>
                </div>
                <div class="row  m-t-xl">
                    <div class="col-md-8">
                        <h3>API Documentation</h3>
                        <p style="font-size: 15px;text-align: justify">You can get reference for all our Services and their APIs in terms of how they work Ready API endpoints. You can view sample code examples. We hope you will find the value for all APIs under our offered services and their APIs </p>
                    </div>
                    <div class="col-md-4">
                        <img src="images/Protect Website Usability.png" class="img-responsive" width="70%" heigh="70%"/>
                        <img src="images/market.svg" class="img-responsive" width="50%" heigh="50%" style="margin-left: 45%"/>
                    </div>
                </div>        
                <div class="row m-t-xl">
                    <div class="col-md-4">                
                        <img src="images/Anti Bot Service to control cost.png" class="img-responsive" width="70%" heigh="70%" />
                        <img src="images/nofees.svg" class="img-responsive" width="50%" heigh="50%"/>
                    </div>
                    <div class="col-md-8">
                        <h3>Credit as per API Usage</h3>
                        <p style="font-size: 15px;text-align: justify">Dashboards and out-of-the-box reports provide visibility into APIs and services from different perspectives. You can understand API traffic dynamics and take informed decisions accordingly.</p>
                    </div>
                </div>
                <div class="row m-t-xl">            
                    <div class="col-md-8">
                        <h3>Additional analytical components</h3>
                        <p style="font-size: 15px;text-align: justify">Dashboards and out-of-the-box reports provide visibility into APIs and services from different perspectives. You can understand API traffic dynamics and take informed decisions accordingly.</p>
                    </div>
                    <div class="col-md-4">
                        <img src="images/Human Experts1.png" class="img-responsive" width="70%" heigh="70%"/>
                        <img src="images/nocensorship.svg" class="img-responsive" width="50%" heigh="50%" style="margin-left: 45%"></a>
                    </div>
                </div>
            </div>
        </section>-->
        <section id="billingFAQ" class="bg-light">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <h2><span class="text-success"><b>How to use  </span>FAQ</b></h2>
                        <p style="font-size: 120%">There are specific requirements for each platform which will run Mobile Trust based applications. Click on specific platform to view the corresponding requirements. </p>
                    </div>
                </div>
                <!--        <div class="row m-t-md">
                            <div class="col-md-6">
                                <h4>What is System requirment to use SDK?</h4>
                                <p style="font-size: 14px;text-align: justify">There are specific requirements for each platform which will run Mobile Trust SDK applications. Click on specific platform to view the corresponding requirements.</p>
                                <div class="dd" id="nestable2" style="width: 555px!important; height: 106px!important">
                                    <ol class="dd-list" style="width: 555px!important; height: 106px!important">
                                        <li class="dd-item" data-id="1">
                                            <div class="dd-handle">
                                                <span class="label h-bg-navy-blue"><i class="fa fa-weibo"></i></span> Web.
                                            </div>
                                            <ol class="dd-list">
                                                <li class="dd-item" data-id="2">
                                                    <div class="dd-handle">
                                                        <span class="pull-right"> 12:00 pm </span>
                                                        <span class="label h-bg-navy-blue"><i class="fa fa-cog"></i></span> Free space on hard disk drive (HDD)
                                                    </div>
                                                </li>
                                                <li class="dd-item" data-id="3">
                                                    <div class="dd-handle">
                                                        <span class="pull-right"> 11:00 pm </span>
                                                        <span class="label h-bg-navy-blue"><i class="fa fa-bolt"></i></span> RAM.
                                                    </div>
                                                </li>
                                                <li class="dd-item" data-id="4">
                                                    <div class="dd-handle">
                                                        <span class="pull-right"> 11:00 pm </span>
                                                        <span class="label h-bg-navy-blue"><i class="fa fa-laptop"></i></span> Development environments
                                                    </div>
                                                </li>
                                            </ol>
                                        </li>
                
                                        <li class="dd-item" data-id="5">
                                            <div class="dd-handle">
                                                <span class="label h-bg-navy-blue"><i class="fa fa-ioxhost"></i></span> iOS platform.
                                            </div>
                                            <ol class="dd-list">
                                                <li class="dd-item" data-id="6">
                                                    <div class="dd-handle">
                                                        <span class="pull-right"> iPhone 5 or newer iPhone </span>
                                                        <span class="label h-bg-navy-blue"><i class="fa fa-users"></i></span> Device.
                                                    </div>
                                                </li>
                                                <li class="dd-item" data-id="6">
                                                    <div class="dd-handle">
                                                        <span class="pull-right">Mac OS X 10.7.x or newer.</span>
                                                        <span class="label h-bg-navy-blue"><i class="fa fa-users"></i></span> OS.
                                                    </div>
                                                </li>
                                                <li class="dd-item" data-id="6">
                                                    <div class="dd-handle">
                                                        <span class="pull-right">2 GHz or better processor is recommended.</span>
                                                        <span class="label h-bg-navy-blue"><i class="fa fa-users"></i></span> Processor.
                                                    </div>
                                                </li>
                                                <li class="dd-item" data-id="7">
                                                    <div class="dd-handle">
                                                        <span class="pull-right"> At least 20 MB of free RAM  </span>
                                                        <span class="label h-bg-navy-blue"><i class="fa fa-bomb"></i></span> RAM
                                                    </div>
                                                </li>
                                                <li class="dd-item" data-id="8">
                                                    <div class="dd-handle">
                                                        <span class="pull-right"> At least 30 MB required  </span>
                                                        <span class="label h-bg-navy-blue"><i class="fa fa-child"></i></span> Free storage.
                                                    </div>
                                                </li>
                                                <li class="dd-item" data-id="8">
                                                    <div class="dd-handle">
                                                        <span class="pull-right">Xcode 6.4 or newer</span>
                                                        <span class="label h-bg-navy-blue"><i class="fa fa-child"></i></span> Development environment.
                                                    </div>
                                                </li>
                                            </ol>
                                        </li>                        
                                        <li class="dd-item" data-id="5">
                                            <div class="dd-handle">
                                                <span class="label h-bg-navy-blue"><i class="fa fa-android"></i></span> Android platform.
                                            </div>
                                            <ol class="dd-list">
                                                <li class="dd-item" data-id="6">
                                                    <div class="dd-handle">
                                                        <span class="pull-right">A smartphone that is running Android 4.4 OS or newer.</span>
                                                        <span class="label h-bg-navy-blue"><i class="fa fa-users"></i></span> Device.
                                                    </div>
                                                </li>
                                                <li class="dd-item" data-id="6">
                                                    <div class="dd-handle">
                                                        <span class="pull-right">Android 4.4 OS or newer.</span>
                                                        <span class="label h-bg-navy-blue"><i class="fa fa-users"></i></span> OS.
                                                    </div>
                                                </li>
                                                <li class="dd-item" data-id="6">
                                                    <div class="dd-handle">
                                                        <span class="pull-right">1.5 GHz processor</span>
                                                        <span class="label h-bg-navy-blue"><i class="fa fa-users"></i></span> Processor.
                                                    </div>
                                                </li>
                                                <li class="dd-item" data-id="7">
                                                    <div class="dd-handle">
                                                        <span class="pull-right">At least 20 MB of free RAM</span>
                                                        <span class="label h-bg-navy-blue"><i class="fa fa-bomb"></i></span> RAM
                                                    </div>
                                                </li>
                                                <li class="dd-item" data-id="8">
                                                    <div class="dd-handle">
                                                        <span class="pull-right">At least 30 MB required</span>
                                                        <span class="label h-bg-navy-blue"><i class="fa fa-child"></i></span> Free storage.
                                                    </div>
                                                </li>
                                                <li class="dd-item" data-id="8">
                                                    <div class="dd-handle">
                                                        <span class="pull-right">Java SE JDK 6 (or higher), Android SDK</span>
                                                        <span class="label h-bg-navy-blue"><i class="fa fa-child"></i></span> Development environment.
                                                    </div>
                                                </li>
                                            </ol>
                                        </li>
                                    </ol>
                                </div>
                            </div>
                            <div class="col-md-6">              
                                <h4>What Downloads contents</h4>
                                <p style="font-size: 14px;text-align: justify">Downloads includes the library and other supportive files for Mobile trust SDK, Click on specific platform to view the corresponding lib and files.</p>                
                                <div class="dd" id="nestable">
                                    <ol class="dd-list">
                                        <li class="dd-item" data-id="11">
                                            <div class="dd-handle">
                                                <span class="label h-bg-navy-blue"><i class="fa fa-weibo"></i></span> Web.
                                            </div>
                                            <ol class="dd-list">
                                                <li class="dd-item" data-id="12">
                                                    <div class="dd-handle">
                                                        <span class="pull-right"> 12:00 pm </span>
                                                        <span class="label h-bg-navy-blue"><i class="fa fa-cog"></i></span> Free space on hard disk drive (HDD)
                                                    </div>
                                                </li>
                                                <li class="dd-item" data-id="13">
                                                    <div class="dd-handle">
                                                        <span class="pull-right"> 11:00 pm </span>
                                                        <span class="label h-bg-navy-blue"><i class="fa fa-bolt"></i></span> RAM.
                                                    </div>
                                                </li>
                                                <li class="dd-item" data-id="14">
                                                    <div class="dd-handle">
                                                        <span class="pull-right"> 11:00 pm </span>
                                                        <span class="label h-bg-navy-blue"><i class="fa fa-laptop"></i></span> Development environments
                                                    </div>
                                                </li>
                                            </ol>
                                        </li>
                
                                        <li class="dd-item" data-id="15">
                                            <div class="dd-handle">
                                                <span class="label h-bg-navy-blue"><i class="fa fa-ioxhost"></i></span> iOS platform.
                                            </div>
                                            <ol class="dd-list">
                                                <li class="dd-item" data-id="16">
                                                    <div class="dd-handle">
                                                        <span class="pull-right"> iPhone 5 or newer iPhone </span>
                                                        <span class="label h-bg-navy-blue"><i class="fa fa-users"></i></span> Device.
                                                    </div>
                                                </li>
                                                <li class="dd-item" data-id="6">
                                                    <div class="dd-handle">
                                                        <span class="pull-right">Mac OS X 10.7.x or newer.</span>
                                                        <span class="label h-bg-navy-blue"><i class="fa fa-users"></i></span> OS.
                                                    </div>
                                                </li>
                                                <li class="dd-item" data-id="6">
                                                    <div class="dd-handle">
                                                        <span class="pull-right">2 GHz or better processor is recommended.</span>
                                                        <span class="label h-bg-navy-blue"><i class="fa fa-users"></i></span> Processor.
                                                    </div>
                                                </li>
                                                <li class="dd-item" data-id="7">
                                                    <div class="dd-handle">
                                                        <span class="pull-right"> At least 20 MB of free RAM  </span>
                                                        <span class="label h-bg-navy-blue"><i class="fa fa-bomb"></i></span> RAM
                                                    </div>
                                                </li>
                                                <li class="dd-item" data-id="8">
                                                    <div class="dd-handle">
                                                        <span class="pull-right"> At least 30 MB required  </span>
                                                        <span class="label h-bg-navy-blue"><i class="fa fa-child"></i></span> Free storage.
                                                    </div>
                                                </li>
                                                <li class="dd-item" data-id="8">
                                                    <div class="dd-handle">
                                                        <span class="pull-right">Xcode 6.4 or newer</span>
                                                        <span class="label h-bg-navy-blue"><i class="fa fa-child"></i></span> Development environment.
                                                    </div>
                                                </li>
                                            </ol>
                                        </li>                        
                                        <li class="dd-item" data-id="5">
                                            <div class="dd-handle">
                                                <span class="label h-bg-navy-blue"><i class="fa fa-android"></i></span> Android platform.
                                            </div>
                                            <ol class="dd-list">
                                                <li class="dd-item" data-id="6">
                                                    <div class="dd-handle">
                                                        <span class="pull-right">A smartphone that is running Android 4.4 OS or newer.</span>
                                                        <span class="label h-bg-navy-blue"><i class="fa fa-users"></i></span> Device.
                                                    </div>
                                                </li>
                                                <li class="dd-item" data-id="6">
                                                    <div class="dd-handle">
                                                        <span class="pull-right">Android 4.4 OS or newer.</span>
                                                        <span class="label h-bg-navy-blue"><i class="fa fa-users"></i></span> OS.
                                                    </div>
                                                </li>
                                                <li class="dd-item" data-id="6">
                                                    <div class="dd-handle">
                                                        <span class="pull-right">1.5 GHz processor</span>
                                                        <span class="label h-bg-navy-blue"><i class="fa fa-users"></i></span> Processor.
                                                    </div>
                                                </li>
                                                <li class="dd-item" data-id="7">
                                                    <div class="dd-handle">
                                                        <span class="pull-right">At least 20 MB of free RAM</span>
                                                        <span class="label h-bg-navy-blue"><i class="fa fa-bomb"></i></span> RAM
                                                    </div>
                                                </li>
                                                <li class="dd-item" data-id="8">
                                                    <div class="dd-handle">
                                                        <span class="pull-right">At least 30 MB required</span>
                                                        <span class="label h-bg-navy-blue"><i class="fa fa-child"></i></span> Free storage.
                                                    </div>
                                                </li>
                                                <li class="dd-item" data-id="8">
                                                    <div class="dd-handle">
                                                        <span class="pull-right">Java SE JDK 6 (or higher), Android SDK</span>
                                                        <span class="label h-bg-navy-blue"><i class="fa fa-child"></i></span> Development environment.
                                                    </div>
                                                </li>
                                            </ol>
                                        </li>
                                    </ol>
                                </div>
                            </div>
                        </div>-->

                <div class="row">
                    <div class="col-lg-12">
                        <div class="hpanel">
                            <ul class="nav nav-tabs">
                                <!--                        <li class="active"><a data-toggle="tab" href="#tab-1"> <i class="fa fa-laptop"></i> </a></li>-->
                                <li class="active"><a data-toggle="tab" href="#tab-2"><i class="fa fa-apple fa-2x"></i> </a></li>
                                <li class=""><a data-toggle="tab" href="#tab-3"><i class="fa fa-android fa-2x"></i> </a></li>
                            </ul>
                            <div class="tab-content">
                                <!--                        <div id="tab-1" class="tab-pane active">
                                                            <div class="panel-body">
                                                                <h4><strong>What is System requirment to use SDK?</strong></h4>
                                
                                                                <div class="text-muted font-bold m-b-xs">
                                                                <ul>                                    
                                                                    <li><h5>PC or laptop with x86 (32-bit) or x86-64 (64-bit) compatible processors.</h5>
                                                                        <ul>
                                                                            <li style="font-weight: 400">2 GHz or better processor is recommended.</li>                                            
                                                                        </ul>
                                                                    </li>
                                                                    <li><h5>Free space on hard disk drive (HDD)</h5>
                                                                        <ul>
                                                                            <li style="font-weight: 400">at least 1 GB required for the development.</li>
                                                                            <li style="font-weight: 400">100 MB required for Mobile Trust SDK deployment.</li>
                                                                        </ul>
                                                                    </li>
                                                                    <li><h5>At least 128 MB of free RAM should be available for the application.</h5>                                    
                                                                    </li>
                                                                    <li><h5>Database engine or connection with it. SDK generated data can be saved into any DB (including files) supporting binary data saving. Mobile Trust SDK contains the following support modules</h5>
                                                                        <ul>
                                                                            <li style="font-weight: 400">MySQL</li>
                                                                            <li style="font-weight: 400">Oracle.</li>
                                                                        </ul>
                                                                    </li>
                                                                    <li><h5>Development environments</h5>
                                                                        <ul>
                                                                            <li style="font-weight: 400">Java SE JDK 6 (or higher)</li>
                                                                            <li style="font-weight: 400">Eclipse Indigo (3.7) IDE or newer / NetBeans IDE (6.0) or newer.</li>
                                                                        </ul>
                                                                    </li>
                                                                </ul>
                                                                </div>                                
                                                            </div>
                                                            <div class="panel-body">
                                                                <h4><strong>What downloads contents?</strong></h4>
                                
                                                                <div class="text-muted font-bold m-b-xs">
                                                                <ul>                                    
                                                                    <li><h5>Downloads contents programming samples, Libs with Headers for iOS, Jar with Interface for Android and JS files for Web Trust SDK.</h5>                                        
                                                                    </li>                                    
                                                                </ul>
                                                                </div>                                
                                                            </div>
                                                            <div class="panel-body">
                                                                <h4><strong>What are that files?</strong></h4>
                                                                <div class="text-muted font-bold m-b-xs">
                                                                    <ul>                                    
                                                                        <li><h5>JS files</h5>
                                                                            <ul>
                                                                                <li style="font-weight: 400">2 GHz or better processor is recommended.</li>                                            
                                                                            </ul>
                                                                        </li>
                                                                                                                
                                                                        <li><h5>Database engine or connection with it. SDK generated data can be saved into any DB (including files) supporting binary data saving. Mobile Trust SDK contains the following support modules</h5>
                                                                            <ul>
                                                                                
                                                                                <li style="font-weight: 400">MySQL</li>
                                                                                <li style="font-weight: 400">Oracle.</li>
                                                                            </ul>
                                                                        </li>
                                                                        
                                                                    </ul>
                                                                </div>                                    
                                                            </div>
                                                        </div>-->
                                <div id="tab-2" class="tab-pane active">
                                    <div class="panel-body">
                                        <h4><strong>What is System requirment to use SDK? <a class="navy-link btn btn-sm" onclick="downloadSDK()" role="button" style="margin-left: 55%">Download Lib</a></strong></h4>
                                        
                                        <div class="text-muted font-bold m-b-xs">
                                            <ul>                                    
                                                <li><h5>One of the following devices</h5>
                                                    <ul>
                                                        <li style="font-weight: 400">iPhone 5 or newer iPhone.</li>                                            
                                                    </ul>
                                                </li>
                                                <li><h5>Free space</h5>
                                                    <ul>                                            
                                                        <li style="font-weight: 400">30 MB required for Mobile Trust SDK deployment.</li>
                                                    </ul>
                                                </li>
                                                <li><h5>At least 20 MB of free RAM should be available for the application.</h5>                                    
                                                </li>                                    
                                                <li><h5>Development environments</h5>
                                                    <ul>
                                                        <li style="font-weight: 400">a Mac running Mac OS X 6.x or newer.</li>                                            
                                                    </ul>
                                                </li>
                                                <li><h5>Supports</h5>
                                                    <ul>
                                                        <li style="font-weight: 400"> iOS 6 to iOS 11 versions</li>                                            
                                                    </ul>
                                                </li>
                                            </ul>
                                        </div>                                
                                    </div>
                                    <div class="panel-body">
                                        <h4><strong>What downloads contents?</strong></h4>
                                        <div class="text-muted font-bold m-b-xs">
                                            <ul>                                    
                                                <li><h5>Downloads contents programming samples, Lib files for simulator and device, Header files.</h5>                                        
                                                </li>                                    
                                            </ul>
                                        </div>                                
                                    </div>
                                    <div class="panel-body">
                                        <h4><strong>What are that download files?</strong></h4>
                                        <div class="text-muted font-bold m-b-xs">
                                            <ul>                                    
                                                <li><h4>Header files</h4>
                                                    <ul>                                                
                                                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                                            <div class="panel panel-default">
                                                                <div class="panel-heading" role="tab" id="headingOne">
                                                                    <h4 class="panel-title">
                                                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                                            Axiom.h
                                                                        </a>
                                                                    </h4>
                                                                </div>
                                                                <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                                                    <div class="panel-body">
                                                                        Axiom interface basically expose methods which initialize utility classes for device profiling, GIO location details, and other PKI properties.
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="panel panel-default">
                                                                <div class="panel-heading" role="tab" id="headingTwo">
                                                                    <h4 class="panel-title">
                                                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                                                            AxiomException.h
                                                                        </a>
                                                                    </h4>
                                                                </div>
                                                                <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                                                    <div class="panel-body">
                                                                        AxiomException interface have customized exception defined in SDK, Which is thrown by SDKs internal methods.
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="panel panel-default">
                                                                <div class="panel-heading" role="tab" id="headingThree">
                                                                    <h4 class="panel-title">
                                                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                                                            AxiomLocation.h
                                                                        </a>
                                                                    </h4>
                                                                </div>
                                                                <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                                                    <div class="panel-body">
                                                                        AxiomLocation interface defines users device location details. AxiomLocation instance useful for other methods inside SDK.
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="panel panel-default">
                                                                <div class="panel-heading" role="tab" id="headingThree">
                                                                    <h4 class="panel-title">
                                                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                                                            AxiomOTPResponse.h
                                                                        </a>
                                                                    </h4>
                                                                </div>
                                                                <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                                                    <div class="panel-body">
                                                                        AxiomOTPResponse holds response from getOTPPlus() method which has OTP, OTPPlus, and result Code.
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="panel panel-default">
                                                                <div class="panel-heading" role="tab" id="headingThree">
                                                                    <h4 class="panel-title">
                                                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                                                            AxiomPKIResponse.h
                                                                        </a>
                                                                    </h4>
                                                                </div>
                                                                <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                                                    <div class="panel-body">
                                                                        AxiomPKIResponse interface is a response for PKI signature methods which holds signature payload and response code.  which is use for parsing response from signing method.
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="panel panel-default">
                                                                <div class="panel-heading" role="tab" id="headingFour">
                                                                    <h4 class="panel-title">
                                                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                                                            AxiomTrust.h
                                                                        </a>
                                                                    </h4>
                                                                </div>
                                                                <div id="collapseSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
                                                                    <div class="panel-body">
                                                                        Axiom interface basically expose methods which initialize utility classes for device profiling, GIO location details, and other PKI properties.
                                                                    </div>
                                                                </div>
                                                            </div>                            
                                                            <div class="panel panel-default">
                                                                <div class="panel-heading" role="tab" id="headingThree">
                                                                    <h4 class="panel-title">
                                                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                                                                            Device.h
                                                                        </a>
                                                                    </h4>
                                                                </div>
                                                                <div id="collapseSeven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                                                    <div class="panel-body">
                                                                        Device holds information of device like MAC address, Vendor ID, UUID, CFUUID, ADID, Platform and Device name.
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="panel panel-default">
                                                                <div class="panel-heading" role="tab" id="headingThree">
                                                                    <h4 class="panel-title">
                                                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                                                                            MobilityTrustKickStart.h
                                                                        </a>
                                                                    </h4>
                                                                </div>
                                                                <div id="collapseEight" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                                                    <div class="panel-body">
                                                                        MobilityTrustKickStart interface contains initialize(), confirm(), SilentIntialize(), enableORDisableDebug(), CheckStatus(), Update() and selfDestroy() methods. Please follow API Documentation for method details.
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="panel panel-default">
                                                                <div class="panel-heading" role="tab" id="headingThree">
                                                                    <h4 class="panel-title">
                                                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
                                                                            MobilityTrustVault.h
                                                                        </a>
                                                                    </h4>
                                                                </div>
                                                                <div id="collapseNine" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                                                    <div class="panel-body">
                                                                        MobilityTrustVault contains load(), setdateForotp(), unLoad(), GetOTPPlus(), GetSOTPPlus() methods. Please follow API docmentation for more details.
                                                                    </div>
                                                                </div>
                                                            </div>   
                                                        </div>
                                                    </ul>
                                                </li>
                                                <li><h4>Simulator Library</h4>
                                                    <ul>                                                        
                                                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                                            <div class="panel panel-default">
                                                                <div class="panel-heading" role="tab" id="headingOne">
                                                                    <h4 class="panel-title">
                                                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse1" aria-expanded="true" aria-controls="collapse1">
                                                                            simulatorLib_libIOSMobileTrust.a
                                                                        </a>
                                                                    </h4>
                                                                </div>
                                                                <div id="collapse1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                                                    <div class="panel-body">
                                                                        This library can be use for simulator testing purpose. this is iOS static library which need to be added in project frameworks. This library contains all implemented methods exposed in header files. This library supports from iOS 6 to iOS 11 versions 
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </ul>
                                                </li>
                                                <li><h4>Device Library</h4>
                                                    <ul>
                                                        <!--                                                <li style="font-weight: 400">libIOSMobileTrust.a - </li>                                                -->
                                                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                                            <div class="panel panel-default">
                                                                <div class="panel-heading" role="tab" id="headingOne">
                                                                    <h4 class="panel-title">
                                                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-expanded="true" aria-controls="collapse2">
                                                                            deviceLib_libIOSMobileTrust.a
                                                                        </a>
                                                                    </h4>
                                                                </div>
                                                                <div id="collapse2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                                                    <div class="panel-body">
                                                                        This library can be use for production release. this is iOS static library which need to be added in project frameworks. This library contains all implemented methods exposed in header files. This library supports from iOS 6 to iOS 11 versions.
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </ul>
                                                </li>
                                                <li><h4>Sample Program</h4>
                                                    <ul>                                                        
                                                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                                            <div class="panel panel-default">
                                                                <div class="panel-heading" role="tab" id="headingOne">
                                                                    <h4 class="panel-title">
                                                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-expanded="true" aria-controls="collapse2">
                                                                           ViewController.m
                                                                        </a>
                                                                    </h4>
                                                                </div>
                                                                <div id="collapse2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                                                    <div class="panel-body">
                                                                        View Controller is a sample program file that provides clearity to how to call our SDK methods. 
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </div>                                    
                                    </div>
                                    <div class="panel-body">
                                        <h4><strong>How to get license for SDK</strong></h4>

                                        <div class="text-muted font-bold m-b-xs">
                                            <ul>                                    
                                                <li><h5>You get license by calling API GetLicenseKeys which return MobileTrustStatus includes license details. For further API description please see our API Docs.</h5>                                        
                                                </li>                                    
                                            </ul>
                                        </div>                                
                                    </div>
                                </div>
                                <div id="tab-3" class="tab-pane">
                                    <div class="panel-body">
                                        <h4><strong>What is System requirment to use SDK? <a class="navy-link btn btn-sm" onclick="downloadAndriodSDK()" role="button" style="margin-left: 55%">Download Lib</a></strong></h4>

                                        <div class="text-muted font-bold m-b-xs">
                                            <ul>                                    
                                                <li><h5>A smartphone or tablet that is running Android 4.4 OS or newer.</h5>
                                                    <ul>
                                                        <li style="font-weight: 400">API level 22 is the recommended target for code compilation</li>                                            
                                                    </ul>
                                                </li>
                                                <li><h5>ARM-based 1.5 GHz processor recommended for efficient processing in the specified time. Slower processors may be also used, but the processing will take longer time.</h5>                                    
                                                </li>            
                                                <li><h5>Free space</h5>
                                                    <ul>                                            
                                                        <li style="font-weight: 400">30 MB required for Mobile Trust SDK deployment.</li>
                                                    </ul>
                                                </li>
                                                <li><h5>At least 20 MB of free RAM should be available for the application.</h5>                                    
                                                </li>                                    
                                                <li><h5>Development environments</h5>
                                                    <ul>
                                                        <li style="font-weight: 400">Java SE JDK 6 (or higher).</li>                                                        
                                                    </ul>
                                                </li>
                                            </ul>
                                        </div>                                
                                    </div>                            
                                    <div class="panel-body">
                                        <h4><strong>What downloads contents?</strong></h4>

                                        <div class="text-muted font-bold m-b-xs">
                                            <ul>                                    
                                                <li><h5>Downloads contents programming samples, Jar with Interface for Android SDK.</h5>                                        
                                                </li>                                    
                                            </ul>
                                        </div>                                
                                    </div>
                                    <div class="panel-body">
                                        <h4><strong>What are that files?</strong></h4>
                                        <div class="text-muted font-bold m-b-xs">
                                            <ul>                                    
                                                <li><h4>Jar files</h4>
                                                    <ul>                                                        
                                                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                                            <div class="panel panel-default">
                                                                <div class="panel-heading" role="tab" id="headingOne">
                                                                    <h4 class="panel-title">
                                                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse21" aria-expanded="true" aria-controls="collapse21">
                                                                            androidmobiletrust.jar
                                                                        </a>
                                                                    </h4>
                                                                </div>
                                                                <div id="collapse21" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                                                    <div class="panel-body" style="font-weight: 20px">
                                                                        This is android library which need to be copied into project lib directory in android project which has  all classes exposed for mobile trust, Follow API documentation for more details.  
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </ul>
                                                </li>
                                            </ul>
                                            
                                        </div>                                    
                                    </div>
                                    <div class="panel-body">
                                        <h4><strong>How to get license for SDK</strong></h4>

                                        <div class="text-muted font-bold m-b-xs">
                                            <ul>                                    
                                                <li><h5>You get license by calling API GetLicenseKeys which return MobileTrustStatus includes license details. For further API description please see our API Docs.</h5>                                        
                                                </li>                                    
                                            </ul>
                                        </div>                                
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </section>
        <section id="contact">
            <div class="container">
                <div class="row text-center">
                    <div class="col-md-6 col-md-offset-3">
                        <h2><span class="text-success">Contact with us</span> anytime</h2>
                        <p>
                            If you are interested in learning more about our products & solutions or having an authorized channel partner contact you with more information. Please drop a message with below details.
                        </p>
                    </div>
                </div>
                <div class="row text-center m-t-lg">
                    <div class="col-md-4 col-md-offset-3">
                        <form class="form-horizontal" role="form" method="post" action="#">
                            <div class="form-group">
                                <label for="name" class="col-sm-2 control-label">Name</label>

                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="name" name="name" placeholder="Your full name" value="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="email" class="col-sm-2 control-label">Email</label>

                                <div class="col-sm-10">
                                    <input type="email" class="form-control" id="email" name="email" placeholder="user@example.com" value="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="message" class="col-sm-2 control-label">Message</label>

                                <div class="col-sm-10">
                                    <textarea class="form-control" rows="3" name="message"  placeholder="Your message here..."></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <input id="submit" name="submit" type="submit" value="Send us a message" class="btn btn-success" onclick="sendMsg()">
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-3 text-left">
                        <strong><span class="navy">Our Office</span></strong><br/>
                        <address>
                            <strong><span class="navy">Blue Bricks Pty. Ltd.</span></strong><br/>
                            Unit 39, 118 Adderton Road,<br/>
                            Carlingford 2118 NSW, Australia<br/>
                            <abbr title="Phone">P:</abbr> +61 423 394 252 / +61 478 665 482
                        </address>
                        <address>
                            <strong><span class="navy">Also we have offices in,</span></strong><br/>                    
                            <strong><span class="navy">Malaysia</span></strong><br/>
                            <strong><span class="navy">India</span></strong><br/>
                            <strong><span class="navy">USA</span></strong><br/>
                        </address>                
                    </div>
                </div>
            </div>
        </section>

        <!-- Vendor scripts -->
        <script src="vendor/jquery/dist/jquery.min.js"></script>
        <script src="vendor/jquery-ui/jquery-ui.min.js"></script>
        <script src="vendor/slimScroll/jquery.slimscroll.min.js"></script>
        <script src="vendor/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="vendor/metisMenu/dist/metisMenu.min.js"></script>
        <script src="vendor/iCheck/icheck.min.js"></script>
        <script src="vendor/sparkline/index.js"></script>
        <script src="vendor/peity/jquery.peity.min.js"></script>
        <script src="vendor/nestable/jquery.nestable.js"></script>

        <!-- App scripts -->
        <script src="scripts/homer.js"></script>

        <!-- Local script for menu handle -->
        <!-- It can be also directive -->
        <script>
                            $(document).ready(function () {

                                // Page scrolling feature
                                $('a.page-scroll').bind('click', function (event) {
                                    var link = $(this);
                                    $('html, body').stop().animate({
                                        scrollTop: $(link.attr('href')).offset().top - 50
                                    }, 500);
                                    event.preventDefault();
                                });

                                $('body').scrollspy({
                                    target: '.navbar-fixed-top',
                                    offset: 80
                                });

                            });
        </script>
        <script>

            $(function () {

                var updateOutput = function (e) {
                    var list = e.length ? e : $(e.target),
                            output = list.data('output');
                    if (window.JSON) {
                        output.val(window.JSON.stringify(list.nestable('serialize')));//, null, 2));
                    } else {
                        output.val('JSON browser support required for this demo.');
                    }
                };
                // activate Nestable for list 1
                $('#nestable').nestable({
                    group: 1
                }).on('change', updateOutput);

                //activate Nestable for list 2
                $('#nestable2').nestable({
                    group: 1
                }).on('change', updateOutput);

                // output initial serialised data
                updateOutput($('#nestable').data('output', $('#nestable-output')));
                updateOutput($('#nestable2').data('output', $('#nestable2-output')));

                $('#nestable-menu').on('click', function (e) {
                    var target = $(e.target),
                            action = target.data('action');
                    if (action === 'expand-all') {
                        $('.dd').nestable('expandAll');
                    }
                    if (action === 'collapse-all') {
                        $('.dd').nestable('collapseAll');
                    }
                });
                function collapseOnLoad() {
                    $('.dd').nestable('collapseAll');
                }
                collapseOnLoad();
            });
        </script>
    </body>
</html>
