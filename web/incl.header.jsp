<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
      	<meta name="viewport" content="width=device-width, initial-scale=1">
      	
      	<title>MP Services</title>
      
      	<!--Including Bootstrap style files-->
      	<script src="vendor/bootstrap/dist/js/bootstrap.min.js"></script>
      	
		<style>
            tr {
                line-height: 30px;
            }
            td {
                padding: 5px;
            }

            .divBorder {
	            margin-top:10px;
	            border: #eeeeee medium solid;
	            border-radius: 10px;
	            -moz-border-radius: 10px;
	            -webkit-border-radius: 10px;
                padding: 15px;
            }

            .wSmall {
                width: 120px;
            }
            
            .wMedium {
                width: 200px;
            }
            
            .colCentered{
			    float: none;
			    margin: 0 auto;
			}
            
            #card-number, #cvv, #expiration-date {
				-webkit-transition: border-color 160ms;
				transition: border-color 160ms;
				height: 25px;
				width: 250px;
				-moz-appearance: none;
				border: 0 none;
				border-radius: 5px;
				box-shadow: 0 0 4px 1px #a5a5a5 inset;
				color: #DDDBD9;
				display: inline-block;
				float: left;
				font-size: 13px;
				height: 40px;
				margin-right: 2.12766%;
				padding-left: 10px;
			}
        </style>
      	
  	</head>
  	
  	
        <body>

		<div class="container-fluid">

            <div class="well" style="text-align: center;">                
                <img src="images/paypal-784404_960_720.png" alt="Paypal" height="70px" width="120px"/>
                <h2 class="text-center"> Pay with PayPal</h2>
            </div>
            
        </div>
        