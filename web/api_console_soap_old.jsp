<%@include file="header.jsp"%>

<!-- Main Wrapper -->
<div id="wrapper">
	
	<div class="small-header transition animated fadeIn">
        <div class="hpanel">
            <div class="panel-body">
                <div id="hbreadcrumb" class="pull-right">
                    <ol class="hbreadcrumb breadcrumb">
                        <li><a href="home.jsp">Dashboard</a></li>
						<li><a href="api_console.jsp">API Console</a></li>
						<li><a href="api_console_access.jsp">SMS</a></li>
                        <li class="active">
                            <span>Console</span>
                        </li>
                    </ol>
                </div>
                <h2 class="font-light m-b-xs">
                    Console
                </h2>
                <small>Short description to developer what is this page about</small>
            </div>
        </div>
    </div>
	
	<div class="normalheader transition animated fadeIn">
	
		<a href="https://developer.tm.com.my/APIDoc/apidocs.jsp#accessPointSMS" class="btn btn-success m-b-lg run-tour" target="_blank">User Guide !</a>
	
		<a href="compose_email.jsp" class="btn btn-danger m-b-lg run-tour" target="_blank">Helpdesk</a>
		
		<a href="compose_production.jsp" class="btn btn-primary m-b-lg run-tour">Subscribe to Production</a>
	
		<ul class="nav nav-tabs">
			<li class="active"><a data-toggle="tab" href="#soap"> SOAP Console</a></li>
			<li class=""><a data-toggle="tab" href="#restful"> RESTFUL Console</a></li>
		</ul>
		
		<div class="tab-content">
			<div id="soap" class="tab-pane active">
				<div class="panel-body">
					<div class="hpanel">
						<div class="panel-body">
							<a class="small-header-action" href="">
								<div class="clip-header">
									<i class="fa fa-arrow-up"></i>
								</div>
							</a>
							
							<h2 class="font-light m-b-xs">						
								<small><a href="https://developer.tm.com.my:8443/SMSSBV1/SMSImpl?wsdl" target="_blank">https://developer.tm.com.my:8443/SMSSBV1/SMSImpl?wsdl</a></small>						
							</h2>
						</div>
					</div>
			
					<div class="content animate-panel">
						<div class="row">
							<div class="col-lg-6">				
								<div class="hpanel">		
									<div class="panel-heading">
										Request
									</div>				
									<div class="panel-body">					
										<form method="get" class="form-horizontal">
											<h3>Header</h3>
											
											<div class="form-group"><label class="col-sm-3 control-label">APITokenID</label>
												<div class="col-sm-9"><input type="text" class="form-control" value="vEBDceyF4TRRkZq1qBo+NaYRgV4="></div>
											</div>
											<div class="form-group"><label class="col-sm-3 control-label">PartnerID</label>
												<div class="col-sm-9"><input type="text" class="form-control" value="ZoTMCE7MA5GYRGeEe/U2JmLbErk="></div>
											</div>
											<div class="form-group"><label class="col-sm-3 control-label">PartnerTokenID</label>
												<div class="col-sm-6"><input type="text" class="form-control" value="Zwmr7ldMOl1TpAUEGmiQ2HmIOC0="></div>
												<div class="col-sm-3">
													<button class="btn btn-info btn-circle" type="button"><i class="fa fa-plus"></i></button>
													<button class="btn btn-danger btn-circle" type="button"><i class="fa fa-minus"></i></button>
												</div>
											</div>
											<div class="hr-line-dashed"></div>
												
											<h3>Basic Authentication</h3>
											<div class="form-group">
												<div class="col-sm-6"><input type="text" class="form-control" placeholder="Username"></div>
												<div class="col-sm-6"><input type="text" class="form-control" placeholder="Password"></div>
											</div>
											<div class="hr-line-dashed"></div>
												
											<h3>Body</h3>
										
											<textarea class="form-control" style="min-height: 500px;">
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:web="http://webservice.wsdl.wadl.sg.mollatech.com/">
    <soapenv:Header/>
    <soapenv:Body>
        <web:sendmsg>
            <!--Optional:-->
            <username>?</username>
            <!--Optional:-->
            <password>?</password>
            <!--Optional:-->
            <msgtype>?</msgtype>
            <!--Optional:-->
            <message>?</message>
            <!--Optional:-->
            <to>?</to>
            <!--Optional:-->
            <hashkey>?</hashkey>
            <!--Optional:-->
            <filename>?</filename>
            <!--Optional:-->
            <transcid>?</transcid>
        </web:sendmsg>
    </soapenv:Body>
</soapenv:Envelope>
																	
											</textarea>
											
											<div class="hr-line-dashed"></div>
											<button class="btn btn-primary " type="button"><i class="fa fa-check"></i> Submit</button>
										</form>								
									</div>					
								</div>				
							</div>
							<div class="col-lg-6">
								<div class="hpanel">
									<div class="panel-heading">
										Response
									</div>
									<div class="panel-body">
										<textarea class="form-control" style="min-height: 500px;">
<S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/">
    <S:Body>
        <ns2:sendmsgResponse xmlns:ns2="http://webservice.wsdl.wadl.sg.mollatech.com/">
            <return>
                <responseResult xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="xs:string">-2</responseResult>
                <responseCode>200</responseCode>
            </return>
        </ns2:sendmsgResponse>
    </S:Body>
</S:Envelope>

										</textarea>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div id="restful" class="tab-pane">
				<div class="panel-body">
					<div class="hpanel">
						<div class="panel-body">
							<a class="small-header-action" href="">
								<div class="clip-header">
									<i class="fa fa-arrow-up"></i>
								</div>
							</a>
							
							<h2 class="font-light m-b-xs">
							
								<small><a href="https://developer.tm.com.my:8443/SMSSBV1/SMSImpl/application.wadl" target="_blank">https://developer.tm.com.my:8443/SMSSBV1/SMSImpl/application.wadl</a></small>
							</h2>
						</div>
					</div>
			
					<div class="content animate-panel">
						<div class="row">
							<div class="col-lg-6">						
								<div class="hpanel">							
									<div class="panel-heading">
										Request
									</div>
									
									<div class="panel-body">
										
										<form method="get" class="form-horizontal">
											<h3>Header</h3>
											
											<div class="form-group"><label class="col-sm-3 control-label">APITokenID</label>
												<div class="col-sm-9"><input type="text" class="form-control" value="STcOL0mmsGBKCj1bmIFl26b5k6Y="></div>
											</div>
											<div class="form-group"><label class="col-sm-3 control-label">PartnerID</label>
												<div class="col-sm-9"><input type="text" class="form-control" value="ZoTMCE7MA5GYRGeEe/U2JmLbErk="></div>
											</div>
											<div class="form-group"><label class="col-sm-3 control-label">PartnerTokenID</label>
												<div class="col-sm-6"><input type="text" class="form-control" value="Zwmr7ldMOl1TpAUEGmiQ2HmIOC0="></div>
												<div class="col-sm-3">
													<button class="btn btn-info btn-circle" type="button"><i class="fa fa-plus"></i></button>
													<button class="btn btn-danger btn-circle" type="button"><i class="fa fa-minus"></i></button>
												</div>
											</div>
											<div class="hr-line-dashed"></div>
												
											<h3>Basic Authentication</h3>
											<div class="form-group">
												<div class="col-sm-6"><input type="text" class="form-control" placeholder="Username"></div>
												<div class="col-sm-6"><input type="text" class="form-control" placeholder="Password"></div>
											</div>
											<div class="hr-line-dashed"></div>
												
											<h3>Body</h3>
										
											<textarea class="form-control" style="min-height: 500px;">
{
  "username" : null,
  "password" : null,
  "msgtype" : null,
  "message" : null,
  "to" : null,
  "hashkey" : null,
  "filename" : null,
  "transcid" : null
}
											</textarea>
											
											<div class="hr-line-dashed"></div>
											<button class="btn btn-primary " type="button"><i class="fa fa-check"></i> Submit</button>
										</form>
									</div>						
								</div>						
							</div>
							<div class="col-lg-6">
								<div class="hpanel">
									<div class="panel-heading">
										Response
									</div>
									<div class="panel-body">
										<textarea class="form-control" style="min-height: 500px;">
{
  "responseResult" : "-2\n",
  "responseCode" : 200
}
										</textarea>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

    <!-- Footer-->
    <%@include file="footer.jsp"%>