/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function strCompare(a, b)
{
    return (a < b ? -1 : (a > b ? 1 : 0));
}
function Alert4Users(msg) {
    bootboxmodel("<h5>" + msg + "</h5>");
}
function downloadDocments(id) {
    var s = './downloadDoc?id=' + id;
    //alert("id from js "+id);
    window.location.href = s;
    return false;
}
function uploadCertificate() {
    $('#buttonUploadCert').attr("disabled", true);
    var s = './UploadKeyOrCert?type=test';
    $.ajaxFileUpload({
        fileElementId: 'certificatetoupload',
        url: s,
        dataType: 'json',
        success: function (data, status) {
            //alert(data);
            if (strcmpserviceguard(data.result, "error") == 0) {
                Alert4serviceguard("<span><font color=red>" + data.message + "</font></span>");
                $('#buttonUploadCert').attr("disabled", false);
            } else if (strcmpserviceguard(data.result, "success") == 0) {
                Alert4serviceguard("<span><font color=blue>" + data.message + "</font></span>");
                $('#buttonUploadCert').attr("disabled", false);
            }
        },
    });
}
function downloadCert(id, env) {
    if (env === 'test') {
        var l = $('#downTcert').ladda();
        l.ladda('start');
    } else {
        var l = $('#downPcert').ladda();
        l.ladda('start');
    }
    initializeToastr();
    var s = './DownloadCert?pid=' + id + "&env=" + env;
    window.location.href = s;
    l.ladda('stop');
//    $('#dowModal').modal('hide');
    return false;
}
function addcert() {
//    $('#addnewpartner').attr("disabled", true);
    var env = document.getElementById("selectPEnv").value;
    var sid = $("#_partnerid").val();
    var s = './addCertificate?_sid=' + encodeURIComponent(sid) + "&env=" + env;
    $.ajax({
        fileElementId: 'addcertificate',
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#uploadCertFileForm").serialize(),
        success: function (data) {
            if (strcmpserviceguard(data._result, "error") == 0) {
                $('#upload-partner-certificate').html("<span><font color=red>" + data._message + "</font></span></small>");
            } else if (strcmpserviceguard(data._result, "success") == 0) {
                $('#upload-partner-certificate').html("<span><font color=blue>" + data._message + "</font></span></small>");
                $('#addPartnerButton').attr("disabled", false);
                window.setTimeout(RefreshpartnerList(), 5000);
            }
        }
    });
}
function RefreshpartnerList() {
    window.location.href = "./profile.jsp";
}
function loadCertificate(_pid) {
//   _orpid= encodeURIComponent(_orpid);
    $('#envModal').modal('hide');
    $('#_partnerid').val(_pid);
//    alert(_pid);
    //alert($('#merchantId').val());
    $("#uploadCertificate").modal();
}
function bootboxmodel(content) {
    var popup = $('<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="popup" class="modal">' +
            '<div class="modal-dialog"><div class="modal-content"><div class="modal-body">' +
            '<button aria-hidden="true" data-dismiss="modal" class="close" type="button">X</button>' +
            content +
            '</div></div></div></div>');
    popup.modal();
}
var waiting;
function pleasewait() {
    waiting = $('<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="popup" class="modal fade">' +
            '<div class="modal-dialog"><div class="modal-content"><div class="modal-body">' + '<h4>Processing...</h4>' +
            '<div class="progress progress-striped active">' +
            '<div class="progress-bar progress-bar-info" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width: 100%">' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div></div></div></div>');
    waiting.modal();
}

function showAlert(message, type, closeDelay) {
    if ($("#alerts-container").length == 0) {
        // alerts-container does not exist, create it
        $("body")
                .append($('<div id="alerts-container" style="position: fixed;' +
                        'width: 50%; left: 25%; top: 10%;">'));
    }
    // default to alert-info; other options include success, warning, danger
    type = type || "info";
    // create the alert div
    message = '<i class="fa fa-info-circle"></i> ' + message;
    var alert = $('<div class="alert alert-' + type + ' fade in">')
            .append(
                    $('<button type="button" class="close" data-dismiss="alert">')
                    .append("&times;")
                    )
            .append(message);
    // add the alert div to top of alerts-container, use append() to add to bottom
    $("#alerts-container").prepend(alert);
    // if closeDelay was passed - set a timeout to close the alert
    if (closeDelay)
        window.setTimeout(function () {
            alert.alert("close")
        }, closeDelay);
}

function updateProfile() {
    var s = './EditProfile';
    var emailID = document.getElementById("pemail").value;
    var contactNo = document.getElementById("pmobNo").value;
    var ipTest = document.getElementById("testIpDe").value;
    var ipLive = document.getElementById("liveIpDe").value;
    if (ipTest.trim() === '' || ipLive.trim() === '') {
        //Alert4Users("<font color=red>"+ "IP's should not be empty."+ "</font>");
        showAlert("IP should not be empty.", "danger", 3000);
        return;
    }
    if (emailID.trim() === '' || contactNo.trim() === '') {
        //Alert4Users("<font color=red>"+ "EmailId or Contact number should not be empty."+ "</font>");
        showAlert("EmailId or Contact number should not be empty.", "danger", 3000);
        return;
    }
    pleasewait();
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#update-profile").serialize(),
        success: function (data) {
            if (strCompare(data._result, "error") === 0) {
                waiting.modal('hide');
                //Alert4Users("<font color=red>"+data._message+"</font>");
                showAlert(data._message, "danger", 3000);
                setTimeout(function () {
                    window.location.href = "./profile.jsp";
                }, 3000);
            } else if (strCompare(data._result, "success") === 0) {
                waiting.modal('hide');
                //Alert4Users("<font color=blue>"+data._message+"</font>");
                showAlert(data._message, "success", 3000);
                setTimeout(function () {
                    window.location.href = "./profile.jsp";
                }, 3000);
            }
        }
    });
}
function cancelProfile() {
    window.setTimeout(function () {
        window.location.href = "home.jsp";
    }, 3000);
}
function strcmpserviceguard(a, b)
{
    return (a < b ? -1 : (a > b ? 1 : 0));
}
function viewCrtData(id, env) {
    if (env === 'test') {
        var l = $('#viewTestcert').ladda();
        l.ladda('start');
    } else {
        var l = $('#viewProdcert').ladda();
        l.ladda('start');
    }
    initializeToastr();
    var s = './ViewCertData?pid=' + id + "&env=" + env;
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        success: function (data) {
            if (strcmpserviceguard(data._result, "error") === 0) {
                //Alert4Users("<font color=red>" + data._message + "</font>") 
                l.ladda('stop');
//                showAlert(data._message,"danger",3000);
                toastr.error('Error - ' + data._message);
            } else if (strcmpserviceguard(data._result, "success") === 0) {

                $('#live_cert').modal();
                if (env === 'test') {
                    document.getElementById('userDetails').innerHTML = 'Test Environment';
                } else {
                    document.getElementById('userDetails').innerHTML = 'Live Enviornment';
                }
//                $('#_pName').html(data.name);
                $('#_srNo').val(data._srno);
                $('#_algo').val(data._algoname);
                $('#_issuerdn').val(JSON.stringify(data._issuerdn));
                $('#_subjectdn').val(JSON.stringify(data._subjectdn));
                $('#_notafter').val(data._notafter);
                $('#_notbefore').val(data._notbefore);
                $('#_version').val(data._version);
                l.ladda('stop');
            }
        }
    });

}
function showTokenForPartner(msg, msg1) {
    var msgToken = "<p>" + msg + "</p>";
    var type = "Token For Test enviroment is";
    var tokenType = "<h4>" + type + "<h4>";
    document.getElementById('tokenType').innerHTML = tokenType;
    document.getElementById('test1').innerHTML = msgToken;
    $('#tmTermsAndCondition').modal();
}
function showLiveTokenForPartner(msg, msg1) {
    var msgToken = "<p><b>" + msg1 + "</b></p>";
    var type = "Token For Live enviroment is";
    var tokenType = "<h4>" + type + "<h4>";
    //document.getElementById('tokenType').innerHTML = tokenType;
    document.getElementById('test1').innerHTML = msgToken;
    $('#tmTermsAndCondition').modal();
}
function showPartnerId(PartnerId) {
    var msgToken = "<p><b>" + PartnerId + "</b></p>";
    var type = "Token For Live enviroment is";
    var tokenType = "<h4>" + type + "<h4>";
    //document.getElementById('tokenType').innerHTML = tokenType;
    document.getElementById('test12').innerHTML = msgToken;
    $('#showPartnerIdModal').modal();
}

function reGenerateAPToken(partnerId, partnerName, partnerEmail, partnerPhone, env) {
//    var env = "Test";
//    $('#dowModal').modal('hide');
    if (env === 'Test') {
        var l = $('#regTestToken').ladda();
        l.ladda('start');
    } else {
        var l = $('#regProdToken').ladda();
        l.ladda('start');
    }
    var s = './ReAssignAPToken?_partnerid=' + partnerId + '&env=' + env + '&_partnername=' + partnerName + '&_partneremail=' + partnerEmail + '&_partnerphone=' + partnerPhone;
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        success: function (data) {
            initializeToastr();
            var msg = "";
            if (strCompare(data._result, "error") === 0) {
                setTimeout(function () {
                    l.ladda('stop');
                    toastr.error('Error - ' + 'Token regenerated for ' + env + ' environment failed.');
                }, 3000);
                setTimeout(function () {
                    window.location.href = "./token_manager.jsp";
                }, 5000);
            } else if (strCompare(data._result, "success") === 0) {
                setTimeout(function () {
                    l.ladda('stop');
                    toastr.success('success - ' + 'Token regenerated for ' + env + ' environment successfully.');
                }, 3000);
                setTimeout(function () {
                    window.location.href = "./token_manager.jsp";
                }, 5000);
            }
        }
    });
}

function emailAPToken(partnerName, partnerEmail, testToken, productionToken, env) {
    if (env === 'Test') {
        var l = $('#sendTestToken').ladda();
        l.ladda('start');
    } else {
        var l = $('#sendProdToken').ladda();
        l.ladda('start');
    }
    var apToken = testToken;
    if (env !== 'Test') {
        apToken = productionToken;
    }
    var s = './EmailAPToken?partnerName=' + partnerName + '&partnerEmail=' + partnerEmail + '&apToken=' + apToken + '&env=' + env;
    initializeToastr();
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        success: function (data) {
            if (strCompare(data.result, "error") === 0) {
                l.ladda('stop');
                toastr.error('Error - ' + 'Token send for ' + env + ' environment failed.');
            } else if (strCompare(data.result, "success") === 0) {
                toastr.success('success - ' + 'Token send for ' + env + ' environment successfully.');
                l.ladda('stop');
            }
        }
    });
}

function assignToken(accesspoint_Name) {
    var accesspointName = accesspoint_Name;
    var resourceName = document.getElementById("apId").value;
    var version = document.getElementById("versionName").value;
    //var api = document.getElementById("_APIForSlabPricing2").value;
    //var packageID = document.getElementById("packageID").value;
    var s = './assignToken.jsp?_apId=' + accesspointName + '&_resourceId=' + resourceName + '&version=' + version;
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#_slabPriceWindow').html(data);
            // acesspointChangeAPIPrice(value);
        }
    });
}

function updatePartnerName(partnerId) {
    var companyName = document.getElementById("companyName").value;
    var companyRegNo = document.getElementById("companyRegNo").value;
    var companyGstNo = document.getElementById("companyGstNo").value;
    var l = $('#partUpdate').ladda();
    $("#personalInfo").validate({
        rules: {
            partnerName: {
                required: true
            },
            companyFixedNo: {
                required: true
            },
            companyAddress: {
                required: true
            },
            companyState: {
                required: true
            },
            compnyPcode: {
                required: true
            },
            companyCountry: {
                required: true
            },
            bilingAdd: {
                required: true
            },
            billingState: {
                required: true
            },
            bilingPcode: {
                required: true
            },
            bilingCountry: {
                required: true
            }
        },
        submitHandler: function (form) {
            l.ladda('start');
            initializeToastr();
            var partnerName = document.getElementById("partnerName").value;
            var s = './EditPartner?partnerId=' + partnerId + '&companyName=' + companyName + '&companyRegNo=' + companyRegNo + '&companyGstNo=' + companyGstNo;
            $.ajax({
                type: 'POST',
                url: s,
                data: $("#personalInfo").serialize(),
                dataType: 'json',
                success: function (data) {
                    if (strCompare(data._result, "error") === 0) {
                        toastr.error('Error - ' + data._message);
                        l.ladda('stop');
                    } else {
                        toastr.success('Success - ' + data._message);
                        l.ladda('stop');
//                    window.location.href = "./my_profile.jsp";
                        setTimeout(function () {
                            window.location.href = "./my_profile.jsp";
                        }, 3000);
                    }
                }
            });
        }
    });
}

function validateProfileForm(partnerId) {
    $("#personalInfo").validate({
        rules: {
            partnerName: {
                required: true
            },
            mobileNo: {
                required: true
            },
            emailId: {
                required: true
            },
            addr1: {
                required: true
            },
            addr2: {
                required: true
            },
            city: {
                required: true
            },
            country: {
                required: true
            },
            pinCode: {
                required: true
            },
            state: {
                required: true
            }
        },
        submitHandler: function (form) {
            updateDeveloperName(partnerId);
        }
    });
}

function callValidateProfileForm(partnerId) {
    document.querySelector('.validateProfileForm').click();
}

function updateDeveloperName(partnerId) {
    var l = $('#partUpdate').ladda();
    var parId = document.getElementById("partnerId").value;
    l.ladda('start');
    initializeToastr();
    var s = './EditDeveloperDetails?partnerId=' + parId;
    $.ajax({
        type: 'POST',
        url: s,
        data: $("#personalInfo,#analyticScriptForm").serialize(),
        dataType: 'json',
        success: function (data) {
            if (strCompare(data._result, "error") === 0) {
                toastr.error('Error - ' + data._message);
                l.ladda('stop');
            } else {
                toastr.success('Success - ' + data._message);
                l.ladda('stop');
                setTimeout(function () {
                    window.location.href = "./header.jsp";
                }, 3000);
            }
        }
    });
}

function removeImageFromSession(file){
    var s = './RemoveUploadImageFromSession?file='+file;
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        success: function (data) {
            if (strCompare(data._result, "error") === 0) {
                
            } else if (strCompare(data._result, "success") === 0) {
              
            }
        }
    });
}