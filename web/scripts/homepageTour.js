function generateTopService() {
    var apiname = [];
    var partnerCount = [];
    var otherUserCount = [];
//    var response = "fasle";
    apiname.push("api name");
    partnerCount.push('Your Count');
    otherUserCount.push('Others Count');
    var s = './TopFiveServices';
    var jsonData = $.ajax({
        url: s,
        dataType: "json",
        async: false
    }).responseText;
    console.log(jsonData);
    var jsonobj = JSON.parse(jsonData);
    for (var key in jsonobj) {
        var value = jsonobj[key];
        otherUserCount.push(value.apiCount);
        partnerCount.push(value.partApiCount);
        apiname.push(value.apiName);
//        if(value.apiCount > 0 || value.partApiCount > 0){
//            response = "true";
//        }
//        alert(JSON.stringify(value.apiCount)+" :: "+JSON.stringify(value.apiName));
    }
    console.log("otherUserCount :: " + JSON.stringify(otherUserCount));
    console.log("partnerCount :: " + partnerCount);
    console.log("apiname :: " + apiname);
//    if(response === "true"){
    var chart = c3.generate({
        bindto: '#topFiveSer',
        size: {
            height: 240,
        },
        data: {
            x: 'api name',
            columns: [
                apiname,
                otherUserCount,
                partnerCount
            ],
            colors: {
                otherUserCount: '#62cb31',
                partnerCount: '#FF7F50'
//                  callCountArr: '#62cb31',
//                  TransactionAmount: '#FF7F50'
            },
            types: {
                //CallCount: 'bar',
                CallCount: 'bar'
            },
            groups: [
                ['Your Count', 'Others Count']
            ]
        },
        subchart: {
            show: false
        },
        axis: {
            x: {
                type: 'category',
                categories: apiname
            },
            y: {
                padding: {top: 200, bottom: 0}
            },
        }
    });
//    document.getElementById("homereportImage").style.display='none';
//    document.getElementById("topFiveSer").style.display = "block";
    document.getElementById("homeReportLable").innerHTML = "Todays top five services count";
    setTimeout(function () {
        generateTopServiceV2();
    }, 1000);
//}else{
//    document.getElementById("homeReportLable").innerHTML = "Last 30 days top five services";
//    document.getElementById("topFiveSer").style.display = "none";
//        document.getElementById("homereportImage").style.display='block';
//}
}
///////// top 5 trending services
//function toptrndingServices() {
//    var apiname = [];
//    var s = './TopfiveTrendingSer';
//    var jsonData = $.ajax({
//        url: s,
//        dataType: "json",
//        async: false
//    }).responseText;
//    var jsonobj = JSON.parse(jsonData);
//
//    var i = 0;
//    var obj = [];
//    for (var key in jsonobj) {
//        var value = jsonobj[key];
//        apiname.push(value.apiName);
//        obj.push(value.credit);
//        i++;
//    }
//    var values = apiname;
//    new Chartist.Bar('#ct-chart4', {
//        labels: apiname,
//        series: [obj
//            
//        ]
//    }, {
//        seriesBarDistance: 10,
//            reverseData: true,
//            horizontalBars: true,
//            axisY: {
//                offset: 80
//        }
//    });
//}
////////  3)     Your mostly used five services
function yourMostlyService() {
    document.getElementById("mostlyservices").style.display = 'none';
    var apiname = [];
    var s = './DeveloperFiveService';
    var jsonData = $.ajax({
        url: s,
        dataType: "json",
        async: false
    }).responseText;
    var jsonobj = JSON.parse(jsonData);
    var i = 0;
    var temp = 5;
    var obj = [];
    for (var key in jsonobj) {
        var value = jsonobj[key];
        apiname.push(value.apiName);
        obj[i] = value.apiCount;
        //alert(value.apiCount);
        if (value.apiCount === 0) {
            temp--;
        }
        i++;
//        alert(JSON.stringify(key)+" :: "+JSON.stringify(value.apiName));
    }
    document.getElementById("mostlyFive").innerHTML = "Your Mostly used " + temp + " services";
    if (temp === 0) {
        document.getElementById("topmostServices").style.display = 'none';
        document.getElementById("mostlyservices").style.display = 'block';
        return;
    }
//    if(temp == 5){
//        document.getElementById("mostlyFive").innerHTML="newtext";
//    }
    console.log("apiname developer:: " + JSON.stringify(apiname));
//    console.log("api value :: "+)
    var values = apiname;
    var arr1 = new Array();
    for (var i = 0; i < 5; i++) {
        var key3 = apiname[i];
        arr1[key3] = obj[i];
    }
//    if(obj[0] !== 0){
    var data1 = apiname[0];
    var value11 = parseInt(obj[0]);
//    } if(obj[1] !== 0){
    var data2 = apiname[1];
    var value22 = parseInt(obj[1]);
//    } if(obj[2] !== 0){
    var data3 = apiname[2];
    var value33 = parseInt(obj[2]);
//    } if(obj[3] !== 0){
    var data4 = apiname[3];
    var value44 = parseInt(obj[3]);
//    } if(obj[4] !== 0){
    var data5 = apiname[4];
    var value55 = parseInt(obj[4]);
//    }
    var chart1 = c3.generate({
        bindto: '#flot-pie-chart',
        data: {
            // iris data from R
            columns: [
                [data1, value11],
                [data2, value22],
                [data3, value33],
                [data4, value44],
                [data5, value55]
            ],
            type: 'pie'
        },
        colors: {
            data1: '#F39C12',
            data2: '#F8C471',
            data3: '#F5B041',
            data4: '#F39C12',
            data5: '#D68910'
        },
        legend: {
            show: false
        },
        size: {
            width: 150,
            height: 175
        },
        label: {
            format: function (value, ratio, id) {
                return d3.format('$')(value);
            }
        }
    });
//    var pieChartData = [
//        {label: data1, data: value11, color: "#F39C12", },
//        {label: data2, data: value22, color: "#F8C471", },
//        {label: data3, data: value33, color: "#F5B041", },
//        {label: data4, data: value44, color: "#F39C12", },
//        {label: data5, data: value55, color: "#D68910", }
//    ];
//
//    var pieChartOptions = {
//        series: {
//            pie: {
//                show: true
//            }
//        },
//        grid: {
//            hoverable: true
//        },
//        tooltip: true,
//        tooltipOpts: {
//            content: "%p.0%, %s", // show percentages, rounding to 2 decimal places
//            shifts: {
//                x: 20,
//                y: 0
//            },
//            defaultTheme: false
//        }
//    };

//    $.plot($("#flot-pie-chart"), pieChartData, pieChartOptions);

}

////// Developers least used service
function leastUsedService() {
    var apiname = [];
    var s = './DeveloperLeastService';
    var jsonData = $.ajax({
        url: s,
        dataType: "json",
        async: false
    }).responseText;
    console.log(jsonData);
    var jsonobj = JSON.parse(jsonData);
    var i = 0, temp = 5;
    var obj = {};
    for (var key in jsonobj) {
        var value = jsonobj[key];
        apiname.push(value.apiName);
        obj[i] = value.apiCount;
        if (value.apiCount === 0) {
            temp--;
        }
        i++;
    }

    document.getElementById("leastusedTitle").innerHTML = "Your Least used " + temp + " services";
    var data1;
    var value11 = 0;
    var data2;
    var value22 = 0;
    var data3;
    var value33 = 0;
    var data4;
    var value44 = 0;
    var data5;
    var value55 = 0;
    if (temp !== 0) {
        if (typeof apiname[0] !== 'undefined') {
            data1 = apiname[0];
            value11 = parseInt(obj[0]);
        }
        if (typeof apiname[1] !== 'undefined') {
            data2 = apiname[1];
            value22 = parseInt(obj[1]);
        }
        if (typeof apiname[2] !== 'undefined') {
            data3 = apiname[2];
            value33 = parseInt(obj[2]);
        }
        if (typeof apiname[3] !== 'undefined') {
            data4 = apiname[3];
            value44 = parseInt(obj[3]);
        }
        if (typeof apiname[4] !== 'undefined') {
            data5 = apiname[4];
            value55 = parseInt(obj[4]);
        }





    } else {
        data1 = "You did'nt used any service";
        value11 = 100;
        data2 = "";
        value22 = "";
        data3 = "";
        value33 = "";
        data4 = "";
        value44 = "";
        data5 = "";
        value55 = "";
    }
//    alert(data1+" :: "+value11);
    var chart1 = c3.generate({
        bindto: '#flot-line-chart',
        data: {
            // iris data from R
            columns: [
                [data1, value11],
                [data2, value22],
                [data3, value33],
                [data4, value44],
                [data5, value55]
            ],
            type: 'pie'
        },
        colors: {
            data1: '#F39C12',
            data2: '#F8C471',
            data3: '#F5B041',
            data4: '#F39C12',
            data5: '#D68910'
        },
        legend: {
            show: false
        },
        size: {
            width: 150,
            height: 175
        },
        label: {
            format: function (value, ratio, id) {
                return d3.format('$')(value);
            }
        }
    });

}

/////// 5 ) Total amount
function todayusedCredit(value) {
    var s = './DailyUsedCredits?operation=' + value;
    $.ajax({
        type: 'GET',
        url: s,
        contentType: "application/json",
        dataType: "text",
        async: false,
        success: function (data) {
            if (typeof data === 'string') {
                data = JSON.parse(data);
            }
            var totalAmount = data.amount;
            var per = data.percentage;
            var message = "Today you have used total " + totalAmount + " credits for all services";
            creditUsed.innerText = totalAmount;
            perCreditUsed.innerText = per + '%';
            console.log("per >> " + per);
            console.log("totalAmount >> " + totalAmount);
            console.log("per >> " + per);
            document.getElementById('percentage').style.width = per + '%';
        },
        error: function (data) {
            console.log("error" + JSON.stringify(data));
        }
    });
}

/////// 6) Last week totl amount
function weeklyusedCredit(value) {
    var s = './WeeklyAndMonthlyExpense?timePeriod=' + value;
    $.ajax({
        type: 'GET',
        url: s,
        contentType: "application/json",
        dataType: "text",
        async: false,
        success: function (data) {
            if (typeof data === 'string') {
                data = JSON.parse(data);
            }
            var totalAmount = data.amount;
            var message = "You have used total " + totalAmount + " credits in last week for all services";
            lastWeekAmount.innerText = totalAmount;
            weekMsg.innerText = message;
        },
        error: function (data) {
            //alert("error" + JSON.stringify(data));
        }
    });
}

//// 8) Subscribed package
function devSubscribePack() {
    var s = './DevSubscribePack';
    $.ajax({
        type: 'GET',
        url: s,
        contentType: "application/json",
        dataType: "text",
        async: false,
        success: function (data) {
            if (typeof data === 'string') {
                data = JSON.parse(data);
            }
            var _packamount = data.packamount;
            var package = data.package;
            var message = "You are subscribe with.";
            _packamount.innerText = _packamount,
                    subscribePackage.innerText = package;
            subMsg.innerText = message;
        },
        error: function (data) {

        }
    });
}
//// monthly credit used
function monthlyCredit(value) {
    var s = './WeeklyAndMonthlyExpense?timePeriod=' + value;
    $.ajax({
        type: 'GET',
        url: s,
        contentType: "application/json",
        dataType: "text",
        async: false,
        success: function (data) {
            if (typeof data === 'string') {
                data = JSON.parse(data);
            }
            var totalAmount = data.amount;
            var message = "You have used total " + totalAmount + " credits in last month for all services";
            monthCredit.innerText = totalAmount;
            monthMsg.innerText = message;
        },
        error: function (data) {
            console.log("error" + JSON.stringify(data));
        }
    });
}
/// 
function creditperSer() {
    var apiname = [];
    var obj = [];
//    var response = "false";
    var s = './CreditsAsPerService';
    var jsonData = $.ajax({
        url: s,
        dataType: "json",
        async: false
    }).responseText;
    var jsonobj = JSON.parse(jsonData);
    var i = 0;
    apiname.push("api name");
    obj.push('Credit Count');
    for (var key in jsonobj) {
        var value = jsonobj[key];
        apiname.push(value.apiName);
        obj.push(value.credit);
//        if(value.credit > 0){
//            response = "true";
//        }
        i++;
    }
//    alert(JSON.stringify(apiname));
//    alert(JSON.stringify(obj));
//if(response === "true"){
    var chart = c3.generate({
        bindto: '#topFiveSer',
        size: {
            height: 240,
        },
        data: {
            x: 'api name',
            columns: [
                apiname,
                obj,
            ],
            colors: {
                otherUserCount: '#62cb31',
                partnerCount: '#FF7F50'
//                  callCountArr: '#62cb31',
//                  TransactionAmount: '#FF7F50'
            },
            types: {
                //CallCount: 'bar',
                CallCount: 'bar'
            },
            groups: [
                ['Credit Count']
            ]
        },
        subchart: {
            show: false
        },
        axis: {
            x: {
                type: 'category',
                categories: apiname
            }
        }
    });
//    document.getElementById("homereportImage").style.display='none';
//    document.getElementById("topFiveSer").style.display = "block";
    document.getElementById("homeReportLable").innerHTML = "Todays Total Expense as per advertisement";
//    }else{
//        document.getElementById("homeReportLable").innerHTML = "Todays total expenditure as per services";
//        document.getElementById("topFiveSer").style.display = "none";
//        document.getElementById("homereportImage").style.display='block';
//        
//    }
}

///duplicate
function generateTopServiceV2() {
    var apiname = [];
    var partnerCount = [];
    var otherUserCount = [];
//    var response = "fasle";
    apiname.push("api name");
    partnerCount.push('Your Count');
    otherUserCount.push('Others Count');
    var s = './TopFiveServices';
    var jsonData = $.ajax({
        url: s,
        dataType: "json",
        async: false
    }).responseText;
    console.log(jsonData);
    var jsonobj = JSON.parse(jsonData);
    for (var key in jsonobj) {
        var value = jsonobj[key];
        otherUserCount.push(value.apiCount);
        partnerCount.push(value.partApiCount);
        apiname.push(value.apiName);
//        if(value.apiCount > 0 || value.partApiCount > 0){
//            response = "true";
//        }
//        alert(JSON.stringify(value.apiCount)+" :: "+JSON.stringify(value.apiName));
    }
    console.log("otherUserCount :: " + JSON.stringify(otherUserCount));
    console.log("partnerCount :: " + partnerCount);
    console.log("apiname :: " + apiname);
//    if(response === "true"){
    var chart = c3.generate({
        bindto: '#topFiveSer',
        size: {
            height: 240,
        },
        data: {
            x: 'api name',
            columns: [
                apiname,
                otherUserCount,
                partnerCount
            ],
            colors: {
                otherUserCount: '#62cb31',
                partnerCount: '#FF7F50'
//                  callCountArr: '#62cb31',
//                  TransactionAmount: '#FF7F50'
            },
            types: {
                //CallCount: 'bar',
                CallCount: 'bar'
            },
            groups: [
                ['Your Count', 'Others Count']
            ]
        },
        subchart: {
            show: false
        },
        axis: {
            x: {
                type: 'category',
                categories: apiname
            },
            y: {
                padding: {top: 200, bottom: 0}
            },
        }
    });
//    document.getElementById("homereportImage").style.display='none';
//    document.getElementById("topFiveSer").style.display = "block";
//    document.getElementById("homeReportLable").innerHTML = "Last 30 days top five services";
//}else{
    document.getElementById("homeReportLable").innerHTML = "Todays top five services";
//    document.getElementById("topFiveSer").style.display = "none";
//        document.getElementById("homereportImage").style.display='block';
//}
}
function toptrndingServices() {
    var apiname = [];
    var obj = [];
//    var response = "false";
    var s = './TopfiveTrendingSer';
    var jsonData = $.ajax({
        url: s,
        dataType: "json",
        async: false
    }).responseText;
    var jsonobj = JSON.parse(jsonData);

    var i = 0;
    apiname.push("api name");
    obj.push("Credit Used");
    for (var key in jsonobj) {
        var value = jsonobj[key];
        apiname.push(value.apiName);
        obj.push(value.apiCount);
//        if(value.apiCount > 0){
//            response = "true";
//        }
        i++;
    }
//    if(response === "true"){
    var chart = c3.generate({
        bindto: '#topFiveSer',
        size: {
            height: 240,
        },
        data: {
            x: 'api name',
            columns: [
                apiname,
                obj,
            ],
            colors: {
                otherUserCount: '#62cb31',
                partnerCount: '#FF7F50'
//                  callCountArr: '#62cb31',
//                  TransactionAmount: '#FF7F50'
            },
            types: {
                //CallCount: 'bar',
                CallCount: 'bar'
            },
            groups: [
                ['Credit used']
            ]
        },
        subchart: {
            show: false
        },
        axis: {
            x: {
                type: 'category',
                categories: apiname
            },
            y: {
                show: true,
                min: 0,
                padding: {bottom: 0}
            }
        }
    });
//    document.getElementById("homereportImage").style.display='none';
//    document.getElementById("topFiveSer").style.display = "block";
    document.getElementById("homeReportLable").innerHTML = "Todays trending services";
//}else{
//    document.getElementById("homeReportLable").innerHTML = "Todays top five trending services";
//    document.getElementById("topFiveSer").style.display = "none";
//        document.getElementById("homereportImage").style.display='block';
//}
}
function chartddd() {
    c3.generate({
        bindto: '#pie',
        data: {
            columns: [
                ['shailendra', 30],
                ['data2', 120]
            ],
            legend: false,
            colors: {
                shailendra: '#62cb31',
                data2: '#CFCFCF'
            },
            type: 'pie'
        }
    });

//         var chart = c3.generate({
//             bindto: '#pie',
//      data: {
//        columns: [
//          ['data1', 30],
//          ['data2', 50]
//        ],
//        type: 'pie'
//      },
//      pie: {
//        label: {
//          format: function(value, ratio, id) {
//            return value;
//          }
//        }
//      }
//    });
}


function generateTopServiceTour() {
    var apiname = ['api name','DocumentUtility','ValueAddedServices','InstantWebChat','GoogleAuthToken','ValueAddedServices'];
    var partnerCount = ['Your Count',130,190,250,90,160];
    var otherUserCount = ['Others Count',90,230,170,70,140];
    console.log("otherUserCount :: " + JSON.stringify(otherUserCount));
    console.log("partnerCount :: " + partnerCount);
    console.log("apiname :: " + apiname);
    var chart = c3.generate({
        bindto: '#topFiveSer',
        size: {
            height: 240,
        },
        data: {
            x: 'api name',
            columns: [
                apiname,
                otherUserCount,
                partnerCount
            ],
            colors: {
                otherUserCount: '#62cb31',
                partnerCount: '#FF7F50'
            },
            types: {
                CallCount: 'bar'
            },
            groups: [
                ['Your Count', 'Others Count']
            ]
        },
        subchart: {
            show: false
        },
        axis: {
            x: {
                type: 'category',
                categories: apiname
            },
            y: {
                padding: {top: 200, bottom: 0}
            },
        }
    });   
    document.getElementById("homeReportLable").innerHTML = "Todays top five services count";
    setTimeout(function () {
        generateTopServiceTourV2();
    }, 1000);
}

function generateTopServiceTourV2() {
    var apiname = ['api name','DocumentUtility','ValueAddedServices','InstantWebChat','GoogleAuthToken','ValueAddedServices'];
    var partnerCount = ['Your Count',290,230,170,270,140];
    var otherUserCount = ['Others Count',130,190,250,290,160];

    console.log("otherUserCount :: " + JSON.stringify(otherUserCount));
    console.log("partnerCount :: " + partnerCount);
    console.log("apiname :: " + apiname);

    var chart = c3.generate({
        bindto: '#topFiveSer',
        size: {
            height: 240,
        },
        data: {
            x: 'api name',
            columns: [
                apiname,
                otherUserCount,
                partnerCount
            ],
            colors: {
                otherUserCount: '#62cb31',
                partnerCount: '#FF7F50'
//                  callCountArr: '#62cb31',
//                  TransactionAmount: '#FF7F50'
            },
            types: {
                //CallCount: 'bar',
                CallCount: 'bar'
            },
            groups: [
                ['Your Count', 'Others Count']
            ]
        },
        subchart: {
            show: false
        },
        axis: {
            x: {
                type: 'category',
                categories: apiname
            },
            y: {
                padding: {top: 200, bottom: 0}
            },
        }
    });
    document.getElementById("homeReportLable").innerHTML = "Todays top five services";
}

function yourMostlyServiceTour() {
    //var apiname = [];
    
    document.getElementById("mostlyFive").innerHTML = "Your Mostly used 5 services";
    
    var chart1 = c3.generate({
        bindto: '#flot-pie-chart',
        data: {
            // iris data from R
            columns: [
                ["Image Utility", 70],
                ["Tokenization", 50],
                ["InAppPush", 30],
                ["ReverseProxy", 20],
                ["IP2GIO", 40]
            ],
            type: 'pie'
        },
        colors: {
            data1: '#F39C12',
            data2: '#F8C471',
            data3: '#F5B041',
            data4: '#F39C12',
            data5: '#D68910'
        },
        legend: {
            show: false
        },
        size: {
            width: 150,
            height: 175
        },
        label: {
            format: function (value, ratio, id) {
                return d3.format('$')(value);
            }
        }
    });
}