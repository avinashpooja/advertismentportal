/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


function removeKYCFromSession(file){
    var s = './RemoveUploadKYCFromSession?file='+file;
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        success: function (data) {
            if (strCompare(data._result, "error") === 0) {
                
            } else if (strCompare(data._result, "success") === 0) {
              
            }
        }
    });
}

function validateRegistrationForm() {
    $("#personalInfo").validate({
        rules: {
            partnerName: {
                required: true
            },
            mobileNo: {
                required: true,
                number:true
            },
            emailId: {
                required: true
            },
            comapnyName: {
                required: true
            },
            regNo: {
                required: true
            },
            landlineNo: {
                required: true
            },
            companyAddress:{
                required: true
            },            
            cityName:{
                required: true
            },
            country: {
                required: true
            },
            pinCode: {
                required: true
            },
            state: {
                required: true
            }
        },
        submitHandler: function (form) {
            createAdvertiser();
        }
    });
}
function callValidateRegistrationForm() {
    document.querySelector('.validateProfileForm').click();
}

function createAdvertiser() {
    var l = $('#partUpdate').ladda();   
    l.ladda('start');
    initializeToastr();
    var s = './CreateAdvertiserDetails';
    $.ajax({
        type: 'POST',
        url: s,
        data: $("#personalInfo").serialize(),
        dataType: 'json',
        success: function (data) {
            if (strCompare(data._result, "error") === 0) {
                toastr.error('Error - ' + data._message);
                l.ladda('stop');
            } else {
                //toastr.success('Success - ' + data._message);
                l.ladda('stop');
                var modal = document.getElementById('myModal');
                modal.style.display = "block";
                getPackageInvoice();
            }
        }
    });
}

function getPackageInvoice() {
    $('#signupWindow').hide();
    var s = "getInvoiceFirstSignUp.jsp";
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#paymentWindow').html(data);
            $('#paymentWindow').show();
        },
        error: function (data) {
        }
    });
}