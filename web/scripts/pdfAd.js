/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


function removePDFAdImageFromSession(file){
    var s = './RemovePDFAdImageFromSession?file='+file;
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        success: function (data) {
            if (strCompare(data._result, "error") === 0) {
                
            } else if (strCompare(data._result, "success") === 0) {
              
            }
        }
    });
}

function updatePDFAd() {
    var l = $('#partUpdate').ladda();   
    l.ladda('start');
    initializeToastr();
    var s = './CreatePDFAd';
    $.ajax({
        type: 'POST',
        url: s,        
        dataType: 'json',
        success: function (data) {
            if (strCompare(data._result, "error") === 0) {
                toastr.error('Error - ' + data._message);
                l.ladda('stop');
            } else {
                toastr.success('Success - ' + data._message);
                l.ladda('stop');
                setTimeout(function () {
                    window.location.href = "./header.jsp";
                }, 3000);
            }
        }
    });
}
function updatePushAd() {
    var l = $('#partUpdate').ladda();   
    l.ladda('start');
    initializeToastr();
    var s = './CreatePushAd';
    $.ajax({
        type: 'POST',
        url: s,        
        dataType: 'json',
        success: function (data) {
            if (strCompare(data._result, "error") === 0) {
                toastr.error('Error - ' + data._message);
                l.ladda('stop');
            } else {
                toastr.success('Success - ' + data._message);
                l.ladda('stop');
                setTimeout(function () {
                    window.location.href = "./header.jsp";
                }, 3000);
            }
        }
    });
}

function validateEmailAdForm() {
    $("#emailAdvertisementForm").validate({
        rules: {
            message: {
                required: true
            },
            emailAdTitle:{
                required: true
            }
        },
        submitHandler: function (form) {
            createEmailAdvertisement();
        }
    });
}
function callValidateEmailAdForm() {
    document.querySelector('.validateEmailForm').click();
}

function createEmailAdvertisement(){
    var l = $('#partUpdate').ladda();   
    l.ladda('start');
    initializeToastr();
    var s = './CreateEmailAd';
    $.ajax({
        type: 'POST',
        url: s,        
        dataType: 'json',
        data: $("#emailAdvertisementForm").serialize(),
        success: function (data) {
            if (strCompare(data._result, "error") === 0) {
                toastr.error('Error - ' + data._message);
                l.ladda('stop');
            } else {
                toastr.success('Success - ' + data._message);
                l.ladda('stop');
                setTimeout(function () {
                    window.location.href = "./header.jsp";
                }, 3000);
            }
        }
    });
}