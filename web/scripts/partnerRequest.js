/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//validate emailID 

function goFunction(event) {
    if (event.keyCode == 13) {
        event.preventDefault();
        document.getElementById('_name').onkeypress = function (event) {
            return;
        };
        //document.getElementById("goButton").click();
    }
}
function loginFunction(event) {
    if (event.keyCode == 13) {
        event.preventDefault();
        if (document.getElementById('_passwd')) {
            document.getElementById('_passwd').onkeypress = function (event) {
                return;
            };
        } else {
            document.getElementById('_otp').onkeypress = function (event) {
                return;
            };
        }
        document.getElementById("loginButton").click();
    }
}




function enableLogin() {
    initializeToastr();
    if (document.getElementById('_name').value === '') {
        toastr.error('Error - ' + 'Please Enter Email Id.');
        return;
    }
    var l = $('#goButton').ladda();
    l.ladda('start');
    document.getElementById("goButton").type = "button";
    var s = './CheckLogin?mailId=' + document.getElementById('_name').value;
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        success: function (data) {
            if (strCompare(data._result, "error") === 0) {
                setTimeout(function () {
                    l.ladda('stop');
                    toastr.error('Error - ' + data._message);
                }, 3000);
                document.getElementById("goButton").type = "submit";
                document.getElementById('_name').onkeypress = goFunction;
            } else if (strCompare(data._result, "otp") === 0) {
                document.getElementById("goButton").type = "button";
                document.getElementById("loginButton").type = "submit";
                window.location = 'validateOTP.jsp?ref=s&_to=' + data._id;
            } else if (strCompare(data._result, "secondTimeLogin") === 0) {
                setTimeout(function () {
                    l.ladda('stop');
                    toastr.success('Success - ' + data._message);
                }, 1000);
                setTimeout(function () {
                    document.getElementById('goButton').style.display = "none";
                    document.getElementById('passDiv').style.display = "block";
                    document.getElementById('loginButton').style.display = "block";
                    document.getElementById("_name").disabled = true;
                    document.getElementById("loginButton").type = "submit";

                }, 3000);
                setTimeout(function () {
                    document.getElementById("_passwd").focus();
                    setLoginFocus();
                }, 4000);

            } else {
                document.getElementById("goButton").type = "button";
                document.getElementById("loginButton").type = "submit";
                window.location = 'validateOTP.jsp?ref=e&_to=' + data._id;
            }
        }
    });
}

function setLoginFocus() {
    $('body').on('keydown', '#_passwd', function (e) {
        if (e.which == 9) {
            e.preventDefault();
            document.getElementById("loginButton").focus();
            document.getElementById('loginButton').href = "#/";
        }
    });
}


// developer login function
function userLogin(type) {
    document.getElementById("_name").disabled = false;
    if (document.getElementById("_passwd")) {
        var password = document.getElementById("_passwd").value;
        if (password === null || password === '') {
            toastr.error('Error - ' + 'Please enter password');
            return;
        }
    }
    loginButton.disabled = true;
    var s = './login';
    var l = $('#loginButton').ladda();
    l.ladda('start');
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#login_form").serialize(),
        success: function (data) {
            if (strCompare(data._result, "error") === 0) {
                if (type === 'jsp') {
                    waiting.modal('hide');
                    showAlert(data._message, "danger", 3000);
                } else {
                    setTimeout(function () {
                        l.ladda('stop');
                        toastr.error('Error - ' + data._message);
                    }, 3000);
                    loginButton.disabled = false;
                    loginButton.innerHTML = 'Login';
                }
                if (document.getElementById('_passwd')) {
                    document.getElementById('_passwd').onkeypress = loginFunction;
                } else {
                    document.getElementById('_otp').onkeypress = loginFunction;

                }
                document.getElementById('_passwd').onkeypress = loginFunction;
            } else if (strCompare(data._result, "error1") === 0) {
                if (type === 'jsp') {
                    waiting.modal('hide');
                    showAlert(data._message, "danger", 3000);
                } else {
                    setTimeout(function () {
                        l.ladda('stop');
                        toastr.error('Error - ' + data._message);
                    }, 3000);
                    loginButton.disabled = false;
                    loginButton.innerHTML = 'Login';
                }
                setTimeout(function () {
                    window.location.href = './logout.jsp';
                }, 5000);
                if (document.getElementById('_passwd')) {
                    document.getElementById('_passwd').onkeypress = loginFunction;
                } else {
                    document.getElementById('_otp').onkeypress = loginFunction;

                }
            } else if (strCompare(data._result, "success") === 0) {
                if (type === 'jsp') {
                    waiting.modal('hide');
                    showAlert(data._message, "success", 2000);
                }
                if (data._url === "redirect") {
                    $('#login_btn').prop("disabled", true);
                    $('#signup_btn').prop("disabled", true);
                    $("#signup_btn").removeAttr('href');
                    document.getElementById("2fa_auth").style.display = "block";
                } else {
                    setTimeout(function () {
                        l.ladda('stop');
                        toastr.success('Success - ' + data._message);
                    }, 1000);
                    setTimeout(function () {
                        window.location.href = data._url;
                    }, 3000);
                }
            } else if (strCompare(data._result, "redirect") === 0) {
                if (type === 'jsp') {
                    waiting.modal('hide');
                    showAlert(data._message, "success", 2000);
                } else {
                    setTimeout(function () {
                        l.ladda('stop');
                        toastr.success('Success - ' + data._message);
                    }, 1000);
                    setTimeout(function () {
                        window.location.href = './home.jsp';
                    }, 3000);
                    return;
                }
                window.location.href = './home.jsp';
            }
        }
    });
}

//resend OTP
function resendOTP(userid) {
    var s = './CheckLogin?mailId=' + userid + '&resendOTPRequest=' + true;
    var l = $('#resendOTPButton').ladda();
    l.ladda('start');
    initializeToastr();
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        success: function (data) {
            if (strCompare(data._result, "error") === 0) {
                setTimeout(function () {
                    l.ladda('stop');
                    toastr.error('Error - ' + data._message);
                }, 3000);
            } else if (strCompare(data._result, "otp") === 0) {
                setTimeout(function () {
                    l.ladda('stop');
                    toastr.success('Success - ' + data._message);
                }, 3000);
//                $('#sendotp').modal();
                //window.location = 'validateOTP.jsp?ref=s&_to=' + data._id;
            } else {
                setTimeout(function () {
                    l.ladda('stop');
                    toastr.error('Error - ' + data._message);
                }, 3000);
//                $('#resendotperror').modal();
//                document.getElementById("resendOTPErrorMsg").innerHTML = data._message;
                //window.location = 'validateOTP.jsp?ref=e&_to=' + data._id;
            }
        }
    });
}


// reset password
function resetDeveloperPassword(requestFrom) {
//    var _oUsername = document.getElementById('_oUsername').value;
    var _oEmailId = document.getElementById('_oEmailId').value;
    //var _oRandomString = document.getElementById('_oRandomString').value;
    var l = $('#resetButton').ladda();
    l.ladda('start');
    initializeToastr();
    var s = './ResetDeveloperPassword?' + "_oEmailId=" + _oEmailId + "&_oRandomString=";
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function (data) {
            if (strCompare(data.result, "error") == 0) {
//                $('#password-reset-result').html("<span><font color=red>" + data.message + "</font></span></small>");
                setTimeout(function () {
                    l.ladda('stop');
                    toastr.error('Error - ' + data.message);
                }, 3000);
            } else {
                //$('#password-reset-result').html("<span><font color=blue>" + data.message + "</font></span>");
                setTimeout(function () {
                    l.ladda('stop');
                    toastr.success('Success - ' + data.message);
                }, 3000);
                setTimeout(function () {
                    window.location = "./login.jsp";
                }, 6000);

            }
        }
    });
}

// terms and condition 
function viewTMTermsCond() {
    $('#tmTermsAndCondition').modal();
}

// developer sign in
//modified userReg func
function userRegistration(type) {
    if (type === 'jsp') {
        var cname = document.getElementById("partnerComName").value;
        var landline = document.getElementById("partnerLandlineNo").value;
        var fax = document.getElementById("partnerFax").value;
        var website = document.getElementById("partnerWebSite").value;
        var ip = document.getElementById("partnerIP").value;
        var iplive = document.getElementById("partnerIPLive").value;
        var address = document.getElementById("partnerAddress").value;
        var pincode = document.getElementById("partnerPincode").value;
        var name = document.getElementById("partnerName").value;
        var email = document.getElementById("partnerEmail").value;
        var mobno = document.getElementById("partnerMobNo").value;
        var fileuploaded = document.getElementById("ziptoupload").value;

        if (cname === "" || landline === "" || fax === "" || website === "" || ip === "" || iplive === "" || address === "" || pincode === "" || name === "" || email === "" || mobno === "") {
//        bootboxmodel("<h3><font style='color:red'>" + "Please check all details!!!" + "</font></h3>");
            showAlert("Please fill all details !!!", "danger", 3000);
            return;
        } else if (fileuploaded === "") {
            showAlert("Please attach documents !!!", "danger", 3000);
            return;
        } else {
            document.getElementById("register").disabled = true;
            var s = './UploadZip';
            pleasewaitV1();
            $.ajaxFileUpload({
                type: 'POST',
                fileElementId: 'ziptoupload',
                url: s,
                dataType: 'json',
                success: function (data) {
                    if (strCompare(data.result, "error") === 0) {
                        waitingV1.modal('hide');
                        showAlert(data.message, "danger", 4000);
                        document.getElementById("register").disabled = false;
                    } else if (strCompare(data.result, "success") === 0) {
                        //Alert4Users(data._message);                    
                        $('#_filename').val(data.filename);
                        AddPartner(type);
//                    showAlert(data._message,"success",3000);
//                    setTimeout(function () {
//                        window.location.href = "./login.jsp";
//                    }, 3000);
                    }
                },
                error: function (data, status, e)
                {
                    waitingV1.modal('hide');
                    alert(e);
                }
            });
        }
    } else {
        initializeToastr();
        $("#register_form").validate({
            rules: {
                username_pp: {
                    required: true
                },
                partnerEmail: {
                    required: true
                },
                partnerMobNo: {
                    required: true,
                    number: true
                },
                pp_password: {
                    required: true,
                    minlength: 8
                }
            },
            submitHandler: function (form) {
                var terms = document.getElementById('termsCondition').checked;
                if (!terms) {
                    setTimeout(function () {
                        toastr.error('Error - ' + 'Please accept terms and condition');
                    }, 3000);
                    return;
                }
                AddPartner(type);
            }
        });
    }
}

function AddPartner(type) {
    var s = './registerPartner?type=' + type;
    var l = $('#registrationButton').ladda();
    l.ladda('start');
    $.ajax({
        type: 'POST',
        fileElementId: 'fileImageToUpload',
        url: s,
        dataType: 'json',
        data: $("#register_form").serialize(),
        success: function (data) {
            if (strCompare(data._result, "error") === 0) {
                if (type === 'jsp') {
                    waitingV1.modal('hide');
                    showAlert(data._message, "danger", 3000);
                } else {

                    setTimeout(function () {
                        toastr.error('Error - ' + data._message);
                        l.ladda('stop');
                    }, 3000);
                    return;
                }
            } else if (strCompare(data._result, "success") === 0) {
                if (type === 'jsp') {
                    waitingV1.modal('hide');
                    showAlert(data._message, "success", 3000);
                    setTimeout(function () {
                        window.location.href = "./login.jsp";
                    }, 3000);
                } else {
                    l.ladda('stop');
                    setTimeout(function () {
                        toastr.success('Success - ' + data._message);
                        l.ladda('stop');
                        redirectToLogin();
                    }, 3000);
                }
            }
        }
    });
}

function redirectToLogin() {
    setTimeout(function () {
        window.location.href = "./login.jsp";
    }, 5000);
}
