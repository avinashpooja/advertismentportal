function apiServices() {
    if ((test.className).indexOf("page-small") > -1) {
        test.classList.remove("show-sidebar");
        test.classList.add("hide-sidebar");
    }
    $('#otherPage').empty();
    $('#wrapper1').hide();
    document.getElementById("wrapper3").style.display = "block";
    var s = './apiConsole.jsp';
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#otherPage').html(data);
        },
        complete: function () {
            document.getElementById("wrapper3").style.display = "none";
            $('#wrapper2').show();
        }

    });
}
var test;

function apiServicesTour() {

    var s = './apiConsole.jsp';

    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            //$('#apiTour').show();
            $('#wrapperAPIConsoleTour').html(data);
//            console.log("apiTable >> "+data);
//            document.getElementById("apiConsoleChangeService").style.display='none';
        }
    });
}

function dashboardTour() {
    var s = './tourToDashboard.jsp';
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            // $('#apiConsoleTour').show();
            $('#tourDiv').html(data);
        }
    });
}

function apiConsoleTour() {
    var s = './apiConsoleWindowTour.jsp';

    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#apiConsoleTour').show();
            $('#wrapperAPIConsoleWindowTour').html(data);
        }
    });
}

function inAppPushDownload(){
    $('#otherPage').empty();
    $('#wrapper1').hide();
    document.getElementById("wrapper3").style.display = "block";
    var s = "downloadInAppPushFiles.jsp";
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#otherPage').html(data);
            $('#wrapper2').show();
            document.getElementById("wrapper3").style.display = "none";
            
        }
    });
}

function mobileTrustFiles(){
    $('#otherPage').empty();
    $('#wrapper1').hide();
    document.getElementById("wrapper3").style.display = "block";
    var s = "downloadMobileTrustFiles.jsp";
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#otherPage').html(data);
            $('#wrapper2').show();
            document.getElementById("wrapper3").style.display = "none";
            
        }
    });
}
function reportsTour() {
    var s = "reportTourWindow.jsp";
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
//            $('#reportTour').show();
            $('#reportWindowTour').html(data);
        }
    });
}

function myInvoiceTour() {
    var s = "my_invoice.jsp";
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            // $('#invoiceTour').show();
            $('#invoiceWindowTour').html(data);
            document.getElementById("invoiceHeader").style.display = 'none';
            document.getElementById("footerInvoice").style.display = 'none';
            document.getElementById("invoiceMarigin").style.display = 'none';
        }
    });
}

function subscriptionTour() {
    //var s="ala-carte-package.jsp";
    var s = "unsubscribePackage.jsp";
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            //$('#subscribeTour').show();
            $('#subscribeWindowTour').html(data);
            document.getElementById("subscriptionPackageHeader").style.display = 'none';
            document.getElementById("subscriptionFooter").style.display = 'none';
            document.getElementById("marginFooter").style.display = 'none';
        }
    });
}

function helpDeskTour() {
    var s = "compose_email_details.jsp";
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            //$('#helpDeskTour').show();
            $('#helpDeskWindowTour').html(data);
            document.getElementById("helpDeskHeader").style.display = 'none';
        }
    });
}

function clearPanels() {
    $('#apiTour').hide();
    $('#apiConsoleTour').hide();
    $('#reportWindowTour').hide();
    $('#invoiceWindowTour').hide();
    $('#subscribeWindowTour').hide();
    $('#helpDeskWindowTour').hide();
    $('#tourDiv').hide();
//     document.getElementById("footerPage").style.display='block';
}
function sethomepage() {
    $('#otherPage').empty();
    $('#wrapper1').hide();
    document.getElementById("wrapper3").style.display = "block";
    var s = './apiConsole.jsp';
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            document.getElementById("wrapper3").style.display = "none";
            $('#homepage').html(data);
        }

    });
}
function testApi(value1, value2, value3, value4) {
    $('#otherPage').empty();
    $('#wrapper1').hide();
    document.getElementById("wrapper3").style.display = "block";
    var s = "testAPI.jsp?_resId=" + value1 + "&_apid=" + value2 + "&methodName=" + value3 + "&envt=" + value4;
//    var s = './apiConsole.jsp';
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#otherPage').html(data);
            document.getElementById("wrapper3").style.display = "none";
            $('#wrapper2').show();


        },
    });
}
//function myInvoice() {
//    var s="my_invoice.jsp";
//    $.ajax({
//        type: 'GET',
//        url: s,
//        success: function (data) {
//            $('#wrapper1').hide();
//            $('#wrapper2').hide();
//            $('#wrapper3').hide();
//            $('#wrapper5').hide();
//            $('#wrapper6').hide();
//            $('#wrapper7').hide();
//            $('#myInvoiceReport').html(data);
//            $('#wrapper4').show();
//            
//        }
//    });
//}

function reports() {
    if ((test.className).indexOf("page-small") > -1) {
        test.classList.remove("show-sidebar");
        test.classList.add("hide-sidebar");
    }
    $('#otherPage').empty();
    $('#wrapper1').hide();
    document.getElementById("wrapper3").style.display = "block";
    var s = "reports.jsp";
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#otherPage').html(data);
//            document.getElementById("wrapper3").style.display = "none";
//            $('#wrapper2').show();
        },
        complete: function () {
            document.getElementById("wrapper3").style.display = "none";
            $('#wrapper2').show();
        }
    });
}
function subscribe() {
    if ((test.className).indexOf("page-small") > -1) {
        test.classList.remove("show-sidebar");
        test.classList.add("hide-sidebar");
    }
    $('#otherPage').empty();
    $('#wrapper1').hide();
    document.getElementById("wrapper3").style.display = "block";
    var s = "unsubscribePackage.jsp";
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#otherPage').html(data);
            document.getElementById("wrapper3").style.display = "none";
            $('#wrapper2').show();

        },
        error: function (data) {
        }
    });
}

function subscribeNew() {
    
    $('#otherPage').empty();
    $('#wrapper1').hide();
    document.getElementById("wrapper3").style.display = "block";
    //var s = "ala-carte-package.jsp";
    var s = "packageListToSubscribe.jsp";
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#otherPage').html(data);
            document.getElementById("wrapper3").style.display = "none";
            $('#wrapper2').show();

        },
        error: function (data) {
            alert(data);
        }
    });

}

//my_subscription_package_detail_live.jsp?_name=<%=packageObj[i].getBucketName()%>
//function subscriptionPackDetails(value){
//    alert("hello");
//    var s="my_subscription_package_detail_live.jsp?_name="+value;
//    $.ajax({
//        type: 'GET',
//        url: s,
//        success: function (data) {
//            $('#wrapper1').hide();
//            $('#homepage').hide();
//            $('#testApi').hide();
//            $('#wrapper2').hide();
//            $('#wrapper3').hide();
//            $('#wrapper4').hide();
//            $('#wrapper5').hide();
//            $('#wrapper6').hide();
//            $('subscribePackage1').html(data);
//            $('#wrapper7').show();
//        },
//    });
//}


function helpDesk() {
    if ((test.className).indexOf("page-small") > -1) {
        test.classList.remove("show-sidebar");
        test.classList.add("hide-sidebar");
    }
    if ((collapse.className).indexOf("in") > -1) {
        collapse.classList.remove("in");
    }
    $('#otherPage').empty();
    $('#wrapper1').hide();
    document.getElementById("wrapper3").style.display = "block";
    var s = "compose_email_details.jsp";
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#otherPage').html(data);
            document.getElementById("wrapper3").style.display = "none";
            $('#wrapper2').show();

        }
    });
}

function raiseTicket() {
    $('#otherPage').empty();
    $('#wrapper1').hide();
    document.getElementById("wrapper3").style.display = "block";
    var s = "compose_email_technical.jsp";
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#otherPage').html(data);
            document.getElementById("wrapper3").style.display = "none";
            $('#wrapper2').show();

        }
    });
}

function changePassword() {
    $('#otherPage').empty();
    $('#wrapper1').hide();
    document.getElementById("wrapper3").style.display = "block";
    var s = "change_password.jsp";
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#otherPage').html(data);
            document.getElementById("wrapper3").style.display = "none";
            $('#wrapper2').show();
        }

    });
    $('.mobile-menu-toggle').click();
}

//function raiseTicket(){
//     var s="compose_email_details.jsp";
//    $.ajax({
//        type: 'GET',
//        url: s,
//        success: function (data) {
//            $('#wrapper1').hide();
//            $('#homepage').hide();
//            $('#testApi').hide();
//            $('#helpdesk').hide();
//            $('#wrapper2').hide();
//            $('#wrapper3').hide();
//            $('#wrapper4').hide();
//            $('#wrapper5').hide();
//            $('#wrapper6').hide();
//            $('#wrapper7').hide();
//            $('#reports').hide();
//            $('#myInvoiceReport').hide();
//            $('#subscribePackage').hide();
//            $('#changePassWord').hide();
//            $('#wrapper8').hide();
//            $('#raiseTicket').html(data);
//            
//        },
//    });
//}

var collapse;
function myInvoice() {
    $('#otherPage').empty();
    $('#wrapper1').hide();
    document.getElementById("wrapper3").style.display = "block";
    var s = "my_invoice.jsp";
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            document.getElementById("wrapper3").style.display = "none";
            $('#otherPage').html(data);
            $('#wrapper2').show();
        }
    });
}

function videoGallery() {
    if ((test.className).indexOf("page-small") > -1) {
        test.classList.remove("show-sidebar");
        test.classList.add("hide-sidebar");
    }
    if ((collapse.className).indexOf("in") > -1) {
        collapse.classList.remove("in");
    }
    $('#otherPage').empty();
    $('#wrapper1').hide();
    document.getElementById("wrapper3").style.display = "block";
    var s = "testDiv.jsp";
    ///var s = "videoGa2.jsp";
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            document.getElementById("wrapper3").style.display = "none";
            $('#otherPage').html(data);
            $('#wrapper2').show();
        }
    });
}

function emailAD() {
    $('#otherPage').empty();
    $('#wrapper1').hide();
    document.getElementById("wrapper3").style.display = "block";
    var s = "createEmailAdDetail.jsp";    
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            document.getElementById("wrapper3").style.display = "none";
            $('#otherPage').html(data);
            $('#wrapper2').show();
        }
    });
}

function emailADDetails() {
    $('#otherPage').empty();
    $('#wrapper1').hide();
    document.getElementById("wrapper3").style.display = "block";
    var s = "getEmailAdDetailList.jsp";    
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            document.getElementById("wrapper3").style.display = "none";
            $('#otherPage').html(data);
            $('#wrapper2').show();
        }
    });
}


function pdfAD() {
    $('#otherPage').empty();
    $('#wrapper1').hide();
    document.getElementById("wrapper3").style.display = "block";
    var s = "createPDFAdDetail.jsp";    
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            document.getElementById("wrapper3").style.display = "none";
            $('#otherPage').html(data);
            $('#wrapper2').show();
        }
    });
}

function pdfADDetails() {
    $('#otherPage').empty();
    $('#wrapper1').hide();
    document.getElementById("wrapper3").style.display = "block";
    var s = "getPDFAdDetailList.jsp";    
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            document.getElementById("wrapper3").style.display = "none";
            $('#otherPage').html(data);
            $('#wrapper2').show();
        }
    });
}

function pushAD() {
    $('#otherPage').empty();
    $('#wrapper1').hide();
    document.getElementById("wrapper3").style.display = "block";
    var s = "createPushAdDetail.jsp";    
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            document.getElementById("wrapper3").style.display = "none";
            $('#otherPage').html(data);
            $('#wrapper2').show();
        }
    });
}

function pushADDetails() {
    $('#otherPage').empty();
    $('#wrapper1').hide();
    document.getElementById("wrapper3").style.display = "block";
    var s = "getPushAdDetailList.jsp";    
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            document.getElementById("wrapper3").style.display = "none";
            $('#otherPage').html(data);
            $('#wrapper2').show();
        }
    });
}

function subscription() {
    $('#otherPage').empty();
    $('#wrapper1').hide();
    document.getElementById("wrapper3").style.display = "block";
    var s = "packageListToSubscribe.jsp";
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            console.log(data);
            $('#otherPage').html(data);
            $('#wrapper2').show();
            document.getElementById("wrapper3").style.display = "none";
        }
    });
    $('.mobile-menu-toggle').click();
}

function home() {
    $('#otherPage').empty();
    $('#wrapper2').hide();
    document.getElementById("wrapper3").style.display = "block";

    document.getElementById("wrapper3").style.display = "none";
    $('#wrapper1').show();
}


function downloadsMobile() {
    $('#otherPage').empty();
    $('#wrapper1').hide();
    document.getElementById("wrapper3").style.display = "block";
    var s = "downloadRequiredFiles.jsp";
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#otherPage').html(data);
            $('#wrapper2').show();
            document.getElementById("wrapper3").style.display = "none";

        }
    });
    $('.mobile-menu-toggle').click();
}

function downloads() {
    $('#otherPage').empty();
    $('#wrapper1').hide();
    document.getElementById("wrapper3").style.display = "block";
    var s = "downloadRequiredFiles.jsp";
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#otherPage').html(data);
            $('#wrapper2').show();
            document.getElementById("wrapper3").style.display = "none";

        }
    });
}

function apiServicesNew() {
    $('#otherPage').empty();
    $('#wrapper1').hide();
    document.getElementById("wrapper3").style.display = "block";
    var s = './readyToUseServices.jsp';
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#otherPage').html(data);
        },
        complete: function () {
            document.getElementById("wrapper3").style.display = "none";
            $('#wrapper2').show();
        }
    });
}

function apiServicesV2(accesspointId, resourceId) {
    $('#otherPage').empty();
    $('#wrapper1').hide();
    document.getElementById("wrapper3").style.display = "block";
    var s = './apiConsole.jsp?accesspointId=' + accesspointId + '&resourceId=' + resourceId;
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#otherPage').html(data);
        },
        complete: function () {
            document.getElementById("wrapper3").style.display = "none";
            $('#wrapper2').show();
        }
    });
}

function myProfile() {
    $('#otherPage').empty();
    $('#wrapper1').hide();
    document.getElementById("wrapper3").style.display = "block";
    var s = "my_profile.jsp";
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#otherPage').html(data);
            document.getElementById("wrapper3").style.display = "none";
            $('#wrapper2').show();
        }
    });
}