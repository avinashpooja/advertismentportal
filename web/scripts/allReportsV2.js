/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function getReportConsole(reportType) {
    console.log("reportType >> " + reportType);
    if (reportType == 1) {
        var s = './getAPIUsageReportDiv.jsp';
        $.ajax({
            type: 'GET',
            url: s,
            success: function (data) {
                document.getElementById("reportTypeDiv").classList.remove("content");
                document.getElementById("reportTypeDiv").classList.remove("animate-panel");
                $('#reportTypeDiv').html(data);
            }
        });
    } else if (reportType == 2) {
        var s = './getTransactionReportDiv.jsp';
        $.ajax({
            type: 'GET',
            url: s,
            success: function (data) {
                document.getElementById("reportTypeDiv").classList.remove("content");
                document.getElementById("reportTypeDiv").classList.remove("animate-panel");
                $('#reportTypeDiv').html(data);
            }
        });
    } else if (reportType == 3) {
        var s = './getPerformanceReportDiv.jsp';
        $.ajax({
            type: 'GET',
            url: s,
            success: function (data) {
                document.getElementById("reportTypeDiv").classList.remove("content");
                document.getElementById("reportTypeDiv").classList.remove("animate-panel");
                $('#reportTypeDiv').html(data);
            }
        });
    } else {
        var s = './getReportDiv.jsp';
        $.ajax({
            type: 'GET',
            url: s,
            success: function (data) {
                document.getElementById("reportTypeDiv").classList.remove("content");
                document.getElementById("reportTypeDiv").classList.remove("animate-panel");
                $('#reportTypeDiv').html(data);
            }
        });
    }

}
