/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(function () {
// Data used for weekly chart
    var chartData = weeklyAreaChart();
    var arrXaxis = [];
    var arrYaxis = [];
    for (var key in chartData) {
        var attrName = key;
        var attrValue = chartData[key];
        arrXaxis.push(attrValue.strDate);
        arrYaxis.push(attrValue.intTotal)
    }
    if(arrXaxis.length === 1){
        var tempStore = arrXaxis;
        var date = new Date(arrXaxis);
        date.setDate(date.getDate() - 1);
        arrXaxis = [];
        arrXaxis.push(date.getFullYear() + '/' + (date.getMonth() + 1) + '/' + date.getDate());
        arrXaxis.push(tempStore);
        arrYaxis.push(0);
    }
//Data used for weekly chart ends here
    /**
     * Options for Line chart
     */
    function apiCharData() {
        var s = './APILineChartreport';
        var jsonData = $.ajax({
            url: s,
            dataType: "json",
            async: false
        }).responseText;
        if (jsonData.length === 0) {
            jsonData = [{"label": "NA", "value": 0, "value5": 0}];
            return jsonData;
        } else {
            var myJsonObj = JSON.parse(jsonData);
            return myJsonObj;
        }
    }

    var apiData = apiCharData();
    var arrXaxis1 = [];
    var arrYaxis1 = [];
    var temp;
    for (var key in apiData) {
        var attrName1 = key;
        var attrValue1 = apiData[key];
        temp = (attrValue1.label).split(" ");

//                                arrXaxis1.push(temp[1] + " call : " + attrValue1.value);
        arrXaxis1.push(temp[1]);
        arrYaxis1.push(attrValue1.value5);
    }
//    alert(JSON.stringify(arrXaxis1));
    if(arrXaxis1.length <= 1){
        if(arrXaxis1.length === 1){
            var a = arrXaxis1[0];
            var tim = new Date(moment(a, ['h:m a', 'H:m']));
            arrXaxis1.push((tim.getHours() + 1 )+":59");
            arrYaxis1.push(0);
        }else{
            arrXaxis1.push("00.59");
            arrXaxis1.push("1:59");
            arrYaxis1.push(0);
            arrYaxis1.push(0);
        }
   
    }
    var lineDataTime = {
        labels: arrXaxis1,
        datasets: [
            {
                label: "Example dataset",
                fillColor: "rgba(98,203,49,0.5)",
                strokeColor: "rgba(98,203,49,0.7)",
                pointColor: "rgba(98,203,49,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(26,179,148,1)",
                //                    data: [33, 48, 40, 19, 54, 27, 54, 23]
                data: arrYaxis1
            }
        ]
    };
    
// Daily transaction end here

// weekly transaction Starts here
    
    var lineData = {
        labels: arrXaxis,
        datasets: [
            {
                label: "Example dataset",
                fillColor: "rgba(98,203,49,0.5)",
                strokeColor: "rgba(98,203,49,0.7)",
                pointColor: "rgba(98,203,49,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(26,179,148,1)",
                //                    data: [33, 48, 40, 19, 54, 27, 54, 23]
                data: arrYaxis
            }
        ]
    };
// weeekly transaction end here


    var sharplineOptions_status = {
        scaleShowGridLines: true,
        scaleGridLineColor: "rgba(0,0,0,.05)",
        scaleGridLineWidth: 1,
        bezierCurve: false,
        pointDot: true,
        pointDotRadius: 4,
        pointDotStrokeWidth: 1,
        pointHitDetectionRadius: 20,
        datasetStroke: true,
        datasetStrokeWidth: 1,
        datasetFill: true,
        responsive: true
    };

    var ctx = document.getElementById("flot-line-chart").getContext("2d");
    var myNewChart = new Chart(ctx).Line(lineData, sharplineOptions_status);

    var ctx1 = document.getElementById("dailychart").getContext("2d");
    myNewChart = new Chart(ctx1).Line(lineDataTime, sharplineOptions_status);
});

function weeklyAreaChart() {
    var s = './AreaChart';
    var jsonData = $.ajax({
        url: s,
        dataType: "json",
        async: false
    }).responseText;
    if (jsonData.length == 0) {
        var date = new Date();
        date.setDate(date.getDate() - 1);
        var yesterDay = (date.getDate() + '/' + (date.getMonth() + 1) + '/' + date.getFullYear());
        jsonData = [{"strDate": yesterDay, "intTotal": 0}];
        return jsonData;
    } else {
        var myJsonObj = JSON.parse(jsonData);
        return myJsonObj;
    }
}
