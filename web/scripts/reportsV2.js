function pageRefreshPartnerreport(type) {
    window.location.href = "./partnerReports.jsp?_type=" + type + "&_opType=0";
}

var waiting;
function pleasewait() {
    waiting = $('<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="popup" class="modal fade">' +
            '<div class="modal-dialog"><div class="modal-content"><div class="modal-body">' + '<h4>Processing...</h4>' +
            '<div class="progress progress-striped active">' +
            '<div class="progress-bar progress-bar-info" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width: 100%">' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div></div></div></div>');
    //waiting = $('<div class="modal hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-header"><h1>Processing...</h1></div><div class="modal-body"><div class="progress progress-striped active"></div></div></div>');
    waiting.modal();
}
function showAlert(message, type, closeDelay) {
    if ($("#alerts-container").length == 0) {
        $("body")
                .append($('<div id="container" style="' +
                        'width: %; margin-left: 65%; margin-top: 10%;">'));
    }
    type = type || "info";
    var alert = $('<div class="alert alert-' + type + ' fade in">')
            .append(
                    $('<button type="button" class="close" data-dismiss="alert">')
                    .append("&times;")
                    )
            .append(message);
    $("#alerts-container").prepend(alert);
    if (closeDelay)
        window.setTimeout(function () {
            alert.alert("close")
        }, closeDelay);
}

function generatereports(type, opType, partner) {
    var l = $('#generateButton').ladda();
    l.ladda('start');
    var sdate = document.getElementById('datapicker1').value;
    var edate = document.getElementById('datapicker2').value;
    var start_date = new Date(sdate);
    var end_date = new Date(edate);
    initializeToastr();

    var oneDay = 24 * 60 * 60 * 1000;
    var diffDays = Math.round(Math.abs((start_date.getTime() - end_date.getTime()) / (oneDay)));
    if (diffDays >= 31) {
        toastr.error('Error - ' + 'Select date between one month only !');
        l.ladda('stop');
        return;
    }
    if (sdate === '' || sdate === '') {
        toastr.error('Error - ' + 'Please Select the Date ranges !');
        l.ladda('stop');
        return;
    }
    var Accesspoint = document.getElementById('_Accesspoint').value;
    var Group = -1;
    var Resource = -1;
    var stime = "00:00 AM";
    var etime = "23:59 PM";

    var s = './chartreports.jsp?_type=' + type + '&_accesspoint=' + Accesspoint + '&_group=' + Group + '&_resources=' + Resource + '&_partner=' + partner + '&_sdate=' + sdate + '&_edate=' + edate + '&_stime=' + stime + '&_etime=' + etime;
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
//            $('#lineOptions_status').html(data);
            $('#report_data').html(data);
            if (type === 1) {
                $("#bargraph").empty();
                var day_data = UserReportBarChart(opType, Accesspoint, Group, Resource, partner, sdate, edate, stime, etime);
//                alert("data  ::: "+JSON.stringify(day_data));
                var arrXaxis = [];
                var arrYaxis = [];
                for (var key in day_data) {
                    var attrName = key;
                    var attrValue = day_data[key];
                    arrXaxis.push(attrValue.label);
                    arrYaxis.push(attrValue.value);
                }
                var singleBarOptions = {
                    scaleBeginAtZero: true,
                    scaleShowGridLines: true,
                    scaleGridLineColor: "rgba(0,0,0,.05)",
                    scaleGridLineWidth: 1,
                    barShowStroke: true,
                    barStrokeWidth: 1,
                    barValueSpacing: 5,
                    barDatasetSpacing: 1,
                    responsive: true
                };

                /**
                 * Data for Bar chart
                 */
                var singleBarData = {
//            labels: ["January", "February", "March", "April", "May", "June", "July"],
                    labels: arrXaxis,
                    datasets: [
                        {
                            label: "value",
                            fillColor: "rgba(98,203,49,0.5)",
                            strokeColor: "rgba(98,203,49,0.8)",
                            highlightFill: "rgba(98,203,49,0.75)",
                            highlightStroke: "rgba(98,203,49,1)",
//                    data: [10, 20, 30, 40, 30, 50, 60]
                            data: arrYaxis
                        }
                    ]
                };

                var ctx = document.getElementById("bargraph").getContext("2d");
                var myNewChart = new Chart(ctx).Bar(singleBarData, singleBarOptions);

                var linedata = UserReportLineChart(opType, Accesspoint, Group, Resource, partner, sdate, edate, stime, etime);
                var arrYaxis1 = [];
                var arrXaxis1 = [];
                for (var key in linedata) {
                    var attrName1 = key;
                    var attrValue1 = linedata[key];
                    arrXaxis1.push(attrValue1.label);
                    arrYaxis1.push(attrValue1.value);
                }
                if (arrXaxis1.length === 1) {
                    var tempStore = arrXaxis1;
                    var date = new Date(arrXaxis1);
                    date.setDate(date.getDate() - 1);
                    arrXaxis1 = [];
                    arrXaxis1.push(date.getFullYear() + '/' + (date.getMonth() + 1) + '/' + date.getDate());
                    arrXaxis1.push(tempStore);
                    arrYaxis1.push(0);
                }
                $("#linegraph").empty();
                var lineData = {
//            labels: ["11/02/2017", "12/02/2017", "13/02/2017", "14/02/2017", "15/02/2017", "16/02/2017", "17/02/2017", "18/02/2017", "19/02/2017", "20/02/2017", "21/02/2017", "12/02/2017"],
                    labels: arrXaxis1,
                    datasets: [
                        {
                            label: "Example dataset",
                            fillColor: "rgba(98,203,49,0.5)",
                            strokeColor: "rgba(98,203,49,0.7)",
                            pointColor: "rgba(98,203,49,1)",
                            pointStrokeColor: "#fff",
                            pointHighlightFill: "#fff",
                            pointHighlightStroke: "rgba(26,179,148,1)",
//                    data: [33, 48, 40, 19, 54, 27, 54, 23, 43, 10, 22, 17]
                            data: arrYaxis1
                        }
                    ]
                };

                var lineOptions_status = {
                    scaleShowGridLines: true,
                    scaleGridLineColor: "rgba(0,0,0,.05)",
                    scaleGridLineWidth: 1,
                    bezierCurve: true,
                    bezierCurveTension: 0.4,
                    pointDot: true,
                    pointDotRadius: 4,
                    pointDotStrokeWidth: 1,
                    pointHitDetectionRadius: 20,
                    datasetStroke: true,
                    datasetStrokeWidth: 1,
                    datasetFill: true,
                    responsive: true
                };


                var ctx = document.getElementById("linegraph").getContext("2d");
                var myNewChart = new Chart(ctx).Line(lineData, lineOptions_status);
                document.getElementById("report").style.display = "block";
                l.ladda('stop');


//                Morris.Line({
//                    element: 'linegraph',
//                    data: linedata,
//                    xkey: 'label',
//                    ykeys: ['value'],
//                    xLabels: 'day',
//                    labels: ['value']
//                });
            } else if (type === 2) {
                $("#linertgraph").empty();
                var linedata = ResponseReportLineChart(opType, Accesspoint, Group, Resource, partner, sdate, edate, stime, etime);
                var arrYaxisper = [];
                var arrXaxisper = [];
                for (var key in linedata) {
                    var attrName1 = key;
                    var attrValue1 = linedata[key];
                    arrXaxisper.push(attrValue1.label);
                    arrYaxisper.push(attrValue1.value);
                }
                if (arrXaxisper.length === 1) {
                    var tempStore = arrXaxisper;
                    var date = new Date(arrXaxisper);
                    date.setDate(date.getDate() - 1);
                    arrXaxisper = [];
                    arrXaxisper.push(date.getFullYear() + '/' + (date.getMonth() + 1) + '/' + date.getDate());
                    arrXaxisper.push(tempStore);
                    arrYaxisper.push(0);
                }
                /// Performanc line chart

                var lineData = {
                    labels: arrXaxisper,
                    datasets: [
                        {
                            label: "Example dataset",
                            fillColor: "rgba(98,203,49,0.5)",
                            strokeColor: "rgba(98,203,49,0.7)",
                            pointColor: "rgba(98,203,49,1)",
                            pointStrokeColor: "#fff",
                            pointHighlightFill: "#fff",
                            pointHighlightStroke: "rgba(26,179,148,1)",
                            data: arrYaxisper
                        }
                    ]
                };

                var lineOptions_status = {
                    scaleShowGridLines: true,
                    scaleGridLineColor: "rgba(0,0,0,.05)",
                    scaleGridLineWidth: 1,
                    bezierCurve: true,
                    bezierCurveTension: 0.4,
                    pointDot: true,
                    pointDotRadius: 4,
                    pointDotStrokeWidth: 1,
                    pointHitDetectionRadius: 20,
                    datasetStroke: true,
                    datasetStrokeWidth: 1,
                    datasetFill: true,
                    responsive: true
                };


                var ctx = document.getElementById("linertgraph").getContext("2d");
                var myNewChart = new Chart(ctx).Line(lineData, lineOptions_status);
                l.ladda('stop');
            }

        }
    });
}


function generatereports1() {
    var waiting = $('<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="popup" class="modal">' +
            '<div class="modal-dialog"><div class="modal-content"><div class="modal-body"><h2><font style="color:  blue; font-weight: bolder">Please Wait</fon></h2></div></div></div></div>');
    waiting.modal();
    var type = document.getElementById('_type').value;
    var Accesspoint = document.getElementById('_Accesspoint').value;
    var Group = document.getElementById('_Group').value;
    var Resource = document.getElementById('_Resources').value;
    var partner = document.getElementById('_Partner').value;
    var sdate = document.getElementById('_startdate').value;
    var edate = document.getElementById('_enddate').value;
    var stime = document.getElementById('_ApStartTime').value;
    var etime = document.getElementById('_ApEndTime').value;
    var s = './chartreports.jsp?_type=' + type + '&_accesspoint=' + Accesspoint + '&_group=' + Group + '&_resources=' + Resource + '&_partner=' + partner + '&_sdate=' + sdate + '&_edate=' + edate + '&_stime=' + stime + '&_etime=' + etime;
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#report_data').html(data);

            $("#linegraph").empty();
            var linedata = UserReportLineChart(Accesspoint, Group, Resource, partner, sdate, edate, stime, etime);
            Morris.Line({
                element: 'linegraph',
                data: linedata,
                xkey: 'label',
                ykeys: ['value'],
                xLabels: 'day',
                labels: ['value']
            });
            $("#linertgraph").empty();
            var linedata = ResponseReportLineChart(Accesspoint, Group, Resource, partner, sdate, edate, stime, etime);
            Morris.Line({
                element: 'linertgraph',
                data: linedata,
                xkey: 'label',
                ykeys: ['value'],
                xLabels: 'day',
                labels: ['value']
            });
            waiting.modal('hide');
        }
    });
}

function UserReportBarChart(opType, Accesspoint, Group, Resource, partner, sdate, edate, stime, etime) {
    var s = './barchartreport?_opType=' + opType + '&_accesspoint=' + Accesspoint + '&_group=' + Group + '&_resource=' + Resource + '&_partner=' + partner + '&_sdate=' + sdate + '&_edate=' + edate + '&_stime=' + stime + '&_etime=' + etime;
    var jsonData = $.ajax({
        url: s,
        dataType: "json",
        async: false
    }).responseText;
    var myJsonObj = JSON.parse(jsonData);

    return myJsonObj;
}

function UserReportLineChart(opType, Accesspoint, Group, Resource, partner, sdate, edate, stime, etime) {
    var s = './linechartreport?_opType=' + opType + '&_accesspoint=' + Accesspoint + '&_group=' + Group + '&_resource=' + Resource + '&_partner=' + partner + '&_sdate=' + sdate + '&_edate=' + edate + '&_stime=' + stime + '&_etime=' + etime;
    var jsonData = $.ajax({
        url: s,
        dataType: "json",
        async: false
    }).responseText;
    var myJsonObj = JSON.parse(jsonData);
    return myJsonObj;
}

function ResponseReportLineChart(opType, Accesspoint, Group, Resource, partner, sdate, edate, stime, etime) {
    var s = './linechartresponsereport?_opType=' + opType + '&_accesspoint=' + Accesspoint + '&_group=' + Group + '&_resource=' + Resource + '&_partner=' + partner + '&_sdate=' + sdate + '&_edate=' + edate + '&_stime=' + stime + '&_etime=' + etime;
    var jsonData = $.ajax({
        url: s,
        dataType: "json",
        async: false
    }).responseText;
    var myJsonObj = JSON.parse(jsonData);
    return myJsonObj;
}

function userReportPDF(Accesspoint, Group, Resource, partner, sdate, edate, stime, etime) {
//    var val1 = encodeURIComponent(document.getElementById('_auditStartDate').value);
//    var val2 = encodeURIComponent(document.getElementById('_auditEndDate').value);
//    var val4 = encodeURIComponent(document.getElementById('_auditUserID').value);
    var s = './getReports?_apid=' + Accesspoint + "&_grid=" + Group + "&_resid=" + Resource + "&_pid=" + partner + "&_sdate=" + sdate + "&_stime=" + stime + "&_edate=" + edate + "&_etime=" + etime + "&_format=0";
    window.location.href = s;
    return false;
}

function userReportCSV(Accesspoint, Group, Resource, partner, sdate, edate, stime, etime) {
    var s = './getReports?_apid=' + Accesspoint + "&_grid=" + Group + "&_resid=" + Resource + "&_pid=" + partner + "&_sdate=" + sdate + "&_stime=" + stime + "&_edate=" + edate + "&_etime=" + etime + "&_format=1";
    window.location.href = s;
    return false;
}

function userReportTXT(Accesspoint, Group, Resource, partner, sdate, edate, stime, etime) {
    var s = './getReports?_apid=' + Accesspoint + "&_grid=" + Group + "&_resid=" + Resource + "&_pid=" + partner + "&_sdate=" + sdate + "&_stime=" + stime + "&_edate=" + edate + "&_etime=" + etime + "&_format=2";
    window.location.href = s;
    return false;
}
function chart() {
    var waiting = $('<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="popup" class="modal">' +
            '<div class="modal-dialog"><div class="modal-content"><div class="modal-body"><h2><font style="color:  blue; font-weight: bolder">Please Wait</fon></h2></div></div></div></div>');
    waiting.modal();
    var settingId = document.getElementById('_Ressource').value;
    var start = document.getElementById('_startdate').value;
    var end = document.getElementById('_enddate').value;
    $('#tabchart').empty();
    var s = './chart.jsp?_settingId=' + settingId + '&_startdate=' + start + '&_enddate=' + end;
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            waiting.modal('hide');
            $('#tabchart').html(data);
        }
    });
}
function textualreport()
{
    var waiting = $('<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="popup" class="modal">' +
            '<div class="modal-dialog"><div class="modal-content"><div class="modal-body"><h2><font style="color:  blue; font-weight: bolder">Please Wait</fon></h2></div></div></div></div>');
    waiting.modal();
    var settingId = document.getElementById('_Ressource').value;
    var start = document.getElementById('_startdate').value;
    var end = document.getElementById('_enddate').value;
    $('#tabText').empty();
    var s = './settingExecutionTextReport.jsp?_settingId=' + settingId + '&_startdate=' + start + "&_enddate=" + end;
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            waiting.modal('hide');
            $('#tabText').html(data);
        }
    });
}

function realtime()
{
    var settingId = document.getElementById('_Ressource').value;
    var s = './realtime.jsp?_settingId=' + settingId;
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#realtimegraph').html(data);
        }
    });
}

function ReportTXT(name, type, checkfor)
{
    var s = './DownloadReport?name=' + name + "&_reporttype=" + 2 + "&monitorType=" + type + "&ChechFor=" + checkfor;

    window.location.href = s;
    return false;
}
function ReportPDF(name, type, checkfor)
{
    var s = './DownloadReport?name=' + name + "&_reporttype=" + 0 + "&monitorType=" + type + "&ChechFor=" + checkfor;

    window.location.href = s;
    return false;
}

function ReportCSV(name, type, checkfor)
{
    var s = './DownloadReport?name=' + name + "&_reporttype=" + 1 + "&monitorType=" + type + "&ChechFor=" + checkfor;
    window.location.href = s;
    return false;

}

function generatereportsV2(type, opType, partner,event){    
    var l = $('#generateButtonV2').ladda();
    l.ladda('start');
    setTimeout(function(){
            
    var sdate = document.getElementById('datapicker1').value;
    var edate = document.getElementById('datapicker2').value;
    var start_date = new Date(sdate);
    var end_date = new Date(edate);
    initializeToastr();
    var oneDay = 24 * 60 * 60 * 1000;
    var diffDays = Math.round(Math.abs((start_date.getTime() - end_date.getTime()) / (oneDay)));

    if (diffDays >= 32) {
        toastr.error('Error - ' + 'Select date between one month only !');
        l.ladda('stop');
        return;
    }
    if (sdate === '' || sdate === '') {
        toastr.error('Error - ' + 'Please Select the Date ranges !');
        l.ladda('stop');
        return;
    }
    var Accesspoint = document.getElementById('_Accesspoint').value;
    var Group = -1;
    var Resource = -1;
    var stime = "00:00 AM";
    var etime = "23:59 PM";
    var recordFound = "false";
     $("#report_data").empty();
        var day_data = UserReportBarChart(opType, Accesspoint, Group, Resource, partner, sdate, edate, stime, etime);
    //                alert("data  ::: "+JSON.stringify(day_data));
        var arrXaxis = [];
        var arrYaxis = [];
        arrXaxis.push("Call Count");
        arrYaxis.push("Transactions Status");
        for (var key in day_data) {
            var attrName = key;
            var attrValue = day_data[key];
            arrXaxis.push(attrValue.label);
            arrYaxis.push(attrValue.value);
            var positiveValue = attrValue.value;
            if(positiveValue > 0){
                recordFound = "true";
            }
        }
        if(recordFound==='true'){                       
            document.getElementById("report").style.display = "block"; 
            document.getElementById("noRecordFoundData").style.display = "none";
            document.getElementById("report_data").style.display = "block";
            c3.generate({
                bindto: '#report_data',
                data: {
                    x: 'Call Count',
                    //xFormat: '%Y-%m-%d %H:%M:%S',
                    columns: [
                        arrXaxis,
                        arrYaxis                        
                    ],
                    colors: {
                        //data1: '#62cb31',
                        arrYaxis: '#FF7F50'
                    },
                    type: 'bar',
                    groups: [
                        ['Transactions Status']
                    ]
                },
                subchart: {
                    show: true
                },
                axis: {
                    x: {
                        type: 'category',
                        // if true, treat x value as localtime (Default)
                        // if false, convert to UTC internally                        
                        categories: arrXaxis
                    }
                }
            });
            
        }else{
            document.getElementById("noRecordFoundData").style.display = "block";
            document.getElementById("report").style.display = "none";
            document.getElementById("report_data").style.display = "none";
        }    
            document.getElementById("fillBlankSpace").style.display = "none";
            document.getElementById("report_data_button").style.display = "block";
            document.getElementById("homeFirstReportLabel").style.display = "block";
//            document.getElementById("noRecordFoundData").style.display = "none";
            document.getElementById("homeFirstReportLabel").innerHTML = "Transaction Status";
            l.ladda('stop');
            if(event === 1){
                setTimeout(function () {
                    generateReportPart2(type, opType, partner,1);
                    document.getElementById("homeFirstReportLabel").innerHTML = "Transaction Per Day";
                }, 4000);
            }
    },2000);
}

function generateReportPart2(type, opType, partner,event){
    var sdate = document.getElementById('datapicker1').value;
    var edate = document.getElementById('datapicker2').value;
    var start_date = new Date(sdate);
    var end_date = new Date(edate);
    var Accesspoint = document.getElementById('_Accesspoint').value;
    var Group = -1;
    var Resource = -1;
    var stime = "00:00 AM";
    var etime = "23:59 PM";
    var recordFound = "false";
    var linedata = UserReportLineChart(opType, Accesspoint, Group, Resource, partner, sdate, edate, stime, etime);
    var arrYaxis1 = [];
    var arrXaxis1 = [];
    arrXaxis1.push("Data e Ora");
    arrYaxis1.push('Transactrion Count');
    for (var key in linedata) {
        var attrName1 = key;
        var attrValue1 = linedata[key];
        arrXaxis1.push(attrValue1.label);
        arrYaxis1.push(attrValue1.value);
        var positiveValue = attrValue1.value;
        if(positiveValue > 0){
            recordFound = "true";
        }
    }
//    if (arrXaxis1.length === 1) {
//        var tempStore = arrXaxis1;
//        var date = new Date(arrXaxis1);
//        date.setDate(date.getDate() - 1);
//        arrXaxis1 = [];
//        arrXaxis1.push(date.getFullYear() + '/' + (date.getMonth() + 1) + '/' + date.getDate());
//        arrXaxis1.push(tempStore);
//        arrYaxis1.push(0);
//    }
    $("#report_data").empty();
    console.log("Date >> " + arrXaxis1);
    console.log("Call Count >> " + arrYaxis1);
    if(recordFound==='true'){ 
     document.getElementById("noRecordFoundData").style.display = "none";
     document.getElementById("report_data").style.display = "block"; 
        c3.generate({
        bindto: '#report_data',
        data: {
            x: 'Data e Ora',
            xFormat: '%Y-%m-%d %H:%M:%S',
            columns: [
                arrXaxis1,
                arrYaxis1
                        //data1
            ],
            colors: {
                //arrYaxis1: '#62cb31',
                arrYaxis1: '#FF7F50'
            },
            types: {
                arrXaxis1: 'bar'
            },
            groups: [
                ['Call Count']
            ]
        },
        subchart: {
            show: true
        },
        axis: {
            x: {
                type: 'timeseries',
                // if true, treat x value as localtime (Default)
                // if false, convert to UTC internally                        
                tick: {
                    format: '%Y-%m-%d'
                            //fomat:function (x) { return x.getFullYear(); }
                }
            }
        }
    }); 
    }else{
       document.getElementById("noRecordFoundData").style.display = "block"; 
       document.getElementById("report_data").style.display = "none";
    }
    document.getElementById("homeFirstReportLabel").innerHTML = "Transaction Per Day";
    if(event === 1){
        setTimeout(function () {
            generatereportsV2(type, opType, partner, 2);
            document.getElementById("homeFirstReportLabel").innerHTML = "Transaction Per Day";
        }, 4000);
    }
}

function generatePerformanceReport(type, opType, partner){
    var sdate = document.getElementById('datapicker1').value;
    var edate = document.getElementById('datapicker2').value;
    var start_date = new Date(sdate);
    var end_date = new Date(edate);
    var Accesspoint = document.getElementById('_Accesspoint').value;
    var Group = -1;
    var Resource = -1;
    var stime = "00:00 AM";
    var etime = "23:59 PM";
    var linedata = ResponseReportLineChart(opType, Accesspoint, Group, Resource, partner, sdate, edate, stime, etime);
    var arrYaxis1 = [];
    var arrXaxis1 = [];
    arrXaxis1.push("Data e Ora");
    arrYaxis1.push('Response Time');
    for (var key in linedata) {
        var attrName1 = key;
        var attrValue1 = linedata[key];
        arrXaxis1.push(attrValue1.label);
        arrYaxis1.push(attrValue1.value);
    }
    if (arrXaxis1.length === 1) {
        var tempStore = arrXaxis1;
        var date = new Date(arrXaxis1);
        date.setDate(date.getDate() - 1);
        arrXaxis1 = [];
        arrXaxis1.push(date.getFullYear() + '/' + (date.getMonth() + 1) + '/' + date.getDate());
        arrXaxis1.push(tempStore);
        arrYaxis1.push(0);
    }
    $("#report_data").empty();
    console.log("Date >> " + arrXaxis1);
    console.log("Call Count >> " + arrYaxis1);
    c3.generate({
        bindto: '#report_data',
        data: {
            x: 'Data e Ora',
            xFormat: '%Y-%m-%d %H:%M:%S',
            columns: [
                arrXaxis1,
                arrYaxis1
                        //data1
            ],
            colors: {
                //arrYaxis1: '#62cb31',
                arrYaxis1: '#FF7F50'
            },
            types: {
                arrXaxis1: 'bar'
            },
            groups: [
                ['Response Time']
            ]
        },
        subchart: {
            show: true
        },
        axis: {
            x: {
                type: 'timeseries',
                // if true, treat x value as localtime (Default)
                // if false, convert to UTC internally                        
                tick: {
                    format: '%Y-%m-%d %H:%M'
                            //fomat:function (x) { return x.getFullYear(); }
                }
            }
        }
    }); 
    document.getElementById("homeFirstReportLabel").innerHTML = "Response time in Milliseconds";    
}
function PerformanceReportV2LineChart(Accesspoint, Group, Resource, partner, _apiCallDay, _apiCallMonth,_apiCallYear, stime, etime) {
    var s = './ResponseReportV2?_accesspoint=' + Accesspoint + '&_group=' + Group + '&_resource=' + Resource + '&_partner=' + partner + '&_apiCallDay=' + _apiCallDay + '&_apiCallMonth=' + _apiCallMonth + '&_stime=' + stime + '&_etime=' + etime +'&_apiCallYear='+_apiCallYear;
    var jsonData = $.ajax({
        url: s,
        dataType: "json",
        async: false
    }).responseText;
    var myJsonObj = JSON.parse(jsonData);
    return myJsonObj;
}
function generatePerformanceReportV2(partner){    
    var l = $('#generatePerformanceButtonV2').ladda();
    l.ladda('start');
    window.setTimeout(function(){            
    var Accesspoint = document.getElementById('_Accesspoint').value;
    var _apiCallDay = document.getElementById('_apiCallDay').value;
    var _apiCallMonth = document.getElementById('_apiCallMonth').value;
    var _apiCallYear = document.getElementById('_apiCallYear').value;
    var Group = -1;
    var Resource = -1;
    var stime = "00:00 AM";
    var etime = "23:59 PM";
    var recordFound = "false";
    var linedata = PerformanceReportV2LineChart(Accesspoint, Group, Resource, partner, _apiCallDay, _apiCallMonth,_apiCallYear, stime, etime);
    var arrYaxis1 = [];
    var arrXaxis1 = [];
    arrXaxis1.push("Data e Ora");
    arrYaxis1.push('Response Time');
    for (var key in linedata) {
        var attrName1 = key;
        var attrValue1 = linedata[key];
        arrXaxis1.push(attrValue1.label);
        arrYaxis1.push(attrValue1.value);
        var positiveValue = attrValue1.value;
        if(positiveValue > 0){
            recordFound = "true";
        }
    }
    $("#report_data").empty();
    console.log("Date >> " + arrXaxis1);
    console.log("Call Count >> " + arrYaxis1);
    if(recordFound==='true'){ 
        document.getElementById("report").style.display = "block"; 
        document.getElementById("fillBlankSpace").style.display = "none"; 
        document.getElementById("noRecordFoundDataPer").style.display = "none";
        document.getElementById("report_data").style.display = "block"; 
       c3.generate({
        bindto: '#report_data',
        data: {
            x: 'Data e Ora',
            xFormat: '%Y-%m-%d %H:%M:%S',
            columns: [
                arrXaxis1,
                arrYaxis1
                        //data1
            ],
            colors: {
                //arrYaxis1: '#62cb31',
                arrYaxis1: '#FF7F50'
            },
            types: {
                arrXaxis1: 'bar'
            },
            groups: [
                ['Response Time']
            ]
        },
        subchart: {
            show: true
        },
        axis: {
            x: {
                type: 'timeseries',
                // if true, treat x value as localtime (Default)
                // if false, convert to UTC internally                        
                tick: {
                    format: '%H:%M'
                            //fomat:function (x) { return x.getFullYear(); }
                }
            }
        }
    });
        
    }else{
        document.getElementById("noRecordFoundDataPer").style.display = "block";
        document.getElementById("fillBlankSpace").style.display = "none"; 
        document.getElementById("report_data").style.display = "none"; 
    }
    
    l.ladda('stop');
   },2000);    
}


function generatereportsOnButtonClick(type, opType, partner,event){    
    
            
    var sdate = document.getElementById('datapicker1').value;
    var edate = document.getElementById('datapicker2').value;
    var start_date = new Date(sdate);
    var end_date = new Date(edate);
    initializeToastr();
    var oneDay = 24 * 60 * 60 * 1000;
    var diffDays = Math.round(Math.abs((start_date.getTime() - end_date.getTime()) / (oneDay)));
    if (diffDays >= 32) {
        toastr.error('Error - ' + 'Select date between one month only !');
        l.ladda('stop');
        return;
    }
    if (sdate === '' || sdate === '') {
        toastr.error('Error - ' + 'Please Select the Date ranges !');
        l.ladda('stop');
        return;
    }
    var Accesspoint = document.getElementById('_Accesspoint').value;
    var Group = -1;
    var Resource = -1;
    var stime = "00:00 AM";
    var etime = "23:59 PM";
    var recordFound = "false";
     $("#report_data").empty();
        var day_data = UserReportBarChart(opType, Accesspoint, Group, Resource, partner, sdate, edate, stime, etime);
    //                alert("data  ::: "+JSON.stringify(day_data));
        var arrXaxis = [];
        var arrYaxis = [];
        arrXaxis.push("Call Count");
        arrYaxis.push("Transactions Status");
        for (var key in day_data) {
            var attrName = key;
            var attrValue = day_data[key];
            arrXaxis.push(attrValue.label);
            arrYaxis.push(attrValue.value);
            var positiveValue = attrValue.value;
            if(positiveValue > 0){
                recordFound = "true";
            }
        }
        if(recordFound==='true'){                       
            c3.generate({
                bindto: '#report_data',
                data: {
                    x: 'Call Count',
                    //xFormat: '%Y-%m-%d %H:%M:%S',
                    columns: [
                        arrXaxis,
                        arrYaxis                        
                    ],
                    colors: {
                        //data1: '#62cb31',
                        arrYaxis: '#FF7F50'
                    },
                    type: 'bar',
                    groups: [
                        ['Transactions Status']
                    ]
                },
                subchart: {
                    show: true
                },
                axis: {
                    x: {
                        type: 'category',
                        // if true, treat x value as localtime (Default)
                        // if false, convert to UTC internally                        
                        categories: arrXaxis
                    }
                }
            });
            document.getElementById("report").style.display = "block";             
        }else{
            document.getElementById("noRecordFoundData").style.display = "block";            
        }    
        document.getElementById("fillBlankSpace").style.display = "none";
        document.getElementById("report_data_button").style.display = "block";
        document.getElementById("homeFirstReportLabel").style.display = "block";
        document.getElementById("homeFirstReportLabel").innerHTML = "Transaction Status";        
        if (event === 1) {
            setTimeout(function () {
                generateReportPart2(type, opType, partner, 1);
                document.getElementById("homeFirstReportLabel").innerHTML = "Transaction Per Day";
            }, 4000);
        }    
}
