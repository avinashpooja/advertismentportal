function generateHash() {
    var merchantid = document.getElementById("MERCHANTID").value.toUpperCase();
    var merchant_pass = document.getElementById("PSWD").value.toUpperCase();
    var merchant_trId = document.getElementById("MERCHANT_TRANID").value.toUpperCase();
    var amount = document.getElementById("AMOUNT").value.toUpperCase();
    var str = "##" + merchantid + "##" + merchant_pass + "##" + merchant_trId + "##" + amount + "##0##";
    var finalStr = hex_sha1(str).toUpperCase();
    document.getElementById("SIGNATURE").value = finalStr;
    var childTranscation = "<txn>"
                    + "<child_txn>"
                    + "<sub_merchant_id>" + merchantid + "</sub_merchant_id>"
                    + "<sub_order_id>11100111</sub_order_id>"
                    + "<gross_amount>" + amount + "</gross_amount>"
                    + "<gbt_amount>" + amount + "</gbt_amount>"
                    + //"<gbt_amount>"+_totalAmountWithoutTax+"</gbt_amount>" +
                    "<nett_amount>00.00</nett_amount>"
                    + "<account_no>" + merchantid + "</account_no>"
                    + "<revenue_code>" + 772 + "</revenue_code>"
                    + "<misc_amount>00.00</misc_amount>"
                    + "</child_txn>"
                    + "</txn>";
    document.getElementById("TOTAL_CHILD_TRANSACTION").value = 1;
    document.getElementById("CHILD_TRANSACTION").value = childTranscation;
    document.getElementById("apiPayment").disabled =false;
    document.getElementById("RETURN_URL").disabled =false;
}
