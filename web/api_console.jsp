<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.ResourceManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.ResourceDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.WarFileManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.TransformManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.TransformDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.AccessPointManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.Warfiles"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.Accesspoint"%>
<%@include file="header.jsp"%> 
<!-- Main Wrapper -->

<div id="wrapper">
    <div class="small-header transition animated fadeIn">
        <div class="hpanel">
            <div class="panel-body">
                <div id="hbreadcrumb" class="pull-right">
                    <ol class="hbreadcrumb breadcrumb">
                        <li><a href="home.jsp">Dashboard</a></li>
                        <li class="active">
                            <span>API Console</span>
                        </li>
                    </ol>
                </div>
                <h2 class="font-light m-b-xs">
                    API Console
                </h2>
                <small>Get your API to do Sandbox</small>
            </div>
        </div>
    </div>
    <div class="content animate-panel">
        <div class="row">
            <div class="col-lg-12">
                <div class="text-center m-b-xl">
                    <h3>Available APIs</h3>
                </div>
                <hr class="m-b-xl"/>
                <div class="row">
                    <%
                        int accessPointId, resourceId;

                        Accesspoint[] accesspoints1 = null;
                        Warfiles warfiles = null;
                        accesspoints1 = new AccessPointManagement().getAllAccessPoint(SessionId, ChannelId);
                        if (accesspoints1 != null) {
                            for (int i = 0; i < accesspoints1.length; i++) {
                                if (accesspoints1[i].getStatus() != GlobalStatus.DELETED && accesspoints1[i].getStatus() != GlobalStatus.SUSPEND && Integer.parseInt(
                                        parObj.getPartnerGroupId()) == accesspoints1[i].getGroupid()) {
                                    accessPointId = accesspoints1[i].getApId();
                                    resourceId = Integer.parseInt(accesspoints1[i].getResources().split(",")[0]);
                                    Accesspoint apdetails = accesspoints1[i];
                                    TransformDetails transformDetails = new TransformManagement().getTransformDetails(SessionId, ChannelId, apdetails.getApId(), Integer.parseInt(apdetails.getResources().split(",")[0]));
                                    if (transformDetails != null) {
                                        warfiles = new WarFileManagement().getWarFile(SessionId, ChannelId, apdetails.getApId());
                                        if (warfiles != null) {
                                            String acesspointName = apdetails.getName();

                    %>

                    <div class="col-sm-4">
                        <div class="hpanel plan-box">
                            <div class="panel-heading hbuilt text-center">
                                <h4 class="font-bold"><%=acesspointName%></h4>
                            </div>
                            <div class="panel-body text-center">
                                <i class="pe pe-7s-display1 big-icon text-warning"></i>
                                <h4 class="font-bold">
                                    1 Credit/API
                                </h4>


                                <a href="api_console_access.jsp?_apid=<%=accessPointId%>&_resId=<%=resourceId%>&_envt=Test&ver=1" class="btn btn-warning btn-sm">Select <%=acesspointName%> API</a>
                            </div>
                        </div>
                    </div>
                    <%}
                                    }
                                }
                            }
                        }%>
                </div>
            </div>
        </div>
    </div>
    <!-- Footer-->
    <%@include file="footer.jsp"%>
