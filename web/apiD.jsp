<%@page import="org.json.JSONArray"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.UtilityFunctions"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.Channels"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.RemoteAccessUtils"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.ChannelsUtils"%>
<%@page import="org.hibernate.Session"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.SessionFactoryUtil"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.SessionManagement"%>
<%@page import="org.bouncycastle.util.encoders.Base64"%>
<%@page import="org.json.JSONObject"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.SettingsManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.MSConfig"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.MethodName"%>
<%@page import="java.util.List"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.Methods"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.Serializer"%>
<%@page import="java.util.Map"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.ResourceDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.ResourceManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.WarFileManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.TransformManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.TransformDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.GlobalStatus"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.AccessPointManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.Accesspoint"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.Warfiles"%>
<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <title>API Reference</title>
        <link href="stylesheets/screen.css" rel="stylesheet" media="screen" />
        <link href="stylesheets/print.css" rel="stylesheet" media="print" />
        <link href="stylesheets/apicss.css" rel="stylesheet" type="text/css"/>
        <script src="javascripts/all.js"></script>
        <link rel="shortcut icon" type="image/png" href="images/frontImage.png"/>
        <style> 
        a.testWordWrap {
            width: 11em; 
            word-wrap: break-word !important;
        }
        p.testWordWrap {
/*            width: 11em; */
            word-wrap: break-word !important;
        }
        </style>
    </head>

    <body class="index" data-languages="[&quot;shell&quot;,&quot;ruby&quot;,&quot;python&quot;,&quot;javascript&quot;,&quot;csharp&quot;,&quot;cURL&quot;,&quot;goInput&quot;,&quot;objectiveC&quot;,&quot;php&quot;,&quot;swift&quot;]">
        <a href="#" id="nav-button">
            <span>
                NAV        
                <img src="images/navbar.png" />
            </span>
        </a>
        <div class="tocify-wrapper">        
            <img src="images/123.png" width="100%" height="67px" />        
            <div class="lang-selector">
                <a href="#" data-language-name="shell">shell</a>
                <a href="#" data-language-name="ruby">ruby</a>
                <a href="#" data-language-name="python">python</a>
                <a href="#" data-language-name="javascript">Javascript</a>              
                <a href="#" data-language-name="csharp">C #</a>
                <a href="#" data-language-name="cURL">cURL</a>
                <a href="#" data-language-name="goInput">Go</a>
                <a href="#" data-language-name="objectiveC">Objective C</a>
                <a href="#" data-language-name="php">PHP</a>
                <a href="#" data-language-name="swift">Swift</a>
            </div>
            <div class="search">
                <input type="text" class="search" id="input-search" placeholder="Search">
            </div>
            <ul class="search-results"></ul>
            <div id="toc">
            </div>
            <ul class="toc-footer">
                <li><a href='https://services.blue-bricks.com/MPServicesPortal/'>Sign Up for a Developer Key</a></li>
                <li><a href='#'>Powered by Service Guard</a></li>
            </ul>
        </div>
        <div class="page-wrapper">
            <div class="dark-box"></div>
            <div class="content">
                <h1 id="introduction">Introduction</h1>

                <p>Welcome to the Blue Bricks API documentation section! You can get reference for all our Services and their APIs in terms of how they work Blue Bricks API endpoints.</p>

                <p>We have language bindings in 7 languages from Ruby,PHP, Java, JS, Python and many more! You can view code examples in the dark area to the right, and you can switch the programming language of the examples with the tabs in the top right.</p>

                <p>We hope you will find the value for all APIs under our offered services and their APIs. For suggestion and comments, please drop us an email at <a>support@mollatech.com.</a></p> 
                <%
                    String groupId = request.getParameter("g");
                    int iGrupId = 0;
                    if (groupId != null && !groupId.isEmpty()) {
                        if (UtilityFunctions.isValidPhoneNumber(groupId)) {
                            iGrupId = Integer.parseInt(groupId);
                        }
                    }
                    SessionManagement sManagement = new SessionManagement();
                    String _channelName = "ServiceGuard";
                    SessionFactoryUtil suChannel = new SessionFactoryUtil(SessionFactoryUtil.channels);
                    Session sChannel = suChannel.openSession();
                    ChannelsUtils cUtil = new ChannelsUtils(suChannel, sChannel);
                    SessionFactoryUtil suRemoteAcess = new SessionFactoryUtil(SessionFactoryUtil.remoteaccess);
                    Session sRemoteAcess = suRemoteAcess.openSession();
                    RemoteAccessUtils rUtil = new RemoteAccessUtils(suRemoteAcess, sRemoteAcess);
                    Channels channel = cUtil.getChannel(_channelName);
                    String[] credentialInfo = rUtil.GetRemoteAccessCredentials(channel.getChannelid());
                    String SessionId = sManagement.OpenSession(channel.getChannelid(), credentialInfo[0], credentialInfo[1], request.getSession().getId());
                    Object sobject = null;
                    sobject = new SettingsManagement().getSetting(SettingsManagement.MATERSLAVE, SettingsManagement.PREFERENCE_ONE);
                    MSConfig msConfig = (MSConfig) sobject;
                    Warfiles warfiles = null;
                    Accesspoint[] accessObj = null;
                    accessObj = new AccessPointManagement().getAceesspoints();
                    if (iGrupId != 0) {
                        accessObj = new AccessPointManagement().getAccessPointByGroupId(SessionId, channel.getChannelid(), iGrupId);
                    }
                    if (accessObj != null) {
                        for (int i = 0; i < accessObj.length; i++) {
                            if (accessObj[i].getStatus() != GlobalStatus.DELETED && accessObj[i].getStatus() != GlobalStatus.SUSPEND) {
                                Accesspoint apdetails = accessObj[i];
                                TransformDetails transformDetails = new TransformManagement().getTransformDetails(apdetails.getChannelid(), apdetails.getApId(), Integer.parseInt(apdetails.getResources().split(",")[0]));
                                if (transformDetails != null) {
                                    warfiles = new WarFileManagement().getWarFile(apdetails.getChannelid(), apdetails.getApId());
                                    if (warfiles != null) {
                %>
                <h1 id="accessPoint<%=apdetails.getName()%>"><%=apdetails.getName()%></h1>
                <%
                    String[] resources = apdetails.getResources().split(",");
                    JSONObject errJSON = null;
                    ResourceDetails resObj = null;
                    if (resources != null) {
                        for (int j = 0; j < resources.length; j++) {
                            int version = 1;
                            resObj = new ResourceManagement().getResourceById(Integer.parseInt(resources[j]));
                            Map map = (Map) Serializer.deserialize(warfiles.getWfile());
                            version = map.size() / 2;
                            byte[] meBy = transformDetails.getMethods();
                            Object obj = Serializer.deserialize(meBy);
                            Map tmpMethods = (Map) obj;
                            Methods methods = (Methods) tmpMethods.get("" + version);
                            List list = methods.methodClassName.methodNames;
                            String mName = "";

                            String errorCodeDetails = resObj.getErrorCodeDetails();
                            if (errorCodeDetails != null) {
                                errJSON = new JSONObject(errorCodeDetails);
                            }
                %>
<!--                <p>
                    <b>Note: For User Defined Classes, Please Refer Swagger URL : </b><br>
                    <a href="#" target="_blank" id="<%=apdetails.getName() + resObj.getName()%>"></a>
                </p>-->
                <%if (resObj.getVideoLinkDetails() != null) {%>
                <p>
                    <b>Note: For Resource Information, Please Refer URL : </b><br>
                    <a class="testWordWrap" href="<%=resObj.getVideoLinkDetails()%>" target="_blank"><%=resObj.getVideoLinkDetails()%></a>
                </p>
                <%}%>
                <h2 id="resource<%=resObj.getName()%>"><%=resObj.getName()%></h2>

                <%if (resObj.getResImg() != null) {%>
                <img alt="" src="data:image/jpg;base64,<%=resObj.getResImg()%>"  style="height:350px; width:400px; margin-left: 70px">
                <%}%>
                <%if (methods.methodClassName.resourceNote != null && !methods.methodClassName.resourceNote.isEmpty()) {%>
                <aside class="notice">
                    <%=methods.methodClassName.resourceNote%>
                </aside>
                <%
                    }
                    for (int k = 0; k < list.size(); k++) {
                        MethodName methodName = (MethodName) list.get(k);
                        if (methodName.visibility.equalsIgnoreCase("yes")) {
                            ///count++;
                            mName = methodName.methodname.split("::")[0];
                            if (!methodName.transformedname.equals("")) {
                                mName = methodName.transformedname.split("::")[0];
                            }
                            String methodNameStr = mName;
                            //String sampleInputOutput = methodName.apiInOut;
                            JSONObject inputOutputJson = null;
                            if (methodName.apiInOut != null) {
                                String sampleInputOutput = new String(Base64.decode(methodName.apiInOut));
                                inputOutputJson = new JSONObject(sampleInputOutput);
                            }
                            int swaggerId = 0;
                            for (Object key : map.keySet()) {
                                if (key.toString().split(":")[1].equalsIgnoreCase("Running")) {
                                    String ver = key.toString().split(":")[0].replace("SB", "");
                                    String env = "";
                                    //String strEnv = "Production";
                                    org.json.JSONObject object = new JSONObject(transformDetails.getImplClassName());
                                    String implName = object.getString(ver);
                                    if (key.toString().split(":")[0].endsWith("SB")) {
                                        env = "SB";
                                        //strEnv = "Test";
                                    }
                                    swaggerId++;
                                    
                                    if (msConfig != null) {
                                        String ssl = "http";
                                        if (msConfig.ssl.equalsIgnoreCase("yes")) {
                                            ssl = "https";
                                        }
                                        if (!env.isEmpty() && env.equalsIgnoreCase("SB")) {
                                           String swaggerURL = ssl+"://"+msConfig.hostIp+apdetails.getName().replaceAll(" ", "") + "" + "V" + ver+"/"+implName+"/swagger.json";      
                %>
                <%if (ver.equalsIgnoreCase("1")) {%>
                <h3 id="api<%=methodNameStr%>" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=methodNameStr%></h3>
                <%} else {%>
                <h3 id="api<%=methodNameStr%>" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=methodNameStr%> V<%=ver%></h3>
                <%}%>
                <p>
                    <b>Note: For User Defined Classes, Please Refer Swagger URL : </b><br>
                    <a class="testWordWrap" href="<%=ssl%>://<%=msConfig.hostIp%>:<%=msConfig.hostPort%>/<%=apdetails.getName().replaceAll(" ", "") + "" + "V" + ver%>/<%=implName%>/swagger.json" target="_blank" id="<%=apdetails.getName() + resObj.getName() + swaggerId%>"><%=ssl%>://<%=msConfig.hostIp%>:<%=msConfig.hostPort%>/<%=apdetails.getName().replaceAll(" ", "") + "" + "V" + ver%>/<%=implName%>/swagger.json</a>
                </p>
<!--                <script>
                         var link = document.getElementById("<%=apdetails.getName() + resObj.getName() +swaggerId%>");
                         link.setAttribute("href", "<%=ssl%>://<%=msConfig.hostIp%>:<%=msConfig.hostPort%>/<%=apdetails.getName().replaceAll(" ", "") + "" + "V" + ver%>/<%=implName%>/swagger.json");
//                    var anchor = document.getElementsByClassName("quality");
                         link.innerHTML = "<%=ssl%>://<%=msConfig.hostIp%>:<%=msConfig.hostPort%>/<%=apdetails.getName().replaceAll(" ", "") + "" + "V" + ver%>/<%=implName%>/swagger.json";
                </script>-->
                <pre class="highlight ruby">
                    <% if (inputOutputJson != null) {
                            if (inputOutputJson.has("javaInput")) {%>  
<code>
<%=inputOutputJson.getString("javaInput")%>
</code>
<a href="#/" style="font-size: 15px; text-decoration: none" onclick="copy('<%= new String(Base64.encode(inputOutputJson.getString("javaInput").getBytes()))%>')"><b>Copy</b></a>
                    <%}
                    } else {
                    %>
    <code>
        No description found
    </code>
                    <%}%>
                </pre>
                <pre class="highlight python">
                    <% if (inputOutputJson != null) {
                            if (inputOutputJson.has("pythonInput")) {%>  
<code>
<%=inputOutputJson.getString("pythonInput")%>
</code>
<a href="#/" style="font-size: 15px; text-decoration: none" onclick="copy('<%= new String(Base64.encode(inputOutputJson.getString("pythonInput").getBytes()))%>')"><b>Copy</b></a>
                    <%}
                    } else {
                    %>
    <code>
        No description found
    </code>
                    <%}%>
                </pre>
                <pre class="highlight shell">
                    <% if (inputOutputJson != null) {
                            if (inputOutputJson.has("ShellInput")) {
                    %>                
    <code>
<%=inputOutputJson.getString("ShellInput")%>
    </code>
<a href="#/" style="font-size: 15px; text-decoration: none" onclick="copy('<%= new String(Base64.encode(inputOutputJson.getString("ShellInput").getBytes()))%>')"><b>Copy</b></a>
                    <%}
                    } else {
                    %>
    <code>
        No description found
    </code>
                    <%}%>
                </pre>
                <pre class="highlight javascript">
                    <% if (inputOutputJson != null) {
if (inputOutputJson.has("javaScriptInput")) {
                    %>                
    <code>
<%=inputOutputJson.getString("javaScriptInput")%>
    </code>
<a href="#/" style="font-size: 15px; text-decoration: none" onclick="copy('<%= new String(Base64.encode(inputOutputJson.getString("javaScriptInput").getBytes()))%>')"><b>Copy</b></a>
                    <%}
                    } else {
                    %>
    <code>
        No description found
    </code>
                    <%}%>
                </pre>
                <pre class="highlight csharp">
                    <% if (inputOutputJson != null) {
                            if (inputOutputJson.has("csharpInput")) {
                    %>                
    <code>
<%=inputOutputJson.getString("csharpInput")%>
    </code>
<a href="#/" style="font-size: 15px; text-decoration: none" onclick="copy('<%= new String(Base64.encode(inputOutputJson.getString("csharpInput").getBytes()))%>')"><b>Copy</b></a>
                    <%}
                    } else {
                    %>
    <code>
        No description found
    </code>
                    <%}%>
                </pre>
                <pre class="highlight cURL">
                    <% if (inputOutputJson != null) {
                            if (inputOutputJson.has("cURLInput")) {
                    %>                
    <code>
<%=inputOutputJson.getString("cURLInput")%>
    </code>
<a href="#/" style="font-size: 15px; text-decoration: none" onclick="copy('<%= new String(Base64.encode(inputOutputJson.getString("cURLInput").getBytes()))%>')"><b>Copy</b></a>
                    <%}
                    } else {
                    %>
    <code>
        No description found
    </code>
                    <%}%>
                </pre>
                <pre class="highlight goInput">
                    <% if (inputOutputJson != null) {
                            if (inputOutputJson.has("GoInput")) {
                    %>                
    <code>
<%=inputOutputJson.getString("GoInput")%>
    </code>
<a href="#/" style="font-size: 15px; text-decoration: none" onclick="copy('<%= new String(Base64.encode(inputOutputJson.getString("GoInput").getBytes()))%>')"><b>Copy</b></a>
                    <%}
                    } else {
                    %>
    <code>
        No description found
    </code>
                    <%}%>
                </pre>
                <pre class="highlight objectiveC">
                    <% if (inputOutputJson != null) {
                            if (inputOutputJson.has("ObjCInput")) {
                    %>                
    <code>
<%=inputOutputJson.getString("ObjCInput")%>
    </code>
<a href="#/" style="font-size: 15px; text-decoration: none" onclick="copy('<%= new String(Base64.encode(inputOutputJson.getString("ObjCInput").getBytes()))%>')"><b>Copy</b></a>
                    <%}
                    } else {
                    %>
    <code>
        No description found
    </code>
                    <%}%>
                </pre>
                <pre class="highlight php">
                    <% if (inputOutputJson != null) {
                            if (inputOutputJson.has("PHPInput")) {
                    %>                
    <code>
<%=inputOutputJson.getString("PHPInput")%>
    </code>
<a href="#/" style="font-size: 15px; text-decoration: none" onclick="copy('<%= new String(Base64.encode(inputOutputJson.getString("PHPInput").getBytes()))%>')"><b>Copy</b></a>
                    <%}
                    } else {
                    %>
    <code>
        No description found
    </code>
                    <%}%>
                </pre>
                <pre class="highlight swift">
                    <% if (inputOutputJson != null) {
                            if (inputOutputJson.has("swiftInput")) {
                    %>                
    <code>
<%=inputOutputJson.getString("swiftInput")%>
    </code>
<a href="#/" style="font-size: 15px; text-decoration: none" onclick="copy('<%= new String(Base64.encode(inputOutputJson.getString("swiftInput").getBytes()))%>')"><b>Copy</b></a>
                    <%}
                    } else {
                    %>
    <code>
        No description found
    </code>
                    <%}%>
                </pre>
                <blockquote>
                    <p>The above command returns JSON structured like this:</p>
                </blockquote>
                <pre class="highlight json">
                    <% if (inputOutputJson != null) {
                            if (inputOutputJson.has("javaOutput")) {
                    %>                
    <code>
<%=inputOutputJson.getString("javaOutput")%>
    </code>
                    <%}
                    } else {
                    %>
    <code>
        No Output found
    </code>
                    <%}%>
                </pre>

                <p>
                    <%if (methodName.description != null) {%>
                    <%=new String(Base64.decode(methodName.description))%>
                    <%} else {%>
                    This endpoint retrieves all APIs. No further information available
                    <%}%>
                </p>

                <h4 id="http-request"><b><%=ssl%> Request</b></h4>

<!--<p><code class="prettyprint">POST <%=ssl%>://<%=msConfig.hostIp%>:<%=msConfig.hostPort%>/<%=apdetails.getName().replaceAll(" ", "") + env + "V" + ver%>/<%=implName%>/application.wadl%></code></p>-->

                <p class="testWordWrap"><code class="prettyprint">POST <%=ssl%>://<%=msConfig.hostIp%>:<%=msConfig.hostPort%>/<%=apdetails.getName().replaceAll(" ", "") + "V" + ver%>/<%=implName%>/<%=implName + "RS"%>/<%=methodNameStr%></code></p>

                <h4 id="query-parameters"><b>Parameters</b></h4>

                <table><thead>
                        <tr>
                            <th>Parameter</th>
                            <th>Parameter Type</th>
                            <th>Required</th>
                            <th>Description</th>
                        </tr>
                    </thead><tbody>
                        <%
                            if (methodName.paramses != null && !methodName.paramses.isEmpty()) {
                                for (int l = 0; l < methodName.paramses.size(); l++) {
                        %>    
                        <tr>
                            <td><code class="prettyprint"><%=methodName.paramses.get(l).name.split(";")[0]%></code></td>
                            <td><%=methodName.paramses.get(l).type.replace("<", "&lt;").replace(">", "&gt;")%></td>
                            <%if (methodName.paramses.get(l).isMandatory == true) {%>
                            <td><span style="color:green "> Mandatory</span></td>
                            <%} else {%>
                            <td> Optional</td>
                            <%}%>
                            <td><%if (methodName.paramses.get(l).pDescription != null) {%>
                                <%=new String(Base64.decode(methodName.paramses.get(l).pDescription))%>
                                <%} else {%>
                                No information available
                                <%}%>
                            </td>
                        </tr>
                        <%}
                        } else {%>
                        <tr>
                            <td>Not required</td>
                            <td>Not required</td>
                            <td>Not required</td>
                            <td>Not required</td>
                        </tr>
                        <%}%>
                    </tbody></table>
                        <%
                            if(resObj.getResponseInformationDetails() != null && !resObj.getResponseInformationDetails().isEmpty()){ 
                                String jsonString = resObj.getResponseInformationDetails();
                                int titleCount = 0;
                                JSONArray jsOld = new JSONArray(jsonString);                                
                                for (int temp = 0; temp < jsOld.length(); temp++) {
                                    JSONObject jsonexists1 = jsOld.getJSONObject(temp);
                                    if (jsonexists1.has(methodNameStr)) {
                                        String responseKey = jsonexists1.getString(methodNameStr);
                                        JSONObject responseDetails = new JSONObject(responseKey);
                                        Iterator itr = responseDetails.keys();
                                            while (itr.hasNext()) {                                                
                                                Object jsonkey = itr.next();
                                                String keyData = (String) jsonkey;
                                                String value = responseDetails.getString((String) keyData);
                                                JSONObject responseAPIDetails = new JSONObject(value);
                                                titleCount ++;
                                                if(titleCount == 1){
                        %>
                        <h4 id="query-parameters"><b>Response</b></h4>
                        <%}%>     
                        <%if(titleCount == 1){%>
                    <table>
                        
                        <thead>
                            <tr>
                                <th>Field Name</th>
                                <th>Data Type</th>                                
                                <th>Description</th>
                            </tr>
                        </thead>
                       
                        <tbody>
                            <%}%> 
                            <%
                                Iterator itrAPIDetails = responseAPIDetails.keys();
                                    while (itrAPIDetails.hasNext()) {
                                        Object jsonAPIkey = itrAPIDetails.next();
                                        String keyAPIData = (String) jsonAPIkey;
                                        String apivalue = responseAPIDetails.getString((String) keyAPIData);
                                        String[] keyAPIDataArr = keyAPIData.split(";");
                            %>
                            <tr>
                                <td><code class="prettyprint"><%=keyData%>.<%=keyAPIDataArr[1]%></code></td>
                                <td><%=keyAPIDataArr[2]%></td>                                
                                <td><%=apivalue%></td>
                            </tr>
                            <%}%>
                        
                    <%  }
                            }
                                }
%>
</tbody>
                    </table>
                        <%                                   }
                                        }
                    %>

                <%
                                }
                            }
                        }
                    }
                    String apiNote = methodName.apiNote;
                    if (apiNote != null && !apiNote.isEmpty()) {
                %>
                <aside class="success">
                    Note - <%=new String(Base64.decode(apiNote.getBytes()))%>
                </aside>                                
                <%}
                        }
                    }
                    if (errJSON != null && errJSON.length() != 0) {
                %>
                <h4 id="query-parameters"><span style="font-size: small"> Error Codes For <%=resObj.getName()%></span></h4>
                <table><thead>
                        <tr>
                            <th>Error Code</th>
                            <th>Meaning</th>
                        </tr>
                    </thead><tbody>
                        <%
                            Iterator<String> keys = errJSON.keys();
                            while (keys.hasNext()) {
                                String keyData = keys.next();
                                String value = errJSON.getString(keyData);
                        %>
                        <tr>
                            <td><%=keyData%></td>
                            <td><%=value%></td>
                        </tr>
                        <%                        }
                        %>
                    </tbody>
                </table>
                <%
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                %>

                <hr style="width: 625px;hight:539px;">
                <div id="disqus_thread" style="width: 625px;hight:539px;margin-left: 5px"></div>
            </div>
            <div class="dark-box">
                <div class="lang-selector">
                    <a href="#" data-language-name="ruby">Java</a>
                    <a href="#" data-language-name="shell">Shell</a>                
                    <a href="#" data-language-name="python">Python</a>
                    <a href="#" data-language-name="javascript">Js</a>                
                    <a href="#" data-language-name="csharp">C #</a>
                    <a href="#" data-language-name="cURL">cURL</a>
                    <a href="#" data-language-name="goInput">Go</a>
                    <a href="#" data-language-name="objectiveC">Obj C</a>
                    <a href="#" data-language-name="php">PHP</a>
                    <a href="#" data-language-name="swift">Swift</a>
                </div>
            </div>      
        </div>
        <script>
            /**
             *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
             *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/

            var disqus_config = function () {
                this.page.url = "https://sgenrich.mollatech.com:8443/APIDoc/apidocs.jsp";  // Replace PAGE_URL with your page's canonical URL variable
                this.page.identifier = "75395142"; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
            };

            (function () { // DON'T EDIT BELOW THIS LINE
                var d = document, s = d.createElement('script');
                s.src = '//developer-portal-1.disqus.com/embed.js';
                s.setAttribute('data-timestamp', +new Date());
                (d.head || d.body).appendChild(s);
            })();



            function copy(text) {
                document.getElementById("copyTarget").value = Base64.decode(text);
                document.getElementById("copyTarget").style.display = 'block';
                copyToClipboardMsg(document.getElementById("copyTarget"), "msg");
                document.getElementById("copyTarget").style.display = 'none';
            }


            function copyToClipboardMsg(elem, msgElem) {
                var succeed = copyToClipboard(elem);
                var msg;
                if (!succeed) {
                    msg = "Copy not supported or blocked.  Press Ctrl+c to copy."
                } else {
                    msg = "Text copied to the clipboard."
                }
                console.log(msg);
//                if (typeof msgElem === "string") {
//                    msgElem = document.getElementById(msgElem);
//                }
//                msgElem.innerHTML = msg;
//                setTimeout(function () {
//                    msgElem.innerHTML = "";
//                }, 2000);
            }

            function copyToClipboard(elem) {
                // create hidden text element, if it doesn't already exist
                var targetId = "_hiddenCopyText_";
                var isInput = elem.tagName === "INPUT" || elem.tagName === "TEXTAREA";
                var origSelectionStart, origSelectionEnd;
                if (isInput) {
                    // can just use the original source element for the selection and copy
                    target = elem;
                    origSelectionStart = elem.selectionStart;
                    origSelectionEnd = elem.selectionEnd;
                } else {
                    // must use a temporary form element for the selection and copy
                    target = document.getElementById(targetId);
                    if (!target) {
                        var target = document.createElement("textarea");
                        target.style.position = "absolute";
                        target.style.left = "-9999px";
                        target.style.top = "0";
                        target.id = targetId;
                        document.body.appendChild(target);
                    }
                    target.textContent = elem.textContent;
                }
                // select the content
                var currentFocus = document.activeElement;
                target.focus();
                target.setSelectionRange(0, target.value.length);

                // copy the selection
                var succeed;
                try {
                    succeed = document.execCommand("copy");
                } catch (e) {
                    succeed = false;
                }
                // restore original focus
                if (currentFocus && typeof currentFocus.focus === "function") {
                    currentFocus.focus();
                }

                if (isInput) {
                    // restore prior selection
                    elem.setSelectionRange(origSelectionStart, origSelectionEnd);
                } else {
                    // clear temporary content
                    target.textContent = "";
                }
                return succeed;
            }


            var Base64 = {_keyStr: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=", encode: function (e) {
                    var t = "";
                    var n, r, i, s, o, u, a;
                    var f = 0;
                    e = Base64._utf8_encode(e);
                    while (f < e.length) {
                        n = e.charCodeAt(f++);
                        r = e.charCodeAt(f++);
                        i = e.charCodeAt(f++);
                        s = n >> 2;
                        o = (n & 3) << 4 | r >> 4;
                        u = (r & 15) << 2 | i >> 6;
                        a = i & 63;
                        if (isNaN(r)) {
                            u = a = 64
                        } else if (isNaN(i)) {
                            a = 64
                        }
                        t = t + this._keyStr.charAt(s) + this._keyStr.charAt(o) + this._keyStr.charAt(u) + this._keyStr.charAt(a)
                    }
                    return t
                }, decode: function (e) {
                    var t = "";
                    var n, r, i;
                    var s, o, u, a;
                    var f = 0;
                    e = e.replace(/[^A-Za-z0-9+/=]/g, "");
                    while (f < e.length) {
                        s = this._keyStr.indexOf(e.charAt(f++));
                        o = this._keyStr.indexOf(e.charAt(f++));
                        u = this._keyStr.indexOf(e.charAt(f++));
                        a = this._keyStr.indexOf(e.charAt(f++));
                        n = s << 2 | o >> 4;
                        r = (o & 15) << 4 | u >> 2;
                        i = (u & 3) << 6 | a;
                        t = t + String.fromCharCode(n);
                        if (u != 64) {
                            t = t + String.fromCharCode(r)
                        }
                        if (a != 64) {
                            t = t + String.fromCharCode(i)
                        }
                    }
                    t = Base64._utf8_decode(t);
                    return t
                }, _utf8_encode: function (e) {
                    e = e.replace(/rn/g, "n");
                    var t = "";
                    for (var n = 0; n < e.length; n++) {
                        var r = e.charCodeAt(n);
                        if (r < 128) {
                            t += String.fromCharCode(r)
                        } else if (r > 127 && r < 2048) {
                            t += String.fromCharCode(r >> 6 | 192);
                            t += String.fromCharCode(r & 63 | 128)
                        } else {
                            t += String.fromCharCode(r >> 12 | 224);
                            t += String.fromCharCode(r >> 6 & 63 | 128);
                            t += String.fromCharCode(r & 63 | 128)
                        }
                    }
                    return t
                }, _utf8_decode: function (e) {
                    var t = "";
                    var n = 0;
                    var r = c1 = c2 = 0;
                    while (n < e.length) {
                        r = e.charCodeAt(n);
                        if (r < 128) {
                            t += String.fromCharCode(r);
                            n++
                        } else if (r > 191 && r < 224) {
                            c2 = e.charCodeAt(n + 1);
                            t += String.fromCharCode((r & 31) << 6 | c2 & 63);
                            n += 2
                        } else {
                            c2 = e.charCodeAt(n + 1);
                            c3 = e.charCodeAt(n + 2);
                            t += String.fromCharCode((r & 15) << 12 | (c2 & 63) << 6 | c3 & 63);
                            n += 3
                        }
                    }
                    return t
                }}
        </script>
        <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>

    </script>
    <script id="dsq-count-scr" src="//developer-portal-1.disqus.com/count.js" async></script>
    <input type="text" id="copyTarget" style="display: none">
</body>
</html>