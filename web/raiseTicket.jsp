<%@page import="java.util.Calendar"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.TransformManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.TransformDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.ResourceManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.ResourceDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.AccessPointManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.Accesspoint"%>
<%@page import="java.net.URL"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.ProductionAccessEnvtManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgDeveloperProductionEnvt"%>
<%@page import="java.math.BigDecimal"%>
<!DOCTYPE html>
<%@page import="org.json.JSONArray"%>
<%@page import="org.json.JSONObject"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.ProductionAccessManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgProductionAccess"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.EmailManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgEmailticket"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.GlobalStatus"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PaymentManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgPaymentdetails"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PackageSubscriptionManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgSubscriptionDetails"%>
<%@page import="com.mollatech.service.nucleus.crypto.LoadSettings"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.LoanManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgLoanDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgCreditInfo"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.CreditManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.PartnerDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.SessionManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgUsers"%>
<%@page import="java.util.List"%>
<html>
    <%response.addHeader("X-FRAME-OPTIONS", "DENY");%>    
    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <META Http-Equiv="Cache-Control" Content="no-cache">
        <META Http-Equiv="Pragma" Content="no-cache">
        <META Http-Equiv="Expires" Content="0">
        <!-- Page title -->
        <title>Ready APIs</title>

        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
        <link rel="shortcut icon" type="images/front-logo.png" href="images/front-logo.png" />

        <!-- Vendor styles -->
        <link rel="stylesheet" href="vendor/fontawesome/css/font-awesome.css" />
        <link rel="stylesheet" href="vendor/metisMenu/dist/metisMenu.css" />
        <link rel="stylesheet" href="vendor/animate.css/animate.css" />
        <link rel="stylesheet" href="vendor/bootstrap/dist/css/bootstrap.css" />
        <link rel="stylesheet" href="vendor/bootstrap-star-rating/css/star-rating.css" />
        <link rel="stylesheet" href="vendor/datatables.net-bs/css/dataTables.bootstrap.min.css" />
        <link rel="stylesheet" href="vendor/select2-3.5.2/select2.css" />
        <link rel="stylesheet" href="vendor/select2-bootstrap/select2-bootstrap.css" />
        <link rel="stylesheet" href="vendor/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css" />
        <link rel="stylesheet" href="vendor/bootstrap-datepicker-master/dist/css/bootstrap-datepicker3.min.css" />
        <link rel="stylesheet" href="vendor/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" />
        <link rel="stylesheet" href="vendor/clockpicker/dist/bootstrap-clockpicker.min.css" />
        <link rel="stylesheet" href="vendor/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" />
        <link rel="stylesheet" href="vendor/summernote/dist/summernote.css" />
        <link rel="stylesheet" href="vendor/summernote/dist/summernote-bs3.css" />
        <link rel="stylesheet" href="vendor/jquery-ui/themes/base/all.css" />
        <link rel="stylesheet" href="vendor/dropzone/dropzone.css">
        <link rel="stylesheet" href="vendor/sweetalert/lib/sweet-alert.css" />
        <link rel="stylesheet" href="vendor/ladda/dist/ladda-themeless.min.css" />
        <link rel="stylesheet" href="vendor/toastr/build/toastr.min.css" />
        <link rel="stylesheet" href="vendor/codemirror/style/codemirror.css" />
        <link rel="stylesheet" href="vendor/fooTable/css/footable.core.min.css" />
        <!--         <link rel="stylesheet" href="vendor/c3/c3.min.css" />-->
        <!-- App styles -->
        <link rel="stylesheet" href="fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css" />
        <link rel="stylesheet" href="fonts/pe-icon-7-stroke/css/helper.css" />
        <link rel="stylesheet" href="styles/static_custom.css">
        <link rel="stylesheet" href="styles/style.css">
        <link rel="stylesheet" href="vendor/bootstrap-tour/build/css/bootstrap-tour.min.css" />

        <!-- Need call first -->
        <script src="scripts/header.js" type="text/javascript"></script>
        <script src="vendor/jquery/dist/jquery.min.js"></script>
        <script src="vendor/jquery-ui/jquery-ui.min.js"></script>
        <script src="vendor/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="vendor/summernote/dist/summernote.min.js"></script>
        <script src="vendor/codemirror/script/codemirror.js"></script>
        <script src="vendor/codemirror/javascript.js"></script> 
        <link rel="stylesheet" href="vendor/c3/c3.min.css" />
        <script src="vendor/d3/d3.min.js"></script>
        <script src="vendor/c3/c3.min.js"></script>
        <link rel="stylesheet" href="vendor/chartist/custom/chartist.css" />
        <script src="vendor/chartist/dist/chartist.min.js"></script>
        <script src="scripts/homepage.js" type="text/javascript"></script>
        <script src="vendor/bootstrap-tour/build/js/bootstrap-tour.min.js"></script>

        <!--<script src="vendor/jquery-flot/jquery.flot.pie.js" type="text/javascript"></script>-->

        <!-- Vendor scripts -->
        <script src="scripts/utilityFunction.js" type="text/javascript"></script>
        <script src="vendor/slimScroll/jquery.slimscroll.min.js"></script>
        <script src="vendor/metisMenu/dist/metisMenu.min.js"></script>
        <script src="vendor/iCheck/icheck.min.js"></script>
        <script src="vendor/moment/moment.js"></script>
        <script src="vendor/sparkline/index.js"></script>
        <script src="vendor/chartjs/Chart.min.js"></script>
        <script src="vendor/jquery-flot/jquery.flot.js"></script>
        <script src="vendor/jquery-flot/jquery.flot.resize.js"></script>
        <script src="vendor/jquery-flot/jquery.flot.pie.js"></script>
        <script src="vendor/jquery.flot.spline/index.js"></script>
        <script src="vendor/flot.curvedlines/curvedLines.js"></script>
        <script src="vendor/peity/jquery.peity.min.js"></script>
        <script src="vendor/bootstrap-star-rating/js/star-rating.min.js"></script>
        <script src="vendor/codemirror/script/codemirror.js"></script>
        <script src="vendor/codemirror/javascript.js"></script>
        <script src="vendor/xeditable/bootstrap3-editable/js/bootstrap-editable.min.js"></script>
        <script src="vendor/select2-3.5.2/select2.min.js"></script>
        <script src="vendor/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js"></script>
        <script src="vendor/bootstrap-datepicker-master/dist/js/bootstrap-datepicker.min.js"></script>
        <script src="vendor/clockpicker/dist/bootstrap-clockpicker.min.js"></script>
        <script src="vendor/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
        <script src="vendor/dropzone/dropzone.js"></script>

        <script src="vendor/ladda/dist/spin.min.js"></script>
        <script src="vendor/ladda/dist/ladda.min.js"></script>
        <script src="vendor/ladda/dist/ladda.jquery.min.js"></script>
        <script src="vendor/jquery-ui/jquery-ui.min.js"></script>
        <script src="vendor/jquery-validation/jquery.validate.min.js"></script>
        <script src="vendor/sweetalert/lib/sweet-alert.min.js"></script>
        <script src="vendor/toastr/build/toastr.min.js"></script>
        <!-- DataTables -->
        <script src="vendor/datatables/media/js/jquery.dataTables.min.js"></script>
        <script src="vendor/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

        <!-- DataTables buttons scripts -->
        <script src="vendor/pdfmake/build/pdfmake.min.js"></script>
        <script src="vendor/pdfmake/build/vfs_fonts.js"></script>
        <script src="vendor/datatables.net-buttons/js/buttons.html5.min.js"></script>
        <script src="vendor/datatables.net-buttons/js/buttons.print.min.js"></script>
        <script src="vendor/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
        <script src="vendor/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
        <script src="vendor/fooTable/dist/footable.all.min.js"></script>

        <!-- App scripts -->
        <script src="scripts/homer.js"></script>
        <script src="scripts/utilityFunction.js" type="text/javascript"></script>
        <script src="scripts/charts.js"></script>
        <script src="scripts/tourToDashboard.js" type="text/javascript"></script>
    </head>
    <%
        response.setHeader("Cache-Control", "no-cache,must-revalidate"); //Forces caches to obtain a new copy of the page from the origin server
        response.setHeader("Cache-Control", "no-store"); //Directs caches not to store the page under any circumstance
        response.setDateHeader("Expires", 0); //Causes the proxy cache to see the page as "stale"
        response.setHeader("Pragma", "no-cache"); //HTTP 1.0 backward compatibility
        response.addHeader("X-FRAME-OPTIONS", "DENY");
        final int ADMIN = 0;
        final int OPERATOR = 1;
        final int REPORTER = 2;
        SgUsers rUser = null;
        String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
        String ChannelId = (String) request.getSession().getAttribute("_ChannelId");
        PartnerDetails parObj = (PartnerDetails) request.getSession().getAttribute("_partnerDetails");
        String profileImageUrl = (String) request.getSession().getAttribute("profileImageUrl");
        String socialNamed = (String) request.getSession().getAttribute("socialNamed");
        SgUsers usrObj = (SgUsers) request.getSession().getAttribute("_SgUsers");
        String showTourFirstSignUp = (String) request.getSession().getAttribute("showTourFirstSignUp");
        if (SessionId == null) {
            response.sendRedirect("logout.jsp");
            return;
        }
        if (socialNamed == null) {
            socialNamed = parObj.getPartnerName();
        }
        String PartnerType = "ADMIN";
        rUser = (SgUsers) request.getSession().getAttribute("_SgUsers");
        if (rUser.getType() == OPERATOR) {
            PartnerType = "OPERATOR";
        } else if (rUser.getType() == REPORTER) {
            PartnerType = "REPORTER";
        }
        if (rUser.getType() == ADMIN) {
            PartnerType = "ADMIN";
        }
        //String mailBoxFunctionality = LoadSettings.g_sSettings.getProperty("functionalityOf.ticket.email.booleanValue");

        SgDeveloperProductionEnvt productObj = new ProductionAccessEnvtManagement().getProudDetailsByPartnerId(parObj.getPartnerId());
        int notificationCount = 0;
        String appurl = (request.getRequestURL().toString());
        URL myAppUrl = new URL(appurl);
        int port = myAppUrl.getPort();
        if (myAppUrl.getProtocol().equals("https") && port == -1) {
            port = 443;
        } else if (myAppUrl.getProtocol().equals("http") && port == -1) {
            port = 80;
        }
        String apiDocsURL = myAppUrl.getProtocol() + "://" + myAppUrl.getHost() + ":" + port + "/APIDoc" + "/";
        
        
        
        // find the Service Count
        int serviceCount = 0;
        Accesspoint Res1[] = null;
        String base64ProfilePic = null;
        JSONObject usrJSONObj = null;
        if (usrObj != null) {
                String usrProfileDesc = usrObj.getProfileDetails();
                if (usrProfileDesc != null) {
                    usrJSONObj = new JSONObject(usrProfileDesc);
                    if (usrJSONObj.has("profilePic")) {
                        base64ProfilePic = (String) usrJSONObj.get("profilePic");
                    }
                }
            }
        Res1 = new AccessPointManagement().getAllAccessPoint(SessionId, ChannelId);
        if (Res1 != null) {
            for (int i = 0; i < Res1.length; i++) {
                if (Res1[i].getStatus() == GlobalStatus.ACTIVE) {
                    Accesspoint apdetails = Res1[i];
                    if (apdetails != null) {
                        int resID = -1;
                        String resourceList = apdetails.getResources();
                        String[] resids = resourceList.split(",");
                        for (int j = 0; j < resids.length; j++) {
                            if (!resids[j].isEmpty() && resids[j] != null) {
                                resID = Integer.parseInt(resids[j]);
                                ResourceDetails rsName = new ResourceManagement().getResourceById(SessionId, ChannelId, resID);
                                TransformDetails tmdetail = new TransformManagement().getTransformDetails(SessionId, ChannelId, apdetails.getApId(), resID);
                                if (rsName != null && tmdetail != null) {
                                    int g = Integer.parseInt(parObj.getPartnerGroupId());
                                    if (g == apdetails.getGroupid()) {
                                        serviceCount++;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    %>

    <body id="Ashish" class="fixed-navbar">
        <style>
            span.capitalize {
                text-transform: capitalize;
            }
        </style>
        <script language="JavaScript" type="text/javascript">
            function changeclass(id) {
                var NAME = document.getElementById(id);
                NAME.className = "active";
            }
        </script>
        <!-- Simple splash screen-->
        <div class="splash"> <div class="color-line"></div><div class="splash-title"><h1>Loading...</h1><div class="spinner"> <div class="rect1"></div> <div class="rect2"></div> <div class="rect3"></div> <div class="rect4"></div> <div class="rect5"></div> </div> </div> </div>
        <!--[if lt IE 7]>
        <p class="alert alert-danger">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- Header -->
        <div id="header">
            <div class="color-line">
            </div>
            <div id="logo" class="light-version">
                <span>
                    Ready APIs
                </span>
            </div>
            <nav role="navigation">
                <div id="menu-button" class="header-link hide-menu"><i class="fa fa-bars"></i></div>
                <div class="small-logo">
                    <span class="text-primary">Ready APIs</span>
                </div>
                <div class="mobile-menu">
                    <button type="button" class="navbar-toggle mobile-menu-toggle" data-toggle="collapse" data-target="#mobile-collapse">
                        <i class="fa fa-chevron-down"></i>
                    </button>
                    <div class="collapse mobile-navbar" id="mobile-collapse">
                        <ul class="nav navbar-nav">
                            <li>
                                <a onclick="changePasswordMobile()"> <i class="pe pe-7s-key text-info"></i>  Change Password</a>
                            </li>
                            <li>
                                <a onclick="helpDeskMobile()"><i class="pe pe-7s-mail text-warning"></i> Raise Ticket</a>
                            </li>
                            <li>
                                <a href="<%=apiDocsURL%>apidocs.jsp?g=<%=parObj.getPartnerGroupId()%>" target="_blank"><i class="pe pe-7s-help2 text-danger"></i> API User Guide</a>
                            </li>
                            <li>
                                <a onclick="subscriptionMobile()"><i class="pe pe-7s-shopbag text-info"></i> Subscription</a>
                            </li>
                            <li>
                                <a onclick="myInvoiceMobile()"><i class="pe pe-7s-cash text-warning"></i> Invoice</a>
                            </li>
                            <li>
                                <a onclick="firstTimeTour()"><i class="pe pe-7s-info text-danger"></i>Quick Tour </a>
                            </li>
                            <li>
                                <a onclick="downloadsMobile()"><i class="pe pe-7s-download text-danger"></i>Downloads </a>
                            </li>
                            <li>
                                <a href="logout.jsp">
                                    <i class="pe-7s-upload" title="Logout"> Logout</i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="navbar-right">
                    <ul class="nav navbar-nav no-borders">
                        <li class="dropdown">
                            <a class="dropdown-toggle" href="#" data-toggle="dropdown">
                                <i class="pe-7s-keypad" title="My Menu"></i>
                            </a>

                            <div class="dropdown-menu hdropdown bigmenu animated flipInX">
                                <table>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <a onclick="changePassword()">
                                                    <i class="pe pe-7s-key text-info"></i>
                                                    <h5>Change Password</h5>
                                                </a>
                                            </td>
                                            <!--                                            <td>
                                                                                            <a href="my_profile.jsp">
                                                                                                <i class="pe pe-7s-users text-success"></i>
                                                                                                <h5>My Profile</h5>
                                                                                            </a>
                                                                                        </td>-->
                                            <td>
                                                <a onclick="helpDesk()">
                                                    <i class="pe pe-7s-mail text-warning"></i>
                                                    <h5>Raise Ticket</h5>
                                                </a>
                                            </td>
                                            <td>
                                                <a href="<%=apiDocsURL%>apidocs.jsp?g=<%=parObj.getPartnerGroupId()%>" target="_blank">
                                                    <i class="pe pe-7s-help2 text-danger"></i>
                                                    <h5>API User Guide</h5>
                                                </a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <a onclick="subscription()">
                                                    <i class="pe pe-7s-shopbag text-info"></i>
                                                    <h5>Subscription</h5>
                                                </a>
                                            </td> 
                                            <td>
                                                <a onclick="myInvoice()">
                                                    <i class="pe pe-7s-cash text-warning"></i>
                                                    <h5>Invoice</h5>
                                                </a>
                                            </td>
                                            <td>
                                                <a onclick="firstTimeTour()">
                                                    <i class="pe pe-7s-info text-danger"></i>
                                                    <h5>Quick Tour</h5>
                                                </a>
                                            </td> 

                                        </tr>
                                        <tr style="display: none">
                                            <td>
                                                <!-- Jack Remark: Temporary hide it as no need from now -->
                                                <a href="access_point_policy.jsp">
                                                    <i class="pe pe-7s-speaker text-warning"></i>
                                                    <h5>Access Point Policy</h5>
                                                </a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </li>
                        <li class="dropdown">
                            <a class="dropdown-toggle" href="#" data-toggle="dropdown">
                                <i class="pe-7s-download" title="My Menu"></i>
                            </a>

                            <div class="dropdown-menu hdropdown bigmenu animated flipInX">
                                <table>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <a onclick="inAppPushDownload()">
                                                    <i class="pe pe-7s-comment text-info"></i>
                                                    <h5>In App Push</h5>
                                                </a>
                                            </td>
                                            <td>
                                                <a onclick="downloads()">
                                                    <i class="pe pe-7s-network text-info"></i>
                                                    <h5>Server Monitoring</h5>
                                                </a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </li>

                        <%
                            SgPaymentdetails pDetailsObj = null;
                            double mainCredit = 0;
                            SgCreditInfo creditObj1 = null;

                            SgSubscriptionDetails subscriObject1 = new PackageSubscriptionManagement().getPartnerSubscriptionbyPId(parObj.getPartnerId());
                            if (subscriObject1 != null) {
                                PartnerDetails partnerObj = (PartnerDetails) request.getSession().getAttribute("_partnerDetails");
                                pDetailsObj = new PaymentManagement().getPaymentDetailsbyPartnerAndSubscriptionID(subscriObject1.getBucketId(), partnerObj.getPartnerId());
                                creditObj1 = new CreditManagement().getDetails(parObj.getPartnerId());
                                if (subscriObject1 != null && subscriObject1.getPaymentMode().equalsIgnoreCase("prepaid")) {
                                    String lowBalance1 = LoadSettings.g_sSettings.getProperty("prepaid.low.balance");
                                    float lBalance1 = 0.0f;
                                    int subscripaidId = subscriObject1.getBucketId();
                                    if (lowBalance1 != null) {
                                        lBalance1 = Float.parseFloat(lowBalance1);
                                    }
                                    if (creditObj1 != null) {
                                        if (creditObj1.getMainCredit() < lBalance1 && creditObj1.getStatus() != GlobalStatus.UPDATED) {
                                            String hefeature = subscriObject1.getFeatureList();
                                            JSONObject hefeatJSONObj = null;
                                            String heloanLimit = "0";
                                            String heinterestRate = "0";
                                            if (hefeature != null) {
                                                JSONArray alertJson = new JSONArray(hefeature);
                                                for (int i = 0; i < alertJson.length(); i++) {
                                                    JSONObject jsonexists1 = alertJson.getJSONObject(i);
                                                    if (jsonexists1.has(subscriObject1.getBucketName())) {
                                                        hefeatJSONObj = jsonexists1.getJSONObject(subscriObject1.getBucketName());
                                                        if (hefeatJSONObj != null) {
                                                            heloanLimit = hefeatJSONObj.getString("loanLimit");
                                                            heinterestRate = hefeatJSONObj.getString("interestRate");
                                                            break;
                                                        }
                                                    }
                                                }
                                            }
                                            if (Float.parseFloat(heloanLimit) > 0 && Float.parseFloat(heinterestRate) > 0) {
                                                SgLoanDetails loandetailObj1 = new LoanManagement().getLoanDetails(SessionId, ChannelId, parObj.getPartnerId(), subscriObject1.getBucketId());
                                                if ((loandetailObj1 != null && loandetailObj1.getStatus() == GlobalStatus.PAID) || loandetailObj1 == null) {
                        %> 

                        <!--                                <li>
                                                            <a href="getLoan.jsp">
                                                                Your balance <%=creditObj1.getMainCredit()%>, is very low. Get Loan
                                                            </a>
                                                        </li>-->
                        <%
                                    }
                                }
                            }
                            if (subscriObject1.getStatus() == GlobalStatus.PAID) {%>
                        <!--                                <li>
                                                            <a onclick="myInvoice()">
                                                                Get Invoice
                                                            </a>
                                                        </li>-->
                        <%  }
                            }
                        } else if (subscriObject1 != null && subscriObject1.getPaymentMode().equalsIgnoreCase("postpaid")) {
                            SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss a");
                            String todayDate = dateFormat.format(new Date());
                            String expiryDate = dateFormat.format(subscriObject1.getExpiryDateNTime());
                            Date curDate = dateFormat.parse(todayDate);
                            Date exDate = dateFormat.parse(expiryDate);
                            if (curDate.after(exDate) && pDetailsObj == null && subscriObject1.getStatus() == GlobalStatus.UNPAID) {
                                notificationCount++;
                        %>         
                        <!--                                <li>
                                                            <a>
                                                                Your package is expired
                                                            </a>
                                                        </li>-->
                        <script>
                            document.getElementById("notificationSpan").innerHTML = '<%=notificationCount%>';
                        </script>
                        <%
                                    }
                                }
                            }
                            int productionAccessRejectioncount = 0;
                            SgProductionAccess[] productionAccessdetails = new ProductionAccessManagement().getAllRejectedDetailsByPartnerId(SessionId, ChannelId, parObj.getPartnerId(), GlobalStatus.REJECTED);
                            if (productionAccessdetails != null) {
                                productionAccessRejectioncount = productionAccessdetails.length;
                            }
                            if (productionAccessRejectioncount >= 1) {
                        %>    
                        <!--                                <li>
                                                            <a href="productionAccessRejected.jsp">
                                                                Production Access
                                                            </a>
                                                        </li>-->
                        <%
                            }
                        %>
                        <!--                            </ul>
                                                </li>-->
                        <li class="dropdown">
                            <a href="logout.jsp">
                                <i class="pe-7s-upload pe-rotate-90" title="Logout"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>

        <!-- Navigation -->
        <aside id="menu">
            <div id="navigation">
                <div class="profile-picture">
                    <a href="./header.jsp">
                        <%if (profileImageUrl != null) {%>
                        <img src="<%=profileImageUrl%>" class="img-circle m-b" alt="profile">
                        <%} else if (base64ProfilePic != null) {%>                        
                        <img src="data:image/jpg;base64,<%=base64ProfilePic%>" class="img-circle m-b" alt="profile" width="90"/>                         
                        <%} else {%>
                        <img src="images/images.png" class="img-circle m-b" alt="profile" width="90"/>
                        <%}%>
                    </a>
                    <div class="stats-label text-color">
                        <span class="font-extra-bold font-uppercase"><%=socialNamed%></span>
                    </div>
                    <div class="tour-1">
                        <div id="sparkline1" class="small-chart m-t-sm"></div>
                    </div>
                    <hr/>
                    <div class="tour-2">
                        <h4 class="font-extra-bold m-b-xs">
                            <%
                                String expiry = "NA";
                                Calendar calendar = Calendar.getInstance();
                                Calendar expireDate = Calendar.getInstance();
                                if (creditObj1 != null) {
                                    mainCredit = creditObj1.getMainCredit();
                                    SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss a");
                                    expiry = dateFormatter.format(subscriObject1.getExpiryDateNTime());
                                    calendar.setTime(subscriObject1.getExpiryDateNTime());
                                    calendar.add(Calendar.DATE, -1);
                                    expireDate.setTime(subscriObject1.getExpiryDateNTime());
                                }
                            %>
                            <font id="mainCreditOfDev"> <%=new BigDecimal(mainCredit).doubleValue()%></font>       
                        </h4>
                        <small class="text-muted">Remaining Credits</small></br>
                        <%if (new Date().getTime() >= expireDate.getTime().getTime() && !expiry.equalsIgnoreCase("NA")) {%>
                        <small class="text-muted"><font color="red">Expired On <%=expiry%></font></small>
                            <%} else if (new Date().getTime() >= calendar.getTime().getTime() && !expiry.equalsIgnoreCase("NA")) {%>
                        <small class="text-muted"><font color="red">Expires On <%=expiry%></font></small>
                            <%} else if (!expiry.equalsIgnoreCase("NA")) {%>
                        <small class="text-muted">Expires On <%=expiry%></small>
                        <%}%>
                    </div>
                </div>

                <ul class="nav" id="side-menu">
                    <!--                    <li class="active">
                                            <a href="home.jsp"> <span class="capitalize">Dashboard</span> </a>
                                        </li>                    -->

                    <li class="tour-3">
                        <!--<a href="apiConsole.jsp"><span class="capitalize">Services <span class="label label-success pull-right"><%=serviceCount%></span> </a>-->
                        <a  onclick="apiServices()"><span class="capitalize">Services </span> </a>
                    </li>
                    <li class="tour-4">
                        <a onclick='reports()'><span class="capitalize">Reports </span> </a>
                    </li>
                    <li class="tour-5">
                        <a onclick='myInvoice()'> <span class="capitalize">Invoice</span></a>
                    </li>
                    <li class="tour-6">
                        <%if (parObj.getStripeSubscriptionId() != null) {%>
                        <a onclick='subscribe()'><span class="capitalize">Subscription</span></a>
                        <%} else {%>
                        <a onclick='subscribeNew()'><span class="capitalize">Subscription</span></a>
                        <%}%>
                    </li>
                    <li class="tour-7">
                        <a href="<%=apiDocsURL%>apidocs.jsp?g=<%=parObj.getPartnerGroupId()%>" target="_blank"> <span class="capitalize">API Docs</span></a>
                    </li>
                </ul>
            </div>
        </aside>
        <div id="wrapper3"  style="display: none">
            <div id="wrapper"><br><br><br><br><br><br><br><br><br><br><br>
                <div id="loading" ><div class="text-center"><h3>Loading...</3></div><div class="spinner"> <div class="rect1"></div> <div class="rect2"></div> <div class="rect3"></div> <div class="rect4"></div> <div class="rect5"></div> </div> </div> 
            </div>
        </div>

        <div id="wrapper2">
            <div id="wrapper">
                <div id="otherPage"></div>
                <%@include file="footer.jsp"%>
            </div>
        </div>
        <!--        <div id="wrapper2">
                    <div id="wrapper">
                        <div id="apiconsole"></div>
                    </div>
                </div>-->
        <!--        <div id="wrapper4">
                    <div id="wrapper">    
                        <div id="myInvoiceReport"></div>
                    </div>
                </div>
                <div id="wrapper3">
                    <div id="wrapper">
                        <div id="testApi"></div>
                    </div>
                </div>
                <div id="wrapper5">
                    <div id="wrapper">
                        <div id="reports"></div>
                    </div>
                </div>
                <div id="wrapper6">
                    <div id="wrapper">
                        <div id="subscribePackage"></div>
                    </div>
                </div>
                <div id="wrapper7">
                    <div id="helpdesk"></div>
                </div>
                <div id="wrapper8">
                    <div id="changePassWord"></div>
                </div>                 -->
        <div id="wrapper1">
            <div id="wrapper">
                <div id="homePage">
                    <div class="content animate-panel">
                        <div class="row">
                            <div class="col-lg-3">
                                <div class="hpanel stats">
                                    <div class="panel-heading">

                                        Today's total credit used
                                    </div>
                                    <div class="panel-body h-200">
                                        <div class="stats-icon text-center ">
                                            <i class="pe-7s-share fa-4x"></i>
                                        </div>
                                        <div class="m-t-xl">
                                            <div class="progress m-t-xs full progress-small">
                                                <div id="percentage" aria-valuemax="100" aria-valuemin="0" aria-valuenow="0" role="progressbar" class=" progress-bar progress-bar-info">
                                                    <span class="sr-only">100%</span>
                                                </div>
                                            </div> 
                                            <div class="row" style="height: 60px">
                                                <div class="col-xs-6">
                                                    <small class="stats-label">Credit used</small>
                                                    <h4  id='creditUsed'></h4>
                                                </div>
                                                <div class="col-xs-6">
                                                    <small class="stats-label">% Credit used</small>
                                                    <h4 class="text-center" id='perCreditUsed'></h4>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="hpanel">
                                    <div class="panel-heading">
                                        Last week total credit used
                                    </div>
                                    <div class="panel-body text-center h-200">
                                        <i class="pe-7s-graph1 fa-4x"></i>

                                        <h1 class="m-b-xs text-info" id="lastWeekAmount"></h1>
                                        <small id="weekMsg"></small>
                                        <h3 class="font-bold no-margins lead text-info row">
                                            Weekly expense
                                        </h3> <!--<small>Lorem ipsum dolor sit amet, consectetur adipiscing elit</small>-->
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-3">
                                <div class="hpanel">
                                    <div class="panel-heading">

                                        Last month total credit used
                                    </div>
                                    <div class="panel-body text-center h-200">
                                        <i class="pe-7s-global fa-4x"></i>
                                        <h1 class="m-b-xs text-info" id="monthCredit"></h1>

                                        <small id="monthMsg"></small>
                                        <h3 class="font-bold no-margins lead text-info row">
                                            Monthly usage
                                        </h3>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3" id="topmostServices" data-child="hpanel" data-effect="fadeInLeft">
                                <div class="hpanel">
                                    <div class="panel-heading">

                                        <span id="mostlyFive"></span>
                                    </div>
                                    <div class="panel-body" style="height: 213px">
                                        <div class="flot-chart text-center" style="width: 200;height:250">
                                            <div class="flot-chart-content"  id="flot-pie-chart"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3" id = "mostlyservices"  style="display: none">
                                <div class="hpanel">
                                    <div class="panel-heading">

                                        Your mostly used services
                                    </div>
                                    <div class="panel-body" style="text-align: center;">                                        
                                        <img src="images/no_record_found.png" alt="No record found" width="200" height="170"/>
                                        <br>                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12" data-child="hpanel" data-effect="fadeInLeft">
                                <div class="hpanel">
                                    <div class="panel-heading">
                                        Services Information And Statistics
                                    </div>
                                    <div class="panel-body">
                                        <div class="text-left">
                                            <i class="fa fa-bar-chart-o fa-fw"></i><span id="homeReportLable">  Last 7 day expenditure</span>
                                        </div>
                                        <div class="btn-group" style="float: right;">
                                            <button  class="btn btn-default btn-xs ladda-button" data-style="zoom-in" onclick="generateTopServiceV2()">Top Five</button>
                                            <button  class="btn btn-default btn-xs ladda-button" data-style="zoom-in" onclick="creditperSer()">Today's Expense</button>
                                            <button  class="btn btn-default btn-xs ladda-button" data-style="zoom-in" onclick="toptrndingServices()">Today's Trending</button>
                                        </div>
                                        <div style="clear: both"></div>
                                        <div  id="topFiveSer" style="height: 200px" class="c3"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>                
                <%@include file="footer.jsp"%>
            </div>
        </div>
        <div id="tourDiv">
            <!-- Tour API Console -->        
            <div id="apiTour">     
                <div id="wrapper">
                    <div id="wrapperAPIConsoleTour" ></div>
                </div>
            </div>
            <!-- API Console End -->

            <!-- Tour API Console Window -->        
            <div id="apiConsoleTour">     
                <div id="wrapper">
                    <div id="wrapperAPIConsoleWindowTour" ></div>
                </div>
            </div>
            <!-- API Console Window End -->

            <!-- Tour report Window -->        
            <div id="reportTour">     
                <div id="wrapper">
                    <div id="reportWindowTour" ></div>
                </div>
            </div>
            <!-- Report Window End -->

            <!-- Tour invoice Window -->        
            <div id="invoiceTour">     
                <div id="wrapper">
                    <div id="invoiceWindowTour" ></div>
                </div>
            </div>
            <!-- Invoice Window End -->

            <!-- Tour subscribe Window -->        
            <div id="subscribeTour">     
                <div id="wrapper">
                    <div id="subscribeWindowTour" ></div>
                </div>
            </div>
            <!-- Subscribe Window End -->

            <!-- Tour Helpdesk Window -->        
            <div id="helpDeskTour">                 
                <div id="wrapper">
                    <div id="helpDeskWindowTour" ></div>  
                </div>
            </div>


        </div>
        <!-- HelpDesk Window End -->    
        <!--        <style>
                    .tour-step-background{background: none!important;}
                </style>     -->

        <script type="text/javascript">

            document.onreadystatechange = function () {
                if (document.readyState === 'complete') {

                    $('#perCreditUsed').ready(function () {
                        todayusedCredit("today");
                    });
                }
                
            };
            function apiHelpDesk() {
                $('#otherPage').empty();
                $('#wrapper1').hide();
                document.getElementById("wrapper3").style.display = "block";
                var s = "compose_email_technical.jsp";
                $.ajax({
                    type: 'GET',
                    url: s,
                    success: function (data) {
                        $('#otherPage').html(data);

                    }, complete: function () {
                        document.getElementById("wrapper3").style.display = "none";
                        $('#wrapper2').show();
                    }
                });
            }
            $('#wrapper2').hide();
            $('#wrapper3').hide();
            $('#wrapper1').hide();
            apiHelpDesk();
        </script>


        <!--</div>-->
        <!--<div id="apiconsole"></div>-->
        <script>/*<![CDATA[*/window.zEmbed || function (e, t) {
                var n, o, d, i, s, a = [], r = document.createElement("iframe");
                window.zEmbed = function () {
                    a.push(arguments)
                }, window.zE = window.zE || window.zEmbed, r.src = "javascript:false", r.title = "", r.role = "presentation", (r.frameElement || r).style.cssText = "display: none", d = document.getElementsByTagName("script"), d = d[d.length - 1], d.parentNode.insertBefore(r, d), i = r.contentWindow, s = i.document;
                try {
                    o = s
                } catch (e) {
                    n = document.domain, r.src = 'javascript:var d=document.open();d.domain="' + n + '";void(0);', o = s
                }
                o.open()._l = function () {
                    var e = this.createElement("script");
                    n && (this.domain = n), e.id = "js-iframe-async", e.src = "https://assets.zendesk.com/embeddable_framework/main.js", this.t = +new Date, this.zendeskHost = "mollatechhelp.zendesk.com", this.zEQueue = a, this.body.appendChild(e)
                }, o.write('<body onload="document._l();">'), o.close()
            }();
            /*]]>*/</script>
        <script>
            //    document.onreadystatechange = function(){
            //     if(document.readyState === 'complete'){
            //         $('#homepage').ready(function() {
            //       sethomepage();
            //        });
            //    }};
            $(document).ready(function () {
                var creditUsedData = fifteenDaysCreditExpenditure();
                var obj = [];
                for (var key in creditUsedData) {
                    var value = creditUsedData[key];
                    obj.push(value.txAmount);
                    //        alert(JSON.stringify(key)+" :: "+JSON.stringify(value.apiName));
                }
                $("#sparkline1").sparkline(obj, {
                    type: 'bar',
                    barWidth: 7,
                    height: '30px',
                    barColor: '#2381c4',
                    negBarColor: '#53ac2a'
                });

            });
            //    
            function fifteenDaysCreditExpenditure() {
                var s = './DailyTransactionAmount';
                var jsonData = $.ajax({
                    url: s,
                    dataType: "json",
                    async: false
                }).responseText;
                if (jsonData.length === 0) {
                    jsonData = [{"label": "NA", "value": 0, "value5": 0}];
                    return jsonData;
                } else {
                    var myJsonObj = JSON.parse(jsonData);
                    return myJsonObj;
                }
            }

            function getCreditStatus() {
                var s = './GetCredit';
                $.ajax({
                    type: 'POST',
                    url: s,
                    datatype: 'json',
                    data: $("#getBody").serialize(),
                    success: function (data) {
                        if (data.credits !== 'NA') {
                            $('#mainCreditOfDev').html(data.credits);
                        }
                    }});
            }

//            $('.mobile-menu-toggle').click();
            getCreditStatus();
            setTimeout(function () {
                if (typeof (tour) != "undefined") {
                    tour.end();
                }
                startTour();
            }, 500);
            setTimeout(function () {
                generateTopServiceV2();
            }, 2000);
            test = document.getElementById("Ashish");
            collapse = document.getElementById("mobile-collapse");
        </script>
        <%if (showTourFirstSignUp != null && showTourFirstSignUp.equalsIgnoreCase("yes")) {%>
        <script>
            $(document).ready(function () {
                setTimeout(function () {
                    firstTimeTour();
                    tour.restart();
                }, 1000);
            });
        </script>
        
        <%  //new chnage for first time sign up only.    
            request.getSession().setAttribute("showTourFirstSignUp", null);
            }
        
        %>
    </body>
</html>