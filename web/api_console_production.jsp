<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.ResourceManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.ResourceDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.WarFileManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.TransformManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.TransformDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.AccessPointManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.Warfiles"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.Accesspoint"%>
<%@include file="header.jsp"%> 
<!-- Main Wrapper -->
<%    
    Map apiMap = new HashMap();
    int accessPointId, resourceId;
    Accesspoint[] accesspoints1 = null;
    Warfiles warfiles = null;
    if (true) {
        accesspoints1 = new AccessPointManagement().getAllAccessPoint(SessionId, ChannelId);
        if (accesspoints1 != null) {
            for (int i = 0; i < accesspoints1.length; i++) {
                if (accesspoints1[i].getStatus() != GlobalStatus.DELETED && accesspoints1[i].getStatus() != GlobalStatus.SUSPEND && Integer.parseInt(
                        parObj.getPartnerGroupId()) == accesspoints1[i].getGroupid()) {
                    Accesspoint apdetails = accesspoints1[i];
                    TransformDetails transformDetails = new TransformManagement().getTransformDetails(SessionId, ChannelId, apdetails.getApId(), Integer.parseInt(apdetails.getResources().split(",")[0]));
                    if (transformDetails != null) {
                        warfiles = new WarFileManagement().getWarFile(SessionId, ChannelId, apdetails.getApId());
                        if (warfiles != null) {
                            String acesspointName = apdetails.getName();
                            accessPointId = apdetails.getApId();
                            resourceId = Integer.parseInt(apdetails.getResources().split(",")[0]);
                            apiMap.put(acesspointName, accessPointId + ":" + resourceId + ":" + apdetails.getDescription());
                        }
                    }
                }
            }
        }
    }
%>
<div id="wrapper">
    <div class="small-header transition animated fadeIn">
        <div class="hpanel">
            <div class="panel-body">
                <div id="hbreadcrumb" class="pull-right">
                    <ol class="hbreadcrumb breadcrumb">
                        <li><a href="home.jsp">Dashboard</a></li>
                        <li class="active">
                            <span>API Console</span>
                        </li>
                    </ol>
                </div>                
                <h2 class="font-light m-b-xs">
                    API Console
                </h2>
                <small>Play with API</small>
            </div>
        </div>
    </div>

    <div class="content animate-panel" data-effect="rotateInDownRight" data-child="element">
        <div class="row">
            <div class="col-lg-12">                                
                <%
                    int boxcount = 0;
                    if(!apiMap.isEmpty()){
                    for (Object key : apiMap.keySet()) {
                        String val = (String) apiMap.get(key);
                        if (boxcount == 4) {
                            boxcount = 1;
                        } else {
                            boxcount++;
                        }
                        if (boxcount == 1) { 
                %>
                <div class="col-md-3 element">
                    <div class="hpanel plan-box">
                        <div class="panel-body h-200">
                            <div class="text-center">
                                <h6 class="font-bold m-b-xs element"><%=key.toString()%></h6>
                                <p class="element">Service</p>
                                <div class="m element">
                                    <i class="pe-7s-science fa-5x text-success"></i>
                                </div>
                                <p class="small element">
                                    <%if (val.split(":")[2] != null && !val.split(":")[2].equalsIgnoreCase("null")) {%>
                                    <%=val.split(":")[2]%>
                                    <%} else {%>
                                    Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                                    <%}%>
                                </p>
                                <a href="api_console_access_pro.jsp?_apid=<%=val.split(":")[0]%>&_resId=<%=val.split(":")[1]%>" class="btn btn-success btn-sm element">Go To APIs</a>
                            </div>
                        </div>
                    </div>
                </div>
                <%} else if (boxcount == 2) {%>
                <div class="col-md-3 element">
                    <div class="hpanel plan-box">
                        <div class="panel-body h-200">
                            <div class="text-center">
                                <h6 class=" font-bold m-b-xs element"><%=key.toString()%></h6>
                                <p class="element">Service</p>
                                <div class="m element">
                                    <i class="pe-7s-global fa-5x text-info"></i>
                                </div>
                                <p class="small element">
                                    <%if (val.split(":")[2] != null && !val.split(":")[2].equalsIgnoreCase("null")) {%>
                                    <%=val.split(":")[2]%>
                                    <%} else {%>
                                    Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                                    <%}%>
                                </p>
                                <a href="api_console_access_pro.jsp?_apid=<%=val.split(":")[0]%>&_resId=<%=val.split(":")[1]%>" class="btn btn-info btn-sm element">Go To APIs</a>
                            </div>
                        </div>
                    </div>
                </div>
                <%} else if (boxcount % 3 == 0) {%>
                <div class="col-md-3 element">
                    <div class="hpanel plan-box">
                        <div class="panel-body  h-200">
                            <div class="text-center">
                                <h6 class="font-bold m-b-xs element"><%=key.toString()%></h6>
                                <p class="element">Service</p>
                                <div class="m element">
                                    <i class="pe-7s-display1 fa-5x text-warning"></i>
                                </div>
                                <p class="small element">
                                    <%if (val.split(":")[2] != null && !val.split(":")[2].equalsIgnoreCase("null")) {%>
                                    <%=val.split(":")[2]%>
                                    <%} else {%>
                                    Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                                    <%}%>
                                </p>
                                <a href="api_console_access_pro.jsp?_apid=<%=val.split(":")[0]%>&_resId=<%=val.split(":")[1]%>" class="btn btn-warning btn-sm element">Go To APIs</a>
                            </div>
                        </div>
                    </div>
                </div>
                <%} else if (boxcount % 4 == 0) {%>
                <div class="col-md-3 element">
                    <div class="hpanel plan-box">
                        <div class="panel-body  h-200">
                            <div class="text-center">
                                <h6 class="font-bold m-b-xs element"><%=key.toString()%></h6>
                                <p class="element">Service</p>
                                <div class="m">
                                    <i class="pe-7s-airplay fa-5x element text-danger"></i>
                                </div>
                                <p class="small element">
                                    <%if (val.split(":")[2] != null && !val.split(":")[2].equalsIgnoreCase("null")) {%>
                                    <%=val.split(":")[2]%>
                                    <%} else {%>
                                    Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                                    <%}%>
                                </p>
                                <a href="api_console_access_pro.jsp?_apid=<%=val.split(":")[0]%>&_resId=<%=val.split(":")[1]%>" class="btn btn-danger btn-sm element">Go To APIs</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                </div>               
                <%      }
                    }
                }else{
                %>
                <div class="row">
                    <div class="col-md-12">
                        <div class="hpanel element">
                            <div class="panel-body element">
                                <i class="pe-7s-science text-success big-icon element"></i>
                                <h1></h1>
                                <strong class="element">Service not found</strong>
                                <p class="element">
                                    Sorry, did not find any service.
                                </p>
                                <a href="home.jsp" class="btn btn-xs btn-success element">Go back to home</a>
                            </div>
                        </div>
                    </div>
                </div>
                <%}%>
            </div>
        </div>
    </div>
    <!-- Footer-->
    <%@include file="footer.jsp"%>
