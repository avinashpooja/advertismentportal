<%@page import="com.mollatech.serviceguard.nucleus.commons.MSConfig"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.SettingsManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.PartnerDetails"%>
<script src="scripts/downloadFiles.js" type="text/javascript"></script>
<%@page import="java.net.URL"%>
<%
    
    PartnerDetails parObj = (PartnerDetails) request.getSession().getAttribute("_partnerDetails");
        
        Object sobject = null;
        sobject = new SettingsManagement().getSetting(SettingsManagement.MATERSLAVE, SettingsManagement.PREFERENCE_ONE);
        MSConfig msConfig = (MSConfig) sobject;
        String ssl = "http";
        if (msConfig.ssl.equalsIgnoreCase("yes")) {
            ssl = "https";
        }
        
        String apiDocsURL = ssl + "://" + msConfig.aphostIp + ":" + msConfig.aphostPort + "/" + "APIDoc/";
        
%>
<style>
    .copied::after {
        position: absolute;
        top: 12%;
        right: 110%;
        display: block;
        content: "copied";
        font-size: 0.75em;
        padding: 2px 3px;
        color: #FFF;
        background-color: #2381c4;
        border-radius: 3px;
        opacity: 0;
        will-change: opacity, transform;
        animation: showcopied 1.5s ease;
    }

    @keyframes showcopied {
        0% {
            opacity: 0;
            transform: translateX(100%);
        }
        70% {
            opacity: 1;
            transform: translateX(0);
        }
        100% {
            opacity: 0;
        }
    }
</style>
<div class="small-header transition animated fadeIn" id="invoiceHeader">
    <div class="hpanel">
        <div class="panel-body">
            <h2 class="font-light m-b-xs">
                Downloads
            </h2>
            <small>Get supportive file for the services</small>
        </div>
    </div>
</div>
<div class="content animate-panel">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="hpanel">               
                <div class="panel-body">
                    <h3><i class="pe-7s-comment text-info fa-1x"></i>&nbsp;&nbsp; Mobile Trust </h3>
                    <hr>
                    <p><h5>Need to do following two steps before continue Mobile Trust service on any of the below platform. There are specific requirements for each platform which will run Mobile Trust service. Click on specific platform to view the corresponding requirements. </h5></p>
                    <ul>                                    
                        <li> <h5>Step 1] Call the API "openSession" of Mobile Trust service which returns a session id. Keep that session id noted for future reference. This will be use in other API calls.</h5></li>
                        <li> <h5>Step 2] Call the API "CreateApplication" of Mobile Trust service which returns a application id. Keep that application id noted for future reference. This will be use in other API calls.</h5></li>
                        <li> <h5>Step 3] Call the API "InitiallizeSettings" of Mobile Trust service which initialize the OTP and SOTP setting. OTP and SOTP send and verify with the setting that you have been initialize.</h5></li>
                        <li> <h5>Step 4] Call the API "CreateUser" of Mobile Trust service which create the user for your SDK, Keep user details noted for future reference that you have pass for CreateUser API call.</h5></li>
                        <li> <h5>Step 5] Call the API "Assign" of Mobile Trust service which assign the token for your user</h5></li>
                        <li> <h5>Step 6] Call the API "GetLicenseKeys" this API return the licence key for your SDK.</h5></li>
                        <li> <h5><i class="fa fa-info-circle text-info"></i> For more information about the APIs follow the <a href="<%=apiDocsURL%>apidocs.jsp?g=<%=parObj.getPartnerGroupId()%>" target="_blank"> API Documentation</a></h5></li>
                    </ul>
                    <br/>
                    <ul class="nav nav-tabs">                        
                        <li class="active"><a data-toggle="tab" href="#tab-3"><i class="fa fa-apple fa-2x"></i> </a></li>
                        <li class=""><a data-toggle="tab" href="#tab-4"><i class="fa fa-android fa-2x"></i> </a></li>
                    </ul>
                    <div class="tab-content">                                                                                                 
                        <div id="tab-3" class="tab-pane active">
                            <div class="panel-body">
                                 <table>
                                    <tr>
                                        <div class="col-lg-8">
                                        <td>
                                            <h3><strong>Code and Implementation for iOS </strong></h3>
                                        </td>
                                        </div>
                                        <div class="col-lg-2">
                                        <td>
                                            <a class="btn btn-info btn-sm" onclick="downloadiOSmobileTrust()" role="button" style="margin-left: 260px">Download</a>
                                        </td>
                                        </div>
                                    </tr>
                                </table>                               
                                <br/>
                                <div class="text-muted font-bold m-b-xs">
                                    <div class="text-muted font-bold m-b-xs">
                                        <ul>                                    
                                            <li><h5>Step 1] Click on download to get iOS H-files and M-files that you need to integrate in your application </h5></li>                                                                                            
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <li>
                                                                <h5>Step 2] Here is sample program program code</h5>
                                                            </li>
                                                        </td>
                                                        <td>
                                                            <font class="col-lg-2" id="iOSSampleProgramCopyDiv"></font>
                                                        </td>
                                                    </tr>
                                                </table>                                            
                                            <br/>
                                                
                                                <textarea class="form-control" name="iOSSamplePrgram" id="iOSSamplePrgram" style="min-height: 270px;">
//
//  ViewController.m
//  mTrustApp
//
//  Created by Ready APIs on 04/04/14.
//  Copyright (c) 2017 Ready APIs. All rights reserved.
//

#import "ViewController.h"
#import "MobilityTrustKickStartImpl.h"
#import "Axiom.h"
#import "MobilityTrustVaultImpl.h"
#import "Device.h"
@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    _tap.enabled = YES;
    return YES;
}

-(void)hideKeyboard
{
    [_emailid resignFirstResponder];
    _tap.enabled = NO;
}

- (IBAction)intialize:(id)sender {
    @try{
        // You can write your connector which is call you Mobile trust service and place that connector URL here or you can directly call SOAP/REST services.
        NSString *serverURL = @"http://192.168.0.116:8080/MobileTrustAxiom/Mobiletrust";
        
        NSString *licensekey=@"Place Your Licence Key here";
        
    MobilityTrustKickStart *m = [[MobilityTrustKickStart alloc]init];
        // You have to set application like below.
        [m SetApplicationID:@"0f55cf6f"];     
    //take regstration code if necessary    
    NSString *regCode = self.regCode.text;
    NSString *userid = self.emailid.text;

        MobilityTrustVaultImpl *mv = [[MobilityTrustVaultImpl alloc]init];
        NSDate *now = [NSDate date];
        [mv setdateForotp:now];
        Axiom *axiom = [[Axiom alloc]init];
    //Call below method to get device profile
        Device *dp = [axiom getDeviceProfile];
        NSString *deviceid = dp.adId;
      //Set geolocations if you are using plus license else set hardcoded lattitue and longitude from your location
     [mv SetGPSCoordinates:@"73.8567" usingWith:@"18.5203"];
       if(userid == NULL || [userid length]== 0){
            self.result.text = @"Enter Userid First!!";
            self.functionNumber.text = 0;
        }        
       //used to check if user already registered on the device if value is 1 it is already registered else register this user
        int status = [m CheckStatus:userid];
          if(status == -5) {
              self.result.text = @" Signature Verified   :    -5";
            self.functionNumber.text = @"11";
          }else if(status != 1){
              self.functionNumber.text = @"1";
          }                                
     if(userid == NULL || [userid length]== 0){
        self.result.text = @"Enter Userid Address !!";
        self.functionNumber.text = 0;
    }
    // Initialize & Activate SDK API Call

     if([self.functionNumber.text isEqualToString:@"1"]){

        //initialize api call gives a payload which is used for further activation of token at server side
         NSString *strInitData = [m initialize:licensekey usingWith:userid usingWith:regCode];
         
         Axiom *axiom = [[Axiom alloc]init];
         NSMutableArray* NameValue = [NSMutableArray arrayWithCapacity:1];
         for(int i=0; i<1; i++) {
             [NameValue addObject:[NSMutableArray arrayWithCapacity:1]];
         }
         
         [[NameValue objectAtIndex:0] insertObject:@"_data" atIndex:0];
         [[NameValue objectAtIndex:0] insertObject:strInitData atIndex:1];
         
         NSMutableArray* Headers = [NSMutableArray arrayWithCapacity:5];
         for(int i=0; i<3; i++) {
             [Headers addObject:[NSMutableArray arrayWithCapacity:1]];
         }
         [[Headers objectAtIndex:0] insertObject:@"_type" atIndex:0];
         [[Headers objectAtIndex:0] insertObject:@"initialize" atIndex:1];
         [[Headers objectAtIndex:1] insertObject:@"_userid" atIndex:0];
         [[Headers objectAtIndex:1] insertObject:userid atIndex:1];
         [[Headers objectAtIndex:2] insertObject:@"regcode" atIndex:0];
         [[Headers objectAtIndex:2] insertObject:regCode atIndex:1];
         
         [[Headers objectAtIndex:3] insertObject:@"devid" atIndex:0];
         [[Headers objectAtIndex:3] insertObject:@"sdev" atIndex:1];
         
// Need to add your application id here
         [[Headers objectAtIndex:4] insertObject:@"appid" atIndex:0];
         [[Headers objectAtIndex:4] insertObject:@"PlaceYourApplicationIdHere" atIndex:1];
         
         NSMutableDictionary *responseFromServer = [axiom SendData:serverURL usingWith:Headers usingWith:NameValue usingwith:false];
         if (responseFromServer != NULL) {
            NSString *message = [responseFromServer objectForKey:@"message"];
             self.result.text = message;                      
             NSLog(@"Main Activity    %@", message);                               
             int iResult = [m confirm:licensekey usingWith:userid usingWith:message];
             if (iResult == 0) {
                 NSLog(@" ConFirm   :    %d",iResult);
                 NSString *strResult = [NSString stringWithFormat:@"%d", iResult];
                 self.result.text = [@" ConFirm   :    "stringByAppendingString:strResult];                                  
             }
         }                  
     }
    // Verify OTP API call
    else if([self.functionNumber.text isEqualToString:@"2"]){
         NSString *stamp = NULL;
                      
             Axiom *axiom = [[Axiom alloc]init];
             NSMutableArray* NameValue = [NSMutableArray arrayWithCapacity:1];
             for(int i=0; i<1; i++) {
                 [NameValue addObject:[NSMutableArray arrayWithCapacity:1]];
             }
             
             [[NameValue objectAtIndex:0] insertObject:@"_data" atIndex:0];
             [[NameValue objectAtIndex:0] insertObject:@"" atIndex:1];
             
             NSMutableArray* Headers = [NSMutableArray arrayWithCapacity:2];
             for(int i=0; i<2; i++) {
                 [Headers addObject:[NSMutableArray arrayWithCapacity:1]];
             }
             [[Headers objectAtIndex:0] insertObject:@"_type" atIndex:0];
             [[Headers objectAtIndex:0] insertObject:@"gettimestamp" atIndex:1];
             [[Headers objectAtIndex:1] insertObject:@"_userid" atIndex:0];
             [[Headers objectAtIndex:1] insertObject:userid atIndex:1];             
         
             NSMutableDictionary *responseFromServer = [axiom SendData:serverURL usingWith:Headers usingWith:NameValue usingwith:false];
             
             if (responseFromServer != NULL) {
                 NSString *message = [responseFromServer objectForKey:@"message"];                 
                 stamp = message;                 
                 NSLog(@"Main Activity    %@", message);                 
                 if (message != NULL) {
                     NSLog(@" TimeStamp   :    %@",message);                   
                 }
         }
         //OTP
                  
         MobilityTrustVaultImpl *mvObj = [[MobilityTrustVaultImpl alloc]init];
         int pass = 0;
         @try{
        [mvObj SetApplicationIDForLoad: @"0f55cf6f"];
         [mvObj load:licensekey usingWith:userid];
         }@catch(NSException *ex){
                  self.result.text = [@" Load Result   :    "stringByAppendingString:ex.reason];
             pass =-1;
         } if(pass == 0){
         
         AxiomOTPResponse *otp = [mvObj GetOTPPlus:NULL];
          NameValue = [NSMutableArray arrayWithCapacity:1];
         for(int i=0; i<1; i++) {
             [NameValue addObject:[NSMutableArray arrayWithCapacity:1]];
         }
         
         [[NameValue objectAtIndex:0] insertObject:@"_data" atIndex:0];
         [[NameValue objectAtIndex:0] insertObject:otp.otpplus atIndex:1];
         
          Headers = [NSMutableArray arrayWithCapacity:3];
         for(int i=0; i<3; i++) {
             [Headers addObject:[NSMutableArray arrayWithCapacity:1]];
         }
         [[Headers objectAtIndex:0] insertObject:@"_type" atIndex:0];
         [[Headers objectAtIndex:0] insertObject:@"verifyotpplus" atIndex:1];
         [[Headers objectAtIndex:1] insertObject:@"_userid" atIndex:0];
         [[Headers objectAtIndex:1] insertObject:userid atIndex:1];
         [[Headers objectAtIndex:2] insertObject:@"_otp" atIndex:0];
         [[Headers objectAtIndex:2] insertObject:otp.otp atIndex:1];
         
         responseFromServer = [axiom SendData:serverURL usingWith:Headers usingWith:NameValue usingwith:false];         
         if (responseFromServer != NULL) {
             NSString *message = [responseFromServer objectForKey:@"message"];                          
             if (message != NULL) {
                 NSLog(@" OTP Verified   :    %@",message);
                    self.result.text = [@" OTP Verified   :    "stringByAppendingString:message];                                  
             }             
         }         
         
         NSDate *d = [[NSDate alloc]init];
         int timestamp = [d timeIntervalSince1970];
         NSLog(@"Main Activity   OTP:: %@  @  %d",otp.otp , timestamp );         
         [mvObj unLoad:userid];                  
         }         
     }
     // Verify SOTP (Signature OTP) API call
    else if([self.functionNumber.text isEqualToString:@"3"]){
         NSString *stamp = NULL;
                  
         Axiom *axiom = [[Axiom alloc]init];
         NSMutableArray* NameValue = [NSMutableArray arrayWithCapacity:1];
         for(int i=0; i<1; i++) {
             [NameValue addObject:[NSMutableArray arrayWithCapacity:1]];
         }
         
         [[NameValue objectAtIndex:0] insertObject:@"_data" atIndex:0];
         [[NameValue objectAtIndex:0] insertObject:@"" atIndex:1];
         
         NSMutableArray* Headers = [NSMutableArray arrayWithCapacity:2];
         for(int i=0; i<2; i++) {
             [Headers addObject:[NSMutableArray arrayWithCapacity:1]];
         }
         [[Headers objectAtIndex:0] insertObject:@"_type" atIndex:0];
         [[Headers objectAtIndex:0] insertObject:@"gettimestamp" atIndex:1];
         [[Headers objectAtIndex:1] insertObject:@"_userid" atIndex:0];
         [[Headers objectAtIndex:1] insertObject:userid atIndex:1];
                  
         NSMutableDictionary *responseFromServer = [axiom SendData:serverURL usingWith:Headers usingWith:NameValue usingwith:false];         
         if (responseFromServer != NULL) {
             NSString *message = [responseFromServer objectForKey:@"message"];             
             stamp = message;             
             NSLog(@"Main Activity    %@", message);             
             if (message != NULL) {
                 NSLog(@" TimeStamp   :    %@",message);
             }
         }
         //SOTP                           
         MobilityTrustVaultImpl *mvObj = [[MobilityTrustVaultImpl alloc]init];
         
         [mvObj load:licensekey usingWith:userid];
         axiom = [[Axiom alloc]init];
         NSString *dataVerification = @"hello,wrold,vikram";
         NSMutableArray *data = [NSMutableArray arrayWithCapacity:3];
         data[0] = @"hello";
         data[1] = @"wrold";
         data[2] = @"vikram";
         
         AxiomOTPResponse *otp = [mvObj GetSOTPPlus:nil usingWith:data];
         
          NameValue = [NSMutableArray arrayWithCapacity:1];
         for(int i=0; i<1; i++) {
             [NameValue addObject:[NSMutableArray arrayWithCapacity:1]];
         }
         
         [[NameValue objectAtIndex:0] insertObject:@"_data" atIndex:0];
         [[NameValue objectAtIndex:0] insertObject:otp.otpplus atIndex:1];
         
          Headers = [NSMutableArray arrayWithCapacity:4];
         for(int i=0; i<4; i++) {
             [Headers addObject:[NSMutableArray arrayWithCapacity:1]];
         }
         [[Headers objectAtIndex:0] insertObject:@"_type" atIndex:0];
         [[Headers objectAtIndex:0] insertObject:@"verifysotpplus" atIndex:1];
         [[Headers objectAtIndex:1] insertObject:@"_userid" atIndex:0];
         [[Headers objectAtIndex:1] insertObject:userid atIndex:1];
         [[Headers objectAtIndex:2] insertObject:@"_otp" atIndex:0];
         [[Headers objectAtIndex:2] insertObject:otp.otp atIndex:1];
         [[Headers objectAtIndex:3] insertObject:@"_dataVerification" atIndex:0];
         [[Headers objectAtIndex:3] insertObject:dataVerification atIndex:1];
         
        responseFromServer = [axiom SendData:serverURL usingWith:Headers usingWith:NameValue usingwith:false];
         
         if (responseFromServer != NULL) {
             NSString *message = [responseFromServer objectForKey:@"message"];                          
             if (message != NULL) {
                 NSLog(@" OTP Verified   :    %@",message);
                  self.result.text = [@" SOTP Verified   :    "stringByAppendingString:message];                 
             }             
         }                  
         NSDate *d = [[NSDate alloc]init];
         int timestamp = [d timeIntervalSince1970];
         NSLog(@"Main Activity   SOTP:: %@  @  %d",otp.otp , timestamp );         
         [mvObj unLoad:userid];                                    
     }
    // Get logs inside SDK for your users session you can use it in debug mode. 
    else if([self.functionNumber.text isEqualToString:@"10"]){
         MobilityTrustVaultImpl *mvObj = [[MobilityTrustVaultImpl alloc]init];         
         NSString * pin = password;
         NSString *result =   [m getLogs];         
         if (result != NULL) {
            NSLog(@"Main Activity   CHECK LOGS %@", result );                                    
         }else{
            NSLog(@"Main Activity   FAILED TO CHECK LOGS %@", @"GETLOGS" );             
            self.result.text = @"FAILED TO CHECK LOGS";
         }         
     }                 
}@catch (NSException *e) {
      self.result.text = [@" Error   :    "stringByAppendingString:e.reason];
      @throw;
}
}
@end
                                            </textarea>
                                            <br/>
                                            <script>
    var doc = document.getElementById('iOSSamplePrgram');
    var dataCode = CodeMirror.fromTextArea(doc, {
        lineNumbers: true,
        matchBrackets: true,
        styleActiveLine: true,
        tabMode: "indent",
    });
                                            </script>
                                            
                                        </ul>
                                    </div>      
                                </div>
                            </div>
                        </div>
                        
                        <div id="tab-4" class="tab-pane">
                            <div class="panel-body">
                                 <table>
                                    <tr>
                                        <td>
                                            <h3><strong>Code and Implementation for Android </strong></h3>
                                        </td>
                                        <td>
                                            <a class="btn btn-info btn-sm" onclick="downloadAndroidmobileTrust()" role="button" style="margin-left: 210px">Download</a>
                                        </td>
                                    </tr>
                                </table>                               
                                <br/>
                                <div class="text-muted font-bold m-b-xs">
                                    <div class="text-muted font-bold m-b-xs">
                                            <ul>                                    
                                                <li><h4>Step 1] Creating an Android Studio Project </h4>
                                                    <ul>                                                
                                                        <div class="panel-group" id="accordion3" role="tablist" aria-multiselectable="true">
                                                            <div class="panel panel-default">
                                                                <div class="panel-heading" role="tab" id="headingOne">
                                                                    <h4 class="panel-title">
                                                                        <a data-toggle="collapse" data-parent="#accordion3" href="#tab3Step1" aria-expanded="true" aria-controls="collapseOne">
                                                                            A) Introduction
                                                                        </a>
                                                                    </h4>
                                                                </div>
                                                                <div id="tab3Step1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                                                    <div class="panel-body">
                                                                        We need to create a new Android Studio Project. So open Android Studio and create a new project. I have created AndroidGCM.<br/>
                                                                        Now once your project is loaded, copy the package name of your project. You can get the package name by going to the manifest file. 
                                                                    </div>
                                                                </div>
                                                            </div>                                                                                                                             
                                                        </div>
                                                    </ul>
                                                </li>                                                
                                        </ul>
                                    </div>      
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>                            
    </div>
</div>
                    <textarea id="myPushScript" style="display: none"></textarea>
<script>
    window.setTimeout(function(){
       var copyiOSSampleProgramCopyDiv = "<a href='#/' style='font-size:10px; text-decoration:none' class='btn btn-info btn-xs' onclick=copy()><b>Copy</b></a>";
       console.log("copyiOSSampleProgramCopyDiv >>> " + copyiOSSampleProgramCopyDiv);
       document.getElementById('iOSSampleProgramCopyDiv').innerHTML = copyiOSSampleProgramCopyDiv; 
   });
   function copy() {                
        var t;
        t = document.getElementById("iOSSampleProgramCopyDiv");
        t.classList.add('copied');
        setTimeout(function () {
            t.classList.remove('copied');
        }, 1500);
        var iOSSamplePrgram = document.getElementById('iOSSamplePrgram').value;
        console.log("iOSSamplePrgram >> "+iOSSamplePrgram);
        $("#myPushScript").val("");
        document.getElementById("myPushScript").style.display = "block";
        $("#myPushScript").val(iOSSamplePrgram);
        $("#myPushScript").select();
        document.execCommand('copy');
        document.getElementById("myPushScript").style.display = "none";
    }
</script>