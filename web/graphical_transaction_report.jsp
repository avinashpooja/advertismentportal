<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.TransformManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.TransformDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.ResourceManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.ResourceDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.AccessPointManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.Accesspoint"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.MSConfig"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.SettingsManagement"%>
<%@include file="header.jsp"%> 
<script src="scripts/reportsV2.js" type="text/javascript"></script>
<script src="vendor/toastr/build/toastr.min.js"></script>
<script src="vendor/ladda/dist/spin.min.js"></script>
<script src="vendor/ladda/dist/ladda.min.js"></script>
<script src="vendor/ladda/dist/ladda.jquery.min.js"></script>
<script src="scripts/operatorsTextReports.js" type="text/javascript"></script>
<%    PartnerDetails pdetails = (PartnerDetails) request.getSession().getAttribute("_partnerDetails");
    int opType = -1;
    String _opType = request.getParameter("_opType");
    if (_opType != null) {
        opType = Integer.parseInt(_opType);
    }
    String type = request.getParameter("_type");
    int Type = -1;
    if (type != null) {
        Type = Integer.parseInt(type);
    }
%>
<!-- Main Wrapper -->
<div id="wrapper">

    <div class="small-header transition animated fadeIn">
        <div class="hpanel">
            <div class="panel-body">
                <div id="hbreadcrumb" class="pull-right">
                    <ol class="hbreadcrumb breadcrumb">
                        <li><a href="home.jsp">Dashboard</a></li>
                        <li class="active">
                            <%if (Type == 1) {%>
                            <span>Transaction Report</span>
                            <%} else {%>
                            <span>Performance Report</span>
                            <%}%>
                        </li>
                    </ol>
                </div>
                <h2 class="font-light m-b-xs">
                    <%if (Type == 1) {%>
                    Transaction Report
                    <%} else {%>
                    Performance Report
                    <%}%>
                </h2>
                <small>Analyse Your API Transaction By Time Period</small>
            </div>
        </div>
    </div>


    <div class="content animate-panel">

        <div class="row">
            <div class="col-lg-12">
                <div class="hpanel">
                    <div class="panel-heading">
                        <div class="panel-tools">
                            <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                        </div>
                        Choose API And Timeframe To Generate Report
                    </div>
                    <div class="panel-body">
                        <div class="col-md-12">
                            <div class="col-sm-3">
                                <select class="form-control m-b" id="_Accesspoint" name="_Accesspoint">
                                    <%
                                        Object sobject = null;
                                        String aps = ",";
                                        sobject = new SettingsManagement().getSetting(SessionId, ChannelId, -1, -1);
                                        MSConfig msConfig = (MSConfig) sobject;
                                        Accesspoint Res[] = null;
                                        Res = new AccessPointManagement().getAllAccessPoint(SessionId, ChannelId);
                                        Accesspoint accesspoint = new AccessPointManagement().getAccessPointByNames("MerchantPaymentGateway");
                                        if (accesspoint != null) {%>
                                    <option value="<%= accesspoint.getApId()%>"><%= accesspoint.getName()%></option>
                                    <%}
                                        if (Res != null) {
                                            int count = 0;
                                            for (int i = 0; i < Res.length; i++) {
                                                if (Res[i].getStatus() == GlobalStatus.ACTIVE && Res[i].getGroupid() == Integer.parseInt(parObj.getPartnerGroupId())) {
                                                    int ap = Res[i].getApId();
                                                    Accesspoint apdetails = Res[i];
                                                    if (apdetails != null) {
                                                        int resID = -1;
                                                        String userStatus = "user-status-value-" + i;
                                                        String resourceList = apdetails.getResources();
                                                        String[] resids = resourceList.split(",");
                                                        for (int j = 0; j < resids.length; j++) {
                                                            if (!resids[j].isEmpty() && resids[j] != null) {
                                                                resID = Integer.parseInt(resids[j]);
                                                                ResourceDetails rsName = new ResourceManagement().getResourceById(SessionId, ChannelId, resID);
                                                                TransformDetails tmdetail = new TransformManagement().getTransformDetails(SessionId, ChannelId, apdetails.getApId(), resID);
                                                                if (rsName != null && tmdetail != null) {
                                                                    int g = Integer.parseInt(pdetails.getPartnerGroupId());
                                                                    if (g == apdetails.getGroupid()) {
                                                                        if (!aps.contains("," + apdetails.getName() + ",")) {
                                                                            aps += apdetails.getName() + ",";
                                                                            count++;
                                    %>
                                    <option value="<%= apdetails.getApId()%>"><%= apdetails.getName()%></option>
                                    <%}
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    %>
                                </select>
                            </div>
                            <div class="col-sm-3"><input id="datapicker1" type="text" class="form-control" placeholder="from"></div>
                            <div class="col-sm-3"><input id="datapicker2" type="text" class="form-control" placeholder="to"></div>
                            <div class="col-sm-3">
                                <%if (Type == 1) {%>
                                <button id="generateButtonV2" class="btn btn-success btn-sm ladda-button btn-block" data-style="zoom-in" onclick="generatereportsV2(<%=Type%>,<%=opType%>,<%=pdetails.getPartnerId()%>,1)"><i class="fa fa-bar-chart"></i> Generate</button>
                                <%} else {%>
                                <button id="generateButtonV2" class="btn btn-success btn-sm ladda-button btn-block" data-style="zoom-in" onclick="generatePerformanceReport(<%=Type%>,<%=opType%>,<%=pdetails.getPartnerId()%>)"><i class="fa fa-bar-chart"></i> Generate</button>
                                <%} %>
                            </div>
                        </div>
                        <br><br>                        
                        <div id="report_data_button">
                            <div style="padding-left: 75.5%">
                                <button  class="btn btn-default btn-xs "  onclick="generatereportsV2(<%=Type%>,<%=opType%>,<%=pdetails.getPartnerId()%>,2)"> Transaction Status</button>
                                <button  class="btn btn-default btn-xs" onclick="generateReportPart2(<%=Type%>,<%=opType%>,<%=pdetails.getPartnerId()%>,2)"> Transaction Per Day</button>
                            </div>
                        </div>
                        <label id="homeFirstReportLabel">Hourly Details</label>    
                        <div  id="report_data">
                        </div>
                            <div style="margin-top: 20px"></div>                       
                        <div class="col-md-12" id="report">
                            <hr class="m-b-xl"/>
                            <div class="col-sm-3">
                                <%
                                    PartnerDetails _partnerDetails = (PartnerDetails) request.getSession().getAttribute("_partnerDetails");
                                    int partid = _partnerDetails.getPartnerId();
                                %>
                                <input type="text" value="<%=partid%>" id="pId" name="pId" hidden>
                                <select id="_Rtype" name="_Rtype" class="form-control span2" style="width: 90px">
                                    <option value="pdf">Pdf</option>
                                    <option value="txt">Text</option>    
                                    <option value="csv">CSV</option>    
                                </select>
                            </div>
                            <div class="col-sm-3"><button class="btn btn-sm btn-primary2 ladda-button btn-block" data-style="zoom-in" id="generateReport" onclick="Operatortextreport(<%=opType%>)" type="button"><i class="fa fa-cloud-download"></i> <span class="bold">Download Report</span></button></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <jsp:include page="footer.jsp"/>
        <script>

            $(function () {
                document.getElementById("report_data_button").style.display = "none";
                document.getElementById("report").style.display = "none";
                document.getElementById("homeFirstReportLabel").style.display = "none";
                $('#datapicker1').datepicker();
                $('#datapicker2').datepicker();

            });

        </script>
