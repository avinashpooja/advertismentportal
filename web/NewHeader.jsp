<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.AdvertiserAdManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgAdvertiserAdDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.AdvertiserCreditManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgAdCreditInfo"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgAdvertiseSubscriptionDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.AdvertiserSubscriptionManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgAdvertiserDetails"%>
<%@page import="java.util.Calendar"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.TransformManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.TransformDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.ResourceManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.ResourceDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.AccessPointManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.Accesspoint"%>
<%@page import="java.net.URL"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.ProductionAccessEnvtManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgDeveloperProductionEnvt"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="org.json.JSONArray"%>
<%@page import="org.json.JSONObject"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.ProductionAccessManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgProductionAccess"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.EmailManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgEmailticket"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.GlobalStatus"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PaymentManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgPaymentdetails"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PackageSubscriptionManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgSubscriptionDetails"%>
<%@page import="com.mollatech.service.nucleus.crypto.LoadSettings"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.LoanManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgLoanDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgCreditInfo"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.CreditManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.PartnerDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.SessionManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgUsers"%>
<%@page import="java.util.List"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>


<!DOCTYPE html>
<html>
    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!--
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <-->
        <meta name='viewport' content='width=device-width, initial-scale=1.0, user-scalable=0' >
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>AuthBridge Research Services:Dashboard</title>

        <!-- Bootstrap -->
        
        <link href="HeaderCSS/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" type="text/css" href="HeaderCSS/global.css"/>
        <link rel="stylesheet" type="text/css" href="HeaderCSS/menu.css"/>
        <link rel="stylesheet" type="text/css" href="HeaderCSS/search_component.css"/>
 <script src="scripts/header.js" type="text/javascript"></script>
        <script src="vendor/jquery/dist/jquery.min.js"></script>        
        <script src="vendor/jquery-ui/jquery-ui.min.js"></script>
        <script src="vendor/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="vendor/summernote/dist/summernote.min.js"></script>
        <script src="vendor/codemirror/script/codemirror.js"></script>
        <script src="vendor/codemirror/javascript.js"></script> 
        <link rel="stylesheet" href="vendor/c3/c3.min.css" />
        <script src="vendor/d3/d3.min.js"></script>
        <script src="vendor/c3/c3.min.js"></script>
        <link rel="stylesheet" href="vendor/chartist/custom/chartist.css" />
        <script src="vendor/chartist/dist/chartist.min.js"></script>
        <script src="scripts/homepage.js" type="text/javascript"></script>
        <script src="vendor/bootstrap-tour/build/js/bootstrap-tour.min.js"></script>
        <link rel="stylesheet" href="vendor/blueimp-gallery/css/blueimp-gallery.min.css" />
        <link rel="stylesheet" href="vendor/blueimp-gallery/css/blueimp-gallery-video.css">
       

        

    </head>
     <%
        response.setHeader("Cache-Control", "no-cache");
        response.setHeader("Cache-Control", "no-store");
        response.setHeader("Pragma", "no-cache");
        response.setDateHeader("Expires", 0);
        
        String SessionId = (String) request.getSession().getAttribute("_advertisorSessionId");
        String ChannelId = (String) request.getSession().getAttribute("_advertiserChannelId");        
        String profileImageUrl = (String) request.getSession().getAttribute("profileImageUrl");
        SgAdvertiserDetails usrObj = (SgAdvertiserDetails) request.getSession().getAttribute("_advertisorDetails");
        String socialNamed = (String) request.getSession().getAttribute("socialNamed");
        String showTourFirstSignUp = (String) request.getSession().getAttribute("showTourFirstSignUp");
        if (SessionId == null) {
            response.sendRedirect("logout.jsp");
            return;
        }
        if (socialNamed == null) {
            socialNamed = usrObj.getAdvertisername();
        }
        int notificationCount = 0;        

        // find the Service Count
        int serviceCount = 0;        
        String base64ProfilePic = null;                
    %>

    <body>    

<div id="main"> 
    <div class="container-fluid">	
	    <div id='loadingmessage' style='display:none' class="overlay">
			<div class="show-delay"><img src="/img/google-loading-icon.gif" alt="Loder" style="height:100px;width:100px"/></div>
		</div>
      <!-- top panel starts here -->
      
	   


 



<div class="row">

        <div class="top-panel box-shadow">
            
         <!--    <div>         
    
        <span style="color: #41f450;margin-bottom: 30px;">
        <font size="6"><b>Advertisement</b></font>
        </span>
        <span><font size="6">Portal</font></span>
            -->
            
  


         <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 green-gr text-center"> 

		  <button type="button" class="navbar-fostrap user"> <span></span> <span></span> <span></span> </button>

			<a href="/dashboard" class="logo sprite"></a>         



				<div class="user-wrap visible-xs"> 

				  <div class="dropdown pull-right">

				  <button class="user dropdown-toggle"  id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">

					<!--<img src="images/user.jpg" class="img-responsive"> -->

					<img src="img/male_user.jpg" class="" alt=""/>
					 <!--<strong class="icon-user top-user-name"> Blue B.. </strong> -->

					<span class="caret"></span>

				  </button>

				  <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">

					<li><a href="/users/change_password">Change Password</a></li>


					<li><a href="/apidoc" target="_blank">Developers API</a></li>

					<li><a href="/users/logout">Logout</a></li>

				  </ul>

				</div><!-- /.Dropdown -->

				</div><!-- /.col-lg-2 -->

          </div>

 

          <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 right-top hidden-xs p_none">
              
             <!-- <span style="color: #41f450;margin-bottom: 30px;">
              <font size="6"><b>Advertisement</b></font>
              </span>
              <span><font size="6">Portal</font></span> -->
              

            <div class="top-wrap">

			 <%                            
                            double mainCredit = 0;
                            SgAdCreditInfo creditObj1 = null;
                            SgAdvertiseSubscriptionDetails subscriObject1 = new AdvertiserSubscriptionManagement().getAdvertiserSubscriptionbyAdId(usrObj.getAdId());                            
                            creditObj1 = new AdvertiserCreditManagement().getDetails(usrObj.getAdId());
                            session.setAttribute("advertiserSubscriptionObject", subscriObject1);
                        %> 


			  <!-- /.search_bar-->

            <div class="search_bar clearfix">

              <nav class="pull-left">

                <div class="nav-fostrap">

                    <ul class="clearfix">                     
           <%
                                String expiry = "NA";
                                Calendar calendar = Calendar.getInstance();
                                Calendar expireDate = Calendar.getInstance();
                                BigDecimal bdRemainTotalCredit = new BigDecimal(0);
                                if (creditObj1 != null) {
                                    mainCredit = creditObj1.getMainCredit();                                    
                                    bdRemainTotalCredit = new BigDecimal(mainCredit);
                                    bdRemainTotalCredit = bdRemainTotalCredit.setScale(2, BigDecimal.ROUND_HALF_EVEN);
                                }
                                if(subscriObject1 != null){
                                    SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss a");
                                    expiry = dateFormatter.format(subscriObject1.getExpiryDate());
                                    calendar.setTime(subscriObject1.getExpiryDate());
                                    calendar.add(Calendar.DATE, -1);
                                    expireDate.setTime(subscriObject1.getExpiryDate());
                                }
                                boolean pdfAdDetailsFind = false; boolean emailAdDetailsFind = false;
                                SgAdvertiserAdDetails adDetails = new AdvertiserAdManagement().getAdvertiserDetails(usrObj.getAdId());
                                if(adDetails!= null && adDetails.getPdfAdImage() != null){
                                   pdfAdDetailsFind = true; 
                                }
                                if(adDetails!= null && adDetails.getEmailAdImage() != null){
                                   emailAdDetailsFind = true; 
                                }
                            %>
                      

                    <li class="tour-3">
                        <a  onclick=""><span class="capitalize">Push Template </span> </a>
                    </li>
                    <li class="tour-4">
                        <a onclick=''><span class="capitalize">Push Ad </span> </a>
                    </li>
                    <li class="tour-5">
                        <a  onclick="emailADDetails()"> <span class="capitalize">Email Ad</span></a>
                    </li>                                 
                    <li class="tourVideo">
                        <a  onclick="pdfADDetails()"> <span class="capitalize">PDF Ad</span></a>
                    </li>
                    <li class="tourVideo">                        
                        <a  onclick=""> <span class="capitalize">Invoices</span></a>
                    </li>
                    <li class="tourVideo">                        
                        <a  onclick=""> <span class="capitalize">Subscription</span></a>
                    </li>
                </ul>
            </div>
                <!-- <div id="sb-search" class="sb-search "> -->

                

                <div id="sb-search" class="sb-search">

               <form action="/dashboard" id="SearchForm" method="get" accept-charset="utf-8">                  

                    

                    <input name="first_name" class="sb-search-input" placeholder="Enter your search term..." id="search" maxlength="100" type="text">
                    

                    

                    <!--<input class="sb-search-submit" type="submit" value="">-->
<br>
                    <span class="sb-icon-search"><img src="img/search_icon.png" alt=""> </span>

                 </form>
                </div><!-- /.sb-search-->               

                

              </div>

              <!-- /.search_bar-->            

            

             <div class="hidden-xs user-wrap">

              <div class="dropdown pull-right">
               
                <img src="img/male_user.jpg" class="" alt="">
                <!--<strong class="icon-user top-user-name"> Blue B.. </strong> -->

                <span class="caret"></span>

                    <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">

                      <li><a href="/users/change_password">Change Password</a></li>

                      <li><a href="/apidoc" target="_blank">Developers API</a></li>

                      <li><a href="/users/logout">Logout</a></li>

                    </ul>

              </div>

              <!-- /.Dropdown --> 

            </div>

            

            </div><!-- /.top-wrap -->

          </div><!-- /.right-top -->

        </div><!-- /.top panel -->

</div><!-- top panel ends here -->

    </div>
</div> <div id="tourDiv">
            <!-- Tour API Console -->        
            <div id="apiTour">     
                <div id="wrapper">
                    <div id="wrapperAPIConsoleTour" ></div>
                </div>
            </div>
            <!-- API Console End -->

            <!-- Tour API Console Window -->        
            <div id="apiConsoleTour">     
                <div id="wrapper">
                    <div id="wrapperAPIConsoleWindowTour" ></div>
                </div>
            </div>
            <!-- API Console Window End -->

            <!-- Tour report Window -->        
            <div id="reportTour">     
                <div id="wrapper">
                    <div id="reportWindowTour" ></div>
                </div>
            </div>
            <!-- Report Window End -->

            <!-- Tour invoice Window -->        
            <div id="invoiceTour">     
                <div id="wrapper">
                    <div id="invoiceWindowTour" ></div>
                </div>
            </div>
            <!-- Invoice Window End -->

            <!-- Tour subscribe Window -->        
            <div id="subscribeTour">     
                <div id="wrapper">
                    <div id="subscribeWindowTour" ></div>
                </div>
            </div>
            <!-- Subscribe Window End -->

            <!-- Tour Helpdesk Window -->        
            <div id="helpDeskTour">                 
                <div id="wrapper">
                    <div id="helpDeskWindowTour" ></div>  
                </div>
            </div>


        </div>        
        <script type="text/javascript">

            document.onreadystatechange = function () {
                if (document.readyState === 'complete') {

                    $('#perCreditUsed').ready(function () {
                        todayusedCredit("today");
                    });
                }
            };

            $('#wrapper2').hide();
            $('#wrapper3').hide();
        </script>

        <script>            
            $(document).ready(function () {
                var creditUsedData = fifteenDaysCreditExpenditure();
                var obj = [];
                for (var key in creditUsedData) {
                    var value = creditUsedData[key];
                    obj.push(value.txAmount);
                    //        alert(JSON.stringify(key)+" :: "+JSON.stringify(value.apiName));
                }
                $("#sparkline1").sparkline(obj, {
                    type: 'bar',
                    barWidth: 7,
                    height: '30px',
                    barColor: '#2381c4',
                    negBarColor: '#53ac2a'
                });

            });
            //    
            function fifteenDaysCreditExpenditure() {
                var s = './DailyTransactionAmount';
                var jsonData = $.ajax({
                    url: s,
                    dataType: "json",
                    async: false
                }).responseText;
                if (jsonData.length === 0) {
                    jsonData = [{"label": "NA", "value": 0, "value5": 0}];
                    return jsonData;
                } else {
                    var myJsonObj = JSON.parse(jsonData);
                    return myJsonObj;
                }
            }

            function getCreditStatus() {
                var s = './GetCredit';
                $.ajax({
                    type: 'POST',
                    url: s,
                    datatype: 'json',
                    data: $("#getBody").serialize(),
                    success: function (data) {
                        if (data.credits !== 'NA') {
                            $('#mainCreditOfDev').html(data.credits);
                        }
                    }});
            }
            getCreditStatus();
            setTimeout(function () {
                if (typeof (tour) != "undefined") {
                    tour.end();
                }
                startTour();
            }, 500);
            setTimeout(function () {
                generateTopServiceV2();
            }, 2000);
            test = document.getElementById("Ashish");
            collapse = document.getElementById("mobile-collapse");

        </script>
        <%if (showTourFirstSignUp != null && showTourFirstSignUp.equalsIgnoreCase("yes")) {
                request.getSession().setAttribute("showTourFirstSignUp", null);
        %>
        <script>
            $(document).ready(function () {
                setTimeout(function () {
                    firstTimeTour();
                    tour.restart();
                }, 1000);
            });
        </script>
        <%}%>
        <!-- Global site tag (gtag.js) - Google Analytics -->
    </body>
</html>