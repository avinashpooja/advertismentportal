<%--<%@page import="com.stripe.model.Plan"%>--%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PackageSubscriptionManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgSubscriptionDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.PartnerDetails"%>
<%--<%@page import="com.stripe.model.Subscription"%>--%>
<%@page import="java.nio.charset.StandardCharsets"%>
<%@page import="com.mollatech.service.nucleus.crypto.LoadSettings"%>
<%@page import="java.nio.charset.Charset"%>
<%@page import="java.nio.file.Files"%>
<%@page import="java.nio.file.Paths"%>
<%@page import="java.util.List"%>
<!DOCTYPE html>
<html>
    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <!-- Page title -->
        <title>Advertisement Portal| Registration</title>

        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
        <!--<link rel="shortcut icon" type="image/ico" href="favicon.ico" />-->

        <!-- Vendor styles -->
        <link rel="stylesheet" href="vendor/fontawesome/css/font-awesome.css" />
        <link rel="stylesheet" href="vendor/metisMenu/dist/metisMenu.css" />
        <link rel="stylesheet" href="vendor/animate.css/animate.css" />
        <link rel="stylesheet" href="vendor/bootstrap/dist/css/bootstrap.css" />
        <link rel="stylesheet" href="vendor/sweetalert/lib/sweet-alert.css" />
        <link rel="stylesheet" href="vendor/ladda/dist/ladda-themeless.min.css" />
        <link rel="stylesheet" href="vendor/toastr/build/toastr.min.css" />
        <script src="scripts/utilityFunction.js" type="text/javascript"></script>
        <script src="scripts/partnerRequest.js" type="text/javascript"></script>
         <link rel="stylesheet" href="vendor/dropzone/dropzone.css">
        <!-- App styles -->
        <script src="scripts/advertismentSignUp.js" type="text/javascript"></script>
        <link rel="stylesheet" href="fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css" />
        <link rel="stylesheet" href="fonts/pe-icon-7-stroke/css/helper.css" />
        <link rel="stylesheet" href="styles/style.css">

        <style>
            /* The Modal (background) */
            .modal {
                display: none; /* Hidden by default */
                position: fixed; /* Stay in place */
                z-index: 1; /* Sit on top */
                padding-top: 110px; /* Location of the box */
                left: 0;
                top: 0;
                width: 100%; /* Full width */
                height: 100%; /* Full height */
                overflow: auto; /* Enable scroll if needed */
                background-color: rgb(0,0,0); /* Fallback color */
                background-color: rgba(0,0,0,0.5);  /*Black w/ opacity */
            }

            /* Modal Content */
            .modal-content {
                background-color: #fefefe;
                margin: auto;
                padding: 20px;
                border: 1px solid #888;
                width: 50%;
            }

            /* The Close Button */
            .close {
                color: #aaaaaa;
                float: right;
                font-size: 28px;
                font-weight: bold;
            }

            .close:hover,
            .close:focus {
                color: #000;
                text-decoration: none;
                cursor: pointer;
            }
        </style>

    </head>
    <body class="blank" style="background: rgba(43,48,131,1);
          background: -moz-linear-gradient(left, rgba(43,48,131,1) 0%, rgba(35,129,196,1) 50%, rgba(43,48,131,1) 100%);
          background: -webkit-gradient(left top, right top, color-stop(0%, rgba(43,48,131,1)), color-stop(50%, rgba(35,129,196,1)), color-stop(100%, rgba(43,48,131,1)));
          background: -webkit-linear-gradient(left, rgba(43,48,131,1) 0%, rgba(35,129,196,1) 50%, rgba(43,48,131,1) 100%);
          background: -o-linear-gradient(left, rgba(43,48,131,1) 0%, rgba(35,129,196,1) 50%, rgba(43,48,131,1) 100%);
          background: -ms-linear-gradient(left, rgba(43,48,131,1) 0%, rgba(35,129,196,1) 50%, rgba(43,48,131,1) 100%);
          background: linear-gradient(to right, rgba(43,48,131,1) 0%, rgba(35,129,196,1) 50%, rgba(43,48,131,1) 100%);
          filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#2b3083', endColorstr='#2b3083', GradientType=1 );">

        <!-- Simple splash screen-->
        <div class="splash"> <div class="color-line"></div><div class="splash-title"><h1>Loading...</h1><div class="spinner"> <div class="rect1"></div> <div class="rect2"></div> <div class="rect3"></div> <div class="rect4"></div> <div class="rect5"></div> </div> </div> </div>
        <!--[if lt IE 7]>
        <p class="alert alert-danger">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        <%
            String selectedpackageDetails = request.getParameter("pa");
            request.getSession().setAttribute("selectedAdvertiserPackageDetails", selectedpackageDetails);
        %>
        <div class="color-line"></div>
        <div class="back-link">
            <a href="login.jsp" class="btn btn-primary">Registered? Login here</a>
        </div>
        <div class="login-container" style="max-width: 800px !important">
            <div class="row">
                <div class="col-lg-12">
                    <div class="text-center m-b-md">
                        <h3 style="color:#fff">Advertisement Registration</h3>
                        <small></small>
                    </div>
                    <div class="hpanel">
                        <div class="panel-body col-centered" >
                            <input type="hidden" id="loginPage" name="loginPage" value="2">
                            <div style="text-align: center">
                            <a class="btn btn-info btn-xs" href="#step1" data-toggle="tab" id="step1">Step 1 - Sign Up</a>
                            <a class="btn btn-info btn-xs disabled" href="#step2" data-toggle="tab" id="step2">Step 2 - Subscribe a package</a>
                            </div>
                            <!--                            <a class="btn btn-info btn-xs disabled" href="#step3" data-toggle="tab" id="step3">Step 3 - Enjoy the Ready APIs</a>-->
                            <br/><br/>
                            <div id="signupWindow">                                
                                <label class="control-label" for="email">
                                    Please fill your details </label><br/>  <br/>  
                                <div style="text-align: center">
<!--                                <a href="#" class="btn btn-danger2 btn-outline ladda-button" id="googleSignIn" data-style="zoom-in" type="button"><i class="fa fa-google"></i> <br/>Google</a>
                                <a href="#" class="btn btn-info btn-outline ladda-button" onclick="fb_login();" data-style="zoom-in" type="button" id="facebookLadda"><i class="fa fa-facebook-official"></i> <br/>Facebook</a>
                                <a href="#" class="btn btn-primary btn-outline ladda-button" type="button" data-style="zoom-in"   onclick="liAuth()" id="linkedInLadda"><i class="fa fa-linkedin"></i> <br/>LinkedIn</a>-->
                                <form id="personalInfo" class="form-horizontal">                                    
                                    <h6><i class="fa fa-user fa-2x text-info">  Personal Information</i></h6><br/>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" >Name <span style="color: red">*</span></label>
                                        <div class="col-sm-3">                                            
                                            <input type="text" id="partnerName" title="Please enter name" name="partnerName" class="form-control" required>                                            
                                        </div>
                                        <label class="col-sm-2 control-label">Mobile No. <span style="color: red">*</span></label>
                                        <div class="col-sm-3">                                           
                                            <input type="number" id="mobileNo" name="mobileNo" class="form-control" required>                                           
                                        </div>                                                                        
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Email <span style="color: red">*</span></label>
                                        <div class="col-sm-3">                                            
                                            <input type="email" id="emailId" name="emailId" class="form-control" required>                                           
                                        </div>
                                    </div>
                                    <h6><i class="fa fa-building fa-2x text-info">  Company Information</i></h6><br/>    
                                    <div class="form-group">                                    
                                        <label class="col-sm-2 control-label" >Name <span style="color: red">*</span></label>
                                        <div class="col-sm-3">                                            
                                            <input type="text" id="comapnyName" name="comapnyName" title="Please enter comapny name" class="form-control">                                           
                                        </div>

                                        <label class="col-sm-2 control-label" >Registration No. <span style="color: red">*</span></label>
                                        <div class="col-sm-3">                                            
                                            <input type="text" id="regNo" name="regNo" title="Please enter registration number" class="form-control">                                            
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" >Landline No. <span style="color: red">*</span></label>
                                        <div class="col-sm-3">                                           
                                            <input type="number" id="landlineNo" name="landlineNo" title="Please enter landline number" class="form-control">                                           
                                        </div>
                                        <label class="col-sm-2 control-label">Address Line<span style="color: red">*</span></label>
                                        <div class="col-sm-3">                                           
                                            <input type="text" id="companyAddress" name="companyAddress" title="Please enter address line" class="form-control">            
                                        </div>
                                    </div>                                    
                                    
                                        <div class="form-group">                                                                        
                                            <label class="col-sm-2 control-label" >City <span style="color: red">*</span></label>
                                            <div class="col-sm-3">                                            
                                                <input type="text" id="cityName" title="Please enter city" name="cityName" class="form-control">                                            
                                            </div>
                                            <label class="col-sm-2 control-label" >State <span style="color: red">*</span></label>
                                            <div class="col-sm-3">                                            
                                                <input type="text" id="state" title="Please enter state" name="state" class="form-control">                                            
                                            </div>                                    
                                        </div>                                
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Country <span style="color: red">*</span></label>
                                            <div class="col-sm-3">                                
                                                <input type="text" id="country" name="country" title="Please enter country" class="form-control">                               
                                            </div>                                    
                                            <label class="col-sm-2 control-label" >Pin Code <span style="color: red">*</span></label>
                                            <div class="col-sm-3">                                
                                                <input type="text" id="pinCode" title="Please enter Pin Code" name="pinCode" class="form-control">                                
                                            </div>
                                        </div>
                                        <button class="btn btn-primary ladda-button validateProfileForm" data-style="zoom-in" onclick="validateRegistrationForm()" style="display: none">Save changes</button>    
                                </form>
                                <h6><i class="fa fa-file-archive-o fa-2x text-info"> KYC Document</h6></i> <br/>
                                <div style="background: #fff; height: 200px">
                                    <div class="col-sm-8 col-lg-8 col-md-8" style="width: 68%;margin-left: 16%">
                                        <form action="UploadEmailAttachme" method="post" class="dropzone" id="my-dropzone"></form>	
                                        <small>Upload zip of your KYC document details.</small>
                                    </div>
                                </div>    
                                </div>
                                    <br/><br/><br/><br/>
                                <div class="form-group">
                                    <div class="col-sm-8" style="margin-left: 40%">                                        
                                        <a class="btn btn-info ladda-button" id="partUpdate" data-style="zoom-in"  onclick="callValidateRegistrationForm()">Save & Proceed</a>
                                    </div>
                                </div>
                            </div>
                            <div id="paymentWindow"></div>                            
                        </div>

                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 text-center" style="color:#fff">
                    <strong>Powered by Service Guard And Axiom Protect 2.0<br>
                        Platform Owned By BlueBricks Technologies 2009-2017 (<a href="http://www.blue-bricks.com" target="_blank">www.blue-bricks.com</a>)</strong>
                </div>
            </div>
            
            <div id="myModal" class="modal">
                <div class="modal-content">
                    <span class="close">&times;</span>
                    <div style="text-align: center">
                        <h1 class="text-success" style="font-size: 50px"><b><i class="fa fa-check fa-2xx text-success"></i> Congratulations! </b></h1>
                        <p style="font-size: 22px;font-family: Open Sans, Helvetica Neue, Helvetica, Arial, sans-serif;"><b> You have successfully created an account with us.</b></p>
                        <p style="font-size: 22px;font-family: Open Sans, Helvetica Neue, Helvetica, Arial, sans-serif;"><b>Check your email for your newly issued password.</b></p>                                                                     
                    </div>                    
                    <br>                                    
                </div>
            </div>  
                
        </div>

        <!-- Vendor scripts -->
        <script src="vendor/jquery/dist/jquery.min.js"></script>
        <script src="vendor/jquery-ui/jquery-ui.min.js"></script>
        <script src="vendor/slimScroll/jquery.slimscroll.min.js"></script>
        <script src="vendor/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="vendor/metisMenu/dist/metisMenu.min.js"></script>
        <script src="vendor/iCheck/icheck.min.js"></script>
        <script src="vendor/sparkline/index.js"></script>
        <script src="vendor/ladda/dist/spin.min.js"></script>
        <script src="vendor/ladda/dist/ladda.min.js"></script>
        <script src="vendor/ladda/dist/ladda.jquery.min.js"></script>
        <script src="vendor/jquery-validation/jquery.validate.min.js"></script>
        <script src="vendor/sweetalert/lib/sweet-alert.min.js"></script>
        <script src="vendor/toastr/build/toastr.min.js"></script>
        <!-- App scripts -->
        <script src="scripts/homer.js"></script>
        <script src="https://apis.google.com/js/platform.js?onload=onLoadGmail" async defer></script>
        <script src="scripts/socialMedia.js"></script>
         <script src="vendor/dropzone/dropzone.js"></script>
        <script type="text/javascript" src="//platform.linkedin.com/in.js">
                                    api_key:81qhnzg756okk4
//                                            api_key:81akug1v7h1k9r
                                            onLoad:onLinkedInLoad
                                    authorize:true
        </script>
        <script >
            onLoadfacebook();
            initializeToastr();

        </script>
        <script>
    $(document).ready(function () {        
        Dropzone.autoDiscover = false;
        var myDropzone = new Dropzone("#my-dropzone", {
            url: "UploadKYCDocument",
            uploadMultiple: false,            
            maxFilesize: 1,
            maxFiles: 1,
            acceptedFiles: "application/zip",
            dictInvalidFileType: "You can't upload files of this type, only Zip file",
            autoProcessQueue: true,
            parallelUploads: 1,
            addRemoveLinks: true,
            dictDefaultMessage: "Drop KYC document zip here to upload",            
            removedfile: function(file) {
                removeKYCFromSession(file.name);    
                var _ref;
                if (file.previewElement) {
                    if ((_ref = file.previewElement) != null) {
                        _ref.parentNode.removeChild(file.previewElement);
                    }
                }
                return this._updateMaxFilesReachedClass();
            }
        });
    });
</script>

        <script>
// Get the modal
var modal = document.getElementById('myModal');

// Get the button that opens the modal
//var btn = document.getElementById("myBtn");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks the button, open the modal 

//btn.onclick = function() {
//    modal.style.display = "block";
//}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
    modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}
</script>

    </body>
</html>