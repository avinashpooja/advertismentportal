<link href="styles/mainVideo.css" rel="stylesheet" type="text/css"/>   
<div class="small-header transition animated fadeIn">
    <div class="hpanel">
        <div class="panel-body">
            <h2 class="font-light m-b-xs">
                Video Gallery
            </h2>
            <small>Learn with video tutorials </small>
        </div>
    </div>
</div>
<div class="content animate-panel">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="hpanel">
                <div class="panel-body">
                    <p>
                        <strong>Video Gallery,</strong>  within our gallery you will find Dashboard Tutorials and Service Tutorials. Learn More about Ready APIs from here.
                    </p>
                    <br>
                    <ul class="nav nav-tabs">                        
                        <li class="active"><a data-toggle="tab" href="#tab-2"> Dashboard</a></li>
                        <li class=""><a data-toggle="tab" href="#tab-3"> Services</a></li>                        
                    </ul>
                    <div class="tab-content">
                        <div id="tab-2" class="tab-pane active"> 
                            <br>
                                <article class="video" data-toggle="tooltip" data-placement="right" title="This video gives the details about how you can change your password and your profile details.">
                                    <figure>
                                        <a class="fancybox fancybox.iframe" href="//www.youtube.com/embed/ZIMbwhKZ7cs"><img class="videoThumb" src="images/video/dashoard.jpg"></a>
                                    </figure>
                                    <h5 class="videoTitle">&nbsp;1) Dashboard</h5>
                                </article>

                                <article class="video" data-toggle="tooltip" data-placement="right" title="This video gives the details about how you can change your password and your profile details.">
                                    <figure>
                                        <a class="fancybox fancybox.iframe" href="//www.youtube.com/embed/2X6XOykpFJY"><img class="videoThumb" src="https://img.youtube.com/vi/2X6XOykpFJY/maxresdefault.jpg"></a>
                                    </figure>
                                    <h5 class="videoTitle" >&nbsp;2) My Profile</h5>
                                </article>

                                <article class="video" data-toggle="tooltip" data-placement="right" title="This video gives the details about the tour option and how it work.">
                                    <figure>
                                        <a class="fancybox fancybox.iframe" href="//www.youtube.com/embed/gpyyIJ1d-a0"><img class="videoThumb" src="images/video/quicktour.jpg"></a>
                                    </figure>
                                    <h5 class="videoTitle">&nbsp;3) Quick Tour</h5>
                                </article>

                                <article class="video" data-toggle="tooltip" data-placement="right" title="This video gives the details about the services and how it work.">
                                    <figure>
                                        <a class="fancybox fancybox.iframe" href="//www.youtube.com/embed/N-uO7D9lb8c"><img class="videoThumb" src="images/video/service.jpg"></a>
                                    </figure>
                                    <h5 class="videoTitle">&nbsp;4) Services</h5>
                                </article>

                                <article class="video" data-toggle="tooltip" data-placement="right" title="This video gives the details about the reports menu and explain each report options.">
                                    <figure>
                                        <a class="fancybox fancybox.iframe" href="//www.youtube.com/embed/MCo3XL1H62g"><img class="videoThumb" src="https://img.youtube.com/vi/MCo3XL1H62g/maxresdefault.jpg"></a>
                                    </figure>
                                    <h5 class="videoTitle">&nbsp;5) Reports</h5>
                                </article>

                                <article class="video" data-toggle="tooltip" data-placement="right" title="This video gives the details about the invoice menu and explain how it works.">
                                    <figure>
                                        <a class="fancybox fancybox.iframe" href="//www.youtube.com/embed/YKzofsLeVB4"><img class="videoThumb" src="images/video/invoice.jpg"></a>
                                    </figure>
                                    <h5 class="videoTitle">&nbsp;6) Invoices</h5>
                                </article>
                        </div>
                        <div id="tab-3" class="tab-pane">                            

                                <article class="video" data-toggle="tooltip" data-placement="right" title="By using DB As Service we can expose our database like oracle, mysql and cassandra . User can fired various queries on database as per the requirement and easily access the database. We can also save our database connection using this services.">
                                    <figure>
                                        <a class="fancybox fancybox.iframe" href="//www.youtube.com/embed/HKo41a2aCHg"><img class="videoThumb" src="https://img.youtube.com/vi/HKo41a2aCHg/maxresdefault.jpg"></a>
                                    </figure>
                                    <h5 class="videoTitle">&nbsp;1) DB As Service<br></h5>
                                </article>

                                <article class="video" data-toggle="tooltip" data-placement="right" title="Service that lets us sign documents digitally without printing, scanning, or faxing. It's a win for both business and personal users because electronic signing is fast, easy, and secure. Electronic signatures are trusted by millions around the world and are available in dozens of languages. Send and sign anywhere, anytime, on any Internet-enabled device. Contracts, agreements, loans, leases, all can be signed quickly and securely.">
                                    <figure>
                                        <a class="fancybox fancybox.iframe" href="//www.youtube.com/embed/zPHjftuZdvI"><img class="videoThumb" src="https://img.youtube.com/vi/zPHjftuZdvI/maxresdefault.jpg"></a>
                                    </figure>
                                    <h5 class="videoTitle">&nbsp;2) Document Utility (Document Signing)</h5>
                                </article>

                                <article class="video" data-toggle="tooltip" data-placement="right" title="This video gives the details about operations perform on PDF like compress PDF, Merge multiple PDF into one, rotate PDF, image to PDF convert and text with all setting, and encryption decryption of PDF. All this features combined into one in document utility service.">
                                    <figure>
                                        <a class="fancybox fancybox.iframe" href="//www.youtube.com/embed/_zNhOuOgbro"><img class="videoThumb" src="https://img.youtube.com/vi/_zNhOuOgbro/maxresdefault.jpg"></a>
                                    </figure>
                                    <h5 class="videoTitle">&nbsp;3) Document Utility (Operation On PDF)</h5>
                                </article>

                                <article class="video" data-toggle="tooltip" data-placement="right" title="This video gives the details about operations like conversion of PDF to image convert and vice versa, adding water mark image and text on PDF this API.">
                                    <figure>
                                        <a class="fancybox fancybox.iframe" href="//www.youtube.com/embed/Hjpmyhs8aLg"><img class="videoThumb" src="https://img.youtube.com/vi/_zNhOuOgbro/maxresdefault.jpg"></a>
                                    </figure>
                                    <h5 class="videoTitle">&nbsp;4) Document Utility (Operation On PDF 2)</h5>
                                </article>

                                <article class="video" data-toggle="tooltip" data-placement="right" title="Google Authentication is an application that implements two-step verification services for authenticating users of mobile applications by Google. This video gives the details about company and user creation with token assignment and verify otp.">
                                    <figure>
                                        <a class="fancybox fancybox.iframe" href="//www.youtube.com/embed/0AdbHoT_LKE"><img class="videoThumb" src="https://img.youtube.com/vi/Hjpmyhs8aLg/maxresdefault.jpg"></a>
                                    </figure>
                                    <h5 class="videoTitle">&nbsp;5) Google Auth Token (Part 1)</h5>
                                </article>

                                <article class="video" data-toggle="tooltip" data-placement="right" title="Google Authentication is an application that implements two-step verification services for authenticating users of mobile applications by Google. This video gives the details about generating reports and remove token, user and company details.">
                                    <figure>
                                        <a class="fancybox fancybox.iframe" href="//www.youtube.com/embed/tVgIXPe-gn0"><img class="videoThumb" src="https://img.youtube.com/vi/_zNhOuOgbro/maxresdefault.jpg"></a>
                                    </figure>
                                    <h5 class="videoTitle">&nbsp;6) Google Auth Token (Part 2)</h5>
                                </article>

                                <article class="video" data-toggle="tooltip" data-placement="right" title="The Image Utility service provides various image processing features like image format conversion to convert an image form one to another image format, extract image metadata feature to retrieve meta data of image, making GIF file from using multiple images, adding text watermark on image, encrypt data in the image and retrieve encrypted data from image.">
                                    <figure>
                                        <a class="fancybox fancybox.iframe" href="//www.youtube.com/embed/ck7u1V0Sjtg"><img class="videoThumb" src="https://img.youtube.com/vi/ck7u1V0Sjtg/maxresdefault.jpg"></a>
                                    </figure>
                                    <h5 class="videoTitle">&nbsp;7) Image utility</h5>
                                </article>

                                <article class="video" data-toggle="tooltip" data-placement="right" title="Tokenization, when applied to data security, is the process of substituting a sensitive data element with a non-sensitive equivalent, referred to as a token, This video explain data processing applications with the authority and interfaces to request tokens, or detokenize back to sensitive data.">
                                    <figure>
                                        <a class="fancybox fancybox.iframe" href="//www.youtube.com/embed/XXcM5_PmzYU"><img class="videoThumb" src="images/video/dashoard.jpg"></a>
                                    </figure>
                                    <h5 class="videoTitle">&nbsp;8) Tokanization</h5>
                                </article>

                                <article class="video" data-toggle="tooltip" data-placement="right" title="Value Added Services provides various features, This video gives the details about EmailId validation to validate email ids">
                                    <figure>
                                        <a class="fancybox fancybox.iframe" href="//www.youtube.com/embed/5uXmt2iZfSU"><img class="videoThumb" src="https://img.youtube.com/vi/5uXmt2iZfSU/maxresdefault.jpg"></a>
                                    </figure>
                                    <h5 class="videoTitle">&nbsp;9) Value Added Services (Bulk email validation)</h5>
                                </article>

                                <article class="video" data-toggle="tooltip" data-placement="right" title="Value Added Services provides various features, This video gives the details about issue QR code with and without password.">
                                    <figure>
                                        <a class="fancybox fancybox.iframe" href="//www.youtube.com/embed/NeYP4HHqCgQ"><img class="videoThumb" src="https://img.youtube.com/vi/NeYP4HHqCgQ/maxresdefault.jpg"></a>
                                    </figure>
                                    <h5 class="videoTitle">&nbsp;10) Value Added Services (Issue QR code with and without password)</h5>
                                </article>

                                <article class="video" data-toggle="tooltip" data-placement="right" title="Value Added Services provides various features, This video gives the details about shorten url and event calender generation.">
                                    <figure>
                                        <a class="fancybox fancybox.iframe" href="//www.youtube.com/embed/nXAgnybLSMc"><img class="videoThumb" src="https://img.youtube.com/vi/nXAgnybLSMc/maxresdefault.jpg"></a>
                                    </figure>
                                    <h5 class="videoTitle">&nbsp;11) Value Added Services (Shorten url and event calender generation)</h5>
                                </article>

                                <article class="video" data-toggle="tooltip" data-placement="right" title="Value Added Services provides various features, This video gives the details about Validation of mobile number, credit card, format of file and bank details.">
                                    <figure>
                                        <a class="fancybox fancybox.iframe" href="//www.youtube.com/embed/KuL2_rSqUJc"><img class="videoThumb" src="https://img.youtube.com/vi/nXAgnybLSMc/maxresdefault.jpg"></a>
                                    </figure>
                                    <h5 class="videoTitle">&nbsp;12) Value Added Services (Validation of mobile number, credit card, format of file and bank details)</h5>
                                </article>
                                
                                <article class="video" data-toggle="tooltip" data-placement="right" title="This service has a goal of providing geo location service to all users . It provide all details of server location like country, state, latitude, longitude, etc. For which ip address we have pass as input.">
                                    <figure>
                                        <a class="fancybox fancybox.iframe" href="//www.youtube.com/embed/xDBSjR968T0"><img class="videoThumb" src="https://img.youtube.com/vi/xDBSjR968T0/maxresdefault.jpg"></a>
                                    </figure>
                                    <h5 class="videoTitle">&nbsp;13) IP2GEO</h5>
                                </article>
                                
                                <article class="video" data-toggle="tooltip" data-placement="right" title="This Service is used to do conference video call. The generated URL will be accessible on chrome, opera, firefox as well android and iOS browsers.">
                                    <figure>
                                        <a class="fancybox fancybox.iframe" href="//www.youtube.com/embed/Y3R_3HVAMfI"><img class="videoThumb" src="https://img.youtube.com/vi/Y3R_3HVAMfI/maxresdefault.jpg"></a>
                                    </figure>
                                    <h5 class="videoTitle">&nbsp;14) Instant web chat</h5>
                                </article>
                                <article class="video" data-toggle="tooltip" data-placement="right" title="A reverse proxy provides an additional level of abstraction and control to ensure the smooth flow of network traffic between clients and servers. This service provides high level of security for servers using reverse proxy url.">
                                    <figure>
                                        <a class="fancybox fancybox.iframe" href="//www.youtube.com/embed/EvFjboHxqeg"><img class="videoThumb" src="https://img.youtube.com/vi/Y3R_3HVAMfI/maxresdefault.jpg"></a>
                                    </figure>
                                    <h5 class="videoTitle">&nbsp;15) Reverse Proxy</h5>
                                </article>
                                <article class="video" data-toggle="tooltip" data-placement="right" title="Monitor your entire app server and get in-depth visibility into key performance indicators of server.Server monitoring as a service for all your servers and devices. We support java servers. Please have a look this video for how to create of app groups and app and make them start.">
                                    <figure>
                                        <a class="fancybox fancybox.iframe" href="//www.youtube.com/embed/C4qzC5Ft6RU"><img class="videoThumb" src="https://img.youtube.com/vi/C4qzC5Ft6RU/maxresdefault.jpg"></a>
                                    </figure>
                                    <h5 class="videoTitle">&nbsp;16) Server Monitoring (Part 1)</h5>
                                </article>
                                <article class="video" data-toggle="tooltip" data-placement="right" title="Monitor your entire app server and get in-depth visibility into key performance indicators of server.Server monitoring as a service for all your servers and devices. We support java servers. Please have a look this video for how editing of app group and app details.">
                                    <figure>
                                        <a class="fancybox fancybox.iframe" href="//www.youtube.com/embed/w2RnYRHzQZM"><img class="videoThumb" src="https://img.youtube.com/vi/w2RnYRHzQZM/maxresdefault.jpg"></a>
                                    </figure>
                                    <h5 class="videoTitle">&nbsp;17) Server Monitoring (Part 2)</h5>
                                </article>
                                <article class="video" data-toggle="tooltip" data-placement="right" title="Monitor your entire app server and get in-depth visibility into key performance indicators of server.Server monitoring as a service for all your servers and devices. We support java servers. Please have a look this video for how to listing of apps group and app details.">
                                    <figure>
                                        <a class="fancybox fancybox.iframe" href="//www.youtube.com/embed/5BihokbW9JM"><img class="videoThumb" src="https://img.youtube.com/vi/5BihokbW9JM/maxresdefault.jpg"></a>
                                    </figure>
                                    <h5 class="videoTitle">&nbsp;18) Server Monitoring (Part 3)</h5>
                                </article>
                                
                                <article class="video" data-toggle="tooltip" data-placement="right" title="Monitor your entire app server and get in-depth visibility into key performance indicators of server.Server monitoring as a service for all your servers and devices. We support java servers. Please have a look this video for how to stop groups, app and remove app details.">
                                    <figure>
                                        <a class="fancybox fancybox.iframe" href="//www.youtube.com/embed/La204FLC3bA"><img class="videoThumb" src="https://img.youtube.com/vi/La204FLC3bA/maxresdefault.jpg"></a>
                                    </figure>
                                    <h5 class="videoTitle">&nbsp;19) Server Monitoring (Part 4)</h5>
                                </article><br>
                                
                                <article class="video" data-toggle="tooltip" data-placement="right" title="Certificate issuance is used for generating the certificate, and issuing it to respective user, we can also issue certificate for application server, by two ways certificate is generated 1) It create the user and issue the cert and 2) without creating the developer and adding it .">
                                    <figure>
                                        <a class="fancybox fancybox.iframe" href="//www.youtube.com/embed/Flstok_J-Yo"><img class="videoThumb" src="https://img.youtube.com/vi/Flstok_J-Yo/maxresdefault.jpg"></a>
                                    </figure>
                                    <h5 class="videoTitle">&nbsp;20) Certificate Issuer</h5>
                                </article>
                                <article class="video" data-toggle="tooltip" data-placement="right" title="Identify all certificates on your public-facing domains, including every SSL termination endpoint. Scan multiple networks and ports for internal certificates to find old certificates. And find much more with this video.">
                                    <figure>
                                        <a class="fancybox fancybox.iframe" href="//www.youtube.com/embed/PvUbuo5zUNg"><img class="videoThumb" src="https://img.youtube.com/vi/PvUbuo5zUNg/maxresdefault.jpg"></a>
                                    </figure>
                                    <h5 class="videoTitle">&nbsp;21) SSL Cert Discovery</h5>
                                </article>
                                <article class="video" data-toggle="tooltip" data-placement="right" title="This video explain, Now one line transactions supports Multi factor authentication. This service provide option for 2nd factor authentication with secure phrase. use can set his own phase as image with selected color and security sweet spot on created image and set sweet spot deviation for set image and sweet spot.">
                                    <figure>
                                        <a class="fancybox fancybox.iframe" href="//www.youtube.com/embed/hOGEHDxV9NM"><img class="videoThumb" src="https://img.youtube.com/vi/hOGEHDxV9NM/maxresdefault.jpg"></a>
                                    </figure>
                                    <h5 class="videoTitle">&nbsp;22) Dynamic Image Auth</h5>
                                </article>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>    
    <script src="scripts/video2/jquery.fancybox.js" type="text/javascript"></script>
    <script src="scripts/video2/global.min.js" type="text/javascript"></script>  
