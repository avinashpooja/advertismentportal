<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.GlobalStatus"%>
<%@page import="org.json.JSONObject"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.AdvertiserSubscriptionManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgAdvertiseSubscriptionDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgAdvertiserDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.AdvertiserAdManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgAdvertiserAdDetails"%>
<style>
    td {
  text-align: center;
  vertical-align: middle;
}
</style>
<div id="wrapper">
    <div class="col-lg-6">
        <div class="modal fade hmodal-success" id="todayExpenditure" tabindex="-1" role="dialog"  aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="color-line"></div>
                    <div class="modal-header" style="padding: 10px 0px !important">
                        <h4 id="todayCreditForAPILable" style="margin-left: 2%">Today Credit Used
                            <button class="close btn btn-default" data-dismiss="modal" aria-hidden="true" style="margin-top: 0%; margin-right: 3%">x</button></h4>                                    
                    </div>
                    <div class="modal-body" style="padding-bottom: 50px !important;text-align: center;">                                
                        <div id="todayCreditForAPI">                        
                        </div>
                        <div id="noRecordFoundData" style="display: none;text-align: center;" style="margin-bottom: 30%">
                            <img src="images/no_record_found.png" alt="No record found" width="300px" height="200px"/>
                        </div> 
                    </div>                                
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="modal fade hmodal-success" id="todayPerformance" tabindex="-1" role="dialog"  aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="color-line"></div>
                    <div class="modal-header" style="padding: 10px 0px !important">
                        <h4 id="todayPerformanceForAPILable" style="margin-left: 2%">Today Performance
                            <button class="close btn btn-default" data-dismiss="modal" aria-hidden="true" style="margin-top: 0%; margin-right: 3%">x</button></h4>                                    
                    </div>
                    <div class="modal-body" style="padding-bottom: 50px !important">                                
                        <div id="todayPerformanceForAPI">                        
                        </div>
                        <div id="noRecordFoundDataPerforamce" style="display: none; text-align: center" style="margin-bottom: 30%">
                            <img src="images/no_record_found.png" alt="No record found" width="300px" height="200px"/>
                        </div>                         
                    </div>                                
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="modal fade hmodal-success" id="monthlyPerformance" tabindex="-1" role="dialog"  aria-hidden="true">
            <div class="modal-dialog modal-lg" >
                <div class="modal-content" >
                    <div class="color-line"></div>
                    <div class="modal-header" style="padding: 10px 0px !important">
                        <h4  style="margin-left: 2%"> Monthly Performance <font id="monthlyPerformanceForAPILable"></font>
                            <button class="close btn btn-default" data-dismiss="modal" aria-hidden="true" style="margin-top: 0%; margin-right: 3%">x</button></h4>                                    
                    </div>
                    <div class="modal-body">
                        <div style="margin-bottom: 20px;">
                            <input type="hidden" id="adIdForMothlyPerformance" name="adIdForMothlyPerformance">
                            <input type="hidden" id="adTypeForMothlyPerformance" name="adTypeForMothlyPerformance">
                            <div class="col-sm-3">
                                <select class="form-control" id="_apiCallMonth" name="_apiCallMonth" onchange="generatePDFADPerformanceByMonth()">                                                                        
                                    <%                                        
                                        SimpleDateFormat format = new SimpleDateFormat("MMM");
                                        Calendar cal = Calendar.getInstance();
                                        cal.setTime(new Date());
                                        cal.add(Calendar.MONTH, -6);
                                        Calendar updatedCal = Calendar.getInstance();
                                        Date sixMonthPerformaceBack = cal.getTime();
                                        updatedCal.setTime(sixMonthPerformaceBack);                                        
                                        for (int month = 1; month <= 6; month++) {
                                            int calMonth = cal.get(Calendar.MONTH) + month;
                                            updatedCal.set(Calendar.MONTH, calMonth);
                                            Date upDate = updatedCal.getTime();
                                            String newMonth = format.format(upDate);
                                            if(newMonth.equalsIgnoreCase("Jan")){
                                                calMonth = 0;
                                            }
                                    %>
                                    <%if (month == 6) {%>
                                    <option value="<%=calMonth + 1%>" selected><%=newMonth%></option> 
                                    <%} else {%>
                                    <option value="<%=calMonth + 1%>"><%=newMonth%></option> 
                                    <%
                                            }
                                        }
                                    %>

                                </select>
                            </div>

                            <!--<div class="col-sm-3"><button id="generatePerformanceByMonth" class="btn btn-success btn-sm ladda-button btn-block" data-style="zoom-in" onclick="generatePerformanceByMonth()"><i class="fa fa-bar-chart"></i> Generate</button></div>-->
                        </div>
                        <br/>
                        <div id="monthlyPerformanceForAPI">                        
                        </div>
                        <div id="noRecordFoundDataPerformance" style="display: none;text-align: center" style="margin-bottom: 27%">
                            <img src="images/no_record_found.png" alt="No record found" width="300px" height="200px"/>
                        </div> 
                    </div>                                
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="modal fade hmodal-success" id="monthlyCredit" tabindex="-1" role="dialog"  aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="color-line"></div>
                    <div class="modal-header" style="padding: 10px 0px !important">
                        <h4 style="margin-left: 2%"> Monthly Credit Used<font id="monthlyCreditForAPILable" ></font>
                            <button class="close btn btn-default" data-dismiss="modal" aria-hidden="true" style="margin-top: 0%; margin-right: 3%">x</button></h4>                                    
                    </div>
                    <div class="modal-body" style="padding-bottom: 50px !important">                                
                        <div style="margin-bottom: 20px;" class="col-centered">
                            <input type="hidden" id="advertiserAdIdForMothlyCredit" name="advertiserAdIdForMothlyCredit">
                            <input type="hidden" id="adType" name="adType">
                            <div class="col-sm-3">
                                <select class="form-control" id="_apiCallMonthMonthlyCredit" name="_apiCallMonthMonthlyCredit" onchange="generateAdvertiserCreditByMonth()">                                                                        

                                    <%
                                        cal.setTime(new Date());
                                        cal.add(Calendar.MONTH, -6);
                                        Date sixMonthBack = cal.getTime();
                                        updatedCal.setTime(sixMonthBack);
                                        for (int month = 1; month <= 6; month++) {
                                            int calMonth = cal.get(Calendar.MONTH) + month;
                                            updatedCal.set(Calendar.MONTH, calMonth);
                                           
                                            Date upDate = updatedCal.getTime();
                                            String newMonth = format.format(upDate);
                                            if(newMonth.equalsIgnoreCase("Jan")){
                                                calMonth = 0;
                                            }
                                    %>
                                    <%if (month == 6) {%>
                                    <option value="<%=calMonth + 1%>" selected><%=newMonth%></option> 
                                    <%} else {%>
                                    <option value="<%=calMonth + 1%>"><%=newMonth%></option> 
                                    <%
                                            }
                                        }
                                    %>
                                </select>
                            </div>
                        </div>
                        <br><br>
                        <div id="monthlyCreditForAPI">                        
                        </div>
                        <div id="noRecordFoundDataMonthlyCredit" style="display: none ;text-align: center;" style="margin-bottom: 30%">
                            <img src="images/no_record_found.png" alt="No record found" width="300px" height="200px"/>
                        </div> 


                    </div>                                
                </div>
            </div>
        </div>
    </div>
</div>
<div class="content">
    <div class="row">
        <div class="col-md-12 tour-13">
            <div class="hpanel">
                <div class="panel-body">
                    <input type="text" class="form-control input-sm m-b-md" id="filter" placeholder="Search in table">                                        
                    <div class="table-responsive">
                        <table id="api" class="footable table table-stripped table-responsive" valign="middle" data-page-size="25" data-filter=#filter>    
                            <thead>
                                <tr>                                    
                                    <th style="text-align: center">Email Ad Logo</th>                                    
                                    <th style="text-align: center" class="tour-15">Ad Title</th>
                                    <th style="text-align: center" class="tour-15">Credits Used</th>
                                    <th style="text-align: center" class="tour-16">Performance</th>
                                    <th data-hide="phone,tablet" style="text-align: center">Credits</th>                                    
                                    <th style="text-align: center">Status</th>
                                    <th style="text-align: center" class="tour-17">Update</th>
                                </tr>
                            </thead>
                            <tbody>
                                <%
                                    SgAdvertiserDetails usrObj = (SgAdvertiserDetails) request.getSession().getAttribute("_advertisorDetails");
                                    SgAdvertiserAdDetails adDetails = new AdvertiserAdManagement().getAdvertiserDetails(usrObj.getAdId());
                                    String base64PDFImage = adDetails.getEmailAdImage(); String creditDeductionPerAd="NA"; String emailAdTitle = "NA";
                                    SgAdvertiseSubscriptionDetails subscriObject1 = new AdvertiserSubscriptionManagement().getAdvertiserSubscriptionbyAdId(usrObj.getAdId());                            
                                    if(subscriObject1 != null){
                                        String emailAdDetails = subscriObject1.getEmailAdConfiguration();
                                        JSONObject pdfJSON = new JSONObject(emailAdDetails);
                                        if(pdfJSON.has("emailCreditDeductionPerAd")){
                                            creditDeductionPerAd = pdfJSON.getString("emailCreditDeductionPerAd");
                                        }
                                    }
                                    if(adDetails.getEmailAdTitle() != null){
                                        emailAdTitle = adDetails.getEmailAdTitle();
                                    }
                                %>
                                <tr>                                   
                                    <td  style="font-size: 15px" style="" align="center" valign="middle">
                                        <image src="data:image/jpg;base64,<%=base64PDFImage%>" class="img-small" alt="PDF Ad Image">
                                    </td>                                                                                                                        
                                    <td>
                                        <p><%=emailAdTitle%></p>
                                    </td>
                                    <td style="" align="center" valign="middle">
                                        <a class="text-error ladda-button" data-style="zoom-in" id="todayCredit" onclick="todayCreditPDFAd('<%=adDetails.getAdvertiserAdId()%>','<%=GlobalStatus.PDF_AD%>')" type="button" href="#" data-toggle="tooltip" data-placement="right" title="Today Credit Used for"><i class="pe pe-7s-graph2 pe-3x"></i></a>                                                    
                                        <a class="text-primary-2 ladda-button" data-style="zoom-in" id="monthlyCredit" onclick="monthlyPDFAdCreditChart('<%=adDetails.getAdvertiserAdId()%>','<%=GlobalStatus.PDF_AD%>')" type="button" href="#" data-toggle="tooltip" data-placement="right" title="Monthly Credit Used for" style="margin-left: 10%"><i class="pe pe-7s-display1 pe-3x"></i></a>
                                    </td>
                                   
                                    <td style="" align="center" valign="middle">
                                        <a class="text-error ladda-button" data-style="zoom-in" id="todayPerformance" onclick="todayPDFAdPerformance('<%=adDetails.getAdvertiserAdId()%>','<%=GlobalStatus.PDF_AD%>')" type="button" href="#" data-toggle="tooltip" data-placement="right" title="Today Performance of "><i class="pe pe-7s-graph2 pe-3x"></i></a>                                                    
                                        <a class="text-primary-2 ladda-button" data-style="zoom-in" id="monthlyPerformance" onclick="monthlyPDFAdPerformance('<%=adDetails.getAdvertiserAdId()%>','<%=GlobalStatus.PDF_AD%>')" type="button" href="#" data-toggle="tooltip" data-placement="right" title="Monthly Performace of " style="margin-left: 10%"><i class="pe pe-7s-display1 pe-3x"></i></a>
                                    </td>
                                    <td style="" align="center" valign="middle">                                        
                                        <p style="font-size: 15px"><%=creditDeductionPerAd%> Per Ad</p>
                                    </td>
                                    <td style="" align="center" valign="middle">
                                        <%if(adDetails.getEmailAdStatus() == GlobalStatus.SENDTO_CHECKER){%>
                                        <p style="font-size: 15px" data-toggle="tooltip" data-placement="right" title="Sent to admin for approval"><i class="fa fa-thumbs-down fa-2x text-warning"></i> </p>
                                        <%}else if(adDetails.getEmailAdStatus() == GlobalStatus.REJECTED){%>
                                        <p style="font-size: 15px" data-toggle="tooltip" data-placement="right" title="Your Ad rejected"><i class="fa fa-thumbs-down fa-2x text-danger"></i> </p>
                                        <%}else{%>
                                        <p style="font-size: 15px" data-toggle="tooltip" data-placement="right" title="Your Ad approved"><i class="fa fa-thumbs-up fa-2x text-success"></i> </p>
                                        <%}%>
                                    </td> 
                                    <td style="" class="tour-iconAPIConsole" align="center" valign="middle">   
                                        <a onclick = "emailAD()" class="text-success" data-toggle="tooltip" data-placement="right" title="Update Ad" id="testFirstAPI"><i class="pe pe-7s-science pe-3x"></i></a>
                                    </td>                                    
                                </tr>                                
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="7">
                                        <ul class="pagination pull-right"></ul>
                                    </td>
                                </tr>
                            </tfoot>

                        </table>
                    </div>                   
                </div>
            </div>
        </div>
        <div class="col-md-12" style="margin-bottom: 28%"></div>
    </div>
</div>
<script>
    $(function () {
        $('#api').footable();
    });
</script>