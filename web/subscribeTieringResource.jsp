<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PackageSubscriptionManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.PartnerDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgSubscriptionDetails"%>
<%@page import="org.json.JSONArray"%>
<%@page import="org.json.JSONObject"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.MethodName"%>
<%@page import="java.util.List"%>
<%@page import="java.util.HashMap"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.Serializer"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.Classes"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.Methods"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.ResourceManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.ResourceDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.TransformDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.TransformManagement"%>
<%@page import="java.util.Map"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.Accesspoint"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.AccessPointManagement"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <div class="control-group">
        <label class="control-label"  for="username"></label>
        <div class="controls">
            <select id="_ResourceForTieringPricing21" name="_ResourceForTieringPricing21"  class="form-control span2" onchange="showSubscribeTieringVersion(this.value)" style="width: 100%">
                <%
                    String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
                    String channelId = (String) request.getSession().getAttribute("_ChannelId");
                    String apName = request.getParameter("_apName");
                    String packageName = request.getParameter("_packageName");
                    PartnerDetails partnerObj = (PartnerDetails) request.getSession().getAttribute("_partnerDetails");
                    SgSubscriptionDetails reqObj = new PackageSubscriptionManagement().getSubscriptionbyPartnerId(SessionId, channelId, partnerObj.getPartnerId());
                    if (!apName.equals("-1")) {                        
                        String apDetails  = reqObj.getTierRateDetails();
                        if(apDetails != null){
                        JSONArray jsOld = new JSONArray(apDetails);
                        String res = "";
                        for(int j = 0; j < jsOld.length(); j++){
                             JSONObject jsonexists1 = jsOld.getJSONObject(j);
                             
                              Iterator<String> keys = jsonexists1.keys();
                               while(keys.hasNext()){
                                String keyData = keys.next();
                                String[] keyDetails = keyData.split(":");
                                if(keyDetails[0].equals(apName)){
                                 res += keyDetails[1]+",";
                                }
                            }
                        }
                        if(!res.equals("")){                            
                            String[]  resourcesDetails = res.split(",");
                            if(resourcesDetails != null){
                            for(int i = 0; i < resourcesDetails.length; i++){   
                %>
                <option value="<%=resourcesDetails[i]%>"><%=resourcesDetails[i]%></option>               
                <%}
                    }
                      }
                        }
                    }else{%>                 
                <option value="-1">Select Resource</option>
                    <%} %>
                </select>           
        </div>
    </div>
                