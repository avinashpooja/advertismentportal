<html
	xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	</head>
	<body>
		<center>
			<table width="600" background="#FFFFFF" style="text-align:left;" cellpadding="0" cellspacing="0">
				<tr>
					<td height="18" width="31" style="border-bottom:1px solid #e4e4e4;">
						<div style="line-height: 0px; font-size: 1px; position: absolute;">&nbsp;</div>
					</td>
					<td height="18" width="131">
						<div style="line-height: 0px; font-size: 1px; position: absolute;">&nbsp;</div>
					</td>
					<td height="18" width="466" style="border-bottom:1px solid #e4e4e4;">
						<div style="line-height: 0px; font-size: 1px; position: absolute;">&nbsp;</div>
					</td>
				</tr>
				<tr>
					<td height="2" width="31" style="border-bottom:1px solid #e4e4e4;">
						<div style="line-height: 0px; font-size: 1px; position: absolute;">&nbsp;</div>
					</td>
					<td height="2" width="131">
						<div style="line-height: 0px; font-size: 1px; position: absolute;">&nbsp;</div>
					</td>
					<td height="2" width="466" style="border-bottom:1px solid #e4e4e4;">
						<div style="line-height: 0px; font-size: 1px; position: absolute;">&nbsp;</div>
					</td>
				</tr>
				<!--GREEN STRIPE-->
				<tr>
					<td background="#greenbackGIF#" width="31" bgcolor="#45a853" style="border-top:1px solid #FFF; border-bottom:1px solid #FFF;" height="113">
						<div style="line-height: 0px; font-size: 1px; position: absolute;">&nbsp;</div>
					</td>
					<!--WHITE TEXT AREA-->
					<td width="131" bgcolor="#FFFFFF" style="border-top:1px solid #FFF; text-align:center;" height="113" valign="middle">
						<span style="font-size:25px; font-family:Trebuchet MS, Verdana, Arial; color:#2e8a3b;">Ready APIs</span>
					</td>
					<!--GREEN TEXT AREA-->
					<td background="#greenbackGIF#" bgcolor="#45a853" style="border-top:1px solid #FFF; border-bottom:1px solid #FFF; padding-left:15px;" height="113">
						<span style="color:#FFFFFF; font-size:18px; font-family:Trebuchet MS, Verdana, Arial;">Thank you for signing with us.</span>
					</td>
				</tr>
				<!--DOUBLE BORDERS BOTTOM-->
				<tr>
					<td height="3" width="31" style="border-top:1px solid #e4e4e4; border-bottom:1px solid #e4e4e4;">
						<div style="line-height: 0px; font-size: 1px; position: absolute;">&nbsp;</div>
					</td>
					<td height="3" width="131">
						<div style="line-height: 0px; font-size: 1px; position: absolute;">&nbsp;</div>
					</td>
					<td height="3" style="border-top:1px solid #e4e4e4; border-bottom:1px solid #e4e4e4;">
						<div style="line-height: 0px; font-size: 1px; position: absolute;">&nbsp;</div>
					</td>
				</tr>
				<tr>
					<td colspan="3">
						<!--CONTENT STARTS HERE-->
						<br />
						<br />
						<table cellpadding="0" cellspacing="0">
							<tr>
								<td width="15">
									<div style="line-height: 0px; font-size: 1px; position: absolute;">&nbsp;</div>
								</td>
								<td width="325" style="padding-right:10px; font-family:Trebuchet MS, Verdana, Arial; font-size:12px;" valign="top">
									<span style="font-family:Trebuchet MS, Verdana, Arial; font-size:17px; font-weight:bold;">Welcome!</span>
									<br />
									<p>You are successfully sign in and can look forward to receiving:</p>
									<br />
									<div style="padding-left:20px; padding-bottom:10px;">
										<img src="#spadeGIF#" alt=""/>&nbsp;&nbsp;&nbsp;Benefit of 16 Services under one roof.
									</div>
									<div style="padding-left:20px; padding-bottom:10px;">
										<img src="#spadeGIF#" alt=""/>&nbsp;&nbsp;&nbsp;Benefit of Special designed API Console.
									</div>
									<div style="padding-left:20px; padding-bottom:10px;">
										<img src="#spadeGIF#" alt=""/>&nbsp;&nbsp;&nbsp;Benefit of Credit as per API Usage
									</div>
									<p>You can login to your Ready APIs Portal with the following credentials:</p>
									<br />
									<div style="padding-left:20px; padding-bottom:10px;">
										<img src="#spadeGIF#" alt=""/>&nbsp;&nbsp;&nbsp;Email&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;#email#.
									</div>
									<div style="padding-left:20px; padding-bottom:10px;">
										<img src="#spadeGIF#" alt=""/>&nbsp;&nbsp;&nbsp;Password&nbsp;&nbsp;&nbsp;#password#
									</div>
									<p>In the meantime, you can 
										<a href="#webSiteURL#" target="_blank">return to Ready APIs</a> to continue Service usage.
									</p>Best Regards,
									<br/>Ready APIs Team
									<br/>
									<br/>This welcome email was sent to {#email#} because you recently signed up for Ready APIs.
								</td>
								<td style="border-left:1px solid #e4e4e4; padding-left:15px;" valign="top">
									<!--RIGHT COLUMN FIRST BOX-->
									<table width="100%" cellpadding="0" cellspacing="0" style="border-bottom:1px solid #e4e4e4; font-family:Trebuchet MS, Verdana, Arial; font-size:12px;">
										<tr>
											<td>
												<div style="font-family:Trebuchet MS, Verdana, Arial; font-size:17px; font-weight:bold; padding-bottom:10px;">Add Us To Your Address Book</div>
												<img src="#addressbookGIF#" align="right" style="padding-left:10px; padding-top:10px; padding-bottom:10px;" alt=""/>
												<p>If you are interested in learning more about our products & solutions or having an authorized channel partner contact you with more information, please add this address to your address book or contacts list: 
													<a href="#enquiryId#">#enquiryEmailLabel#</a>.
												</p>
												<br />
											</td>
										</tr>
									</table>
									<!--RIGHT COLUMN SECOND BOX-->
									<br />
									<table width="100%" cellpadding="0" cellspacing="0" style="border-bottom:1px solid #e4e4e4; font-family:Trebuchet MS, Verdana, Arial; font-size:12px;">
										<tr>
											<td>
												<div style="font-family:Trebuchet MS, Verdana, Arial; font-size:17px; font-weight:bold; padding-bottom:10px;">Have Any Questions?</div>
												<img src="#penpaperGIF#" align="right" style="padding-left:10px; padding-top:10px; padding-bottom:10px;" alt=""/>
												<p>Don't hesitate to ask any question we'll be here to help you with any step along the way. Please contact 
													<a href="#supportId#">#supportEmailLabel#</a>
												</p>
												<br />
											</td>
										</tr>
									</table>
									<!--RIGHT COLUMN THIRD BOX-->
									<br />
									<table cellpadding="0" width="100%" cellspacing="0" style="font-family:Trebuchet MS, Verdana, Arial; font-size:12px;">
										<tr>
											<td>
												<div style="font-family:Trebuchet MS, Verdana, Arial; font-size:17px; font-weight:bold; padding-bottom:10px;">Have A Topic Idea?</div>
												<img src="#lightbulbGIF#" align="right" style="padding-left:10px; padding-top:10px; padding-bottom:10px;" alt=""/>
												<p>We'd love to hear it! Just reply any time and let us know what topics you'd like to know more about at 
													<a href="#ideaId#">#ideaEmailLabel#</a>. 
												</p>
												<br />
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<br />
                        #emailAd#
			<table cellpadding="0" style="border-top:1px solid #e4e4e4; font-family:Trebuchet MS, Verdana, Arial; font-size:12px;" cellspacing="0" width="600">
				<tr>
					<td height="2" style="border-bottom:1px solid #e4e4e4;">
						<div style="line-height: 0px; font-size: 1px; position: absolute;">&nbsp;</div>
					</td>
				</tr>
				<td style="font-family:Trebuchet MS, Verdana, Arial; font-size:12px;">
					<br />
					<b style="font-size: 110%">Our Offices:</b>
					<br/>Malaysia Support Office: Suite Ex5, A-5-10 Empire Tower SS16/1, Subang Jaya 47500, Selangor, Malaysia
					<br/>Australia Office: 36/118 Adderton Road, Carlingford 2118, Sydney, NSW, Australia
					<br/>India Support Office: 321 office number, amanora chamber , east , hadapsar , Pune 411028, India
					<br />
					<br />
					<b style="font-size: 110%">DISCLAIMER:</b>
					<br/>This e-mail with any attachment ("Message") are only for the intended recipient's use and may contain confidential and/or privileged information. Anyone other than intended recipient(s) taking any action in reliance upon, or any review, retransmission, dissemination, distribution, printing and/or copying of this Message or any part there of is strictly prohibited.If this Message is received in error, delete it immediately
					<br />
				</td>
			</tr>
		</table>
	</center>
</body></html>