package com.mollatech.axiom.v2.face.handler.secure.phrase;

import com.mollatech.axiom.v2.core.rss.RssUserCerdentials;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.font.FontRenderContext;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.sf.jmimemagic.Magic;
import net.sf.jmimemagic.MagicException;
import net.sf.jmimemagic.MagicMatch;
import net.sf.jmimemagic.MagicMatchNotFoundException;
import net.sf.jmimemagic.MagicParseException;
import org.bouncycastle.util.encoders.Base64;

@WebServlet(name = "editImage", urlPatterns = { "/editImage" })
public class editImage extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            response.setContentType("image/gif");
            RssUserCerdentials rssUserObj = (RssUserCerdentials) request.getSession().getAttribute("_rssDemoUserCerdentials");
            String _select_color = request.getParameter("_select_color");
            String _securePhrase = request.getParameter("_securePhrase");
            String sampleText = _securePhrase;
            Font font = new Font("Arial", Font.PLAIN, 48);
            FontRenderContext frc = new FontRenderContext(null, true, true);
            java.awt.geom.Rectangle2D bounds = font.getStringBounds(sampleText, frc);
            int w = (int) bounds.getWidth();
            int h = (int) bounds.getHeight();
            BufferedImage image = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
            Graphics2D g = image.createGraphics();
            if (_select_color != null) {
                g.setColor(new Color(Integer.valueOf(_select_color.substring(1, 3), 16), Integer.valueOf(_select_color.substring(3, 5), 16), Integer.valueOf(_select_color.substring(5, 7), 16)));
            }
            g.fillRect(0, 0, w, h);
            g.setColor(Color.WHITE);
            g.setFont(font);
            g.drawString(sampleText, (float) bounds.getX(), (float) -bounds.getY());
            g.dispose();
            String savepath = System.getProperty("catalina.home");
            if (savepath == null) {
                savepath = System.getenv("catalina.home");
            }
            savepath += System.getProperty("file.separator");
            savepath += "serviceguard-settings";
            savepath += System.getProperty("file.separator");
            savepath += "user-phrase";
            savepath += System.getProperty("file.separator");
            File outputfile = new File(savepath + rssUserObj.getRssUser().getUserName() + ".png");
            ImageIO.write(image, "png", outputfile);
            ByteArrayOutputStream baos = new ByteArrayOutputStream(1000);
            BufferedImage img = ImageIO.read(new File(savepath + rssUserObj.getRssUser().getUserName() + ".png"));
            ImageIO.write(img, "png", baos);
            baos.flush();
            byte[] base64String = Base64.encode(baos.toByteArray());
            baos.close();
            File f = new File(savepath + rssUserObj.getRssUser().getUserName() + ".png");
            f.delete();
            byte[] base64Image = base64String;
            if (base64Image == null) {
                return;
            }
            byte[] plainImage = Base64.decode(base64Image);
            MagicMatch match = Magic.getMagicMatch(plainImage, true);
            String mimeType = match.getMimeType();
            if (mimeType.startsWith("image")) {
                response.setContentType(mimeType);
                response.setContentLength(plainImage.length);
                OutputStream out = response.getOutputStream();
                out.write(plainImage, 0, plainImage.length);
            } else {
                return;
            }
        } catch (MagicParseException ex) {
            Logger.getLogger(editImage.class.getName()).log(Level.SEVERE, null, ex);
        } catch (MagicMatchNotFoundException ex) {
            Logger.getLogger(editImage.class.getName()).log(Level.SEVERE, null, ex);
        } catch (MagicException ex) {
            Logger.getLogger(editImage.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
