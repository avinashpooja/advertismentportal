package com.mollatech.axiom.v2.face.handler.secure.phrase;

import axiom.web.service.AxiomWrapper;
import com.mollatech.axiom.v2.core.rss.AxiomCredentialDetails;
import com.mollatech.axiom.v2.core.rss.AxiomStatus;
import com.mollatech.axiom.v2.core.rss.RssUserCerdentials;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author ashish
 */
@WebServlet(name = "AssignAndSendRegCode", urlPatterns = { "/AssignAndSendRegCode" })
public class AssignAndSendRegCode extends HttpServlet {

    public static final int PASSWORD = 1;

    public static final int SOFTWARE_TOKEN = 2;

    public static final int HARDWARE_TOKEN = 3;

    public static final int OUTOFBOUND_TOKEN = 4;

    public static final int SW_WEB_TOKEN = 1;

    public static final int SW_MOBILE_TOKEN = 2;

    public static final int SW_PC_TOKEN = 3;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        AxiomStatus aStatus = null;
        PrintWriter out = response.getWriter();
        AxiomWrapper axWrapper = new AxiomWrapper();
        String tokenType = request.getParameter("tokenType");
        response.setContentType("application/json");
        String result = "success";
        String message = "Web Token updated successfully";
        JSONObject json = new JSONObject();
        String sessionId = null;
        if (request.getSession().getAttribute("_userSessinId") == null) {
            json.put("_result", "error");
            json.put("_message", "Invalid Session");
            response.setContentType("application/json");
            out.print(json);
            return;
        }
        sessionId = request.getSession().getAttribute("_userSessinId").toString();
        RssUserCerdentials _userObj = (RssUserCerdentials) request.getSession().getAttribute("_rssDemoUserCerdentials");
        if (_userObj == null) {
            json.put("_result", "error");
            json.put("_message", "User not found");
            response.setContentType("application/json");
            out.print(json);
            return;
        }
        AxiomCredentialDetails axiomCredentialDetails = new AxiomCredentialDetails();
        axiomCredentialDetails.setCategory(SOFTWARE_TOKEN);
        axiomCredentialDetails.setSubcategory(SW_MOBILE_TOKEN);
        _userObj.getTokenDetails().add(axiomCredentialDetails);
        RssUserCerdentials rssCred = new RssUserCerdentials();
        rssCred.getTokenDetails().add(axiomCredentialDetails);
        rssCred.setRssUser(_userObj.getRssUser());
        aStatus = axWrapper.AssignCredential(sessionId, rssCred, null, null);
        if (aStatus.getErrorcode() == 0) {
            aStatus = axWrapper.sendNotification(sessionId, _userObj.getRssUser().getUserId(), SOFTWARE_TOKEN, null, null);
        }
        if (aStatus != null && aStatus.getErrorcode() != 0) {
            json.put("_result", "error");
            json.put("_message", "Mobile Token Assignment Failed");
            response.setContentType("application/json");
            out.print(json);
            return;
        }
        System.out.println("aStatus.getRegcode() :" + aStatus.getRegcode());
        String msgToSend = aStatus.getRegcode();
        String userIDToSend = request.getSession().getAttribute("_username").toString();
        boolean bActCodeSendStatus = false;
        if (aStatus.getErrorcode() == 0) {
            bActCodeSendStatus = true;
        }
        if (bActCodeSendStatus == true) {
            request.getSession().setAttribute("_apActivationMessage", aStatus.getRegcode());
            request.getSession().setAttribute("ApmessageStatus", "Sent");
            request.getSession().setAttribute("_rssWebUserCerdentials", _userObj);
            request.getSession().setAttribute("_rssDemoUserCerdentials", _userObj);
            json.put("_result", "success");
            json.put("_message", "Registration Code is send to your registered mobile phone number");
            JSONObject jnew = new JSONObject();
            response.setContentType("application/json");
            out.print(json);
            return;
        } else {
            result = "error";
            message = "Failed to send registration code";
            json.put("_result", result);
            json.put("_message", message);
            response.setContentType("application/json");
            out.print(json);
            return;
        }
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
                processRequest(request, response);
       
    }


    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
                processRequest(request, response);
       
    }


    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
