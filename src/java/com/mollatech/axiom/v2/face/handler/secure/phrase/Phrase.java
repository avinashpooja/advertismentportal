package com.mollatech.axiom.v2.face.handler.secure.phrase;

import java.util.Map;
import java.util.TreeMap;

public class Phrase {

    final Map<String, String> map = new TreeMap<String, String>(String.CASE_INSENSITIVE_ORDER);

    final Map<String, String> color = new TreeMap<String, String>(String.CASE_INSENSITIVE_ORDER);

    public Phrase() {
    }

    public String[] getPhrase() {
        map.put("1", "LoveIsGOD");
        map.put("2", "BMW19999");
        map.put("3", "India Shining");
        map.put("4", "Love Life");
        map.put("5", "Life Is Beautiful");
        map.put("6", "Engineers");
        String[] result = map.keySet().toArray(new String[0]);
        return result;
    }

    public String[] getColor() {
        color.put("BLUE", "1");
        color.put("GREEN", "2");
        color.put("RED", "3");
        color.put("GRAY", "4");
        color.put("ORANGE", "5");
        color.put("YELLOW", "6");
        color.put("MAGENTA", "7");
        String[] result = color.keySet().toArray(new String[0]);
        return result;
    }

    public String getCode(String country) {
        String countryFound = map.get(country);
        if (countryFound == null) {
            countryFound = "UK";
        }
        return countryFound;
    }

    public String getColor(String country) {
        String countryFound = color.get(country);
        if (countryFound == null) {
            countryFound = "1";
        }
        return countryFound;
    }
}
