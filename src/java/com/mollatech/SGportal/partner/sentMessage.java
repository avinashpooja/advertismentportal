/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.SGportal.partner;

import com.mollatech.service.nucleus.crypto.LoadSettings;
import com.mollatech.serviceguard.connector.communication.SGStatus;
import com.mollatech.serviceguard.nucleus.commons.MSConfig;
import com.mollatech.serviceguard.nucleus.db.Channels;
import com.mollatech.serviceguard.nucleus.db.connector.ChannelsUtils;
import com.mollatech.serviceguard.nucleus.db.connector.RemoteAccessUtils;
import com.mollatech.serviceguard.nucleus.db.connector.SessionFactoryUtil;
import com.mollatech.serviceguard.nucleus.db.connector.management.SessionManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.serviceguard.nucleus.settings.SendNotification;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.json.JSONObject;

/**
 *
 * @author abhishekingle
 */
@WebServlet(name = "sentMessage", urlPatterns = {"/sentMessage"})
public class sentMessage extends HttpServlet {

    public static final int PENDING = 2;

    public static final int SENT = 0;

    public static final int SEND = 0;

    public static final int RESET = 1;

    public static final int completed = 6;

    static final Logger logger = Logger.getLogger(sentMessage.class);

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException{
        response.setContentType("application/json");
        String channelid = (String) request.getSession().getAttribute("_ChannelId");
        String sessionId = (String) request.getSession().getAttribute("_partnerSessionId");
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        String result = "success";
        String message = "You message sent successfully";
        String name = request.getParameter("name");
        logger.debug("Value of name  = " + name);
        String email = request.getParameter("email");
        logger.debug("Value of email  = " + email);
        String messageUser = request.getParameter("message");
        logger.debug("Value of No. messageUser  = " + messageUser);
        SessionManagement sManagement = new SessionManagement();
        String _channelName = "ServiceGuard";
        SessionFactoryUtil suChannel = new SessionFactoryUtil(SessionFactoryUtil.channels);
        Session sChannel = suChannel.openSession();
        ChannelsUtils cUtil = new ChannelsUtils(suChannel, sChannel);
        SessionFactoryUtil suRemoteAcess = new SessionFactoryUtil(SessionFactoryUtil.remoteaccess);
        Session sRemoteAcess = suRemoteAcess.openSession();
        RemoteAccessUtils rUtil = new RemoteAccessUtils(suRemoteAcess, sRemoteAcess);
        Channels channel = cUtil.getChannel(_channelName);
        String[] credentialInfo = rUtil.GetRemoteAccessCredentials(channel.getChannelid());
        String SessionId = sManagement.OpenSession(channel.getChannelid(), credentialInfo[0], credentialInfo[1], request.getSession().getId());
        try {
            if (name == null || email == null || messageUser == null) {
                result = "error";
                message = "Please fill all details.";
                json.put("result", result);
                json.put("message", message);
                return;
            } else if (name.isEmpty() || email.isEmpty() || messageUser.isEmpty()) {
                result = "error";
                message = "Please fill all details.";
                json.put("result", result);
                json.put("message", message);
                return;
            }
            String regex = "^[_A-Za-z0-9-]+(\\\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\\\.[A-Za-z0-9]+)*(\\\\.[A-Za-z]{2,})$";
            Pattern pattern = Pattern.compile(regex);
            Matcher matcher = pattern.matcher((CharSequence) email);
            if (!matcher.matches()) {
                result = "error";
                message = "Please enter correct email address.";
                json.put("result", result);
                json.put("message", message);
                return;
            }
            Date d = null;
            String tmessage = (String) LoadSettings.g_templateSettings.getProperty("email.contactUs.byDeveloper");
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy hh:mm:ss");
            MSConfig config = (MSConfig) new SettingsManagement().getSetting(SessionId, channel.getChannelid(), SettingsManagement.MATERSLAVE, SettingsManagement.PREFERENCE_ONE);
            String schemes = "http";
            if (config != null) {
                if (config.apssl.equalsIgnoreCase("yes")) {
                    schemes = "https";
                }
            }
            String path = request.getContextPath();
            String greenbackGIF = schemes + "://" + config.aphostIp + ":" + config.aphostPort + path + "/images/email/greenback.gif";
            String spadeGIF = schemes + "://" + config.aphostIp + ":" + config.aphostPort + path + "/images/email/spade.gif";
            String addressbookGIF = schemes + "://" + config.aphostIp + ":" + config.aphostPort + path + "/images/email/addressbook.gif";
            String penpaperGIF = schemes + "://" + config.aphostIp + ":" + config.aphostPort + path + "/images/email/penpaper.gif";
            String lightbulbGIF = schemes + "://" + config.aphostIp + ":" + config.aphostPort + path + "/images/email/lightbulb.gif";
            String enquiryId = (String) LoadSettings.g_sSettings.getProperty("email.enquiry");
            String supportId = (String) LoadSettings.g_sSettings.getProperty("email.question");
            String ideaId = (String) LoadSettings.g_sSettings.getProperty("email.idea");
            String supportEmail = (String) LoadSettings.g_sSettings.getProperty("support.email.id");
            String[] enquiryEmailDetails = enquiryId.split(":");
            String[] supportEmailDetails = supportId.split(":");
            String[] ideaEmailDetails = ideaId.split(":");
            if (tmessage != null) {
                d = new Date();
                tmessage = tmessage.replace("#name#", name);
                tmessage = tmessage.replace("#message#", messageUser);
                tmessage = tmessage.replaceAll("#enquiryId#", enquiryId);
                tmessage = tmessage.replaceAll("#supportId#", supportId);
                tmessage = tmessage.replaceAll("#ideaId#", ideaId);
                tmessage = tmessage.replaceAll("#enquiryEmailLabel#", enquiryEmailDetails[1]);
                tmessage = tmessage.replaceAll("#supportEmailLabel#", supportEmailDetails[1]);
                tmessage = tmessage.replaceAll("#ideaEmailLabel#", ideaEmailDetails[1]);
                tmessage = tmessage.replace("#greenbackGIF#", greenbackGIF);
                tmessage = tmessage.replace("#spadeGIF#", spadeGIF);
                tmessage = tmessage.replace("#addressbookGIF#", addressbookGIF);
                tmessage = tmessage.replace("#penpaperGIF#", penpaperGIF);
                tmessage = tmessage.replace("#lightbulbGIF#", lightbulbGIF);

            }
//            int productType = 3;
//            SGStatus status = new SendNotification().SendEmail(channel.getChannelid(), supportEmail, "Ready APIs, Message from developer", tmessage, null, null, null, null, productType);
//            if (status.iStatus == PENDING || status.iStatus == SENT) {
//                json.put("result", result);
//                logger.debug("You message sent successfully" + result);
//                json.put("message", message);
//                logger.debug("You message sent successfully " + message);
//            } else {
//                result = "error";
//                message = "You message failed to sent.";
//                try {
//                    json.put("result", result);
//                    logger.debug("Response of sentMessage Servlet's Parameter  result is " + result);
//                    json.put("message", message);
//                    logger.debug("Response of sentMessage Servlet's Parameter  message is " + message);
//                } catch (Exception e) {
//                    e.printStackTrace();
//                    logger.info("Exception at #sentMessage from #PPortal " + e);
//                }
//            }
            SendMSGNotification msgNotification = new SendMSGNotification(tmessage, channel.getChannelid(), SessionId, supportEmail);
            Thread signupNotificationThread = new Thread(msgNotification);
            signupNotificationThread.start();
            json.put("result", result);
            logger.debug("You message sent successfully" + result);
            json.put("message", message);
            logger.debug("You message sent successfully " + message);
            return;
        } catch (Exception e) {
            e.printStackTrace();
            logger.info("Exception at #sentMessage from #PPortal " + e);
            result = "error";
            message = "You message failed to sent.";
            json.put("result", result);
            json.put("message", message);

        } finally {
            logger.info("Response of #sentMessage from #PPortal " + json.toString());
            logger.info("Response of #sentMessage from #PPortal Servlet at " + new Date());
            out.print(json);
            out.flush();
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
               processRequest(request, response);
       
    }


    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
               processRequest(request, response);
       
    }


    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

class SendMSGNotification implements Runnable {

    String email;
    String tmessage;
    String channels;
    String sessionId;
    public static final int PENDING = 2;
    public static final int SEND = 0;

    public SendMSGNotification() {

    }

    public SendMSGNotification(String message, String channel, String sessionId, String email) {
        this.email = email;
        this.tmessage = message;
        this.channels = channel;
        this.sessionId = sessionId;
    }
    String mimeType[] = {"application/octet-stream"};
    int productType = 3;

    public void run() {
        SGStatus status = new SendNotification().SendEmail(channels, email, "Ready APIs, Message from developer", tmessage, null, null, null, mimeType, productType);
        if (status.iStatus == PENDING || status.iStatus == SEND) {
            System.out.println("Message Email sent successfully with retCode " + status.iStatus + " to" + email + " at " + new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(new Date()));
        } else {
            System.out.println("Message Email failed to sent with retCode " + status.iStatus + " to " + email + " at " + new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(new Date()));
        }

    }
}
