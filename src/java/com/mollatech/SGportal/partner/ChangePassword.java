package com.mollatech.SGportal.partner;

import com.mollatech.serviceguard.nucleus.db.SgUsers;
import com.mollatech.serviceguard.nucleus.db.connector.management.UsersManagement;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author Bluebricks
 */
@WebServlet(name = "ChangePassword", urlPatterns = {"/ChangePassword"})
public class ChangePassword extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        JSONObject json = new JSONObject();
        String result = "success";
        String message = "Password updated successfully";
        PrintWriter out = response.getWriter();

        String current_password = request.getParameter("current_password");
        String new_password = request.getParameter("new_password");
        String confirm_password = request.getParameter("confirm_password");
        String ChannelId = (String) request.getSession().getAttribute("_ChannelId");
        String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");

        if (new_password.equals("") || confirm_password.equals("")) {
            result = "error";
            message = "Please Enter Password";
        } else if (new_password.equals(confirm_password)) {
            String userId = (String) request.getSession().getAttribute("_rssuserid");
            if (userId != null) {
                if (SessionId != null) {
                    UsersManagement um = new UsersManagement();
                    String present_password = um.getPassword(SessionId, ChannelId, userId);
                    if (present_password == current_password || present_password.equals(current_password)) {
                        int res = um.SetPassword(ChannelId, userId, new_password);
                        if (res == 0) {
                            SgUsers rUser = (SgUsers) request.getSession().getAttribute("_SgUsers");
                            rUser.setPassword(new_password);
                            request.getSession().setAttribute("_SgUsers",rUser);
                            result = "success";
                            message = "Password updated successfully";
                        } else {
                            result = "error";
                            message = "Password updation failed";
                        }
                    } else {
                        result = "error";
                        message = "Current Password is not Valid";
                    }
                } else {
                    result = "error";
                    message = "Password updation failed";
                }
            } else {
                result = "error";
                message = "Password updation failed";
            }
        } else {
            result = "error";
            message = "Password Not Matched";
        }
        try {
            json.put("_result", result);
            json.put("_message", message);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            out.print(json);
            out.flush();
        }
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
