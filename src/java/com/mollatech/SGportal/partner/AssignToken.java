package com.mollatech.SGportal.partner;

import com.mollatech.serviceguard.nucleus.commons.CommonUtility;
import com.mollatech.serviceguard.nucleus.db.PartnerDetails;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.bouncycastle.util.encoders.Base64;
import org.json.JSONObject;

/**
 *
 * @author mohanish
 */
@WebServlet(name = "AssignToken", urlPatterns = { "/AssignToken" })
public class AssignToken extends HttpServlet {

    static final Logger logger = Logger.getLogger(AssignToken.class);

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.info("Request servlet is #EditAPPrice from #PPAdmin at " + new Date());
        logger.info("Remote Ip Address is " + request.getRemoteAddr());
        response.setContentType("application/json");
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        String result = "success";
        String message = "Token have been assigned successfully";
        String channelId = (String) request.getSession().getAttribute("_channelId");
        String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
        PartnerDetails partnerObj = (PartnerDetails) request.getSession().getAttribute("_partnerDetails");
        String apName = request.getParameter("apName");
        logger.debug("Value of apName  = " + apName);
        String resourceName = request.getParameter("resourceName");
        logger.debug("Value of resourceName  = " + resourceName);
        String version = request.getParameter("version");
        logger.debug("Value of No. version  = " + version);
        String apiName = request.getParameter("apiName");
        logger.debug("Value of AP Name  = " + apiName);
        String token = getToken(SessionId, partnerObj);
        if (token == null) {
            result = "error";
            message = "Token generation failed";
            try {
                json.put("result", result);
                logger.debug("Response of AssignToken Servlet's Parameter  result is " + result);
                json.put("message", message);
                logger.debug("Response of AssignToken Servlet's Parameter  message is " + message);
            } catch (Exception e) {
                logger.error("Exception at AssignToken ", e);
            }
            out.print(json);
            out.flush();
            out.close();
            return;
        }
    }

    private String getToken(String sessionID, PartnerDetails partnerObj) {
        String apitoken = "";
        if (partnerObj != null) {
            apitoken = partnerObj.getPartnerName() + partnerObj.getPartnerEmailid() + new Date() + sessionID + partnerObj.getPartnerPhone();
            byte[] SHA1hash = CommonUtility.SHA1(apitoken);
            apitoken = new String(Base64.encode(SHA1hash));
            return apitoken;
        }
        return null;
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
