/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.SGportal.partner;

import com.mollatech.serviceguard.nucleus.db.Channels;
import com.mollatech.serviceguard.nucleus.db.SgUsers;
import com.mollatech.serviceguard.nucleus.db.connector.ChannelsUtils;
import com.mollatech.serviceguard.nucleus.db.connector.RemoteAccessUtils;
import com.mollatech.serviceguard.nucleus.db.connector.SessionFactoryUtil;
import com.mollatech.serviceguard.nucleus.db.connector.management.SessionManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.UsersManagement;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.Session;
import org.json.JSONObject;

/**
 *
 * @author mohanish
 */
@WebServlet(name = "TMForgetPassword", urlPatterns = {"/TMForgetPassword"})
public class TMForgetPassword extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        JSONObject json     = new JSONObject();
        String result       = "success";
        String message      = "Password updated successfully";
        PrintWriter out     = response.getWriter();
        String _channelName = "ServiceGuard";
        SessionFactoryUtil suChannel = new SessionFactoryUtil(SessionFactoryUtil.channels);
        Session sChannel = suChannel.openSession();
        ChannelsUtils cUtil = new ChannelsUtils(suChannel, sChannel);
        Channels channel = cUtil.getChannel(_channelName);
        SessionFactoryUtil suRemoteAcess = new SessionFactoryUtil(SessionFactoryUtil.remoteaccess);
        Session sRemoteAcess = suRemoteAcess.openSession();
        RemoteAccessUtils rUtil = new RemoteAccessUtils(suRemoteAcess, sRemoteAcess);
        String[] credentialInfo = rUtil.GetRemoteAccessCredentials(channel.getChannelid());
        SessionManagement sManagement = new SessionManagement();
        String sessionId        = sManagement.OpenSession(channel.getChannelid(), credentialInfo[0], credentialInfo[1], request.getSession().getId());
        String userName         = request.getParameter("fpUsername");
        String emailID          = request.getParameter("fpEmailID");
        String new_password     = request.getParameter("newPassword");        
        String confirm_password = request.getParameter("confirmPassword");
        String userId           = request.getParameter("userId");
        int retValue = -1;
        if (new_password.equals(confirm_password)) {
            UsersManagement um  = new UsersManagement();
            SgUsers userDetails = um.getSgUsersByUserId(sessionId, channel.getChannelid(), userId);
            if(userDetails != null){
                userDetails.setPassword(new_password);
                retValue = um.editSgUsers(sessionId, channel.getChannelid(), userDetails);
                if(retValue == 0){
                    result = "success";
                    message = "Password updated successfully";
                }
            }else{
                result = "error";
                message = "Password updation failed";
            }
        }else {
            result = "error";
            message = "Password Not Matched";
        }
        try {
            json.put("_result", result);
            json.put("_message", message);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            out.print(json);
            out.flush();
        }
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
