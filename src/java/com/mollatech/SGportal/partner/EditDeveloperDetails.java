/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.SGportal.partner;

import com.mollatech.serviceguard.nucleus.db.PartnerDetails;
import com.mollatech.serviceguard.nucleus.db.SgUsers;
import com.mollatech.serviceguard.nucleus.db.connector.management.PartnerManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.UsersManagement;
import com.sun.org.apache.xml.internal.security.utils.Base64;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author abhishekingle
 */
@WebServlet(name = "EditDeveloperDetails", urlPatterns = {"/EditDeveloperDetails"})
public class EditDeveloperDetails extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        JSONObject json = new JSONObject();
        String result = "success";
        String message = "Your details have been saved successfully.";
        PrintWriter out = response.getWriter();
        String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
        
        String partnerId = request.getParameter("partnerId");
        String partName = request.getParameter("partnerName");
        String mobileNo = request.getParameter("mobileNo");
        String emailId = request.getParameter("emailId");
        String addr1 = request.getParameter("addr1");
        String addr2 = request.getParameter("addr2");
        String addr3 = request.getParameter("addr3");
        String addr4 = request.getParameter("addr4");
        String city = request.getParameter("city");
        String state = request.getParameter("state");
        String country = request.getParameter("country");
        String pinCode = request.getParameter("pinCode");
        String analyticScript = request.getParameter("analyticScript");
        String profilePic = (String) request.getSession().getAttribute("_profilePicUploaded");
        try {
            int partid = 0; String imgBase64 = null;  int res=-1;          
            JSONObject developerDetails = new JSONObject();
            
            if (partnerId != null) {
                partid = Integer.parseInt(partnerId);
            }
            SgUsers sp = new UsersManagement().getSgUsersByPartnerId(partid);
            if(profilePic != null){
                File file = new File(profilePic);
                byte[] imgFileData = Files.readAllBytes(file.toPath());
                imgBase64 = Base64.encode(imgFileData);
                developerDetails.put("profilePic", imgBase64);
                file.delete();                
            }else{
                String usrProfileDesc = sp.getProfileDetails();
                if (usrProfileDesc != null) {
                    JSONObject usrJSONObj = new JSONObject(usrProfileDesc);
                    if (usrJSONObj.has("profilePic")) {
                        String base64ProfilePic  = (String) usrJSONObj.get("profilePic");
                        developerDetails.put("profilePic", base64ProfilePic);
                    }
                }
            }
            
            developerDetails.put("address1", addr1);
            developerDetails.put("address2", addr2);
            developerDetails.put("address3", addr3);
            developerDetails.put("address4", addr4);
            developerDetails.put("city", city);
            developerDetails.put("state", state);
            developerDetails.put("country", country);
            developerDetails.put("pinCode", pinCode);
            
            String ChannelId = (String) request.getSession().getAttribute("_ChannelId");
            
            PartnerDetails partDetails = new PartnerManagement().getPartnerDetails(partid);
            partDetails.setPartnerName(partName);
            partDetails.setPartnerPhone(mobileNo); 
            partDetails.setAnalyticScript(analyticScript);
            res = new PartnerManagement().updateDetails(partDetails);            
            
            sp.setUsername(partName);
            sp.setPhone(mobileNo);
            sp.setProfileDetails(developerDetails.toString());
            new UsersManagement().editSgUsers(SessionId, ChannelId, sp);
            sp = new UsersManagement().getSgUsersByPartnerId(partid);
            partDetails = new PartnerManagement().getPartnerDetails(partid);
            if (res == 0) {
                json.put("_result", result);
                json.put("_message", message);
                request.getSession().setAttribute("_SgUsers", sp);
                request.getSession().setAttribute("_partnerDetails", partDetails);
            } else {
                result = "error";
                message = "Failed to update detail";
                json.put("_result", result);
                json.put("_message", message);
            }
            
        } catch (Exception e) {
            result = "error";
            message = e.getMessage();
            json.put("_result", result);
            json.put("_message", message);
        } finally {
            out.print(json);
            out.flush();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
                processRequest(request, response);
       
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
       
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
