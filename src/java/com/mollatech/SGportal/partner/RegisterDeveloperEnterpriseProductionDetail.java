/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.SGportal.partner;

import com.mollatech.service.nucleus.crypto.LoadSettings;
import com.mollatech.serviceguard.connector.communication.SGStatus;
import com.mollatech.serviceguard.nucleus.commons.GlobalStatus;
import com.mollatech.serviceguard.nucleus.db.PartnerDetails;
import com.mollatech.serviceguard.nucleus.db.SgDeveloperProductionEnvt;
import com.mollatech.serviceguard.nucleus.db.connector.management.ProductionAccessEnvtManagement;
import com.mollatech.serviceguard.nucleus.settings.SendNotification;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.json.JSONObject;

/**
 *
 * @author mohanish
 */
@WebServlet(name = "RegisterDeveloperEnterpriseProductionDetail", urlPatterns = {"/RegisterDeveloperEnterpriseProductionDetail"})
public class RegisterDeveloperEnterpriseProductionDetail extends HttpServlet {
    
    static final Logger logger = Logger.getLogger(RegisterDeveloperEnterpriseProductionDetail.class);
    public static final int PENDING = 2;
    public static final int SEND = 0;
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        logger.info("Request servlet is #RegisterDeveloperProductionDetail from #PPortal at " + new Date());
        logger.info("Remote Ip Address is " + request.getRemoteAddr());
        JSONObject json  = new JSONObject();
        String result    = "success";
        String message   = "Production access details submited successfully";
        PrintWriter out  = response.getWriter();
        String partnership                  = request.getParameter("partnership");
        String partnerCompanyName           = request.getParameter("partnerCompanyName");
        String partnerCompanyRegistrationNo = request.getParameter("partnerCompanyRegistrationNo");
        String partnerGSTNo                 = request.getParameter("partnerGSTNo");
        String partnerFixedLineNumber       = request.getParameter("partnerFixedLineNumber");
        String partnerAddress               = request.getParameter("partnerAddress");
        String partnerState                 = request.getParameter("partnerState");
        String partnerPostcode              = request.getParameter("partnerPostcode");
        String partnerCountry               = request.getParameter("partnerCountry");
        String partnerIndustry              = request.getParameter("partnerIndustry");
        String partnerBillingAddress        = request.getParameter("partnerBillingAddress");
        String partnerBillingPostcode       = request.getParameter("partnerBillingPostcode");
        String partnerBillingState          = request.getParameter("partnerBillingState");
        String partnerBillingCountry        = request.getParameter("partnerBillingCountry");
        String[] selectedAPI                = request.getParameterValues("selectedEnterpriseAPI");
        String partnerProductionDescription = request.getParameter("partnerProductionDescription");
        String partnerWebsiteURL            = request.getParameter("partnerWebsiteURL");
        String partnerBankName              = request.getParameter("partnerBankName");
        String partnerBankBranch            = request.getParameter("partnerBankBranch");
        String partnerBankAccountNo         = request.getParameter("partnerBankAccountNo");
        String partnerSwiftCode             = request.getParameter("partnerSwiftCode");
        String sendEmailTo                  = LoadSettings.g_sSettings.getProperty("support.email.id");
        String zippath                      = (String) request.getSession().getAttribute("_ZipPath");
        PartnerDetails partnerObj = (PartnerDetails) request.getSession().getAttribute("_partnerDetails");
        String sessionId = (String) request.getSession().getAttribute("_partnerSessionId");
        String channels = (String) request.getSession().getAttribute("_ChannelId");
                
        logger.debug("value of partnership : " + partnership);
        logger.debug("value of partnerCompanyName : " + partnerCompanyName);
        logger.debug("value of partnerCompanyRegistrationNo : " + partnerCompanyRegistrationNo);
        logger.debug("value of partnerGSTNo : " + partnerGSTNo);
        logger.debug("value of partnerFixedLineNumber : " + partnerFixedLineNumber);
        logger.debug("value of partnerAddress : " + partnerAddress);
        logger.debug("value of partnerState : " + partnerState);
        logger.debug("value of partnerPostcode : " + partnerPostcode);
        logger.debug("value of partnerCountry : " + partnerCountry);        
        logger.debug("value of partnerIndustry : " + partnerIndustry);
        logger.debug("value of partnerBillingAddress : " + partnerBillingAddress);
        logger.debug("value of partnerBillingPostcode : " + partnerBillingPostcode);
        logger.debug("value of partnerBillingCountry : " + partnerBillingCountry);        
        logger.debug("value of partnerProductionDescription : " + partnerProductionDescription);
        logger.debug("value of partnerWebsiteURL : " + partnerWebsiteURL);
        logger.debug("value of partnerBankName : " + partnerBankName);
        logger.debug("value of partnerBankBranch : " + partnerBankBranch);
        logger.debug("value of partnerBankAccountNo : " + partnerBankAccountNo);
        logger.debug("value of partnerSwiftCode : " + partnerSwiftCode);
        logger.debug("value of partnerBillingState : " + partnerBillingState);
        logger.debug("value of zippath : " + zippath);
        if(zippath == null){
            try {
                    message = "Please attach document in zip file format";
                    json.put("_result", "error");
                    logger.debug("Response of partnerRequestStatus Servlet's Parameter  result is error");
                    json.put("_message", message);
                    logger.debug("Response of partnerRequestStatus Servlet's Parameter  message is " + message);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                out.print(json);
                out.flush();
                return;
        }
        
        byte[] compressed_document = null;
        String mimeType[] = { "application/octet-stream" };
        if(zippath != null){
            Path path = Paths.get(zippath);
            compressed_document = Files.readAllBytes(path);
        }
        String apiDetils = "";
        int partnershipType = 0;
        int retValue  = -1;
        if(selectedAPI != null){
            for(int i=0;i<selectedAPI.length;i++){
                if(selectedAPI[i].equalsIgnoreCase("PaymentGatewayEnterprise")){
                    String pay = selectedAPI[i].replaceAll("PaymentGatewayEnterprise", "PaymentGateway");
                    apiDetils += pay+",";
                    continue;
                }
                apiDetils += selectedAPI[i]+",";
            }
        }
        if(partnership != null && !partnership.isEmpty()){
            partnershipType = Integer.parseInt(partnership);
        }
        String tmessage = (String) LoadSettings.g_templateSettings.getProperty("email.newproductionAccess.request");
        logger.debug("value of selectedAPI : " + apiDetils);
        SgDeveloperProductionEnvt productionDetails = new ProductionAccessEnvtManagement().getProudDetailsByPartnerId(partnerObj.getPartnerId());
        if(productionDetails == null){
            productionDetails = new SgDeveloperProductionEnvt();
            productionDetails.setAddress(partnerAddress);
            tmessage = tmessage.replaceAll("#partnerAddress#", partnerAddress);
            productionDetails.setAddrstate(partnerState);
            tmessage = tmessage.replaceAll("#partnerState#", partnerState);
            productionDetails.setPostcode(partnerPostcode);
            tmessage = tmessage.replaceAll("#partnerPostcode#", partnerPostcode);
            productionDetails.setCountry(partnerCountry);
            tmessage = tmessage.replaceAll("#partnerCountry#", partnerCountry);
            if(partnerBillingAddress != null && !partnerBillingAddress.isEmpty()){
                productionDetails.setBillingAddress(partnerBillingAddress);
                tmessage = tmessage.replaceAll("#partnerBillingAddress#", partnerBillingAddress);
            }else{
                productionDetails.setBillingAddress(partnerAddress);
                tmessage = tmessage.replaceAll("#partnerBillingAddress#", partnerAddress);
            }
            if(partnerBillingState != null && !partnerBillingState.isEmpty()){
                productionDetails.setBillingState(partnerBillingState);
                tmessage = tmessage.replaceAll("#partnerBillingState#", partnerBillingState);
            }else{
                productionDetails.setBillingState(partnerState);
                tmessage = tmessage.replaceAll("#partnerBillingState#", partnerState);
            }
             if(partnerBillingPostcode != null && !partnerBillingPostcode.isEmpty()){
                productionDetails.setBillingPostCode(partnerBillingPostcode);
                tmessage = tmessage.replaceAll("#partnerBillingPostcode#", partnerBillingPostcode);
            }else{
                productionDetails.setBillingPostCode(partnerPostcode);
                 tmessage = tmessage.replaceAll("#partnerBillingPostcode#", partnerPostcode);
            }
            if(partnerBillingCountry != null && !partnerBillingCountry.isEmpty()){
                productionDetails.setBillingCountry(partnerBillingCountry);
                tmessage = tmessage.replaceAll("#partnerBillingCountry#", partnerBillingCountry);
            }else{
                productionDetails.setBillingCountry(partnerCountry);
                tmessage = tmessage.replaceAll("#partnerBillingCountry#", partnerCountry);
            }                                    
            productionDetails.setComapnyName(partnerCompanyName);
            tmessage = tmessage.replaceAll("#partnerCompanyName#", partnerCompanyName);
            productionDetails.setCompanyRegistrationNo(partnerCompanyRegistrationNo);
            tmessage = tmessage.replaceAll("#partnerCompanyRegistrationNo#", partnerCompanyRegistrationNo);
            productionDetails.setCompanyType(partnershipType);
            if(partnershipType == GlobalStatus.Sole_Proprietorship){
                tmessage = tmessage.replaceAll("#partnershipType#", "Sole partnership");
            }else if(partnershipType == GlobalStatus.Partnership){
                tmessage = tmessage.replaceAll("#partnershipType#", "Partnership");
            }else if(partnershipType == GlobalStatus.Sdn_Bhd){
                tmessage = tmessage.replaceAll("#partnershipType#", "Sdn_Bhd");
            }else if(partnershipType == GlobalStatus.Berhad){
                tmessage = tmessage.replaceAll("#partnershipType#", "Berhad");
            }else if(partnershipType == GlobalStatus.Foreign){
                tmessage = tmessage.replaceAll("#partnershipType#", "Foreign");
            }
            productionDetails.setCreatedOn(new Date());
            productionDetails.setDocuments(compressed_document);
            productionDetails.setFixedLineNumber(partnerFixedLineNumber);
            tmessage = tmessage.replaceAll("#partnerFixedLineNumber#", partnerFixedLineNumber);
            productionDetails.setGstNo(partnerGSTNo);
            tmessage = tmessage.replaceAll("#partnerGSTNo#", partnerGSTNo);
            productionDetails.setIndustry(partnerIndustry);
            tmessage = tmessage.replaceAll("#partnerIndustry#", partnerIndustry);
            productionDetails.setPartnerid(partnerObj.getPartnerId());
            if(partnerBankAccountNo != null && !partnerBankAccountNo.isEmpty()){
                productionDetails.setPaymentBankAcctNo(partnerBankAccountNo);
                tmessage = tmessage.replaceAll("#partnerBankAccountNo#", partnerBankAccountNo);
            }else{
                tmessage = tmessage.replaceAll("#partnerBankAccountNo#", "NA");
            }
            if(partnerBankBranch != null && !partnerBankBranch.isEmpty()){
                productionDetails.setPaymentBankBranch(partnerBankBranch);
                tmessage = tmessage.replaceAll("#partnerBankBranch#", partnerBankBranch);
            }else{
                tmessage = tmessage.replaceAll("#partnerBankBranch#", "NA");
            }
            if(partnerBankName != null && !partnerBankName.isEmpty()){
                productionDetails.setPaymentBankName(partnerBankName);
                tmessage = tmessage.replaceAll("#partnerBankName#", partnerBankName);
            }else{
                tmessage = tmessage.replaceAll("#partnerBankName#", "NA");
            }
            if(partnerProductionDescription != null && !partnerProductionDescription.isEmpty()){
                productionDetails.setPaymentProductDescription(partnerProductionDescription);
                tmessage = tmessage.replaceAll("#partnerProductionDescription#", partnerProductionDescription);
            }else{
                tmessage = tmessage.replaceAll("#partnerProductionDescription#", "NA");
            }
            if(partnerSwiftCode != null && !partnerSwiftCode.isEmpty()){
               productionDetails.setPaymentSwiftcode(partnerSwiftCode);
               tmessage = tmessage.replaceAll("#partnerSwiftCode#", partnerSwiftCode);
            }else{
                tmessage = tmessage.replaceAll("#partnerSwiftCode#", "NA");
            }           
            if(partnerWebsiteURL != null && !partnerWebsiteURL.isEmpty()){
               productionDetails.setPaymentWebsiteUrl(partnerWebsiteURL); 
               tmessage = tmessage.replaceAll("#partnerWebsiteURL#", partnerWebsiteURL);
            }else{
                tmessage = tmessage.replaceAll("#partnerWebsiteURL#", "NA");
            }                             
            productionDetails.setPreferedResource(apiDetils);
            productionDetails.setStatus(GlobalStatus.PENDING);
            tmessage = tmessage.replaceAll("#services#", apiDetils);
            retValue = new ProductionAccessEnvtManagement().addDeveloperProudDetails(productionDetails);
            
        }else{
            productionDetails.setAddress(partnerAddress);
            tmessage = tmessage.replaceAll("#partnerAddress#", partnerAddress);
            productionDetails.setAddrstate(partnerState);
            tmessage = tmessage.replaceAll("#partnerState#", partnerState);
            productionDetails.setPostcode(partnerPostcode);
            tmessage = tmessage.replaceAll("#partnerPostcode#", partnerPostcode);
            productionDetails.setCountry(partnerCountry);
            tmessage = tmessage.replaceAll("#partnerCountry#", partnerCountry);
            if(partnerBillingAddress != null && !partnerBillingAddress.isEmpty()){
                productionDetails.setBillingAddress(partnerBillingAddress);
                tmessage = tmessage.replaceAll("#partnerBillingAddress#", partnerBillingAddress);
            }else{
                productionDetails.setBillingAddress(partnerAddress);
                tmessage = tmessage.replaceAll("#partnerBillingAddress#", partnerAddress);
            }
            if(partnerBillingState != null && !partnerBillingState.isEmpty()){
                productionDetails.setBillingState(partnerBillingState);
                tmessage = tmessage.replaceAll("#partnerBillingState#", partnerBillingState);
            }else{
                productionDetails.setBillingState(partnerState);
                tmessage = tmessage.replaceAll("#partnerBillingState#", partnerState);
            }
             if(partnerBillingPostcode != null && !partnerBillingPostcode.isEmpty()){
                productionDetails.setBillingPostCode(partnerBillingPostcode);
                tmessage = tmessage.replaceAll("#partnerBillingPostcode#", partnerBillingPostcode);
            }else{
                productionDetails.setBillingPostCode(partnerPostcode);
                 tmessage = tmessage.replaceAll("#partnerBillingPostcode#", partnerPostcode);
            }
            if(partnerBillingCountry != null && !partnerBillingCountry.isEmpty()){
                productionDetails.setBillingCountry(partnerBillingCountry);
                tmessage = tmessage.replaceAll("#partnerBillingCountry#", partnerBillingCountry);
            }else{
                productionDetails.setBillingCountry(partnerCountry);
                tmessage = tmessage.replaceAll("#partnerBillingCountry#", partnerCountry);
            }                                    
            productionDetails.setComapnyName(partnerCompanyName);
            tmessage = tmessage.replaceAll("#partnerCompanyName#", partnerCompanyName);
            productionDetails.setCompanyRegistrationNo(partnerCompanyRegistrationNo);
            tmessage = tmessage.replaceAll("#partnerCompanyRegistrationNo#", partnerCompanyRegistrationNo);
            productionDetails.setCompanyType(partnershipType);
            if(partnershipType == GlobalStatus.Sole_Proprietorship){
                tmessage = tmessage.replaceAll("#partnershipType#", "Sole partnership");
            }else if(partnershipType == GlobalStatus.Partnership){
                tmessage = tmessage.replaceAll("#partnershipType#", "Partnership");
            }else if(partnershipType == GlobalStatus.Sdn_Bhd){
                tmessage = tmessage.replaceAll("#partnershipType#", "Sdn_Bhd");
            }else if(partnershipType == GlobalStatus.Berhad){
                tmessage = tmessage.replaceAll("#partnershipType#", "Berhad");
            }else if(partnershipType == GlobalStatus.Foreign){
                tmessage = tmessage.replaceAll("#partnershipType#", "Foreign");
            }
            productionDetails.setCreatedOn(new Date());
            productionDetails.setDocuments(compressed_document);
            productionDetails.setFixedLineNumber(partnerFixedLineNumber);
            tmessage = tmessage.replaceAll("#partnerFixedLineNumber#", partnerFixedLineNumber);
            productionDetails.setGstNo(partnerGSTNo);
            tmessage = tmessage.replaceAll("#partnerGSTNo#", partnerGSTNo);
            productionDetails.setIndustry(partnerIndustry);
            tmessage = tmessage.replaceAll("#partnerIndustry#", partnerIndustry);
            productionDetails.setPartnerid(partnerObj.getPartnerId());
            if(partnerBankAccountNo != null && !partnerBankAccountNo.isEmpty()){
                productionDetails.setPaymentBankAcctNo(partnerBankAccountNo);
                tmessage = tmessage.replaceAll("#partnerBankAccountNo#", partnerBankAccountNo);
            }else{
                tmessage = tmessage.replaceAll("#partnerBankAccountNo#", "NA");
            }
            if(partnerBankBranch != null && !partnerBankBranch.isEmpty()){
                productionDetails.setPaymentBankBranch(partnerBankBranch);
                tmessage = tmessage.replaceAll("#partnerBankBranch#", partnerBankBranch);
            }else{
                tmessage = tmessage.replaceAll("#partnerBankBranch#", "NA");
            }
            if(partnerBankName != null && !partnerBankName.isEmpty()){
                productionDetails.setPaymentBankName(partnerBankName);
                tmessage = tmessage.replaceAll("#partnerBankName#", partnerBankName);
            }else{
                tmessage = tmessage.replaceAll("#partnerBankName#", "NA");
            }
            if(partnerProductionDescription != null && !partnerProductionDescription.isEmpty()){
                productionDetails.setPaymentProductDescription(partnerProductionDescription);
                tmessage = tmessage.replaceAll("#partnerProductionDescription#", partnerProductionDescription);
            }else{
                tmessage = tmessage.replaceAll("#partnerProductionDescription#", "NA");
            }
            if(partnerSwiftCode != null && !partnerSwiftCode.isEmpty()){
               productionDetails.setPaymentSwiftcode(partnerSwiftCode);
               tmessage = tmessage.replaceAll("#partnerSwiftCode#", partnerSwiftCode);
            }else{
                tmessage = tmessage.replaceAll("#partnerSwiftCode#", "NA");
            }           
            if(partnerWebsiteURL != null && !partnerWebsiteURL.isEmpty()){
               productionDetails.setPaymentWebsiteUrl(partnerWebsiteURL);  
               tmessage = tmessage.replaceAll("#partnerWebsiteURL#", partnerWebsiteURL);
            }else{
                tmessage = tmessage.replaceAll("#partnerWebsiteURL#", "NA");
            }                             
            productionDetails.setPreferedResource(apiDetils);
            productionDetails.setStatus(GlobalStatus.PENDING);
            tmessage = tmessage.replaceAll("#services#", apiDetils);
            retValue = new ProductionAccessEnvtManagement().updateProductionDetail(sessionId, productionDetails);
        }
        if(retValue >= 0){
            tmessage = tmessage.replaceAll("#channel#", "Service Guard");
            tmessage = tmessage.replaceAll("#name#", partnerObj.getPartnerName());
            tmessage = tmessage.replaceAll("#datetime#", new SimpleDateFormat("dd/MM/yyyy").format(new Date()));
            int productType = 3;
            String filePath[] = {zippath};
//            SGStatus status = new SendNotification().SendEmailWithAttachment(channels, sendEmailTo, "Production Request of "+partnerObj.getPartnerName(), tmessage, null, null, filePath, mimeType, 1);
            SGStatus status = new SendNotification().SendEmail(channels, sendEmailTo, "Production Request of " + partnerObj.getPartnerName(), tmessage, null, null, filePath, mimeType, productType);
            if (status.iStatus != PENDING && status.iStatus != SEND) {
                message = "Details saved but failed to send the email notification to the Admin, Please contact admin";
                try {
                    json.put("_result", "error");
                    logger.debug("Response of partnerRequestStatus Servlet's Parameter  result is error");
                    json.put("_message", message);
                    logger.debug("Response of partnerRequestStatus Servlet's Parameter  message is " + message);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                out.print(json);
                out.flush();
                return;
            }
        }                
        try {
            json.put("_result", result);
            json.put("_message", message);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            out.print(json);
            out.flush();
        }    
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
