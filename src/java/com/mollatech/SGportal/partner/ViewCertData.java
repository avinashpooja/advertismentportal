package com.mollatech.SGportal.partner;

import com.mollatech.serviceguard.nucleus.db.PartnerDetails;
import com.mollatech.serviceguard.nucleus.db.connector.management.PartnerManagement;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;

/**
 *
 * @author mohanish
 */
@WebServlet(name = "ViewCertData", urlPatterns = { "/ViewCertData" })
public class ViewCertData extends HttpServlet {

    static final Logger logger = Logger.getLogger(ViewCertData.class);

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.info("Requested Servlet is ViewCertData at " + new Date());
        logger.info("Remote Ip Address is " + request.getRemoteAddr());
        response.setContentType("application/json");
        String sessionId = (String) request.getSession().getAttribute("_partnerSessionId");
        String channels = (String) request.getSession().getAttribute("_ChannelId");
        String _partnerid = request.getParameter("pid");
        logger.debug("Value of _partnerid = " + _partnerid);
        String env = request.getParameter("env");
        logger.debug("Value of env = " + env);
        int id = Integer.parseInt(_partnerid);
        String result = "success";
        String message = "Failed To Retrive Developer Certificate Details";
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        try {
            PartnerDetails details = new PartnerManagement().getPartnerDetails(sessionId, channels, id);
            File file = File.createTempFile(details.getPartnerName(), ".cer");
            String filePath = file.getCanonicalPath();
            FileOutputStream fos = new FileOutputStream(filePath);
            if (env.equalsIgnoreCase("test")) {
                fos.write(details.getCertData());
            } else {
                fos.write(details.getCertDataForLive());
            }
            fos.close();
            CertificateFactory cf = CertificateFactory.getInstance("X.509");
            FileInputStream in = new FileInputStream(filePath);
            java.security.cert.Certificate c = cf.generateCertificate(in);
            in.close();
            X509Certificate t = (X509Certificate) c;
            json.put("_srno", t.getSerialNumber());
            json.put("_algoname", t.getSigAlgName());
            json.put("_issuerdn", "\"" + t.getIssuerDN() + "\"");
            json.put("_notafter", t.getNotAfter().toString());
            json.put("_notbefore", t.getNotBefore().toString());
            json.put("_version", t.getVersion());
            json.put("_subjectdn", "\"" + t.getSubjectDN() + "\"");
            json.put("name", details.getPartnerName());
            json.put("_result", "success");
            file.delete();
        } catch (Exception ex) {
            try {
                json.put("_result", "error");
                json.put("_message", message);
            } catch (Exception e) {
                logger.error("Exception at ViewCertData ", e);
            }
            logger.error("Exception at ViewCertData ", ex);
        } finally {
            logger.info("Response of ViewCertData " + json.toString());
            logger.info("Response of ViewCertData Servlet at " + new Date());
            out.print(json);
            out.flush();
        }
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
