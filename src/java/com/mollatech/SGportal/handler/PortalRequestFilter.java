package com.mollatech.SGportal.handler;

import com.mollatech.service.nucleus.crypto.LoadSettings;
import static com.mollatech.service.nucleus.crypto.LoadSettings.g_sSettings;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.InetAddress;
import java.net.URL;
import java.util.Enumeration;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class PortalRequestFilter implements Filter {

    private static final boolean debug = true;

    private FilterConfig filterConfig = null;

    public PortalRequestFilter() {
    }

    private void doBeforeProcessing(ServletRequest request, ServletResponse response) throws IOException, ServletException {
        if (debug) {
            log("PortalRequestFilter:DoBeforeProcessing");
        }
    }

    private void doAfterProcessing(ServletRequest request, ServletResponse response) throws IOException, ServletException {
        if (debug) {
            log("PortalRequestFilter:DoAfterProcessing");
        }
    }

    public static String validdomain = null;

    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
        try {
            HttpServletRequest request = (HttpServletRequest) req;
            HttpServletResponse response = (HttpServletResponse) res;
            StringBuffer url = request.getRequestURL();
            URL u = new URL(url.toString());
            boolean flag = false;
            boolean blacklist = false;
            validdomain = LoadSettings.g_sSettings.getProperty("portal.domain");
            if (validdomain != null) {
                if (!validdomain.isEmpty()) {
                    String[] domains = validdomain.split(",");
                    if (domains.length != 0) {
                        for (String domain : domains) {
                            for (InetAddress addr : InetAddress.getAllByName(u.getHost())) {
                                if (addr.getHostAddress().equals(domain)) {
                                    flag = true;
                                    Enumeration enames = request.getParameterNames();
                                    while (enames.hasMoreElements()) {
                                        String name = (String) enames.nextElement();
                                        String value = (String) request.getParameter(name);
                                        Pattern regex = Pattern.compile("[%()'*;{}`+^!]");
                                        Matcher matcher = regex.matcher(value);
                                        if (matcher.find()) {
                                            blacklist = true;
                                            break;
                                        }
                                    }
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            if (flag == true) {
                if (blacklist == true) {
                    sendProcessingError("You trying to perform malicious activity", res);
                    return;
                } else {
                    chain.doFilter(req, res);
                }
            } else {
                sendProcessingError("You trying to access from invalid domain", res);
                return;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Return the filter configuration object for this filter.
     */
    public FilterConfig getFilterConfig() {
        return (this.filterConfig);
    }

    /**
     * Set the filter configuration object for this filter.
     *
     * @param filterConfig The filter configuration object
     */
    public void setFilterConfig(FilterConfig filterConfig) {
        this.filterConfig = filterConfig;
    }

    /**
     * Destroy method for this filter
     */
    public void destroy() {
    }

    /**
     * Init method for this filter
     */
    public void init(FilterConfig filterConfig) {
        this.filterConfig = filterConfig;
        if (filterConfig != null) {
            if (debug) {
                log("AxiomRequestFilter:Initializing filter");
            }
        }
    }

    /**
     * Return a String representation of this object.
     */
    @Override
    public String toString() {
        if (filterConfig == null) {
            return ("AxiomRequestFilter()");
        }
        StringBuffer sb = new StringBuffer("AxiomRequestFilter(");
        sb.append(filterConfig);
        sb.append(")");
        return (sb.toString());
    }

    private void sendProcessingError(String errors, ServletResponse response) {
        try {
            response.setContentType("text/html");
            PrintStream ps = new PrintStream(response.getOutputStream());
            PrintWriter pw = new PrintWriter(ps);
            pw.print("<html>\n<head>\n<title>Error </title>\n</head>\n<body>\n");
            pw.print("<h1><font style='color:red'>Page Not Available </font></h1>");
            pw.print("\n<pre>\n<font>" + errors + ".</pre></body>\n</html>");
            pw.close();
            ps.close();
            response.getOutputStream().close();
        } catch (Exception ex) {
        }
    }

    public static String getStackTrace(Throwable t) {
        String stackTrace = null;
        try {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            t.printStackTrace(pw);
            pw.close();
            sw.close();
            stackTrace = sw.getBuffer().toString();
        } catch (Exception ex) {
        }
        return stackTrace;
    }

    public void log(String msg) {
        filterConfig.getServletContext().log(msg);
    }
}
