//package com.mollatech.SGportal.handler;
//
//import datameer.com.google.common.cache.Cache;
//import datameer.com.google.common.cache.CacheBuilder;
//import java.io.IOException;
//import java.security.SecureRandom;
//import java.util.concurrent.TimeUnit;
//import javax.servlet.*;
//import javax.servlet.http.HttpServletRequest;
//import org.apache.commons.lang.RandomStringUtils;
//
//public class loadSalt implements Filter {
//
//    @Override
//    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
//        HttpServletRequest httpReq = (HttpServletRequest) request;
//        Cache<String, Boolean> csrfPreventionSaltCache = (Cache<String, Boolean>) httpReq.getSession().getAttribute("csrfPreventionSaltCache");
//        if (csrfPreventionSaltCache == null) {
//            csrfPreventionSaltCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(20, TimeUnit.MINUTES).build();
//            httpReq.getSession().setAttribute("csrfPreventionSaltCache", csrfPreventionSaltCache);
//        }
//        String salt = RandomStringUtils.random(20, 0, 0, true, true, null, new SecureRandom());
//        csrfPreventionSaltCache.put(salt, Boolean.TRUE);
//        httpReq.setAttribute("csrfPreventionSalt", salt);
//        chain.doFilter(httpReq, response);
//    }
//
//    @Override
//    public void init(FilterConfig filterConfig) throws ServletException {
//    }
//
//    @Override
//    public void destroy() {
//    }
//}
