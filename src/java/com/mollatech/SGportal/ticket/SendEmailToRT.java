/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.SGportal.ticket;

import static com.mollatech.SGportal.ticket.EmailView.logger;
import com.mollatech.service.nucleus.crypto.LoadSettings;
import com.mollatech.serviceguard.connector.communication.SGStatus;
import com.mollatech.serviceguard.nucleus.db.SgUsers;
import com.mollatech.serviceguard.nucleus.settings.SendNotification;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author Ashu
 */
@WebServlet(name = "SendEmailToRT", urlPatterns = {"/SendEmailToRT"})
public class SendEmailToRT extends HttpServlet {

    public static final int PENDING = 2;
    public static final int SEND = 0;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specifi21 `c error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException{
        response.setContentType("applicaion/json");
        PrintWriter out = response.getWriter();
        JSONObject json = new JSONObject();
        String subjectBody = request.getParameter("subjectBody");
        logger.debug("value of subjectBody : " + subjectBody);
        String msgBody = request.getParameter("msgBody");
        logger.debug("value of msgBody : " + msgBody);
        
        String emailAttachmentFilePath = (String) request.getSession().getAttribute("_EmailWithAttachment");
        if (emailAttachmentFilePath == null) {
            emailAttachmentFilePath = (String) request.getSession().getAttribute("_ZipPath");
        }
        request.getSession().setAttribute("_EmailWithAttachment", null);
        request.getSession().setAttribute("_ZipPath", null);
        logger.debug("value of emailAttachmentFilePath : " + emailAttachmentFilePath);
        try {
            if (subjectBody.equals("")) {
                json.put("_result", "error");
                json.put("_message", "Subject Can Not Be Empty.");
                return;
            }
            if (msgBody.equals("")) {
                json.put("_result", "error");
                json.put("_message", "Message Can Not Be Empty.");
                return;
            }
            SgUsers user = (SgUsers) request.getSession().getAttribute("_SgUsers");
            //SendNotification sendNotification = new SendNotification();
            String mailId = LoadSettings.g_sSettings.getProperty("support.email.id");
            String cc[] = {user.getEmail()};
            String mimeType[] = {"application/octet-stream"};
            //SGStatus axStatus = sendNotification.SendOnEmailByPreference(user.getChannelid(), mailId, subjectBody, msgBody, cc, null, null, null, SendNotification.EMAIL, 1, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
            int productType = 3;

            String filePath[] = {emailAttachmentFilePath};
            SGStatus status = null;
//            SGStatus status = new SendNotification().SendEmailWithAttachment(user.getChannelid(), mailId, subjectBody, msgBody, cc, null, filePath, mimeType, 1);
            if (emailAttachmentFilePath != null) {
                status = new SendNotification().SendEmail(user.getChannelid(), mailId, subjectBody, msgBody, cc, null, filePath, mimeType, productType);
            } else {
                status = new SendNotification().SendEmail(user.getChannelid(), mailId, subjectBody, msgBody, cc, null, null, mimeType, productType);
            }

            json.put("_result", "success");
            if (status.iStatus == SEND) {
                json.put("_message", "Ticket Submitted Successfully.");
                request.getSession().setAttribute("_EmailWithAttachment", null);
            } else {
                json.put("_result", "error");
                json.put("_message", "Error in Ticket Sending");
            }
            return;
        } catch (Exception ex) {
            json.put("_result", "error");
            json.put("_message", ex.getMessage());
            return;
        } finally {
            out.print(json);
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
               processRequest(request, response);
       
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
               processRequest(request, response);
       
    }


    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
