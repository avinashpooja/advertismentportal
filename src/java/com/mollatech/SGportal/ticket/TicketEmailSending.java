
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.SGportal.ticket;

import com.mollatech.serviceguard.nucleus.commons.CommonUtility;
import com.mollatech.serviceguard.nucleus.commons.GlobalStatus;
import com.mollatech.serviceguard.nucleus.db.Operators;
import com.mollatech.serviceguard.nucleus.db.SgEmailticket;
import com.mollatech.serviceguard.nucleus.db.connector.management.EmailManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.OperatorsManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.json.JSONObject;

/**
 *
 * @author bluebricks
 */
@WebServlet(name = "TicketEmailSending", urlPatterns = {"/TicketEmailSending"})
public class TicketEmailSending extends HttpServlet {

    static final Logger logger = Logger.getLogger(TicketEmailSending.class);

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        logger.info("Request servlet is #registerPartner from #PPortal at " + new Date());
        logger.info("Remote Ip Address is " + request.getRemoteAddr());
        String sessionId = (String) request.getSession().getAttribute("_partnerSessionId");
        String partchannelID = (String) request.getSession().getAttribute("_ChannelId");
        JSONObject json = new JSONObject();
        String result = "success";
        String message = "Email send successfully";
        PrintWriter out = response.getWriter();
        int partnerid = (Integer) request.getSession().getAttribute("_partnerID");
        //System.out.println(partnerid+"asdfghjklasdfghjk");
        String emailFrom = String.valueOf(partnerid);
        String sendTo = request.getParameter("to");
        logger.debug("value of Operator id : " + sendTo);
        String msgSubject = request.getParameter("subject");
        logger.debug("value of mesage subject : " + msgSubject);
        String msgCategory = request.getParameter("_setCategory");
        logger.debug("value of message Category : " + msgCategory);
        String msgBody = request.getParameter("msgBody");
        logger.debug("value of message Body : " + msgBody);
        String msgFrom = request.getParameter("from");
        logger.debug("value of message from : " + msgFrom);
        Operators operator = new OperatorsManagement().getOperatorById(partchannelID, sendTo);
        if (operator != null) {
            if(operator.getStatus() != GlobalStatus.SUSPEND){
            if (sessionId != null) {
                SgEmailticket pObj = new SgEmailticket();
                pObj.setId(CommonUtility.getUniqueId(request.getSession().getId(), request.getRequestURL().toString(), msgBody, "" + Thread.currentThread().getId()));
                pObj.setCategory(msgCategory);
                pObj.setChannelid(partchannelID);
                pObj.setCreatedon(new Date());
                pObj.setDeletedByPartner(GlobalStatus.SUCCESS);
                pObj.setDeletedByOperator(GlobalStatus.SUCCESS);
                pObj.setEmailFrom(emailFrom);
                pObj.setEmailTo(sendTo);
                pObj.setMessageBody(msgBody);
                pObj.setReadStatusByPartner(GlobalStatus.SUCCESS);
                pObj.setReadStatusByOperator(GlobalStatus.SUCCESS);
                pObj.setSubject(msgSubject);
                int respose = new EmailManagement().addEmail(pObj, sessionId);
                if (respose == 0) {
                    result = "success";
                    logger.debug("Response of #ticketEmailSending from #PPAdmin Servlet's Parameter  result is " + result);
                    message = "Email send successfully to" + " " + operator.getName() + " !";
                    logger.debug("Response of #ticketEmailSending from #PPAdmin Servlet's Parameter  message is " + message);
                } else {
                    result = "error";
                    logger.debug("Response of #ticketEmailSending from #PPAdmin Servlet's Parameter  result is " + result);
                    message = "Email Sending Failed !";
                    logger.debug("Response of #ticketEmailSending from #PPAdmin Servlet's Parameter  message is " + message);
                }
            } else {
                result = "error";
                logger.debug("Response of #ticketEmailSending from #PPAdmin Servlet's Parameter  result is " + result);
                message = "Email Sending Failed !";
                logger.debug("Response of #ticketEmailSending from #PPAdmin Servlet's Parameter  message is " + message);
            }
            }else{
                result = "error";
                logger.debug("Response of #ticketEmailSending from #PPAdmin Servlet's Parameter  result is " + result);
                message = "Receiver is suspended !";
                logger.debug("Response of #ticketEmailSending from #PPAdmin Servlet's Parameter  message is " + message);
            }
        } else {
            result = "error";
            logger.debug("Response of #ticketEmailSending from #PPAdmin Servlet's Parameter  result is " + result);
            message = "Receiver is not present !";
            logger.debug("Response of #ticketEmailSending from #PPAdmin Servlet's Parameter  message is " + message);

        }
        try {
            json.put("_result", result);
            logger.debug("Response of #ticketEmailSending from #PPAdmin Servlet's Parameter result is " + result);
            json.put("_message", message);
            logger.debug("Response of #ticketEmailSending from #PPAdmin Servlet's Parameter message is " + message);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            out.print(json);
            out.flush();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
