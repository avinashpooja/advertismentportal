package com.mollatech.SGportal.ticket;

import com.mollatech.serviceguard.nucleus.db.PartnerDetails;
import com.mollatech.serviceguard.nucleus.db.connector.management.PartnerManagement;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONObject;

/**
 *
 * @author WALE
 */
public class ChangeStatusTicket extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    final int ACTIVE_STATUS = 1;

    final int SUSPEND_STATUS = 0;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("application/json");
        String SessionId = (String) request.getSession().getAttribute("_PartnerSessionId");
        String ChannelId = (String) request.getSession().getAttribute("_ChannelId");
        String _res = request.getParameter("_partid");
        int _partnerid = Integer.parseInt(_res);
        String _status = request.getParameter("_status");
        int status = Integer.parseInt(_status);
        String result = "success";
        String message = " status updated";
        String _value = "Active";
        if (status == ACTIVE_STATUS) {
            _value = "Active ";
        } else if (status == SUSPEND_STATUS) {
            _value = "Suspended ";
        }
        message = _value + message;
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        if (_res == null || _status == null) {
            result = "error";
            message = "Fill all details";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                e.printStackTrace();
            }
            out.print(json);
            out.flush();
            return;
        }
        int retValue = -1;
        PartnerManagement ppw = new PartnerManagement();
        PartnerDetails partners = ppw.getPartnerDetails(SessionId, ChannelId, _partnerid);
        int istatus = partners.getStatus();
        String strstaus = "Removed";
        if (istatus == ACTIVE_STATUS) {
            strstaus = "Active";
        } else if (istatus == SUSPEND_STATUS) {
            strstaus = "Suspended";
        }
        String strstaus1 = "Removed";
        if (status == ACTIVE_STATUS) {
            strstaus1 = "Active";
        } else if (status == SUSPEND_STATUS) {
            strstaus1 = "Suspended";
        }
        if (istatus == status) {
            try {
                result = "error";
                message = "Status Already " + strstaus;
                json.put("_result", result);
                json.put("_message", message);
                json.put("_value", _value);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                out.print(json);
                out.flush();
            }
            return;
        }
        retValue = ppw.ChangePartnerStatus(SessionId, null, _partnerid, status);
        try {
            json.put("_result", result);
            json.put("_message", message);
            json.put("_value", _value);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            out.print(json);
            out.flush();
        }
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
