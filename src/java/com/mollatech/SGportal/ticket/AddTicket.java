package com.mollatech.SGportal.ticket;

import com.mollatech.service.nucleus.crypto.LoadSettings;
import com.mollatech.serviceguard.connector.communication.SGStatus;
import com.mollatech.serviceguard.nucleus.db.Operators;
import com.mollatech.serviceguard.nucleus.db.PartnerDetails;
import com.mollatech.serviceguard.nucleus.db.SgTicket;
import com.mollatech.serviceguard.nucleus.db.connector.management.OperatorsManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.TicketManagement;
import com.mollatech.serviceguard.nucleus.settings.SendNotification;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.fileupload.FileItem;
import org.apache.log4j.Logger;
import org.json.JSONObject;

/**
 *
 * @author Bluebricks
 */
public class AddTicket extends HttpServlet {

    static final int ACTIVE_STATUS = 1;

    static final int SUSPENDED_STATUS = 0;

    static final int PENDING_STATUS = -2;

    public final int PASSWORD = 1;

    static final int TICKET_TECHNICAL = 1;

    static final int TICKET_OTHERS = 2;

    public static final int USER_PASSWORD = 1;

    public static final int PENDING = 2;

    public static final int SENT = 0;

    public static final int SEND = 0;

    public static final int RESET = 1;

    public static final int completed = 6;

    static final Logger logger = Logger.getLogger(AddTicket.class);

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        response.setContentType("text/html;charset=UTF-8");
        String strError = "";
        String saveFile = "";
        String savepath = "";
        JSONObject json = new JSONObject();
        String result = "success";
        String message = "Ticket added successfully";
        PrintWriter out = response.getWriter();
        String subject = request.getParameter("subject");
        logger.debug("Value of subject  = " + subject);
        String _type = request.getParameter("type");
        logger.debug("Value of _type  = " + _type);
        String Msg = request.getParameter("message");
        logger.debug("Value of Msg  = " + Msg);
        int type = -1;
        if (_type != null) {
            type = Integer.parseInt(_type);
        }
        savepath = System.getProperty("catalina.home");
        if (savepath == null) {
            savepath = System.getenv("catalina.home");
        }
        savepath += System.getProperty("file.separator");
        savepath += "axiomv2-settings";
        savepath += System.getProperty("file.separator");
        savepath += "uploads";
        savepath += System.getProperty("file.separator");
        String optionalFileName = "";
        FileItem fileItem = null;
        String[] files = new String[1];
        String dirName = savepath;
        int i = 0;
        String sessionId = (String) request.getSession().getAttribute("_partnerSessionId");
        String docpath = (String) request.getSession().getAttribute("_ImagePath");
        logger.debug("Value of docpath  = " + docpath);
        String ChannelId = (String) request.getSession().getAttribute("_ChannelId");
        Path path = Paths.get(docpath);
        byte[] pdf = Files.readAllBytes(path);
        PartnerDetails _partnerDetails = (PartnerDetails) request.getSession().getAttribute("_partnerDetails");
        SgTicket retValue = new TicketManagement().addTicket(sessionId, null, _partnerDetails.getPartnerId(), subject, Msg, type, pdf);
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy HH:mm");
        Date d = new Date();
        if (retValue != null) {
            Operators operator = new OperatorsManagement().GetOperatorInner(sessionId, ChannelId, "sysadmin");
            String ausessionId = (String) request.getSession().getAttribute("_userSessionId");
            String ticketType = "NA";
            if (retValue.getType() == TICKET_TECHNICAL) {
                ticketType = "Technical Issue";
            } else if (retValue.getType() == 2) {
                ticketType = "Other Issue";
            }
            String tmessage = (String) LoadSettings.g_templateSettings.getProperty("email.raise.ticket");
            if (tmessage != null) {
                d = new Date();
                tmessage = tmessage.replaceAll("#partnerName#", _partnerDetails.getPartnerName());
                tmessage = tmessage.replaceAll("#issueDetails#", Msg);
                tmessage = tmessage.replaceAll("#ticketNumber#", String.format("%06d", retValue.getId()));
                tmessage = tmessage.replaceAll("#ticketType#", ticketType);
                tmessage = tmessage.replaceAll("#subject#", subject);
                tmessage = tmessage.replaceAll("#date#", sdf.format(d));
            }
            String Finalmsg = tmessage;
            String[] mimetypes = null;
            mimetypes = new String[3];
            mimetypes[0] = "text/html";
            mimetypes[1] = "image/png";
            mimetypes[2] = "image/jpeg";
            String[] filess = new String[1];
            filess[0] = docpath;
            SendNotification send = new SendNotification();
            String supportEmail = LoadSettings.g_sSettings.getProperty("support.email.id");
            String[] ccEmailId = { operator.getEmailid() };
            SGStatus a = send.SendEmail(ChannelId, supportEmail, "Service Guard Portal Ticket", "<font style=\"font-size: 14px\">" + Finalmsg + "</font>", ccEmailId, null, filess, mimetypes, 2);
            if (a.iStatus == PENDING || a.iStatus == SENT) {
                result = "success";
                message = "Ticket raise and email to support team successfully";
            }
        }
        if (retValue == null) {
            result = "error";
            message = "Ticket not added";
        }
        try {
            json.put("_result", result);
            logger.debug("Response of EmailAPIToken Servlet's Parameter  result is " + result);
            json.put("_message", message);
            logger.debug("Response of EmailAPIToken Servlet's Parameter  message is " + message);
        } catch (Exception e) {
            e.printStackTrace();
            json.put("_result", result);
            logger.debug("Response of EmailAPIToken Servlet's Parameter  result is " + result);
            json.put("_message", message);
            logger.debug("Response of EmailAPIToken Servlet's Parameter  message is " + message);
        } finally {
            out.print(json);
            out.flush();
        }
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
                processRequest(request, response);
       
    }


    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
               processRequest(request, response);
       
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
