/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.SGportal.advertisement;

import com.mollatech.serviceguard.nucleus.commons.GlobalStatus;
import com.mollatech.serviceguard.nucleus.db.SgAdvertiserAdDetails;
import com.mollatech.serviceguard.nucleus.db.SgAdvertiserDetails;
import com.mollatech.serviceguard.nucleus.db.connector.management.AdvertiserAdManagement;
import com.sun.org.apache.xml.internal.security.utils.Base64;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author abhishekingle
 */
@WebServlet(name = "CreateEmailAd", urlPatterns = {"/CreateEmailAd"})
public class CreateEmailAd extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        JSONObject json = new JSONObject();
        String result = "success";
        String message = "Your Email Ad saved successfully and sent to admin to validate.";
        PrintWriter out = response.getWriter();        
        String SessionId = (String) request.getSession().getAttribute("_advertisorSessionId");
        String ChannelId = (String) request.getSession().getAttribute("_advertiserChannelId");                
        SgAdvertiserDetails usrObj = (SgAdvertiserDetails) request.getSession().getAttribute("_advertisorDetails");
        String _emailAdImageFileName = (String) request.getSession().getAttribute("_emailAdImage");
        String _emailAdContent = request.getParameter("message");
        String emailAdTitle = request.getParameter("emailAdTitle");
        try {
            String imgBase64 = null;  int res=-1; byte[] imgFileData = null;                      
            if(_emailAdImageFileName != null){
                File file = new File(_emailAdImageFileName);
                imgFileData = Files.readAllBytes(file.toPath());
                imgBase64 = Base64.encode(imgFileData);                
                                
            }else {
                result = "error";
                message = "Please upload email AD logo";
                json.put("_result", result);
                json.put("_message", message);
                out.print(json);
                out.flush();
                return;
            }                                          
            SgAdvertiserAdDetails adDetails = new AdvertiserAdManagement().getAdvertiserDetails(usrObj.getAdId());
            if(adDetails != null){
                adDetails.setEmailAdStatus(GlobalStatus.SENDTO_CHECKER);
                adDetails.setEmailAdImage(imgBase64);
                if(_emailAdContent != null){
                    adDetails.setEmailAdContent(_emailAdContent);
                }
                adDetails.setEmailAdTitle(emailAdTitle);
                res = new AdvertiserAdManagement().updateDetails(adDetails);
            }else{
                adDetails = new SgAdvertiserAdDetails();
                adDetails.setChannelId(ChannelId);
                adDetails.setCreationDate(new Date());
                adDetails.setAdvertiserId(usrObj.getAdId());
                adDetails.setEmailAdStatus(GlobalStatus.SENDTO_CHECKER);
                adDetails.setEmailAdImage(imgBase64);
                if(_emailAdContent != null){
                    adDetails.setEmailAdContent(_emailAdContent);
                }
                adDetails.setEmailAdTitle(emailAdTitle);
                res = new AdvertiserAdManagement().CreateAdvertiserAd(SessionId, ChannelId, adDetails);
            }            
            if (res >= 0) {
                json.put("_result", result);
                json.put("_message", message);  
                File file = new File(_emailAdImageFileName);
                file.delete();
            } else {
                result = "error";
                message = "Failed to update email AD details";
                json.put("_result", result);
                json.put("_message", message);
            }
            
        } catch (Exception e) {
            result = "error";
            message = e.getMessage();
            json.put("_result", result);
            json.put("_message", message);
        } finally {
            out.print(json);
            out.flush();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
    

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
            processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
