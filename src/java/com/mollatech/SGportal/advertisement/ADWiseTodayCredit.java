/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.SGportal.advertisement;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;
import com.mollatech.SGportal.reports.bar;
import com.mollatech.serviceguard.nucleus.db.SgAdvertisertracking;
import com.mollatech.serviceguard.nucleus.db.connector.management.AdvertisementTrackingManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author abhishekingle
 */
@WebServlet(name = "ADWiseTodayCredit", urlPatterns = {"/ADWiseTodayCredit"})
public class ADWiseTodayCredit extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        PrintWriter out = response.getWriter();        
        String ChannelId = (String) request.getSession().getAttribute("_advertiserChannelId");        
        String advertiserAdId = request.getParameter("advertiserAdId");
        String adType = request.getParameter("adType");
        SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");    
        int iAdvertiserID = 0; int iAdType = 0;
        if(advertiserAdId != null){
            iAdvertiserID = Integer.parseInt(advertiserAdId);
        }
        if(adType != null){
            iAdType = Integer.parseInt(adType);
        }
        DateFormat writeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        try{
            Date cdate = format.parse(format.format(new Date()));            
            cdate.setHours(0);
            cdate.setMinutes(59);                       
            ArrayList<bar> sample = new ArrayList<bar>();
            for (int i = 0; i < writeFormat.parse(writeFormat.format(new Date())).getHours(); i++) {
                Float creditUsed = 0.0f;
                Integer adShown = 0;
                Date date = new Date();
                date.setHours(i);
                date.setMinutes(59);

                Date newDate = new Date();
                newDate.setHours(i+1);
                newDate.setMinutes(59);                
                SgAdvertisertracking[] shs = new AdvertisementTrackingManagement().getTxDetails(iAdvertiserID, date, newDate, iAdType);
                if(shs != null){
                    adShown = shs.length;
                    for(int j=0; j < shs.length; j++){                        
                        creditUsed += shs[j].getCreditDeducted();                        
                    }
                    sample.add(new bar(adShown, creditUsed.floatValue(), writeFormat.format(date)));
                }else{
                    sample.add(new bar(0,0, writeFormat.format(date)));
                }
                
            }
            Gson gson = new Gson();
                JsonElement element = gson.toJsonTree(sample, new TypeToken<List<bar>>() {
                }.getType());
                JsonArray jsonArray = element.getAsJsonArray();
                out.print(jsonArray);
                out.flush();
                out.close();
                return;
        }catch(Exception e){
            e.printStackTrace();
        }finally {
            out.close();
            out.flush();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
