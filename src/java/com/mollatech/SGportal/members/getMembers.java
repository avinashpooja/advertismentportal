package com.mollatech.SGportal.members;

import com.mollatech.serviceguard.nucleus.db.SgUsers;
import com.mollatech.serviceguard.nucleus.db.connector.management.UsersManagement;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author Bluebricks
 */
public class getMembers extends HttpServlet {

    public final int OPERATOR = 1;

    public final int REPORTER = 2;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        String userId = request.getParameter("_userId");
        String result = "success";
        String message = "Member details loaded";
        String name = null;
        String email = null;
        int status = -1;
        String phoneno = null;
        String operatorType = null;
        String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
        String channelId = (String) request.getSession().getAttribute("_ChannelId");
        UsersManagement um = new UsersManagement();
        if (SessionId != null) {
            SgUsers rssUser = um.getSgUsersByUserId(SessionId, channelId, userId);
            name = rssUser.getUsername();
            email = rssUser.getEmail();
            status = rssUser.getStatus();
            phoneno = rssUser.getPhone();
            int Type = rssUser.getType();
            if (Type == OPERATOR) {
                operatorType = "OPERATOR";
            } else if (Type == REPORTER) {
                operatorType = "REPORTER";
            }
            message = "Member details loaded";
        } else {
            result = "error";
            message = "Unable to edit details";
        }
        try {
            json.put("_result", result);
            json.put("name", name);
            json.put("email", email);
            json.put("phoneno", phoneno);
            json.put("status", status);
            json.put("operatorType", operatorType);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            out.print(json);
            out.flush();
        }
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
