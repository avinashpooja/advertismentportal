package com.mollatech.SGportal.members;

import com.mollatech.serviceguard.nucleus.db.PartnerDetails;
import com.mollatech.serviceguard.nucleus.db.SgUsers;
import com.mollatech.serviceguard.nucleus.db.connector.management.UsersManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author Bluebricks
 */
public class addTeamMember extends HttpServlet {

    static final int ACTIVE_STATUS = 1;

    static final int SUSPENDED_STATUS = 0;

    static final int PENDING_STATUS = -2;

    public final int PASSWORD = 1;

    public static final int USER_PASSWORD = 1;

    public final int OPERATOR = 1;

    public final int REPORTER = 2;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        response.setContentType("text/html;charset=UTF-8");
        JSONObject json = new JSONObject();
        String result = "success";
        String message = "Team Member added successfully";
        PrintWriter out = response.getWriter();
        String memName = request.getParameter("memName");
        String pName = request.getParameter("pName");
        PartnerDetails pDetails = (PartnerDetails) request.getSession().getAttribute("_partnerDetails");
        String memEmail = request.getParameter("memEmail");
        String memMobileNo = request.getParameter("memMobileNo");
        String Operator = request.getParameter("memOperator");
        int pStatus = PENDING_STATUS;
        String memStatus = request.getParameter("memStatus");
        int imemStatus = 0;
        if (memStatus != null) {
            imemStatus = Integer.parseInt(memStatus);
        }
        String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
        String ChannelId = (String) request.getSession().getAttribute("_ChannelId");
        UsersManagement um = new UsersManagement();
        if (SessionId != null) {
            SgUsers user = um.getSgUsersByEmailPhone(SessionId, ChannelId, memEmail, memMobileNo);
            if (user == null) {
                user = new SgUsers();
                user.setUsername(memName);
                user.setEmail(memEmail);
                user.setChannelid(ChannelId);
                user.setPhone(memMobileNo);
                user.setAttempts(0);
                user.setPasswordupdatedOn(new Date());
                user.setCreatedon(new Date());
                user.setLastlogindate(new Date());
                if (Operator.equalsIgnoreCase("OPERATOR")) {
                    user.setType(OPERATOR);
                }
                if (Operator.equalsIgnoreCase("REPORTER")) {
                    user.setType(REPORTER);
                }
                user.setPartnerid(pDetails.getPartnerId());
                user.setStatus(ACTIVE_STATUS);
                int Errorcode = um.AddSgUsers(SessionId, ChannelId, user);
                if (Errorcode == 0) {
                    message = "Team Member added successfully.";
                    json.put("_result", result);
                    json.put("_message", message);
                }
            } else {
                message = "User Already Exists.";
                json.put("_result", result);
                json.put("_message", message);
            }
        }
        try {
            json.put("_result", result);
            json.put("_message", message);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            out.print(json);
            out.flush();
        }
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
              processRequest(request, response);
       
    }


    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
              processRequest(request, response);
       
    }


    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
