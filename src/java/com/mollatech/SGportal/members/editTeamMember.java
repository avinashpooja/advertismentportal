package com.mollatech.SGportal.members;

import com.mollatech.serviceguard.nucleus.db.PartnerDetails;
import com.mollatech.serviceguard.nucleus.db.SgUsers;
import com.mollatech.serviceguard.nucleus.db.connector.management.UsersManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author Bluebricks
 */
public class editTeamMember extends HttpServlet {

    static final int ACTIVE_STATUS = 1;

    static final int SUSPENDED_STATUS = 0;

    static final int PENDING_STATUS = -2;

    public final int PASSWORD = 1;

    public static final int USER_PASSWORD = 1;

    public final int OPERATOR = 1;

    public final int REPORTER = 2;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        JSONObject json = new JSONObject();
        String result = "success";
        String message = "Team Member Updated successfully";
        PrintWriter out = response.getWriter();
        String memID = request.getParameter("ememID");
        String memName = request.getParameter("ememName");
        PartnerDetails pDetails = (PartnerDetails) request.getSession().getAttribute("_partnerDetails");
        String channelId = (String) request.getSession().getAttribute("_ChannelId");
        String partnerId = pDetails.getPartnerName();
        String memEmail = request.getParameter("ememEmail");
        String memMobileNo = request.getParameter("ememMobileNo");
        String Operator = request.getParameter("ememOperator");
        int pStatus = PENDING_STATUS;
        String memStatus = request.getParameter("ememStatus");
        int imemStatus = 0;
        if (memStatus != null) {
            imemStatus = Integer.parseInt(memStatus);
        }
        UsersManagement um = new UsersManagement();
        String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
        if (SessionId != null) {
            SgUsers users = um.getSgUsers(SessionId, channelId, memName);
            if (users != null) {
                users.setUsername(memName);
                users.setEmail(memEmail);
                users.setPhone(memMobileNo);
                if (Operator.equalsIgnoreCase("OPERATOR")) {
                    users.setType(OPERATOR);
                }
                if (Operator.equalsIgnoreCase("REPORTER")) {
                    users.setType(REPORTER);
                }
                int Errorcode = um.editSgUsers(SessionId, channelId, users);
                if (Errorcode == 0) {
                    message = "Member details are updated Successfully.";
                    json.put("_result", result);
                    json.put("_message", message);
                } else if (Errorcode == 1) {
                    result = "error";
                    message = "User Details Changed Succesfully but email could not be sent";
                } else if (Errorcode == 9) {
                    result = "error";
                    message = "User Details Changed Succesfully but failed to send alert";
                }
            }
        }
        try {
            json.put("_result", result);
            json.put("_message", message);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            out.print(json);
            out.flush();
        }
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
               processRequest(request, response);
       
    }


    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
               processRequest(request, response);
       
    }


    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
