/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.SGportal.reports;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;
import com.mollatech.serviceguard.nucleus.db.Accesspoint;
import com.mollatech.serviceguard.nucleus.db.PartnerDetails;
import com.mollatech.serviceguard.nucleus.db.ResourceDetails;
import com.mollatech.serviceguard.nucleus.db.connector.management.AccessPointManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.RequestTrackingManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.ResourceManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author abhishekingle
 */
@WebServlet(name = "APITranscationForMonth", urlPatterns = {"/APITranscationForMonth"})
public class APITranscationForMonth extends HttpServlet {

    final int ALLOWED = 0;
    final boolean failedTranscation  = false;
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        PrintWriter out = response.getWriter();                      
        String channelId = (String) request.getSession().getAttribute("_ChannelId");
        String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
        PartnerDetails partnerObj = (PartnerDetails) request.getSession().getAttribute("_partnerDetails");
        int partnerid = -1, apid = -1, version = -1, resid = -1;
        int apiAllowedTranscationCount;
        String _accesspointPlusResource = request.getParameter("_accesspointPlusResource");
        String _version = request.getParameter("_version");                
        String _APIForAPICallReport = request.getParameter("_api");
        String month = request.getParameter("_month");
        String year = request.getParameter("_year");
        DateFormat sdf = new SimpleDateFormat("yyyy-MMM-dd");        
        //DateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd");
        
        // Date formatter for C3 Graph
        DateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try{
            String[] accAndRes = _accesspointPlusResource.split(":");
            Accesspoint accesspoint = new AccessPointManagement().getAccessPointByName(SessionId, channelId, accAndRes[0]);
            ResourceDetails rs= new ResourceManagement().getResourceByName(SessionId, channelId, accAndRes[1]);
            Date today = sdf.parse(year + "-" + month + "-01");            
            String incDate;
            today.setHours(0);
            today.setMinutes(0);
            today.setSeconds(0);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(today);
            int maxDay = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
            if(accesspoint != null){
                apid = accesspoint.getApId();
            }
            if(rs != null){
                resid = rs.getResourceId();
            }
            if(_version != null && !_version.isEmpty()){
                version = Integer.parseInt(_version);
            }
            if(partnerObj != null){
                partnerid = partnerObj.getPartnerId();
            }
            ArrayList<line> sample = new ArrayList<line>();
            for(int co=0; co<maxDay; co++){
                if(co != 0){
                    calendar.add(Calendar.DATE, 1);
                }
                Date endDate = calendar.getTime();
                endDate.setHours(0);
                endDate.setMinutes(0);
                endDate.setSeconds(0);
                calendar.setTime(endDate);
                incDate = sdf.format(calendar.getTime());
                apiAllowedTranscationCount = new RequestTrackingManagement().getAPITranscationByStatus(channelId, apid, resid, partnerid, _APIForAPICallReport, version,ALLOWED, endDate,failedTranscation);
//                System.out.println("Date No"+co + "Date "+incDate);
                String datess = formatter1.format(endDate);
                sample.add(new line(apiAllowedTranscationCount, datess));
            }
            Gson gson = new Gson();
            JsonElement element = gson.toJsonTree(sample, new TypeToken<List<line>>() {
            }.getType());
            JsonArray jsonArray = element.getAsJsonArray();
            out.print(jsonArray);
        }catch(Exception e){
            e.printStackTrace();
        }finally {
            out.close();
        }
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
