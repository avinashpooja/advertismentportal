package com.mollatech.SGportal.reports;

import java.util.Date;

/**
 *
 * @author mohanish
 */
public class AreaChartGraph {

    String strDate;

    float intTotal;
    Integer callCount;
    
    public AreaChartGraph(String strDate, float intTotal) {
        this.intTotal = intTotal;
        this.strDate = strDate;
    }
    public AreaChartGraph(String strDate, float intTotal,Integer callCount) {
        this.intTotal = intTotal;
        this.strDate = strDate;
        this.callCount = callCount;
    }
    public String getStrDate() {
        return strDate;
    }

    public void setStrDate(String strDate) {
        this.strDate = strDate;
    }

    public float getIntTotal() {
        return intTotal;
    }

    public void setIntTotal(float intTotal) {
        this.intTotal = intTotal;
    }
}
