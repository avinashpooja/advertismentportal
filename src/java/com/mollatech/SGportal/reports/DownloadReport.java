package com.mollatech.SGportal.reports;

import com.mollatech.serviceguard.nucleus.db.Monitortracking;
import com.mollatech.serviceguard.nucleus.db.connector.management.MonitorReportManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.MonitorSettingsManagement;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author pramod
 */
public class DownloadReport extends HttpServlet {

    private static int PDF_TYPE = 0;

    private static int CSV_TYPE = 1;

    private static int TXT_TYPE = 2;

    private static final int BUFSIZE = 4096;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            String sessionId = (String) request.getSession().getAttribute("_partnerSessionId");
            int type = Integer.parseInt(request.getParameter("_reporttype"));
            String name = request.getParameter("name");
            int monitorType = Integer.parseInt(request.getParameter("monitorType"));
            String checkfor = request.getParameter("ChechFor");
            int iFormat = -9999;
            if (PDF_TYPE == type) {
                iFormat = PDF_TYPE;
            } else if (CSV_TYPE == type) {
                iFormat = CSV_TYPE;
            } else {
                iFormat = TXT_TYPE;
            }
            String filepath = null;
            Monitortracking[] mtrack = new MonitorSettingsManagement().getMonitorTrackingByName(sessionId, null, name);
            filepath = new MonitorReportManagement().generateReport(sessionId, mtrack, type, monitorType, checkfor);
            File file = new File(filepath);
            int length = 0;
            ServletOutputStream outStream = response.getOutputStream();
            ServletContext context = getServletConfig().getServletContext();
            String mimetype = context.getMimeType(filepath);
            if (mimetype == null) {
                mimetype = "application/octet-stream";
            }
            response.setContentType(mimetype);
            response.setContentLength((int) file.length());
            String fileName = (new File(filepath)).getName();
            response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
            byte[] byteBuffer = new byte[BUFSIZE];
            DataInputStream in = new DataInputStream(new FileInputStream(file));
            while ((in != null) && ((length = in.read(byteBuffer)) != -1)) {
                outStream.write(byteBuffer, 0, length);
            }
            in.close();
            outStream.close();
            file.delete();
        } catch (Exception e) {
        }
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
