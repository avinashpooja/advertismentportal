package com.mollatech.SGportal.reports;

/**
 *
 * @author mollatech1
 */
public class bar {

    String label;

    int value;

    int value1;

    int value2;

    int value3;

    int value4;

    float value5;

    public bar(int value, String label) {
        this.label = label;
        this.value = value;
    }

    public bar(float value5, String label) {
        this.label = label;
        this.value5 = value5;
    }

    public bar(int value, float value5, String label) {
        this.label = label;
        this.value = value;
        this.value5 = value5;
    }

    public bar(int value, int value1, int value2, int value3, int value4, String label) {
        this.label = label;
        this.value = value;
        this.value1 = value1;
        this.value2 = value2;
        this.value3 = value3;
        this.value4 = value4;
    }

    public bar(String label, int value, int value1, int value2) {
        this.label = label;
        this.value = value;
        this.value1 = value1;
        this.value2 = value2;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public int getValue1() {
        return value1;
    }

    public void setValue1(int value1) {
        this.value1 = value1;
    }

    public int getValue2() {
        return value2;
    }

    public void setValue2(int value2) {
        this.value2 = value2;
    }
}
