/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.SGportal.TMPayment;

import com.mollatech.service.nucleus.crypto.LoadSettings;
import com.mollatech.serviceguard.nucleus.commons.GlobalStatus;
import com.mollatech.serviceguard.nucleus.db.PartnerDetails;
import com.mollatech.serviceguard.nucleus.db.SgPaymentdetails;
import com.mollatech.serviceguard.nucleus.db.SgSubscriptionDetails;
import com.mollatech.serviceguard.nucleus.db.connector.management.PackageSubscriptionManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.PaymentManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

@WebServlet(name = "TMPayment", urlPatterns = {"/TMPayment"})
public class TMPayment extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException  {
        response.setContentType("text/html;charset=UTF-8");
        JSONObject json = new JSONObject();
        String result = "success";
        String message = "TM PG call sucessfully!!!";
        PrintWriter out = response.getWriter();

        PartnerDetails parObj = (PartnerDetails) request.getSession().getAttribute("_partnerDetails");
        String channelId = (String) request.getSession().getAttribute("_ChannelId");
        String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
        //String packageName = request.getParameter("_packageName");
        String _packageName = request.getParameter("originalPackageN");
        String _invoiceId = request.getParameter("_invoiceId");
        String _packagePaymentMode = request.getParameter("_paymentMode");
        String _totalPaid = request.getParameter("AMOUNT");
        String _promocodeId = request.getParameter("_promocodeId");
        String _changeOnPackageCharge = request.getParameter("_changeOnPackageCharge");
        String _reactivationCharge = request.getParameter("_reactivationCharge");
        String _cancellationCharge = request.getParameter("_cancellationCharge");
        String _latePenaltyCharge = request.getParameter("_latePenaltyCharge");

        String _loanID = request.getParameter("_loanId");

        // gst, vat, servicetax 
        String _totalAmountWithoutTax = request.getParameter("_totalAmountWithoutTax");
        String _gstTax = request.getParameter("_gstTax");
        String _vatTax = request.getParameter("_vatTax");
        String _serviceTax = request.getParameter("_serviceTax");

        String tmPaymentGatewayURL = LoadSettings.g_sSettings.getProperty("tm.pg.url");
        String tmPGCurrencyCode = LoadSettings.g_sSettings.getProperty("tm.pg.paymentmode");
        String tmPGTotalChildTranx = LoadSettings.g_sSettings.getProperty("tm.pg.totalChildTransaction");
        String tmPGrevenuecode = LoadSettings.g_sSettings.getProperty("tm.pg.revenuecode");

        String tmMerchantSignature = request.getParameter("SIGNATURE");
        String tmTranscationId = request.getParameter("MERCHANT_TRANID");
        String tmMerchantId = request.getParameter("MERCHANTID");

        System.out.println("tmSignature >>>>>>>>>> " + tmMerchantSignature);
        System.out.println("_totalAmountWithoutTax >>> " + _totalAmountWithoutTax);
        System.out.println("_totalPaid >>> " + _totalPaid);

//        int quantity = Integer.parseInt("1");        
//        String invoiceid = "0";
//        double totalPaid = 0;
//        if (_totalPaid != null && !_totalPaid.isEmpty()) {
//            totalPaid = Double.parseDouble(_totalPaid);
//        }
//        if (_invoiceId != null && !_invoiceId.isEmpty()) {
//            invoiceid = _invoiceId;
//        }
        if (_loanID != null && !_loanID.isEmpty()) {
            int loanId = Integer.parseInt(_loanID);

        }
        if (_changeOnPackageCharge == null) {
            _changeOnPackageCharge = "";
        }
        if (_reactivationCharge == null) {
            _reactivationCharge = "";
        }
        if (_cancellationCharge == null) {
            _cancellationCharge = "";
        }
        if (_latePenaltyCharge == null) {
            _latePenaltyCharge = "";
        }
        try {
            //double roundedTotalPaid = (double) Math.round(totalPaid * 100) / 100;
            // String strTMPaymentPage = "https://osestest.tm.com.my/oses/ReqPaymentMode.jsp";
            String url = (request.getRequestURL().toString());
            URL myUrl = new URL(url);
            int port = myUrl.getPort();
            if (myUrl.getProtocol().equals("https") && port == -1) {
                port = 443;
            } else if (myUrl.getProtocol().equals("http") && port == -1) {
                port = 80;
            }
            int retValue = -1;
            String intiURL = myUrl.getProtocol() + "://" + myUrl.getHost() + ":" + port + request.getContextPath() + "/";
            //String urlToReturn = intiURL + "tmPaymentResponse.jsp?_promocodeId=" + _promocodeId + "&_changeOnPackageCharge=" + _changeOnPackageCharge + "&_reactivationCharge=" + _reactivationCharge + "&_cancellationCharge=" + _cancellationCharge + "&_latePenaltyCharge=" + _latePenaltyCharge + "&_originalPackageName=" + _packageName + "&_totalAmountWithoutTax=" + _totalAmountWithoutTax+ "&_gstTax=" + _gstTax+ "&_vatTax=" + _vatTax+ "&_serviceTax=" + _serviceTax + "&_packageName="+_packageName+"&_invoiceId="+_invoiceId+"&_grossAmount="+_totalPaid;
            String urlToReturn = intiURL + "tmPaymentResponse.jsp";
            urlToReturn = "https://developer.tm.com.my:/DeveloperPortal/" + "tmPaymentResponse.jsp";
            SgSubscriptionDetails packageSubscribed = new PackageSubscriptionManagement().getSubscriptionbyPartnerId(SessionId, channelId, parObj.getPartnerId());
            JSONObject data = new JSONObject();
            int TRANSACTIONTYPE = 4;
            data.put("AMOUNT", _totalPaid);
            data.put("MERCHANTID", tmMerchantId);
            data.put("MERCHANTTRANID", tmTranscationId);
            data.put("SIGNATURE", tmMerchantSignature);
            data.put("TRANSACTIONID", "0");
            data.put("TRANSACTIONTYPE", TRANSACTIONTYPE);
            Double packageAmount = null;
            Double changePackageCharge = null;
            Double subscribedserviceCharge = null;
            Double subscribedreactivationCharge = null;
            Double subscribedcancellationCharge = null;
            Double subscribedlatePenaltyCharge = null;
            JSONObject tierOrSlabUsage = (JSONObject) request.getSession().getAttribute("tierOrSlabUsageDetails");
            JSONObject caasServiceUsage = (JSONObject) request.getSession().getAttribute("caasServiceUsageDetails");
            JSONObject pgTransactionUsage = (JSONObject) request.getSession().getAttribute("pgTransactionDetails");
            SgPaymentdetails paymentObj = new PaymentManagement().getPaymentDetailsbyPartnerAndSubscriptionID(packageSubscribed.getBucketId(), parObj.getPartnerId());
            if (paymentObj == null) {
                paymentObj = new SgPaymentdetails();
                paymentObj.setMerchantId(tmMerchantId);
                paymentObj.setTmTranscationId(tmTranscationId);
                paymentObj.setSubscriptionId(packageSubscribed.getBucketId());
                paymentObj.setPartnerId(parObj.getPartnerId());
                paymentObj.setPaymentStatus(GlobalStatus.START_PROCESS);
                paymentObj.setPaymentDate(new Date());
                paymentObj.setPaymentData(data.toString());
                if (_changeOnPackageCharge != null && !_changeOnPackageCharge.isEmpty()) {
                    paymentObj.setChangeOnPackageCharge(Float.parseFloat(_changeOnPackageCharge));
                    changePackageCharge = Double.parseDouble(_changeOnPackageCharge);
                }
                if (_reactivationCharge != null && !_reactivationCharge.isEmpty()) {
                    paymentObj.setReactivationPackageCharge(Float.parseFloat(_reactivationCharge));
                    subscribedreactivationCharge = Double.parseDouble(_reactivationCharge);
                }
                if (_cancellationCharge != null && !_cancellationCharge.isEmpty()) {
                    paymentObj.setCancellationCharge(Float.parseFloat(_cancellationCharge));
                    subscribedcancellationCharge = Double.parseDouble(_cancellationCharge);
                }
                if (_latePenaltyCharge != null && !_latePenaltyCharge.isEmpty()) {
                    paymentObj.setLatePenaltyCharge(Float.parseFloat(_latePenaltyCharge));
                    subscribedlatePenaltyCharge = Double.parseDouble(_latePenaltyCharge);
                }
                if (tierOrSlabUsage != null) {
                    paymentObj.setTierOrSlabUsage(tierOrSlabUsage.toString());
                }
                if(caasServiceUsage != null && caasServiceUsage.length()!=0){
                    paymentObj.setCaasServiceUsage(caasServiceUsage.toString());
                }
                if(pgTransactionUsage != null && pgTransactionUsage.length()!=0){
                    paymentObj.setPgtransactionUsage(pgTransactionUsage.toString());
                }
                
                paymentObj.setInvoiceNo(_invoiceId);
                paymentObj.setSubscriptionId(packageSubscribed.getBucketId());
                
                retValue = new PaymentManagement().createPaymentDetails(SessionId, channelId, paymentObj);
            } else {
                paymentObj.setMerchantId(tmMerchantId);
                paymentObj.setTmTranscationId(tmTranscationId);
                paymentObj.setPaymentStatus(GlobalStatus.START_PROCESS);
                paymentObj.setPaymentDate(new Date());
                paymentObj.setPaymentData(data.toString());
                if (_changeOnPackageCharge != null && !_changeOnPackageCharge.isEmpty()) {
                    paymentObj.setChangeOnPackageCharge(Float.parseFloat(_changeOnPackageCharge));
                    changePackageCharge = Double.parseDouble(_changeOnPackageCharge);
                }
                if (_reactivationCharge != null && !_reactivationCharge.isEmpty()) {
                    paymentObj.setReactivationPackageCharge(Float.parseFloat(_reactivationCharge));
                    subscribedreactivationCharge = Double.parseDouble(_reactivationCharge);
                }
                if (_cancellationCharge != null && !_cancellationCharge.isEmpty()) {
                    paymentObj.setCancellationCharge(Float.parseFloat(_cancellationCharge));
                    subscribedcancellationCharge = Double.parseDouble(_cancellationCharge);
                }
                if (_latePenaltyCharge != null && !_latePenaltyCharge.isEmpty()) {
                    paymentObj.setLatePenaltyCharge(Float.parseFloat(_latePenaltyCharge));
                    subscribedlatePenaltyCharge = Double.parseDouble(_latePenaltyCharge);
                }
                if (tierOrSlabUsage != null) {
                    paymentObj.setTierOrSlabUsage(tierOrSlabUsage.toString());
                }
                if(caasServiceUsage != null && caasServiceUsage.length()!=0){
                    paymentObj.setCaasServiceUsage(caasServiceUsage.toString());
                }
                if(pgTransactionUsage != null && pgTransactionUsage.length()!=0){
                    paymentObj.setPgtransactionUsage(pgTransactionUsage.toString());
                }
                paymentObj.setInvoiceNo(_invoiceId);
                paymentObj.setSubscriptionId(packageSubscribed.getBucketId());
                retValue = new PaymentManagement().updatePaymentDetails(paymentObj);

            }
            if (retValue >= 0) {
                request.getSession().setAttribute("_promocodeId", _promocodeId);
                request.getSession().setAttribute("_changeOnPackageCharge", _changeOnPackageCharge);
                request.getSession().setAttribute("_reactivationCharge", _reactivationCharge);
                request.getSession().setAttribute("_cancellationCharge", _cancellationCharge);
                request.getSession().setAttribute("_latePenaltyCharge", _latePenaltyCharge);
                request.getSession().setAttribute("_originalPackageName", _packageName);
                request.getSession().setAttribute("_totalAmountWithoutTax", _totalAmountWithoutTax);
                request.getSession().setAttribute("_gstTax", _gstTax);
                request.getSession().setAttribute("_vatTax", _vatTax);
                request.getSession().setAttribute("_serviceTax", _serviceTax);
                request.getSession().setAttribute("_invoiceId", _invoiceId);
                request.getSession().setAttribute("_grossAmount", _totalPaid);
                request.getSession().setAttribute("_packageType", _packagePaymentMode);
                request.getSession().setAttribute("_tmTranscationId", tmTranscationId);
                request.getSession().setAttribute("_loanId", _loanID);
                request.getSession().setAttribute("_tmMerchantId", tmMerchantId);
                String childTranscation = "<txn>"
                        + "<child_txn>"
                        + "<sub_merchant_id>" + tmMerchantId + "</sub_merchant_id>"
                        + "<sub_order_id>11100111</sub_order_id>"
                        + "<gross_amount>" + _totalPaid + "</gross_amount>"
                        + "<gbt_amount>" + _totalPaid + "</gbt_amount>"
                        + //"<gbt_amount>"+_totalAmountWithoutTax+"</gbt_amount>" +
                        "<nett_amount>00.00</nett_amount>"
                        + "<account_no>" + tmMerchantId + "</account_no>"
                        + "<revenue_code>" + tmPGrevenuecode + "</revenue_code>"
                        + "<misc_amount>00.00</misc_amount>"
                        + "</child_txn>"
                        + "</txn>";

                System.out.println("URL                     >> " + tmPaymentGatewayURL + "\n");
                System.out.println("MERCHANTID              >> " + tmMerchantId + "\n");
                System.out.println("MERCHANT_TRANID         >> " + tmTranscationId + "\n");
                System.out.println("CURRENCYCODE            >> " + tmPGCurrencyCode + "\n");
                System.out.println("AMOUNT                  >> " + _totalPaid + "\n");
                System.out.println("CUSTNAME                >> " + parObj.getPartnerName() + "\n");
                System.out.println("CUSTEMAIL               >> " + parObj.getPartnerEmailid() + "\n");
                System.out.println("RETURN_URL              >> " + urlToReturn + "\n");
                System.out.println("SIGNATURE               >> " + tmMerchantSignature + "\n");
                System.out.println("TOTAL_CHILD_TRANSACTION >> " + "1" + "\n");
                System.out.println("CHILD_TRANSACTION       >> " + childTranscation + "\n");

                json.put("merchantID", tmMerchantId);
                json.put("merchantTranscationId", tmTranscationId);
                json.put("currencyCode", tmPGCurrencyCode);
                json.put("amount", _totalPaid);
                json.put("custName", parObj.getPartnerName());
                json.put("custEMAIL", parObj.getPartnerEmailid());
                json.put("returnURL", urlToReturn);
                json.put("signature", tmMerchantSignature);
                json.put("totalChildTransaction", tmPGTotalChildTranx);
                json.put("childTranscation", childTranscation);
                json.put("strXPAYPaymentPage", tmPaymentGatewayURL);
                json.put("result", result);
                json.put("message", message);
            } else {
                json.put("result", "error");
                json.put("message", "Failed to maintain transcation and merchant details");
            }

        } catch (Exception e) {
            e.printStackTrace();
            json.put("result", "error");
            json.put("message", "Failed to create TMPG transcation");
        } finally {
            out.print(json);
            out.flush();
            out.close();
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
        }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
