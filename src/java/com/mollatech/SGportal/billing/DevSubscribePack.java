/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.SGportal.billing;

import com.mollatech.serviceguard.nucleus.db.SgSubscriptionDetails;
import com.mollatech.serviceguard.nucleus.db.connector.management.PackageSubscriptionManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author bluebricks
 */
@WebServlet(name = "DevSubscribePack", urlPatterns = {"/DevSubscribePack"})
public class DevSubscribePack extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
           response.setContentType("application/json");
    response.setCharacterEncoding("UTF-8");
       PrintWriter out = response.getWriter();
       JSONObject json = new JSONObject();
       String result = "success";
       Integer partnerId = (Integer) request.getSession().getAttribute("_partnerID");
       SgSubscriptionDetails subscriObject1 = new PackageSubscriptionManagement().getPartnerSubscriptionbyPId(partnerId);
       SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
       String packageName = null;
        String expireOn = null;
       try {
           if(subscriObject1 != null){
                packageName = subscriObject1.getBucketName();
                expireOn = sdf.format(subscriObject1.getExpiryDateNTime());
           }else{
               packageName = "NA";
           }
                json.put("expire", expireOn);
                json.put("package", packageName);
            } catch (Exception e) {
                e.printStackTrace();
            }
            out.print(json.toString());
            out.flush();
            return;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
