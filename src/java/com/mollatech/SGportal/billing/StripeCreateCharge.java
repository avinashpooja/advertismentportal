/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.SGportal.billing;

import com.mollatech.service.nucleus.crypto.AES;
import com.mollatech.service.nucleus.crypto.LoadSettings;
import com.mollatech.serviceguard.nucleus.db.SgAdvertiserDetails;
import com.mollatech.serviceguard.nucleus.db.SgApprovedAdPackagedetails;
import com.mollatech.serviceguard.nucleus.db.connector.management.AdvertiserManagement;
import com.stripe.Stripe;
import com.stripe.model.Customer;
import com.stripe.model.Subscription;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.json.JSONObject;

/**
 *
 * @author abhishekingle
 */
@WebServlet(name = "StripeCreateCharge", urlPatterns = {"/StripeCreateCharge"})
public class StripeCreateCharge extends HttpServlet {
    static final Logger logger = Logger.getLogger(StripeCreateCharge.class);
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        logger.info("Requested Servlet is StripeCreateCharge at " + new Date());
        logger.info("Remote Ip Address is " + request.getRemoteAddr());
        //response.setContentType("application/json");              
        PrintWriter out = response.getWriter();
        JSONObject json = new JSONObject();
        SgAdvertiserDetails details = (SgAdvertiserDetails) request.getSession().getAttribute("_advertisorDetails");
        String _originalPackageName = (String) request.getSession().getAttribute("_originalPackageName");
        String _invoiceId = (String) request.getSession().getAttribute("_invoiceId");
        String totalPaymentAmountWithTax = (String) request.getSession().getAttribute("_grossAmount");  
        String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
        SgApprovedAdPackagedetails packageObject = (SgApprovedAdPackagedetails) request.getSession().getAttribute("_packageObject");
        try {            
            AES aesObj = new AES();
            String taxDetails = packageObject.getTax();
            //JSONObject taxJson = new JSONObject(taxDetails);
            Float gstTax = Float.parseFloat("0");
            if(taxDetails != null && !taxDetails.isEmpty()){
                gstTax = Float.parseFloat(taxDetails);
            }
            String stripeapikey = LoadSettings.g_sSettings.getProperty("stripe.apiKey");
            stripeapikey = aesObj.PINDecrypt(stripeapikey, AES.getSignature());
            Stripe.apiKey = stripeapikey;
            logger.info("StripeAPIKey " + stripeapikey);
            //String stripeAmount = totalPaymentAmountWithTax.replace(".", "");
            Subscription subscription = null;
            //stripeAmount = stripeAmount+"00";
            // Token is created using Stripe.js or Checkout!
            // Get the payment token ID submitted by the form:            
            String stripeToken = request.getParameter("stripeToken");
            logger.debug("Value of stripePaymentToken  = " + stripeToken);
            //check with stripe 
            String stripeCustId = details.getStripeCustomerId();
            if(stripeCustId != null){
                //Customer customer = Customer.retrieve(stripeCustId);                
                    // Canceling subscriptions

//                    subscription = customer.getSubscriptions().retrieve(details.getStripeSubscriptionId());
//                    subscription.cancel(null);
                    
                    // create subscription
                    logger.info("customer subscribe for plan for recurrence billing "+details.getEmail());
                    Map<String, Object> item = new HashMap<String, Object>();
                    item.put("plan", packageObject.getRecurrenceBillingPlanId());

                    Map<String, Object> items = new HashMap<String, Object>();
                    items.put("0", item);

                    Map<String, Object> recurrencePlan = new HashMap<String, Object>();
                    recurrencePlan.put("customer", stripeCustId);
                    recurrencePlan.put("items", items);
                    recurrencePlan.put("tax_percent", gstTax);
                    subscription = Subscription.create(recurrencePlan);
                    String planSubscriptionStatus = subscription.getStatus();
                    logger.info("customer successfully subscribe for plan "+packageObject.getRecurrenceBillingPlanId()+" with details "+planSubscriptionStatus);                                                
                    SgAdvertiserDetails partObj = new AdvertiserManagement().getAdvertiserDetails(details.getAdId());
                    partObj.setStripeCustomerId(stripeCustId);
                    partObj.setStripeSubscriptionId(subscription.getId());
                    new AdvertiserManagement().updateDetails(partObj); 
                    request.getSession().setAttribute("_advertisorDetails", partObj);
            }else{                
                //create customer with stripe
                logger.info("Creating stripe customer for "+details.getEmail());
                Map<String, Object> customerParams = new HashMap<String, Object>();
//                Map<String, Object> source = new HashMap<String, Object>();
//                source.put("token", stripeToken);
                customerParams.put("email", details.getEmail());
                customerParams.put("source", stripeToken);
                //params.put("source", source);
                
                Customer customer = Customer.create(customerParams);
                String custId = customer.getId();
                logger.info("Stripe customer created  for "+details.getEmail() +" with custId "+custId);
                                
                // subscribe customer to a plan for recureence
                logger.info("customer subscribe for plan for recurrence billing "+details.getEmail());
                Map<String, Object> item = new HashMap<String, Object>();
                item.put("plan", packageObject.getRecurrenceBillingPlanId());

                Map<String, Object> items = new HashMap<String, Object>();
                items.put("0", item);

                Map<String, Object> recurrencePlan = new HashMap<String, Object>();
                recurrencePlan.put("customer", custId);
                recurrencePlan.put("items", items);
                recurrencePlan.put("tax_percent", gstTax);
                subscription = Subscription.create(recurrencePlan);
                String planSubscriptionStatus = subscription.getStatus();
                logger.info("customer successfully subscribe for plan "+packageObject.getRecurrenceBillingPlanId()+" with details "+planSubscriptionStatus);                                                
                SgAdvertiserDetails partObj = new AdvertiserManagement().getAdvertiserDetails(details.getAdId());
                partObj.setStripeCustomerId(custId);
                partObj.setStripeSubscriptionId(subscription.getId());
                new AdvertiserManagement().updateDetails(partObj);
                request.getSession().setAttribute("_advertisorDetails", partObj);
            }
            request.getSession().setAttribute("chargeObj", subscription);
            
            System.out.println("charge" + subscription);
//            // Charge the user's card:
//            Map<String, Object> params = new HashMap<String, Object>();
//            params.put("amount", stripeAmount);
//            params.put("currency", "usd");
//            params.put("description", "Developer "+details.getPartnerName()+" subscribe for "+_originalPackageName+" amount "+totalPaymentAmountWithTax+" at "+new SimpleDateFormat("dd/mm/yyyy HH:mm:ss").format(new Date()));
//            Map<String, String> initialMetadata = new HashMap<String, String>();
//            initialMetadata.put("Developer", details.getPartnerName());
//            initialMetadata.put("Subscribe package", _originalPackageName);
//            initialMetadata.put("Subscription Date", new SimpleDateFormat("dd/mm/yyyy HH:mm:ss").format(new Date()));
//            initialMetadata.put("stripeToken", stripeToken);
//            initialMetadata.put("invoiceId", _invoiceId);
//            params.put("metadata", initialMetadata);
//            params.put("source", stripeToken);
//            Charge charge = Charge.create(params);
//            request.getSession().setAttribute("chargeObj", charge);
//            System.out.println("charge" + charge);
//            json.put("_result", "success");
//            json.put("_message", "payment complete");
//            return;  
//            RequestDispatcher requestDispatcher; 
//            requestDispatcher = request.getRequestDispatcher("/stripeResponse.jsp");
//            requestDispatcher.forward(request, response);           
            response.sendRedirect("./stripeResponse.jsp");
           // rd.forward(request, response);
        }catch(Exception ex){
            ex.printStackTrace();
            json.put("_result", "error");
            json.put("_message", "Transaction failed");
            return;
        }finally {
           // out.print(json);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
              processRequest(request, response);
       
    }


    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
              processRequest(request, response);
       
    }


    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
