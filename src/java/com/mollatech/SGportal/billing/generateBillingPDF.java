package com.mollatech.SGportal.billing;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.SortedMap;
import java.util.TreeMap;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

/**
 *
 * @author mohanish
 */
@WebServlet(name = "generateBillingPDF", urlPatterns = { "/generateBillingPDF" })
public class generateBillingPDF extends HttpServlet {

    private static int PDF_TYPE = 0;

    private static int CSV_TYPE = 1;

    private static int TEXT_TYPE = 2;

    private static final int BUFSIZE = 4096;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String ChannelId = (String) request.getSession().getAttribute("_ChannelId");
        SortedMap apiDetails = (SortedMap) request.getSession().getAttribute("partnerBill");
        if (apiDetails == null) {
            apiDetails = new TreeMap();
        }
        String sdate = request.getParameter("_sdate");
        String stime = request.getParameter("_stime");
        String edate = request.getParameter("_edate");
        String format = request.getParameter("_format");
        
        String apiUsageEnvironment = request.getParameter("_apiUsageEnvironment");
        String apiUsageAccesspoint = request.getParameter("_apiUsageAccesspoint");
        DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
        DateFormat timeformat = new SimpleDateFormat("hh:mm a");
        String filepath = null;
        try {
            Date startDate = null;
            if (sdate != null && !sdate.isEmpty()) {
                startDate = formatter.parse(sdate);
            }
            Date startTime = null;
            if (stime != null && !stime.isEmpty()) {
                startTime = timeformat.parse(stime);
            }
            Date endDate = null;
            if (edate != null && !edate.isEmpty()) {
                endDate = formatter.parse(edate);
            }
            Date endTime = null;
            if (stime != null && !stime.isEmpty()) {
                endTime = timeformat.parse(stime);
            }
            Calendar current = Calendar.getInstance();
            Calendar currenttime = Calendar.getInstance();
            currenttime.setTime(endTime);
            current.setTime(endDate);
            current.set(Calendar.HOUR_OF_DAY, currenttime.get(Calendar.HOUR_OF_DAY));
            current.set(Calendar.MINUTE, currenttime.get(Calendar.MINUTE));
            GregorianCalendar gregorianCalendar = new GregorianCalendar();
            DatatypeFactory datatypeFactory = DatatypeFactory.newInstance();
            XMLGregorianCalendar endDates = datatypeFactory.newXMLGregorianCalendar(gregorianCalendar);
            endDates.setDay(current.get(Calendar.DAY_OF_MONTH));
            endDates.setMonth(current.get(Calendar.MONTH));
            endDates.setYear(current.get(Calendar.YEAR));
            endDates.setHour(current.get(Calendar.HOUR));
            endDates.setMinute(current.get(Calendar.MINUTE));
            endDates.setSecond(current.get(Calendar.SECOND));
            endDates.setMillisecond(current.get(Calendar.MILLISECOND));
            endDate = endDates.toGregorianCalendar().getTime();
            current = Calendar.getInstance();
            currenttime = Calendar.getInstance();
            XMLGregorianCalendar startDates = datatypeFactory.newXMLGregorianCalendar(gregorianCalendar);
            current.setTime(startDate);
            currenttime.setTime(startTime);
            current.set(Calendar.HOUR_OF_DAY, currenttime.get(Calendar.HOUR_OF_DAY));
            current.set(Calendar.MINUTE, currenttime.get(Calendar.MINUTE));
            startDates.setDay(current.get(Calendar.DAY_OF_MONTH));
            startDates.setMonth(current.get(Calendar.MONTH));
            startDates.setYear(current.get(Calendar.YEAR));
            startDates.setHour(current.get(Calendar.HOUR));
            startDates.setMinute(current.get(Calendar.MINUTE));
            startDates.setSecond(current.get(Calendar.SECOND));
            startDates.setMillisecond(current.get(Calendar.MILLISECOND));
            startDate = startDates.toGregorianCalendar().getTime();
            int iFormat = -9999;
            if (format != null && !format.isEmpty()) {
                iFormat = Integer.parseInt(format);
            }
            if (PDF_TYPE == iFormat) {
                iFormat = PDF_TYPE;
            } else if (CSV_TYPE == iFormat) {
                iFormat = CSV_TYPE;
            } else {
                iFormat = TEXT_TYPE;
            }
            //filepath = new DailyTxManagement().generateReport(iFormat, ChannelId, partnerObj, startDate, endDate);
            //filepath = new DailyTxManagement().generateAPIUsageReport(iFormat, ChannelId, apiDetails, endDate, endDate, apiUsageEnvironment, apiUsageAccesspoint);
        } catch (Exception e) {
            e.printStackTrace();
        }
        File file = new File(filepath);
        int length = 0;
        ServletOutputStream outStream = response.getOutputStream();
        ServletContext context = getServletConfig().getServletContext();
        String mimetype = context.getMimeType(filepath);
        if (mimetype == null) {
            mimetype = "application/octet-stream";
        }
        response.setContentType(mimetype);
        response.setContentLength((int) file.length());
        String fileName = (new File(filepath)).getName();
        response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
        byte[] byteBuffer = new byte[BUFSIZE];
        DataInputStream in = new DataInputStream(new FileInputStream(file));
        while ((in != null) && ((length = in.read(byteBuffer)) != -1)) {
            outStream.write(byteBuffer, 0, length);
        }
        in.close();
        outStream.close();
        file.delete();
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
