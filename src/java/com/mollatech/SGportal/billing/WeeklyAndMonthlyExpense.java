/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.SGportal.billing;

import com.mollatech.serviceguard.nucleus.db.SgAdvertiserDetails;
import com.mollatech.serviceguard.nucleus.db.SgAdvertisertracking;
import com.mollatech.serviceguard.nucleus.db.connector.management.AdvertisementTrackingManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author bluebricks
 */
@WebServlet(name = "WeeklyAndMonthlyExpense", urlPatterns = {"/WeeklyAndMonthlyExpense"})
public class WeeklyAndMonthlyExpense extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();
        try {
            JSONObject json = new JSONObject();
            String timePeriod = request.getParameter("timePeriod");
            String ChannelId = (String) request.getSession().getAttribute("_ChannelId");
            SgAdvertiserDetails usrObj = (SgAdvertiserDetails) request.getSession().getAttribute("_advertisorDetails");
            
           AdvertisementTrackingManagement hTmanagement = new AdvertisementTrackingManagement();
            double totalAmount = 0.0;            
            int days = 29;
            if (timePeriod.equals("weekly")) {
                days = 6;
            } else {
                days = 29;
            }
            for (int j = days; j >= 0; j--) {
                Calendar cal = Calendar.getInstance();
                cal.add(Calendar.DATE, -j);
                Date _startDate = cal.getTime();
                _startDate.setHours(00);
                _startDate.setMinutes(00);
                _startDate.setSeconds(00);
                Date _endDate = cal.getTime();
                _endDate.setHours(23);
                _endDate.setMinutes(59);
                _endDate.setSeconds(59);
                String[] count = null;
               SgAdvertisertracking[] dailytx = hTmanagement.getTxDetailsByAdvatisorId(usrObj.getAdId(), _startDate, _endDate,null);
                if (dailytx != null) {
                    for (int i = 0; i < dailytx.length; i++) {
                        totalAmount += dailytx[i].getCreditDeducted();
                    }
                }
            }
            try {
                BigDecimal modifiedCredit = new BigDecimal(totalAmount);
                modifiedCredit = modifiedCredit.setScale(2, BigDecimal.ROUND_HALF_EVEN);
                json.put("amount", modifiedCredit);
            } catch (Exception e) {
                e.printStackTrace();
            }
            out.print(json.toString());
            out.flush();
            return;

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
