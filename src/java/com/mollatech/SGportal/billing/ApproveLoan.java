package com.mollatech.SGportal.billing;

import static com.mollatech.SGportal.partner.EmailAPIToken.PENDING;
import static com.mollatech.SGportal.partner.EmailAPIToken.SENT;
import com.mollatech.service.nucleus.crypto.LoadSettings;
import com.mollatech.serviceguard.connector.communication.SGStatus;
import com.mollatech.serviceguard.nucleus.commons.GlobalStatus;
import com.mollatech.serviceguard.nucleus.db.Operators;
import com.mollatech.serviceguard.nucleus.db.PartnerDetails;
import com.mollatech.serviceguard.nucleus.db.SgCreditInfo;
import com.mollatech.serviceguard.nucleus.db.SgLoanDetails;
import com.mollatech.serviceguard.nucleus.db.connector.management.CreditManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.LoanManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.OperatorsManagement;
import com.mollatech.serviceguard.nucleus.settings.SendNotification;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.json.JSONObject;

/**
 *
 * @author mohanish
 */
@WebServlet(name = "ApproveLoan", urlPatterns = { "/ApproveLoan" })
public class ApproveLoan extends HttpServlet {

    static final Logger logger = Logger.getLogger(ApproveLoan.class);

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.info("Request servlet is #ApproveLoan from #PPortal at " + new Date());
        logger.info("Remote Ip Address is " + request.getRemoteAddr());
        response.setContentType("application/json");
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        String result = "success";
        String message = "Loan approved successfully.";
        String channelName = "ServiceGuard Portal";
        String channelId = (String) request.getSession().getAttribute("_ChannelId");
        String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
        PartnerDetails partnerObj = (PartnerDetails) request.getSession().getAttribute("_partnerDetails");
        int retValue = -1;
        String loanAmount = request.getParameter("_loanAmount");
        String interestRate = request.getParameter("_interestRate");
        String subscriptionId = request.getParameter("_subscriptionId");
        String pacName = request.getParameter("_package");
        logger.debug("Value of _loanAmount = " + loanAmount);
        logger.debug("Value of _interestRate = " + interestRate);
        logger.debug("Value of subscriptionId = " + subscriptionId);
        float loanA = Float.NaN;
        float interestR = Float.NaN;
        int subscriptionIdd = 0;
        if (loanAmount != null) {
            loanA = Float.parseFloat(loanAmount);
        }
        if (interestRate != null) {
            interestR = Float.parseFloat(interestRate);
        }
        if (subscriptionId != null) {
            subscriptionIdd = Integer.parseInt(subscriptionId);
        }
        SgLoanDetails loanObj = null;
        SgCreditInfo creInfoObj = null;
        loanObj = new LoanManagement().getLoanDetails(SessionId, channelId, partnerObj.getPartnerId(), subscriptionIdd);
        if (loanObj != null) {
            if (loanObj.getStatus() == GlobalStatus.APPROVED) {
                result = "error";
                message = "Loan already approved";
                json.put("result", result);
                logger.debug("Response of #ApproveLoan from #PPortal Servlet's Parameter  result is " + result);
                json.put("message", message);
                logger.debug("Response of #ApproveLoan from #PPortal Servlet's Parameter  message is " + message);
                out.print(json);
                out.flush();
                return;
            }else if(loanObj.getStatus() == GlobalStatus.PAID){
                loanObj.setChannelId(channelId);
                loanObj.setPartnerId(partnerObj.getPartnerId());
                loanObj.setInterestRate(interestR);
                loanObj.setLoanAmount(loanA);
                loanObj.setSubscriptionId(subscriptionIdd);
                loanObj.setLoanApproveOn(new Date());
                loanObj.setStatus(GlobalStatus.APPROVED);
                retValue = new LoanManagement().editLoanDetails(SessionId,loanObj);
                    if (retValue >= 0) {
                    SgCreditInfo creditObj = new CreditManagement().getDetails(partnerObj.getPartnerId());
                    if (creditObj != null) {
                        double mainCredit = creditObj.getUpdatedMainCredits();
                        mainCredit = loanA + mainCredit;
                        creditObj.setUpdatedMainCredits(mainCredit);
                        if (creditObj.getStatus() == GlobalStatus.SUCCESS) {
                            creditObj.setStatus(GlobalStatus.UPDATED);
                        }
                        retValue = new CreditManagement().updateDetails(creditObj);
                    }
                }
            }
        } else {
            loanObj = new SgLoanDetails();
            loanObj.setChannelId(channelId);
            loanObj.setPartnerId(partnerObj.getPartnerId());
            loanObj.setInterestRate(interestR);
            loanObj.setLoanAmount(loanA);
            loanObj.setSubscriptionId(subscriptionIdd);
            loanObj.setLoanApproveOn(new Date());
            loanObj.setStatus(GlobalStatus.APPROVED);
            retValue = new LoanManagement().addLoanDetails(loanObj);
            if (retValue >= 0) {
                SgCreditInfo creditObj = new CreditManagement().getDetails(partnerObj.getPartnerId());
                if (creditObj != null) {
                    double mainCredit = creditObj.getUpdatedMainCredits();
                    mainCredit = loanA + mainCredit;
                    creditObj.setUpdatedMainCredits(mainCredit);
                    if (creditObj.getStatus() == GlobalStatus.SUCCESS) {
                        creditObj.setStatus(GlobalStatus.UPDATED);
                    }
                    retValue = new CreditManagement().updateDetails(creditObj);
                }
            }
        }
        try {
            
            if (retValue == 0) {
                Operators[] operatorObj = new OperatorsManagement().getAllOperators(channelId);
                if (operatorObj != null) {
                    String[] operatorEmail = new String[operatorObj.length];
                    for (int i = 0; i < operatorObj.length; i++) {
                        operatorEmail[i] = (String) operatorObj[i].getEmailid();
                    }
                    String tmessage = (String) LoadSettings.g_templateSettings.getProperty("email.loan.approved");
                    SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy HH:mm");
                    if (tmessage != null) {
                        Date d = new Date();
                        tmessage = tmessage.replaceAll("#name#", partnerObj.getPartnerName());
                        tmessage = tmessage.replaceAll("#channel#", channelName);
                        tmessage = tmessage.replaceAll("#datetime#", sdf.format(d));
                        tmessage = tmessage.replaceAll("#packageName#", pacName);
                        tmessage = tmessage.replaceAll("#loanAmount#", loanAmount);
                        tmessage = tmessage.replaceAll("#interestRate#", interestRate);
                        tmessage = tmessage.replaceAll("#email#", partnerObj.getPartnerEmailid());
                    }
                    int productType = 3;
                    SGStatus status = new SendNotification().SendEmail(channelId, partnerObj.getPartnerEmailid(), "Loan Details", tmessage, operatorEmail, null, null, null, productType);
                    if (status.iStatus == PENDING || status.iStatus == SENT) {
                        json.put("result", result);
                        logger.debug("Response of #ApproveLoan from #PPortal Servlet's Parameter  result is " + result);
                        json.put("message", message);
                        logger.debug("Response of #ApproveLoan from #PPortal Servlet's Parameter  message is " + message);
                    } else {
                        result = "error";
                        message = "Failed to send email notification.";
                        json.put("result", result);
                        logger.debug("Response of #ApproveLoan from #PPortal Servlet's Parameter  result is " + result);
                        json.put("message", message);
                        logger.debug("Response of #ApproveLoan from #PPortal Servlet's Parameter  message is " + message);
                    }
                }
            } else {
                result = "error";
                message = "Loan does not approved.";
                json.put("result", result);
                logger.debug("Response of #ApproveLoan from #PPortal Servlet's Parameter  result is " + result);
                json.put("message", message);
                logger.debug("Response of #ApproveLoan from #PPortal Servlet's Parameter  message is " + message);
            }
            out.print(json);
            out.flush();
            out.close();
            return;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            logger.info("Response of #ApproveLoan from #PPortal " + json.toString());
            logger.info("Response of #ApproveLoan from #PPortal Servlet at " + new Date());
            out.print(json);
            out.flush();
        }
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
            processRequest(request, response);
       
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
            processRequest(request, response);
       
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
