/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.SGportal.billing;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;
import com.mollatech.SGportal.reports.ApiDashboard;
import com.mollatech.serviceguard.nucleus.commons.GlobalStatus;
import com.mollatech.serviceguard.nucleus.db.SgAdvertiserDetails;
import com.mollatech.serviceguard.nucleus.db.SgAdvertisertracking;
import com.mollatech.serviceguard.nucleus.db.connector.management.AdvertisementTrackingManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author bluebricks
 */
@WebServlet(name = "CreditsAsPerService", urlPatterns = {"/CreditsAsPerService"})
public class CreditsAsPerService extends HttpServlet {

    int ActiveStatus = 0;
    ArrayList<ApiDashboard> sample = new ArrayList<ApiDashboard>();

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        PrintWriter out = response.getWriter();       
        ArrayList<ApiDashboard> sample = new ArrayList<ApiDashboard>();
        SgAdvertiserDetails usrObj = (SgAdvertiserDetails) request.getSession().getAttribute("_advertisorDetails");
        Integer advertisorId = usrObj.getAdId();
        try {            
                                    
            Date stratDate = new Date();
            stratDate.setHours(00);
            stratDate.setMinutes(00);
            stratDate.setSeconds(00);
            Date endDate = new Date();
            endDate.setHours(23);
            endDate.setMinutes(59);
            endDate.setSeconds(59);
            Double value;
            AdvertisementTrackingManagement requesTr = new AdvertisementTrackingManagement();
            SgAdvertisertracking[] sgHrTxDetails = requesTr.getTxDetailsByAdvatisorId(advertisorId, stratDate, endDate,GlobalStatus.PDF_AD);
            SgAdvertisertracking[] sgemailTxDetails = requesTr.getTxDetailsByAdvatisorId(advertisorId, stratDate, endDate,GlobalStatus.EMAIL_AD);
            SgAdvertisertracking[] sgpushTxDetails = requesTr.getTxDetailsByAdvatisorId(advertisorId, stratDate, endDate,GlobalStatus.PUSH_AD);
            float pdfcredit = 0.0f; float emailcredit = 0.0f; float pushcredit = 0.0f;                       
            if(sgHrTxDetails != null){                
                for(int i=0; i < sgHrTxDetails.length; i++ ){
                    pdfcredit += sgHrTxDetails[i].getCreditDeducted();
                }
                value = Double.parseDouble(String.valueOf(pdfcredit));
                sample.add(new ApiDashboard("PDF Ad", value));
            }else{
                sample.add(new ApiDashboard("PDF Ad", 0.0));
            }
           if(sgemailTxDetails != null){                
                for(int i=0; i < sgemailTxDetails.length; i++ ){
                    emailcredit += sgemailTxDetails[i].getCreditDeducted();
                }
                value = Double.parseDouble(String.valueOf(emailcredit));
                sample.add(new ApiDashboard("Email Ad", value));
            }else{
               sample.add(new ApiDashboard("Email Ad", 0.0));
           }
           if(sgpushTxDetails != null){
                for(int i=0; i < sgpushTxDetails.length; i++ ){
                    pushcredit += sgpushTxDetails[i].getCreditDeducted();
                }
                value = Double.parseDouble(String.valueOf(pushcredit));
                sample.add(new ApiDashboard("Push Ad", value));
            }else{
               sample.add(new ApiDashboard("Push Ad", 0.0));
           }            
                       
        } catch (Exception e) {
            e.printStackTrace();
        }
        Gson gson = new Gson();
        JsonElement element = gson.toJsonTree(sample, new TypeToken<List<ApiDashboard>>() {
        }.getType());
        JsonArray jsonArray = element.getAsJsonArray();
        out.print(jsonArray);
        out.flush();
        out.close();
        return;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
