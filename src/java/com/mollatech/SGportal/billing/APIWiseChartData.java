/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.SGportal.billing;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;
import com.mollatech.SGportal.reports.AreaChartGraph;
import com.mollatech.serviceguard.nucleus.db.SgDailytransactiondetails;
import com.mollatech.serviceguard.nucleus.db.connector.management.DailyTxManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author abhishekingle
 */
public class APIWiseChartData extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        //String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
        String ChannelId = (String) request.getSession().getAttribute("_ChannelId");
        Integer partnerId = (Integer) request.getSession().getAttribute("_partnerID");
         String apId = request.getParameter("apName");
        String resId = request.getParameter("resourceName");
        String version = request.getParameter("version");
        String envt = request.getParameter("envt");
        String apiName = request.getParameter("apiName");
        SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        
        Date currentDate = new Date();
        
        request.getSession().setAttribute("monthOfAPIPerformance", currentDate);
        String fromDate = null;
        String toDate = format.format(currentDate);
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -30);
        Date froDate = cal.getTime();
        fromDate = format.format(froDate);
        String sDate = formatter.format(froDate);      
        int callCount = 0;
        float amount = 0;
        float totalCharge = 0.0f;
        Map seq = new LinkedHashMap();
        try {
            //Date cdate = format.parse(format.format(new Date()));
            String keyInput = apId+":"+resId+":"+version+":"+envt+":"+apiName;
            SgDailytransactiondetails[] dailyTransObj = new DailyTxManagement().getTxDetails(ChannelId, partnerId, fromDate, toDate, "00:00 AM", "11:59 PM");
            if (dailyTransObj != null) {
                for (int i = 0; i <= 30; i++) {
                    cal = Calendar.getInstance();
                    cal.add(Calendar.DATE, (-30 + i));
                    froDate = cal.getTime();
                    sDate = formatter.format(froDate);
                    seq.put(sDate, "{}");
                }
                for (SgDailytransactiondetails transcationdetailse : dailyTransObj) {
                    {
                        try {
                            JSONObject data = new JSONObject(transcationdetailse.getRawData());
                            if (!seq.containsKey("" + formatter.format(transcationdetailse.getExecuutionDate()))) {
                                Date exeDate = transcationdetailse.getExecuutionDate();
                                String exDate = formatter.format(exeDate);
                                seq.put("" + exDate, data.toString());
                            } else {
                                Iterator itr = data.keys();
                                while (itr.hasNext()) {
                                    Object key = itr.next();
                                    JSONObject oldData = new JSONObject((String) seq.get("" + formatter.format(transcationdetailse.getExecuutionDate())));
                                    if (oldData.isNull((String) key)) {
                                        oldData.put((String) key, data.getString((String) key));
                                        seq.put("" + formatter.format(transcationdetailse.getExecuutionDate()), oldData.toString());
                                    } else {
                                        String value = data.getString((String) key);
                                        String value1 = oldData.getString((String) key);
                                        int count = Integer.parseInt(value.split(":")[0]);
                                        count = count + Integer.parseInt(value1.split(":")[0]);
                                        ;
                                        value = count + ":" + value.split(":")[1];
                                        oldData.put((String) key, value);
                                        seq.put("" + formatter.format(transcationdetailse.getExecuutionDate()), oldData.toString());
                                    }
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
                ArrayList<AreaChartGraph> sample = new ArrayList<AreaChartGraph>();
                
                if (seq.size() != 0) {
                    for (Object keyS : seq.keySet()) {
                        float totalAmount = 0.0f;
                        callCount = 0;
                        JSONObject data = new JSONObject((String) seq.get((String) keyS));
                        Iterator itr = data.keys();
                        while (itr.hasNext()) {
                            Object key = itr.next();
                            String keyData = (String) key;
                            if(keyData.equalsIgnoreCase(keyInput)){
                                String value = data.getString((String) key);
    //                            int acc = Integer.parseInt(keyData.split(":")[0]);
    //                            int res = Integer.parseInt(keyData.split(":")[1]);

                                envt = (keyData.split(":")[3]);
                                apiName = (keyData.split(":")[4]);
                                callCount = Integer.parseInt(value.split(":")[0]);                            
                                amount = Float.valueOf(value.split(":")[1]);                                                        
                                totalCharge = callCount * amount;
                                totalAmount = totalAmount + totalCharge;
                            }
                        }
                        //inttotalCharge = (int) Math.round(totalAmount);
                        String keyDate = keyS.toString();

                        //System.out.println("AreaChart Week >> "+keyDate+","+ totalAmount+ ", "+callCount);
                        sample.add(new AreaChartGraph(keyDate, totalAmount,callCount));
                    }
                } 
                Gson gson = new Gson();
                JsonElement element = gson.toJsonTree(sample, new TypeToken<List<AreaChartGraph>>() {
                }.getType());
                JsonArray jsonArray = element.getAsJsonArray();
                out.print(jsonArray);
                out.flush();
                out.close();
                return;
            } else {
                ArrayList<AreaChartGraph> sample = new ArrayList<AreaChartGraph>();
                for (int i = 0; i <= 30; i++) {
                    cal = Calendar.getInstance();
                    cal.add(Calendar.DATE, (-30 + i));
                    froDate = cal.getTime();
                    sDate = formatter.format(froDate);
                    froDate.setHours(0);
                    froDate.setMinutes(0);
                    froDate.setSeconds(0);
                    java.sql.Date sqlFromDate = new java.sql.Date(froDate.getTime());
                    froDate.setHours(23);
                    froDate.setMinutes(59);
                    froDate.setSeconds(0);
                    java.sql.Date sqlToDate = new java.sql.Date(froDate.getTime());
                    Float cdrAmount = 0.0f;
                    
                    //sample.add(new AreaChartGraph(sDate, 0));
                    sample.add(new AreaChartGraph(sDate, cdrAmount,0));
                }
                Gson gson = new Gson();
                JsonElement element = gson.toJsonTree(sample, new TypeToken<List<AreaChartGraph>>() {
                }.getType());
                JsonArray jsonArray = element.getAsJsonArray();
                out.print(jsonArray);
                out.flush();
                out.close();
                return;
            }
        } catch (Exception ee) {
            ee.printStackTrace();
        } finally {
            out.close();
            out.flush();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
