package com.mollatech.serviceguard.api.console;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mollatech.xpay.mockpurchase.AllTrustManager;
import com.mollatech.xpay.mockpurchase.AllVerifier;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.servlet.annotation.WebServlet;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;
import org.json.JSONObject;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

/**
 *
 * @author mohanish
 */
@WebServlet(name = "TestRest", urlPatterns = {"/TestRest"})
public class TestRest extends HttpServlet {

    static final Logger logger = Logger.getLogger(TestRest.class);

    private static SSLContext sslContext = null;

    static {
        try {
            HttpsURLConnection.setDefaultHostnameVerifier(new AllVerifier());
            try {
                sslContext = SSLContext.getInstance("TLS");
            } catch (NoSuchAlgorithmException ex) {
                ex.printStackTrace();
            }
            sslContext.init(null, new TrustManager[]{new AllTrustManager()}, null);
            HttpsURLConnection.setDefaultSSLSocketFactory(sslContext.getSocketFactory());
        } catch (KeyManagementException e) {
            e.printStackTrace();
        }
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("## TestAPI started");
        String urlData = request.getParameter("url").replaceAll("(?<!(http:|https:))[//]+", "/");
        System.out.println("Value of url :: " + urlData);
        String requestType = request.getParameter("restType");
        System.out.println("Value of response type in case of rest :: " + requestType);
        String paramcountRM = request.getParameter("paramcountRMRest");
        System.out.println("Number of header pair  :: " + paramcountRM);
        String paramcountRMReq = request.getParameter("paramcountUrlEncodedFormData");
        System.out.println("Number of Request pair  :: " + paramcountRMReq);
        String type = request.getParameter("type");
        System.out.println("Request type :: " + type);
        String myNameRM, myResType;
        int paramsLength = 0;
        int reqParamsLength = 0;
        response.setContentType("application/json");
        PrintWriter outt = response.getWriter();
        JSONObject jObj = new JSONObject();
        if (!paramcountRM.equals("")) {
            paramsLength = Integer.parseInt(paramcountRM);
        }
        if (paramcountRMReq == null) {
            paramcountRMReq = "-1";
        }
        if (!paramcountRMReq.isEmpty()) {
            reqParamsLength = Integer.parseInt(paramcountRMReq);
        }
        try {

            for (int i = 0; i <= reqParamsLength; i++) {
                int j = i;
                j = j - 1;
                String myNameRMiValue;
                String myResTypeValue;
                if (i == 0) {
                    myNameRMiValue = request.getParameter("_queryParamFirst");
                    myResTypeValue = request.getParameter("_queryParamFirstValue");
                    System.out.println("Param data" + myNameRMiValue + i + "KEY");
                    System.out.println("Param data" + myResTypeValue + i + "VALUE");
                    System.out.println("Param set successfully.");
                    if (!myNameRMiValue.isEmpty()) {
                        urlData += "?" + URLEncoder.encode(myNameRMiValue, "UTF-8") + "=" + URLEncoder.encode(myResTypeValue, "UTF-8");
                    }
                } else {
                    myNameRMiValue = request.getParameter("_queryParameterKey" + j);
                    myResTypeValue = request.getParameter("_queryParameterValue" + j);
                    System.out.println("Param data" + myNameRMiValue + i + "KEY");
                    System.out.println("Param data" + myResTypeValue + i + "VALUE");
                    System.out.println("Param set successfully.");
                    urlData += "&" + URLEncoder.encode(myNameRMiValue, "UTF-8") + "=" + URLEncoder.encode(myResTypeValue, "UTF-8");
                }
            }
            request.getSession().setAttribute("urlAPIConsole", urlData);
            URL url = new URL(urlData);
            URLConnection connection = url.openConnection();
            HttpURLConnection httpConn = (HttpURLConnection) connection;
            String bodyData = request.getParameter("_RESTAPIbody");
            request.getSession().setAttribute("bodyAPIConsole", bodyData);
            logger.debug("Value of body " + bodyData);
            System.out.println("Value of body " + bodyData);
            byte[] b = bodyData.getBytes();
            httpConn.setRequestProperty("Content-Length", String.valueOf(b.length));
            if (requestType != null && requestType.equalsIgnoreCase("json") && type.equalsIgnoreCase("rest")) {
                httpConn.setRequestProperty("Content-Type", "application/json");
            } else if (type.equalsIgnoreCase("rest")) {
                httpConn.setRequestProperty("Content-Type", "application/xml");
            }
            if (type != null && type.equalsIgnoreCase("soap")) {
                httpConn.setRequestProperty("Content-Type", "text/xml; charset=utf-8");
                httpConn.setRequestProperty("SOAPAction", "");
            }
            String ipAddress = null;
            if (request.getHeader("x-forwarded-for") != null) {
                ipAddress = InetAddress.getByName(request.getHeader("x-forwarded-for")).toString();
            }
            if (ipAddress == null) {
                ipAddress = request.getRemoteAddr();
            }
            if (ipAddress != null) {
                httpConn.setRequestProperty("x-forwarded-for", ipAddress);
            }
            if (paramsLength == 0) {
                myNameRM = request.getParameter("_myNameRMRest");
                myResType = request.getParameter("_myResTypeRest");
                System.out.println("header data" + myNameRM + "KEY");
                System.out.println("header data" + myResType + "VALUE");
                httpConn.setRequestProperty(myNameRM, myResType);
                System.out.println("header set successfully.");
                request.getSession().setAttribute("paramsLengthAPIConsole", paramsLength);
                request.getSession().setAttribute("headerKeyAPIConsole0", myNameRM);
                request.getSession().setAttribute("headerValueAPIConsole0", myResType);
            } else {
                for (int i = 0; i <= paramsLength; i++) {
                    int j = i;
                    j = j - 1;
                    String myNameRMiValue;
                    String myResTypeValue;
                    if (i == 0) {
                        myNameRMiValue = request.getParameter("_myNameRMRest");
                        myResTypeValue = request.getParameter("_myResTypeRest");
                        System.out.println("header data " + myNameRMiValue + i + " KEY");
                        System.out.println("header data " + myResTypeValue + i + " VALUE");
                        httpConn.setRequestProperty(myNameRMiValue, myResTypeValue);
                        System.out.println("header set successfully.");
                        request.getSession().setAttribute("paramsLengthAPIConsole", paramsLength);
                        request.getSession().setAttribute("headerKeyAPIConsole" + i, myNameRMiValue);
                        request.getSession().setAttribute("headerValueAPIConsole" + i, myResTypeValue);
                    } else {
                        myNameRMiValue = request.getParameter("RMTextRest" + j);
                        myResTypeValue = request.getParameter("RMTypeRest" + j);
                        System.out.println("header data " + myNameRMiValue + i + " KEY");
                        System.out.println("header data " + myResTypeValue + i + " VALUE");
                        httpConn.setRequestProperty(myNameRMiValue, myResTypeValue);
                        System.out.println("header set successfully.");
                        request.getSession().setAttribute("paramsLengthAPIConsole", paramsLength);
                        request.getSession().setAttribute("headerKeyAPIConsole" + i, myNameRMiValue);
                        request.getSession().setAttribute("headerValueAPIConsole" + i, myResTypeValue);
                    }
                }
            }
            httpConn.setRequestMethod("POST");
            httpConn.setDoOutput(true);
            httpConn.setDoInput(true);
            OutputStream out = httpConn.getOutputStream();
            out.write(b);
            out.close();
            InputStreamReader isr = null;
            String data = "";
            if (httpConn.getResponseCode() == 200) {
                isr = new InputStreamReader(httpConn.getInputStream());
            } else if (httpConn.getResponseCode() == 404) {
                data = "Web Service Is Not Available";
            } else if (httpConn.getErrorStream() != null) {
                isr = new InputStreamReader(httpConn.getErrorStream());
            }
            if (isr != null) {
                BufferedReader in = new BufferedReader(isr);
                String inputLine;

                while ((inputLine = in.readLine()) != null) {
                    data += inputLine;
                }
                System.out.println(httpConn.getContentType());
                if (httpConn.getResponseCode() == 200) {
                    if (requestType != null && requestType.equalsIgnoreCase("json")) {
                        ObjectMapper mapper = new ObjectMapper();
                        Object json = mapper.readValue(data, Object.class);
                        data = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(json);
                        System.out.println("data " + data);
                    } else {
                        data = indentXMLString(data);
                    }
                } else if (httpConn.getContentType() != null) {
                    if (httpConn.getContentType().contains("json")) {
                        ObjectMapper mapper = new ObjectMapper();
                        Object json = mapper.readValue(data, Object.class);
                        data = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(json);
                        System.out.println("data " + data);
                    } else if (httpConn.getContentType().contains("xml")) {
                        data = indentXMLString(data);
                    }
                }
                jObj.put("responseData", data);
                request.getSession().setAttribute("responseAPIConsole", data);
            } else if (data.equals("")) {
                request.getSession().setAttribute("responseAPIConsole", "Content Not Available");
                jObj.put("responseData", "Content Not Available");
            }
            request.getSession().setAttribute("responseAPIConsole", data);
            jObj.put("responseData", data);
            jObj.put("result", "success");
            if (data.contains("Connection refused") || data.contains("Web Service Is Not Available")
                    || data.contains("No Such Partner is Available.")
                    || data.contains("No Such Webservice is Available.")
                    || data.contains("Your Associated Group Is Not Active. Contact Administrator.")
                    || data.contains("Internal Resource IS Not Active. Contact Administrator.")
                    || data.contains("Your Account Is Not Active. Contact Administrator.")
                    || data.contains("Partner Is Not Active.")
                    || data.contains("Service Guard Error:")
                    || data.contains("You Are Not Autherized Partner.")) {
                jObj.put("result", "error");
            }
        } catch (Exception e) {
            jObj.put("result", "error");
            jObj.put("responseData", e.getMessage());
            request.getSession().setAttribute("responseAPIConsole", e.getMessage());
            request.getSession().setAttribute("errorAPIConsole", e.getMessage());
        } finally {
            outt.print(jObj);
            outt.flush();
        }
    }

    public String indentXMLString(String data) {
        try {
            Document document = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new InputSource(new ByteArrayInputStream(data.getBytes("utf-8"))));
            XPath xPath = XPathFactory.newInstance().newXPath();
            NodeList nodeList = (NodeList) xPath.evaluate("//text()[normalize-space()='']", document, XPathConstants.NODESET);
            for (int i = 0; i < nodeList.getLength(); ++i) {
                org.w3c.dom.Node node = nodeList.item(i);
                node.getParentNode().removeChild(node);
            }
            javax.xml.transform.Transformer transformer = TransformerFactory.newInstance().newTransformer();
            transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
            transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
            StringWriter stringWriter = new StringWriter();
            StreamResult streamResult = new StreamResult(stringWriter);
            transformer.transform(new DOMSource(document), streamResult);
            return stringWriter.toString();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
                processRequest(request, response);
       
    }


    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
                processRequest(request, response);
       
    }


    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
