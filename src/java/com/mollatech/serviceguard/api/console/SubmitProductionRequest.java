/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.serviceguard.api.console;

import com.mollatech.serviceguard.nucleus.commons.GlobalStatus;
import com.mollatech.serviceguard.nucleus.commons.MethodClassName;
import com.mollatech.serviceguard.nucleus.commons.MethodName;
import com.mollatech.serviceguard.nucleus.commons.Methods;
import com.mollatech.serviceguard.nucleus.commons.Serializer;
import com.mollatech.serviceguard.nucleus.db.SgProductionAccess;
import com.mollatech.serviceguard.nucleus.db.SgRequestForProduction;
import com.mollatech.serviceguard.nucleus.db.TransformDetails;
import com.mollatech.serviceguard.nucleus.db.connector.management.ProductionAccessManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.RequestProductionManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.TransformManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.Map;
import java.util.logging.Level;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.json.JSONObject;

/**
 *
 * @author Ashu
 */
@WebServlet(name = "SubmitProductionRequest", urlPatterns = {"/SubmitProductionRequest"})
public class SubmitProductionRequest extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    static final Logger logger = Logger.getLogger(SubmitProductionRequest.class);

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException{
        logger.info("Requested Servlet is SubmitProductionRequest at " + new Date());
        logger.info("Remote Ip Address is " + request.getRemoteAddr());
        PrintWriter out = response.getWriter();
        JSONObject json = new JSONObject();
        try {
            String apId = request.getParameter("apId");
            logger.debug("Value of Acceess Point Id   = " + apId);
            String resId = request.getParameter("resId");
            logger.debug("Value of Resource Id  = " + resId);
            String methodName = request.getParameter("methodName");
            logger.debug("Value of API Name  = " + methodName);
            String version = request.getParameter("version");
            logger.debug("Value of Version  = " + version);
            String sampleReq = request.getParameter("_APIbody");
            logger.debug("Value of API Body  = " + sampleReq);
            String sampleRes = request.getParameter("responseID");
            logger.debug("Value of API Output  = " + sampleRes);
            String channelid = (String) request.getSession().getAttribute("_ChannelId");
            String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
            int partnerid = (Integer) request.getSession().getAttribute("_partnerID");
            TransformDetails details = new TransformManagement().getTransformDetails(channelid, Integer.parseInt(apId), Integer.parseInt(resId));
            Map methosdMap = (Map) Serializer.deserialize(details.getMethods());
            Methods methods = (Methods) methosdMap.get(version);
            MethodClassName className = methods.methodClassName;
            String availableMethods = "";
            boolean allDone = false;
            for (int i = 0; i < className.methodNames.size(); i++) {
                MethodName name = className.methodNames.get(i);
                if (name.visibility.equalsIgnoreCase("yes")) {
                    availableMethods += name.methodname + ",";
                }
            }
            SgRequestForProduction forProduction = new RequestProductionManagement().getDetailsByMwthodName(SessionId, channelid, Integer.parseInt(apId), Integer.parseInt(resId), Integer.parseInt(version), partnerid, methodName);
            int res = -1;
            if (forProduction == null) {
                forProduction = new SgRequestForProduction();
                forProduction.setAccesPointId(Integer.parseInt(apId));
                forProduction.setApiInput(sampleReq);
                forProduction.setApiOutput(sampleRes);
                forProduction.setApiname(methodName);
                forProduction.setChannelId(channelid);
                forProduction.setCreatedOn(new Date());
                forProduction.setPartnerid(partnerid);
                forProduction.setResourceId(Integer.parseInt(resId));
                forProduction.setUpdateOn(new Date());
                forProduction.setVersion(Integer.parseInt(version));
                forProduction.setStatus(GlobalStatus.PENDING);
                forProduction.setUpdationFlag(GlobalStatus.SUCCESS);
                res = new RequestProductionManagement().addRequest(SessionId, forProduction);
            } else {
                forProduction.setApiInput(sampleReq);
                forProduction.setApiOutput(sampleRes);
                forProduction.setUpdateOn(new Date());
                forProduction.setUpdationFlag(GlobalStatus.UPDATED);
                forProduction.setStatus(GlobalStatus.PENDING);
                res = new RequestProductionManagement().updateRequest(SessionId, forProduction);
            }
            if (res != 0) {
                json.put("result", "error");
                json.put("message", "Error in Submitting Request.");
                return;
            }
            String submittedMethods = ",";
            SgRequestForProduction[] production = new RequestProductionManagement().getDetails(SessionId, channelid, Integer.parseInt(apId), Integer.parseInt(resId), Integer.parseInt(version), partnerid);
            if (production != null) {
                for (int i = 0; i < production.length; i++) {
                    if(production[i].getStatus() != GlobalStatus.REJECTED){
                        submittedMethods += production[i].getApiname() + ",";
                    }
                }
            }
            if (!submittedMethods.equals(",") && !availableMethods.equals("")) {
                for (int i = 0; i < availableMethods.split(",").length; i++) {
                    if (submittedMethods.contains("," + availableMethods.split(",")[i] + ",")) {
                        allDone = true;
                    } else {
                        allDone = false;
                        break;
                    }
                }
            }
            if (allDone) {
                SgProductionAccess access = new ProductionAccessManagement().getDetails(SessionId, channelid, Integer.parseInt(apId), Integer.parseInt(resId), Integer.parseInt(version), partnerid);
                if (access == null) {
                    access = new SgProductionAccess();
                    access.setAccesPointId(Integer.parseInt(apId));
                    access.setChannelId(channelid);
                    access.setCreatedOn(new Date());
                    access.setPartnerid(partnerid);
                    access.setResourceId(Integer.parseInt(resId));
                    access.setStatus(GlobalStatus.PENDING);
                    access.setUpdateOn(new Date());
                    access.setUpdationFlag(GlobalStatus.SUCCESS);
                    access.setVersion(Integer.parseInt(version));
                    new ProductionAccessManagement().addRequest(SessionId, access);
                } else {
                    access.setUpdateOn(new Date());
                    access.setStatus(GlobalStatus.PENDING);
                    access.setUpdationFlag(GlobalStatus.UPDATED);
                    new ProductionAccessManagement().updateRequest(SessionId, access);
                }
            }
            json.put("result", "success");
            json.put("message", "API Submitted Successfully.");
        } catch (Exception ex) {
            logger.error("Exception at SubmitProductionRequest ", ex);
            json.put("result", "error");
            json.put("message", "Error in Submitting Request.");
        } finally {
            logger.info("Response of SubmitProductionRequest " + json.toString());
            logger.info("Response of SubmitProductionRequest Servlet at " + new Date());
            out.print(json);
            out.flush();
            out.close();
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
               processRequest(request, response);
       
    }


    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
             processRequest(request, response);
       
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
