package com.mollatech.serviceguard.api.console;

import com.mollatech.serviceguard.nucleus.commons.ClassName;
import com.mollatech.serviceguard.nucleus.commons.Classes;
import com.mollatech.serviceguard.nucleus.commons.MethodName;
import com.mollatech.serviceguard.nucleus.commons.MethodParams;
import com.mollatech.serviceguard.nucleus.commons.Methods;
import com.mollatech.serviceguard.nucleus.commons.Params;
import com.mollatech.serviceguard.nucleus.commons.Serializer;
import com.mollatech.serviceguard.nucleus.db.ResourceDetails;
import com.mollatech.serviceguard.nucleus.db.TransformDetails;
import com.mollatech.serviceguard.nucleus.db.connector.management.ResourceManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.TransformManagement;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mollatech.serviceguard.nucleus.db.HttpresourceDetails;
import com.mollatech.serviceguard.nucleus.db.connector.management.HttpResorcemanagment;

/**
 *
 * @author Ash
 */
@WebServlet(name = "GetXmlContent", urlPatterns = {"/GetXmlContent"})
public class GetXmlContent extends HttpServlet {

    static Classes classes = null;

    Methods methods = null;

    static Map classesMap = null;

    static String body = "";

    static String Hbody = "";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    Map wadlMethods = null;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("application/json");
        PrintWriter out = response.getWriter();
        JSONObject jObj = new JSONObject();
        try {
            String _apId = request.getParameter("apId");
            String _resId = request.getParameter("resId");
            String type = request.getParameter("type");
            String restType = request.getParameter("restType");
            if (restType == null) {
                restType = "XML";
            }
            String mName = request.getParameter("methodName");
            String sessionId = (String) request.getSession().getAttribute("_partnerSessionId");
            String channels = (String) request.getSession().getAttribute("_ChannelId");
            TransformDetails details = new TransformManagement().getTransformDetails(sessionId, channels, Integer.parseInt(_apId), Integer.parseInt(_resId));
            HttpresourceDetails httpRes = new HttpResorcemanagment().getHttpReourceId(sessionId, channels, Integer.parseInt(_resId));
            if (httpRes != null) {
                wadlMethods = (HashMap) Serializer.deserialize(httpRes.getImplclass());
            }
            if (details != null) {
                classesMap = new HashMap();
                int version = Integer.parseInt(request.getParameter("version"));
                Object obj = Serializer.deserialize(details.getClasses());
                Map tmpClassess = (Map) obj;
                classes = (Classes) tmpClassess.get("" + version);
                obj = Serializer.deserialize(details.getMethods());
                Map tmpMethods = (Map) obj;
                methods = (Methods) tmpMethods.get("" + version);
                List list = methods.methodClassName.methodNames;
                MethodName methodName = null;
                List<MethodParams> paramses = new ArrayList<MethodParams>();
                String result = "success";
                String message = mName + " API Saved Successfully";
                for (int i = 0; i < list.size(); i++) {
                    methodName = (MethodName) list.get(i);
                    String tempName = methodName.methodname.split("::")[0];
                    if (methodName.transformedname != null && !methodName.transformedname.isEmpty()) {
                        tempName = methodName.transformedname;
                    }
                    if (tempName.equals(mName)) {
                        paramses = methodName.paramses;
                        break;
                    }
                }
                String strPackage = "com.mollatech.sg.wadl.wsdl.webservice";
                if (!classes.pname.packageName.equals("")) {
                    strPackage = classes.pname.packageName;
                    doChanges("");
                }
                String packArray[] = strPackage.replace(".", ",").split(",");
                strPackage = "";
                for (int i = packArray.length - 1; i >= 0; i--) {
                    strPackage += (packArray[i]) + ".";
                }
                body = Hbody = "";
                strPackage = strPackage.substring(0, strPackage.length() - 1);
                String xmlBody = "";
                ResourceDetails rd = new ResourceManagement().getResourceById(Integer.parseInt(_resId));
                if (type.equalsIgnoreCase("SOAP")) {
                    xmlBody = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:web=\"http://" + strPackage + "/\">\n" + "   #HeaderTag#\n" + "   <soapenv:Body>\n" + "     <web:#methodName#>";
                    if (rd.getType().equalsIgnoreCase("WADL")) {
                        xmlBody += "#body#";
                    } else {
                        xmlBody += "#body#";
                    }
                    xmlBody += "     </web:#methodName#>\n" + "   </soapenv:Body>\n" + "</soapenv:Envelope>";
                } else if (rd.getType().equalsIgnoreCase("WADL")) {
                    if (restType.equalsIgnoreCase("JSON")) {
                        xmlBody = "{#body#}";
                    } else {
                        xmlBody = "<#methodName#IN>#body#</#methodName#IN>";
                    }
                } else if (restType.equalsIgnoreCase("JSON")) {
                    xmlBody = "{#body#}";
                } else {
                    xmlBody = "<#methodName#IP>#body#</#methodName#IP>";
                }
                for (int i = 0; i < paramses.size(); i++) {
                    MethodParams mp = paramses.get(i);
                    String name = mp.name.split(";")[0];
                    if (!mp.newname.equalsIgnoreCase("")) {
                        name = mp.newname.split(";")[0];
                    }
                    if (mp.visibility.equalsIgnoreCase("yes") && mp.defaultvalue.equals("")) {
                        if (restType.equalsIgnoreCase("JSON")) {
                            if (rd.getType().equalsIgnoreCase("WADL")) {
                                List pList = doChanges(mp.type);
                                if (pList != null) {
                                    for (Object params : pList) {
                                        Params p = (Params) params;
                                        name = p.name.split(";")[0];
                                        if (!p.newname.equals("")) {
                                            name = p.newname.split(";")[0];
                                        }
                                        if (p.visibility.equalsIgnoreCase("yes") && p.defaultvalue.equals("")) {
                                            writeJOSNContent(name, p.type);
                                        }
                                    }
                                }
                            } else {
                                writeJOSNContent(name, mp.type);
                            }
                        } else if (rd.getType().equalsIgnoreCase("WADL")) {
                            writeXmlContent(name, mp.type);
                        } else {
                            boolean header = false;
                            String headers = mp.name.split(";")[1];
                            if (!headers.contains("no")) {
                                header = true;
                            }
                            if (header) {
                                writeXmlContentForHeaders(name, mp.type, header);
                            } else {
                                writeXmlContent(name, mp.type);
                            }
                        }
                    }
                }
                if (body.equals("")) {
                    xmlBody = xmlBody.replace("<#methodName#IN>#body#</#methodName#IN>", "");
                }
                xmlBody = xmlBody.replace("#body#", body);
                xmlBody = xmlBody.replace("#HeaderTag#", "<soapenv:Header>" + Hbody + "</soapenv:Header>");
                if (!methodName.transformedname.equals("")) {
                    xmlBody = xmlBody.replace("#methodName#", methodName.transformedname.split("::")[0]);
                } else {
                    xmlBody = xmlBody.replace("#methodName#", methodName.methodname.split("::")[0]);
                }
                if (restType.equalsIgnoreCase("JSON")) {
                    ObjectMapper mapper = new ObjectMapper();
                    Object json = mapper.readValue(xmlBody, Object.class);
                    try {
                        xmlBody = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(json);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    xmlBody = indentXMLString(xmlBody);
                }
                jObj.put("apibody", xmlBody);
                out.print(jObj.toString());
                out.flush();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            out.print(jObj.toString());
            out.flush();
        }
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }

    public static List doChanges(String classN) {
        if (classes.pname.classs == null) {
            classes.pname.classs = new ArrayList<ClassName>();
        }
        for (int i = 0; i < classes.pname.classs.size(); i++) {
            if (!classes.pname.packageName.equalsIgnoreCase("")) {
                if (classesMap.get(classes.pname.classs.get(i).classname) == null) {
                    classesMap.put(classes.pname.classs.get(i).classname, classes.pname.classs.get(i).transformedclassname);
                }
            }
            List paramsList = classes.pname.classs.get(i).paramses;
            if (classes.pname.classs.get(i).classname.equals(classN)) {
                if (paramsList != null) {
                    return paramsList;
                }
            }
        }
        return null;
    }

    public static String indentXMLString(String data) {
        try {
            Document document = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new InputSource(new ByteArrayInputStream(data.getBytes("utf-8"))));
            XPath xPath = XPathFactory.newInstance().newXPath();
            NodeList nodeList = (NodeList) xPath.evaluate("//text()[normalize-space()='']", document, XPathConstants.NODESET);
            for (int i = 0; i < nodeList.getLength(); ++i) {
                org.w3c.dom.Node node = nodeList.item(i);
                node.getParentNode().removeChild(node);
            }
            javax.xml.transform.Transformer transformer = TransformerFactory.newInstance().newTransformer();
            transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
            transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
            StringWriter stringWriter = new StringWriter();
            StreamResult streamResult = new StreamResult(stringWriter);
            transformer.transform(new DOMSource(document), streamResult);
            return stringWriter.toString();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void writeXmlContent(String name, String param) {
        if (param.trim().equals("List<String>")) {
            body += "<!--Zero or more repetitions:-->\n" + "<" + name + ">?</" + name + ">\n";
        } else if (param.trim().startsWith("List<")) {
            int sIndex = param.indexOf("<");
            int eIndex = param.indexOf(">");
            param = param.substring(sIndex + 1, eIndex);
            body += "<!--Zero or more repetitions:-->\n" + "<" + name + ">\n";
            List paraList = doChanges(param);
            for (int i = 0; i < paraList.size(); i++) {
                Params p = (Params) paraList.get(i);
                String Pname = p.name.split(";")[0];
                if (!p.newname.equals("")) {
                    Pname = p.newname;
                }
                if (p.visibility.equalsIgnoreCase("yes") && p.defaultvalue.equals("")) {
                    writeXmlContent(Pname, p.type);
                }
            }
            body += "</" + name + ">\n";
        } else if (classesMap.get(param) != null) {
            body += "<!--Optional:-->\n" + "<" + name + ">\n";
            List paraList = doChanges(param);
            for (int i = 0; i < paraList.size(); i++) {
                Params p = (Params) paraList.get(i);
                String Pname = p.name.split(";")[0];
                if (!p.newname.equals("")) {
                    Pname = p.newname;
                }
                if (p.visibility.equalsIgnoreCase("yes") && p.defaultvalue.equals("")) {
                    writeXmlContent(Pname, p.type);
                }
            }
            body += "</" + name + ">\n";
        } else if (doChanges(param) != null) {
            boolean isEnum = false;
            if (doChanges(param) != null) {

                List paraList = doChanges(param);
                for (int i = 0; i < paraList.size(); i++) {
                    Params p = (Params) paraList.get(i);
                    String Pname = p.type.replace("[]", "");
                    if (!param.equals(Pname)) {
                        isEnum = false;
                        break;
                    }
                    isEnum = true;

                }
            }
            if (isEnum) {
                body += "<!--Optional:-->\n" + "<" + name + ">?</" + name + ">\n";
            } else {
                body += "<!--Optional:-->\n" + "<" + name + ">\n";
                List paraList = doChanges(param);
                for (int i = 0; i < paraList.size(); i++) {
                    Params p = (Params) paraList.get(i);
                    String Pname = p.name.split(";")[0];
                    if (!p.newname.equals("")) {
                        Pname = p.newname;
                    }
                    if (p.visibility.equalsIgnoreCase("yes") && p.defaultvalue.equals("")) {
                        writeXmlContent(Pname, p.type);
                    }
                }
                body += "</" + name + ">\n";
            }
        } else {
            body += "<!--Optional:-->\n" + "<" + name + ">?</" + name + ">\n";
        }
    }

    public static void writeXmlContentForHeaders(String name, String param, boolean header) {
        if (param.trim().equals("List<String>")) {
            if (header) {
                Hbody += "<!--Zero or more repetitions:-->\n" + "<web:" + name + ">?</web:" + name + ">\n";
            } else {
                Hbody += "<!--Zero or more repetitions:-->\n" + "<" + name + ">?</" + name + ">\n";

            }
        } else if (param.trim().startsWith("List<")) {
            int sIndex = param.indexOf("<");
            int eIndex = param.indexOf(">");
            param = param.substring(sIndex + 1, eIndex);
            if (header) {
                Hbody += "<!--Zero or more repetitions:-->\n" + "<web:" + name + ">\n";
            } else {
                Hbody += "<!--Zero or more repetitions:-->\n" + "<" + name + ">\n";

            }
            List paraList = doChanges(param);
            for (int i = 0; i < paraList.size(); i++) {
                Params p = (Params) paraList.get(i);
                String Pname = p.name.split(";")[0];
                if (!p.newname.equals("")) {
                    Pname = p.newname;
                }
                if (p.visibility.equalsIgnoreCase("yes") && p.defaultvalue.equals("")) {
                    writeXmlContentForHeaders(Pname, p.type, false);
                }
            }
            if (header) {
                Hbody += "</web:" + name + ">\n";
            } else {
                Hbody += "</" + name + ">\n";
            }
        } else if (classesMap.get(param) != null) {
            if (header) {
                Hbody += "<!--Optional:-->\n" + "<web:" + name + ">\n";
            } else {
                Hbody += "<!--Optional:-->\n" + "<" + name + ">\n";
            }
            List paraList = doChanges(param);
            for (int i = 0; i < paraList.size(); i++) {
                Params p = (Params) paraList.get(i);
                String Pname = p.name.split(";")[0];
                if (!p.newname.equals("")) {
                    Pname = p.newname;
                }
                if (p.visibility.equalsIgnoreCase("yes") && p.defaultvalue.equals("")) {
                    writeXmlContentForHeaders(Pname, p.type, false);
                }
            }
            if (header) {
                Hbody += "</web:" + name + ">\n";
            } else {
                Hbody += "</" + name + ">\n";
            }
        } else if (doChanges(param) != null) {
            boolean isEnum = false;
            if (doChanges(param) != null) {

                List paraList = doChanges(param);
                for (int i = 0; i < paraList.size(); i++) {
                    Params p = (Params) paraList.get(i);
                    String Pname = p.type.replace("[]", "");
                    if (!param.equals(Pname)) {
                        isEnum = false;
                        break;
                    }
                    isEnum = true;

                }
            }
            if (isEnum) {
                if (header) {
                    Hbody += "<!--Optional:-->\n" + "<web:" + name + ">?</web:" + name + ">\n";
                } else {
                    Hbody += "<!--Optional:-->\n" + "<" + name + ">?</" + name + ">\n";
                }
            } else {
                if (header) {
                    Hbody += "<!--Optional:-->\n" + "<web:" + name + ">\n";
                } else {
                    Hbody += "<!--Optional:-->\n" + "<" + name + ">\n";
                }
                List paraList = doChanges(param);
                for (int i = 0; i < paraList.size(); i++) {
                    Params p = (Params) paraList.get(i);
                    String Pname = p.name.split(";")[0];
                    if (!p.newname.equals("")) {
                        Pname = p.newname;
                    }
                    if (p.visibility.equalsIgnoreCase("yes") && p.defaultvalue.equals("")) {
                        writeXmlContentForHeaders(Pname, p.type, false);
                    }
                }
                if (header) {
                    Hbody += "</web:" + name + ">\n";
                } else {
                    Hbody += "</" + name + ">\n";
                }
            }
        } else if (header) {
            Hbody += "<!--Optional:-->\n" + "<web:" + name + ">?</web:" + name + ">\n";
        } else {
            Hbody += "<!--Optional:-->\n" + "<" + name + ">?</" + name + ">\n";
        }
    }

    public static void writeJOSNContent(String name, String param) {
        if (!param.equals("")) {
            param = param.replace("javax.xml.ws.", "");
        }
        if (!body.equals("") && !body.endsWith("{") && !name.equals("")) {
            body += ",";
        }

        if (param.trim().equals("List<String>")) {
            if (!name.equals("")) {
                body += "\"" + name + "\":[]";
            } else {
                body += "[]";
            }
        } else if (param.trim().startsWith("List<")) {
            int sIndex = param.indexOf("<");
            int eIndex = param.indexOf(">", param.trim().length() - 1);
            param = param.substring(sIndex + 1, eIndex);
            if (!name.equals("")) {
                body += "\"" + name + "\":[";
            } else {
                body += "[";
            }
            writeJOSNContent("", param);
            body += "]";
        } else if (param.trim().startsWith("Holder<")) {
            int sIndex = param.indexOf("<");
            int eIndex = param.indexOf(">", param.trim().length() - 1);
            param = param.substring(sIndex + 1, eIndex);
            body += "\"" + name + "\":{\"value\":";
            writeJOSNContent("", param);
            body += "}";
        } else if (classesMap.get(param) != null) {
            body += "\"" + name + "\":{";
            List paraList = doChanges(param);
            for (int i = 0; i < paraList.size(); i++) {
                Params p = (Params) paraList.get(i);
                String Pname = p.name.split(";")[0];
                if (!p.newname.equals("")) {
                    Pname = p.newname;
                }
                if (p.visibility.equalsIgnoreCase("yes") && p.defaultvalue.equals("")) {
                    writeJOSNContent(Pname, p.type);
                }
            }
            body += "}";
        } else if (doChanges(param) != null) {

            boolean isEnum = false;
            if (doChanges(param) != null) {

                List paraList = doChanges(param);
                for (int i = 0; i < paraList.size(); i++) {
                    Params p = (Params) paraList.get(i);
                    String Pname = p.type.replace("[]", "");
                    if (!param.equals(Pname)) {
                        isEnum = false;
                        break;
                    }
                    isEnum = true;

                }
            }
            if (isEnum) {
                body += "\"" + name + "\":null";
            } else {
                if (!name.equals("")) {
                    body += "\"" + name + "\":{";
                } else {
                    body += "{";
                }
                List paraList = doChanges(param);
                for (int i = 0; i < paraList.size(); i++) {
                    Params p = (Params) paraList.get(i);
                    String Pname = p.name.split(";")[0];
                    if (!p.newname.equals("")) {
                        Pname = p.newname;
                    }
                    if (p.visibility.equalsIgnoreCase("yes") && p.defaultvalue.equals("")) {
                        writeJOSNContent(Pname, p.type);
                    }
                }
                body += "}";
            }
        } else if (param.equalsIgnoreCase("float") || param.equalsIgnoreCase("short") || param.equalsIgnoreCase("byte") || param.equalsIgnoreCase("int") || param.equalsIgnoreCase("Integer") || param.equalsIgnoreCase("long") || param.equalsIgnoreCase("double")) {
            body += "\"" + name + "\":0";
        } else {
            body += "\"" + name + "\":null";
        }
    }
}
