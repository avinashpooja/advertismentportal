/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.serviceguard.api.console;

import com.mollatech.serviceguard.nucleus.commons.MSConfig;
import com.mollatech.serviceguard.nucleus.db.PartnerDetails;
import com.mollatech.serviceguard.nucleus.db.SgCreditInfo;
import com.mollatech.serviceguard.nucleus.db.connector.management.SettingsManagement;
import com.rmi.server.ReceiveMessageInterface;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author Ashu
 */
@WebServlet(name = "GetCredit", urlPatterns = {"/GetCredit"})
public class GetCredit extends HttpServlet {

    public String[] listOfMasterip = null;

    public String[] listOfRMIPort = null;

    public String masterIp = null;

    public String rmiPort = null;

    public MSConfig mConf = null;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException{
        response.setContentType("application/json");
        String ChannelId = (String) request.getSession().getAttribute("_ChannelId");
        PartnerDetails parObj = (PartnerDetails) request.getSession().getAttribute("_partnerDetails");
        Object obj = new SettingsManagement().getSetting(ChannelId, SettingsManagement.MATERSLAVE, SettingsManagement.PREFERENCE_ONE);
        PrintWriter out = response.getWriter();
        JSONObject json = new JSONObject();
        if (obj != null) {
            if (obj instanceof MSConfig) {
                mConf = (MSConfig) obj;
            }
        }
        String listOfip = null;
        String listOfPort = null;
        if (mConf != null) {
            listOfip = mConf.ip;
            listOfPort = mConf.port;
        }
        if (listOfip != null) {
            listOfMasterip = listOfip.split(",");
            if (listOfMasterip != null || listOfMasterip.length > 0) {
                masterIp = listOfMasterip[0];
            }
        }
        if (listOfPort != null) {
            listOfRMIPort = listOfPort.split(",");
            if (listOfRMIPort != null || listOfRMIPort.length > 0) {
                rmiPort = listOfRMIPort[0];
            }
        }

        Map<Integer, SgCreditInfo> status = null;
        int count = 0;
        while (status == null) {
            status = getCreditStatus();
            if (status != null && !status.isEmpty()) {
                if (status.get(parObj.getPartnerId()) != null) {
                    double credits = status.get(parObj.getPartnerId()).getMainCredit();
                    BigDecimal bdRemainTotalCredit = new BigDecimal(credits);
                    bdRemainTotalCredit = bdRemainTotalCredit.setScale(2, BigDecimal.ROUND_HALF_EVEN);
                    json.put("credits", bdRemainTotalCredit);
                }
            }
            if (count >= 3) {
                json.put("credits", "NA");
                out.print(json);
                return;
            }
            count++;
        }
        json.put("credits", "NA");
        out.print(json);

    }

    public Map getCreditStatus() {
        try {
            Socket socket = new Socket();
            socket.connect(new InetSocketAddress(masterIp, Integer.parseInt(rmiPort)), 3000);
            socket.close();
            Registry registry = LocateRegistry.getRegistry(masterIp, (new Integer(rmiPort)).intValue());
            ReceiveMessageInterface rmiServer = (ReceiveMessageInterface) (registry.lookup("rmiServer"));
            Map<Integer, SgCreditInfo> sStatus = rmiServer.getCreditInfo();
            return sStatus;
        } catch (Exception ex) {
            changeMasterIpAndPort();
        }
        return null;
    }

    public void changeMasterIpAndPort() {
        String deadIp = listOfMasterip[0];
        String deadPort = listOfRMIPort[0];
        for (int i = 0; i < listOfMasterip.length; i++) {
            if (i <= listOfMasterip.length - 2) {
                listOfMasterip[i] = listOfMasterip[i + 1];
            } else {
                listOfMasterip[i] = deadIp;
            }
        }
        for (int i = 0; i < listOfRMIPort.length; i++) {
            if (i <= listOfRMIPort.length - 2) {
                listOfRMIPort[i] = listOfRMIPort[i + 1];
            } else {
                listOfRMIPort[i] = deadPort;
            }
        }
        if (listOfMasterip != null) {
            masterIp = listOfMasterip[0];
        }
        if (listOfRMIPort != null) {
            rmiPort = listOfRMIPort[0];
        }
        String listOfip = null;
        String listOfPort = null;
        for (int i = 0; i < listOfMasterip.length; i++) {
            if (listOfip == null) {
                listOfip = listOfMasterip[i];
            } else {
                listOfip = listOfip + "," + listOfMasterip[i];
            }
        }
        for (int i = 0; i < listOfRMIPort.length; i++) {
            if (listOfPort == null) {
                listOfPort = listOfRMIPort[i];
            } else {
                listOfPort = listOfPort + "," + listOfRMIPort[i];
            }
        }
        MSConfig mConfig = mConf;
        mConfig.ip = listOfip;
        mConfig.port = listOfPort;
        mConf = mConfig;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
             processRequest(request, response);
       
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
             processRequest(request, response);
       
    }


    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
