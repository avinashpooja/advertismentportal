//package com.mollatech.xpay.mockpurchase;
//
//import com.mollatech.xpay.connector.XPAYConnectorServiceInterfaceImplService;
//import com.mollatech.xpay.connector.Xpayservices;
//import com.mollatech.xpay.pathmazing.XpayResponse;
//import java.net.MalformedURLException;
//import java.net.URL;
//import java.security.KeyManagementException;
//import java.security.NoSuchAlgorithmException;
//import java.util.logging.Level;
//import java.util.logging.Logger;
//import javax.net.ssl.HttpsURLConnection;
//import javax.net.ssl.SSLContext;
//import javax.net.ssl.TrustManager;
//
//public class XPAYConnector {
//
//    static SSLContext sslContext = null;
//
//    static {
//        try {
//            HttpsURLConnection.setDefaultHostnameVerifier(new AllVerifier());
//            try {
//                sslContext = SSLContext.getInstance("TLS");
//            } catch (NoSuchAlgorithmException ex) {
//                ex.printStackTrace();
//            }
//            sslContext.init(null, new TrustManager[] { new AllTrustManager() }, null);
//            HttpsURLConnection.setDefaultSSLSocketFactory(sslContext.getSocketFactory());
//        } catch (KeyManagementException e) {
//            e.printStackTrace();
//        }
//    }
//
//    static String secure = LoadSettings.g_sSettings.getProperty("mock.purchase.secure");
//
//    static String ip = LoadSettings.g_sSettings.getProperty("mock.purchase.ip");
//
//    static String port = LoadSettings.g_sSettings.getProperty("mock.purchase.port");
//
//    static String merchantName = LoadSettings.g_sSettings.getProperty("mock.merchant.name");
//
//    public static XpayResponse openSessionV2(java.lang.String channelId, java.lang.String loginId, java.lang.String password, java.lang.String merchantID, java.lang.String signature, java.lang.String integrityCheckString, com.mollatech.xpay.pathmazing.XpayTransaction xpayTransaction) {
//        URL u = null;
//        try {
//            if (secure.equalsIgnoreCase("yes")) {
//                u = new URL("https://" + ip + ":" + port + "/" + merchantName + "/XPAYConnectorServiceInterfaceImpl?wsdl");
//            } else {
//                u = new URL("http://" + ip + ":" + port + "/" + merchantName + "/XPAYConnectorServiceInterfaceImpl?wsdl");
//            }
//            XPAYConnectorServiceInterfaceImplService service = new XPAYConnectorServiceInterfaceImplService(u);
//            Xpayservices port = service.getXPAYConnectorServiceInterfaceImplPort();
//            return port.openSessionV2(channelId, loginId, password, merchantID, signature, integrityCheckString, xpayTransaction);
//        } catch (MalformedURLException ex) {
//            Logger.getLogger(XPAYConnector.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        return null;
//    }
//}
