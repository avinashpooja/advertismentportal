package com.paypal;

import java.math.BigDecimal;

import com.braintreegateway.BraintreeGateway;
import com.braintreegateway.Result;
import com.braintreegateway.Transaction;
import com.braintreegateway.TransactionRequest;
import com.braintreegateway.ValidationError;

public class BtTransactionHelper {

	private boolean success;
	private Transaction transaction;
	private Result<Transaction> result;
	
	// see 'config.properties'
	public BtConfigReader conf = new BtConfigReader();
	
	protected BraintreeGateway gateway = new BraintreeGateway(
		conf.getEnvironment(), 
		conf.getBtMerchantKey(), 
		conf.getBtPublicKey(),
		conf.getBtPrivateKey()
	);
	
	public String getToken() {
		return gateway.clientToken().generate();
	}

	public void doTransaction(
		String paymentNonce,
		String amount, 
		String firstName,
		String lastName,
		String line1,
		String line2,
		String city,
		String state,
		String postalCode,
		String countryCode) {
		
		String currencyCode = "USD";
		
		System.out.println("doTransaction:");
		System.out.println("  paymentNonce: "	+ paymentNonce);
		System.out.println("  amount: " + amount);
		System.out.println("  firstName: "	+ firstName);
		System.out.println("  lastName: "	+ lastName);
		System.out.println("  line1: "	+ line1);	
		System.out.println("  line2: "	+ line2);	
		System.out.println("  city: "	+ city);	
		System.out.println("  state: "	+ state);	
		System.out.println("  postalCode: "	+ postalCode);	
		System.out.println("  countryCode: "	+ countryCode);	
		System.out.println("  currencyCode: "	+ currencyCode);			
		
		// transaction
		TransactionRequest transactionRequest = new TransactionRequest()
			.creditCard()
				.cardholderName(firstName + " " + lastName)
				.done()
			.channel(conf.getBtChannel())
			.shippingAddress()
				.firstName(firstName)
				.lastName(lastName)
				.streetAddress(line1)
				.extendedAddress(line2)
				.locality(city)
				.region(state)
				.postalCode(postalCode)
				.countryCodeAlpha2(countryCode)
				.done()
			.billingAddress()
				.firstName(firstName)
				.lastName(lastName)
				.streetAddress(line1)
				.extendedAddress(line2)
				.locality(city)
				.region(state)
				.postalCode(postalCode)
				.countryCodeAlpha2(countryCode)
				.done()
			.customer()
				.firstName(firstName)
				.lastName(lastName)
				.done()	
			.amount(new BigDecimal(amount))
			.paymentMethodNonce(paymentNonce)
				.options()
			.submitForSettlement(true).done();
		
		result = gateway.transaction().sale(transactionRequest);
		setSuccess(result.isSuccess());
		setTransaction(result.getTarget());
	}	

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean result) {
		this.success = result;
	}

	public Transaction getTransaction() {
		return transaction;
	}

	public void setTransaction(Transaction transaction) {
		this.transaction = transaction;
	}

	public Result<Transaction> getResult() {
		return result;
	}
	
	public void addSuccessParameters(BtParameterVo vo) {

		System.out.println("Success!: " + transaction.getId());
		vo.add("email" , transaction.getStatusHistory().get(0).getUser());
		vo.add("status", "Approved");
		vo.add("transactionid", transaction.getId());
	}

	public void failurTransactionMessage() {
		System.out.println("Error processing transaction:");
		System.out.println("  Status: " + transaction.getStatus());
		System.out.println("  Code: "	+ transaction.getProcessorResponseCode());
		System.out.println("  Text: "	+ transaction.getProcessorResponseText());		
	}

	public void addFailueLog() {
		for (ValidationError error : result.getErrors().getAllDeepValidationErrors()) {
			System.out.println("Attribute: " + error.getAttribute());
			System.out.println("  Code: " + error.getCode());
			System.out.println("  Message: " + error.getMessage());
		}		
	}
	
	public String getErrorMessage() {
		String message = null;
		
		for (ValidationError error : result.getErrors().getAllDeepValidationErrors()) {
			message = error.getMessage();
		}
		return message;		
	}
	
}
